//
//  ConfirmPaymentVC.swift
//  TenantTag
//
//  Created by maninder on 21/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class ConfirmPaymentVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblPaymentDetails: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(CCSetUpVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        
        
        self.navigationItem.leftBarButtonItems = [barButton];
        
        self.navTitle("CC Set Up" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        tblPaymentDetails.registerNibsForCells(arryNib: ["AmountDisplayCell","RecurringDetailsCell"])
        tblPaymentDetails.dataSource = self
        tblPaymentDetails.delegate = self
        tblPaymentDetails.allowsSelection = true
      //  tblPaymentDetails.backgroundColor = UIColor.groupTableViewBackground

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func actionBtnBackPressed(){
        self.navigationController?.popViewController(animated: true)
    }

    
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: AmountDisplayCell = tableView.dequeueReusableCell(withIdentifier: "AmountDisplayCell") as! AmountDisplayCell
            return cell
        }else{
            let cell: RecurringDetailsCell = tableView.dequeueReusableCell(withIdentifier: "RecurringDetailsCell") as! RecurringDetailsCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 150
        }else
        {
            return 115
        }
    }
}
