//
//  RecurringDetailsCell.swift
//  TenantTag
//
//  Created by maninder on 21/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class RecurringDetailsCell: UITableViewCell {
    @IBOutlet var viewOuter: UIView!

    @IBOutlet var lblTimes: UILabel!

    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblDay: UILabel!
    @IBOutlet var viewInner: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setRecurringData(recurringDet : RecurringDetails)
    {
        
    var strDay = recurringDet.Day.description
        
        if recurringDet.Day == 1{
          strDay =  strDay + "st"
            
        }else if recurringDet.Day == 2
        {
            strDay =   strDay + "nd"
        }else if recurringDet.Day == 3
        {
            strDay =   strDay + "rd"
        }else{
            strDay =   strDay + "th"
        }
        
        
        let font = UIFont(name: APP_FontRegularNew, size: 15)!
        let fontSuper:UIFont? = UIFont(name: APP_FontRegularNew , size:13)
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: strDay , attributes: [NSFontAttributeName:font])
        attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:6], range: NSRange(location: (strDay.characters.count - 2) , length:2))
        lblDay.attributedText = attString;
        lblTimes.text = recurringDet.Times.description + " Times"
    }
    
}
