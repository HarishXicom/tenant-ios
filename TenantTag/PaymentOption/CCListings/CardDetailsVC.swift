//
//  CardDetailsVC.swift
//  TenantTag
//
//  Created by maninder on 23/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class CardDetailsVC: UIViewController {

    var paymentInfo = Payment()
    
    var popUpType : PayType = .CC
    
    @IBOutlet var viewCardDetails: UIView!
    @IBOutlet var lblNameOnCard: UILabel!
    @IBOutlet var lblCCNo: UILabel!
    @IBOutlet var lblExpDate: UILabel!
    @IBOutlet var btnClose: UIButton!
    
    @IBOutlet var lblTitleFirst: UILabel!
    @IBOutlet var lblTitleSecond: UILabel!
    @IBOutlet var lblTitleThird: UILabel!
    
    @IBOutlet var lblNoticeInfo: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.viewCardDetails.cornerRadius(5)
        
        
        if popUpType == .CC{
            
            lblNameOnCard.text = paymentInfo.card.nameOnCard
            lblCCNo.text = "xxxx-" + paymentInfo.card.last4Digits
            lblExpDate.text = paymentInfo.card.month.description + "/" +  paymentInfo.card.year.description
            
        }else{
         
            
            lblTitleFirst.text = "Bank Name"
            lblTitleSecond.text = "Account No."
            lblTitleThird.text = "Routing No."
            lblExpDate.text = paymentInfo.selectedNode.routingNumber
            lblCCNo.text = paymentInfo.selectedNode.accountNo
            lblNameOnCard.text = paymentInfo.selectedNode.bankFullName
            
            
            lblNoticeInfo.text = "If this information is incorrect, you can use another account or add a new account."
            
        }
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
           // DispatchQueue.main.async(execute: { () -> Void in
               self.view.alpha = 1
           // })
        }
    }
    
    //MARK:- IBOutlet Actions
    //MARK:-
    
    @IBAction func actionBtnClosePressed(_ sender: Any) {
     UIView.animate(withDuration: 0.2, animations: {
        self.view.alpha = 0
     }) { (test) in
        //DispatchQueue.main.async(execute: { () -> Void in
            self.dismiss(animated: false, completion: { () -> Void in
            })
       // })
        }
    }
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
