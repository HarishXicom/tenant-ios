//
//  CardCell.swift
//  TenantTag
//
//  Created by maninder on 16/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    var card = CreditCard()
    
    @IBOutlet var btnRecurringStatus: UIButton!
    @IBOutlet var btnSelection: UIButton!
    var payment = Payment()
    
    var callBack :((Payment)->Void)? = nil
    var callBackSelection :((Payment)->Void)? = nil


    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var imgViewCardType: UIImageView!
    @IBOutlet var btnSelected: UIImageView!
    @IBOutlet var lblCardNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnSelection(_ sender: Any) {
        
        if callBackSelection != nil{
            self.callBackSelection!(payment)
        }
        
        
    }
    
    
    @IBAction func actionRecurringStatus(_ sender: Any) {
       
        if callBack != nil{
            self.callBack!(payment)
        }
    }
    
    func assignData(cellIndex : Int , SelcetdIndex : Int)
    {
        card = payment.card
        lblCardNumber.text = "xxxx-" + card.last4Digits
        if card.cardType == "Visa"
        {
            imgViewCardType.image = #imageLiteral(resourceName: "CardVisa")
        }else if card.cardType == "Discover"
        {
            imgViewCardType.image = #imageLiteral(resourceName: "CardDiscover")
            
        }else if card.cardType == "MasterCard"
        {
            imgViewCardType.image = #imageLiteral(resourceName: "CardMaster")
            
        }else if card.cardType == "DinersClub"
        {
            imgViewCardType.image = #imageLiteral(resourceName: "CardDinerClub")
            
        }else if card.cardType == "Amex"
        {
            imgViewCardType.image =  #imageLiteral(resourceName: "CardAmex")
        }else if card.cardType == "JCB"
        {
            imgViewCardType.image =  #imageLiteral(resourceName: "CardJCB")
            
        }else{
            imgViewCardType.image = #imageLiteral(resourceName: "CardUnknown")
        }
        
        if payment.recurringEnabled == true{
            btnRecurringStatus.isHidden = false
        }else{
            btnRecurringStatus.isHidden = true
        }
        
        if cellIndex == SelcetdIndex
        {
            btnSelection.isSelected = true
        }else{
            btnSelection.isSelected = false
        }
        
    }
    
}
