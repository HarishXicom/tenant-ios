//
//  CCListingsVC.swift
//  TenantTag
//
//  Created by maninder on 16/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

let EditedBankNode : String = "EditedBankNode"

class CCListingsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MSUpdateController{
   var changePrevious = false
    @IBOutlet var btnCommonQuestions: UIButton!
    @IBOutlet var imgViewDummy: UIImageView!
    @IBOutlet var lblNotice: UILabel!
    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var btnNextBottom: UIButton!
    
    var arrayCards : [Payment] = [Payment]()
    
   // var payment = Payment()
    var btnNext = UIButton()
    var selectedIndex = -1
    let barRightButton = UIBarButtonItem()


    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var tblCardsListing: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(CCListingsVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        
        
        btnNextBottom.addTarget(self, action: #selector(CCListingsVC.nextBtnPressed), for: .touchUpInside)
        btnNext.setTitle("NEXT", for: .normal)
        btnNext.titleLabel?.font =  UIFont(name: APP_FontRegularNew, size: 15)!
        btnNext.sizeToFit()
        self.btnNext.isHidden = true

        btnNext.setTitleColor(UIColor.gray, for: .highlighted)


        barRightButton.customView = btnNext
        self.navigationItem.leftBarButtonItems = [barButton];
        //self.navigationItem.rightBarButtonItem = barRightButton
     
        self.navTitle("CC Set Up" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        tblCardsListing.registerNibsForCells(arryNib: ["CardCell"])
        tblCardsListing.dataSource = self
        tblCardsListing.delegate = self
        btnSubmit.cornerRadius(5)
        btnNextBottom.cornerRadius(5)

        
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGesture:)))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
         tblCardsListing.addGestureRecognizer(longPressGesture)
        btnCommonQuestions.underLineButton(text: "Common Questions", color: APP_ThemeColor)
        self.getSavedCards()
        
       

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
         if changePrevious == true
         {
            
            self.btnSubmit.isHidden = true
            heightConstraint.constant = 0
            
         }else{
            self.btnSubmit.isHidden = false
            heightConstraint.constant = 45
        }
             
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        if arrayCards.count > 1{
            selectedIndex = 0
        }else{
            
        }
        tblCardsListing.reloadData()
        let point = longPressGesture.location(in: tblCardsListing)
        let indexPath = tblCardsListing.indexPathForRow(at: point)
        
        if indexPath == nil {
            print("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began) {
            print("Long press on row, at \(indexPath!.row)")
            
            showAlertWithMutipleButtons(title: "TenantTag", message: "Are you sure you want to delete card?", cancel: "No", destructive: nil, actions: ["Yes"], controller: self, callBack: { (cancel, destruction, index) in
                                if index == 0{
                                    self.deleteParticularCard(index: indexPath!.row)
            }
            })
        }
    }
    
    func actionBtnBackPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func actionCommonQuestions(_ sender: Any) {
        openInAppBrowser(controller: self)
    }

    @IBAction func actionBtnSubmit(_ sender: Any) {
                //CCSetUpVC
            let rentObj: CCSetUpVC = CCSetUpVC.init(nibName: "CCSetUpVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(rentObj, animated: true)
    }
    
    //MARK:- TableView Delegates
    //MARK:-
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CardCell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as! CardCell
        cell.payment = arrayCards[indexPath.row]
        cell.assignData(cellIndex: indexPath.row, SelcetdIndex: selectedIndex)
        cell.callBack = {(test : Payment) in
          self.openRecurringEditingVC(payment: test)
            
        }
        
        cell.callBackSelection = {(test : Payment) in
            let indexOfObject = self.arrayCards.index(of: test)
            if self.selectedIndex == indexOfObject{
                self.selectedIndex = -1
                
                self.btnNext.isHidden = true
            }else{
                self.selectedIndex = indexOfObject!
                self.btnNext.isHidden = false
            }
            
            self.tblCardsListing.reloadData()
            // self.openRecurringEditingVC(payment: test)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let paymentInfo: CardDetailsVC = CardDetailsVC.init(nibName: "CardDetailsVC", bundle: nil)
       // self.navigationController?.isNavigationBarHidden = false
        paymentInfo.popUpType = .CC
        paymentInfo.paymentInfo = arrayCards[indexPath.row]
        paymentInfo.view.alpha = 0
        paymentInfo.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        paymentInfo.modalPresentationStyle = .overCurrentContext
        
        DispatchQueue.main.async(execute: { () -> Void in
       
       
        self.present(paymentInfo, animated: false, completion: nil)
            
        })
        
    }

    
    func openRecurringEditingVC(payment : Payment)
    {
        let landlordVC: UpdateRecurringVC = UpdateRecurringVC.init(nibName: "UpdateRecurringVC", bundle: nil)
        self.navigationController?.isNavigationBarHidden = false
        landlordVC.payType = .CC
        landlordVC.paymentObj = payment
        landlordVC.delegate = self
        self.navigationController?.pushViewController(landlordVC, animated: true)
    }

    
    func nextBtnPressed()
    {
        if selectedIndex != -1
        {
            
            
            if changePrevious == true{
                let payment = arrayCards[selectedIndex]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EditCC"), object: payment, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
             return
            }
            
            
            let rentObj: RentPaymentFormVC = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            rentObj.payType = .CC
    
//            if arrayCards[selectedIndex].recurringEnabled == true{
//                rentObj.noRecurring = true
//            }else{
//                rentObj.noRecurring = false
//            }
            rentObj.objFinalPayment = arrayCards[selectedIndex]
            //rentObj.cardDetails =
            self.navigationController?.pushViewController(rentObj, animated: true)
        }
    }
   
    func deleteParticularCard(index : NSInteger)
    {
        let paymentWithCard = arrayCards[index]
        HSProgress(.show)
        paymentWithCard.card.deleteSavedCard { (isSuccess, responseData, error) in
            HSProgress(.hide)
            if isSuccess{
                let indexArry = self.arrayCards.index(of: paymentWithCard)
                self.arrayCards.remove(at: indexArry!)
                if self.arrayCards.count > 0 {
                    self.imgViewDummy.isHidden = true
                    self.selectedIndex = 0
                    self.btnNextBottom.isHidden = false
                }else
                {
                    self.selectedIndex = -1
                    self.imgViewDummy.isHidden = false
                    self.btnNextBottom.isHidden = true
                }
                self.tblCardsListing.reloadData()
            }
            
        }
    }
    
    func getSavedCards()
    {
        HSProgress(.show)
        Payment.getSavedCreditCards { (isSuccess, responseData, error) in
            HSProgress(.hide)
            if isSuccess{
                if let arrayCard = responseData as? [Payment]
                {
                  self.arrayCards.removeAll()
                    self.arrayCards.append(contentsOf: arrayCard)
                    self.imgViewDummy.isHidden = true
                   // self.btnNext.isHidden = true

                    if self.arrayCards.count == 0 {
                        self.imgViewDummy.isHidden = false
                         self.btnNextBottom.isHidden = true
                       // return
                    }else
                    {
                        self.selectedIndex = 0
                         self.btnNextBottom.isHidden = false
                       // self.btnNext.isHidden = false
                    }
                    self.tblCardsListing.reloadData()

                }
            }else{
                
                
            }
        }
    }
    
    
    
    func updateRecords() {
        selectedIndex = -1
        self.getSavedCards()
    }
}
