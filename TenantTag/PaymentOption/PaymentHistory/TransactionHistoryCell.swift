//
//  TransactionHistoryCell.swift
//  TenantTag
//
//  Created by maninder on 5/12/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class TransactionHistoryCell: UITableViewCell {
    var payment : Payment?

    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblType: UILabel!
    
    @IBOutlet var lblTxnID: UILabel!
    @IBOutlet var lblStatus: UILabel!
    
    @IBOutlet var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewInner.cornerRadius(4)
        viewOuter.cornerRadius(5)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func assignPaymentInfo()
    {
        if payment != nil{
            lblDate.text =  (payment?.paymentDate)!
            lblTxnID.text = (payment?.paymentID)!
            if payment?.status == true{
                lblStatus.text = "Paid"
                lblStatus.textColor = UIColor(red: 8.0/255.0, green: 117.0/255.0, blue: 11.0/255.0, alpha: 1)
            }else{
                lblStatus.text = "Pending"
                lblStatus.textColor = UIColor(red: 194.0/255.0, green: 26.0/255.0, blue: 24.0/255.0, alpha: 1)
            }
            
            lblAmount.text = "$ " + (payment?.amount)!
            if payment?.paymentType == .CC{
                lblType.text = "CC - " + (payment?.previousAccount)!
            }else{
                lblType.text = "ACH - " + (payment?.previousAccount)!
            }
        }
    }
    

    
    
    
}
