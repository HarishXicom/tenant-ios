//
//  PaymentHistoryVC.swift
//  TenantTag
//
//  Created by maninder on 20/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class PaymentHistoryVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
   
    
    var paymentHistory = [Payment]()
    var checkPresent = false
    @IBOutlet var tblPaymentHistory: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController!.view.backgroundColor = UIColor.white;
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.navigationController?.isNavigationBarHidden = false
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(PaymentHistoryVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        
        
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Payment History" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)

       // tblPaymentHistory.cornerRadius(8)
        tblPaymentHistory.registerNibsForCells(arryNib: ["TransactionHistoryCell"])
        tblPaymentHistory.register(UINib(nibName: "PaymentHeaderView", bundle:nil), forHeaderFooterViewReuseIdentifier: "PaymentHeaderView")
         tblPaymentHistory.dataSource = self
        tblPaymentHistory.delegate = self
        tblPaymentHistory.allowsSelection = true
      //  tblPaymentHistory.addBorderWithColorAndLineWidth(color: UIColor.lightGray, borderWidth: 0.7)
        
        HSProgress(.show)
        Payment.getPreviousTransactions { (isSuccess, data, error) in
            HSProgress(.hide)
            if isSuccess{
               if let arrayResponse = data as? [Payment]
                 {
                    self.paymentHistory.append(contentsOf: arrayResponse)
                    self.tblPaymentHistory.reloadData()
                }
                
            }else{
                
                
            }
        }
       
        //tblPaymentHistory.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false

    }
    func actionBtnBackPressed(){
      if  checkPresent == true
      {
        self.dismiss(animated: true, completion: nil)
     }else{
        self.navigationController?.popViewController(animated: true)
      }
    }

    
    //MARK:- TableView Delegates
    //MARK:-
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
//    func tableView(_ tableView: UITableView,
//                   viewForHeaderInSection section: Int) -> UIView?
//    {
//        return tableView.dequeueReusableHeaderFooterView(withIdentifier: "PaymentHeaderView")
//    }
//    
//     func tableView(_ tableView: UITableView,
//                            heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }
//    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell: TransactionHistoryCell = tableView.dequeueReusableCell(withIdentifier: "TransactionHistoryCell") as! TransactionHistoryCell
        cell.payment = paymentHistory[indexPath.row]
        cell.assignPaymentInfo()
            return cell
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
