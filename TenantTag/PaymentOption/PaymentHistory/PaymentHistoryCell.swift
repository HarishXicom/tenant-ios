//
//  PaymentHistoryCell.swift
//  TenantTag
//
//  Created by maninder on 20/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {

    
    @IBOutlet var lblTransactionId: UILabel!
    
    @IBOutlet var lblPaymentType: UILabel!
    var payment : Payment?
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func assignPaymentInfo()
    {
        if payment != nil{
        lblDate.text = "  " + (payment?.paymentDate)!
       lblTransactionId.text = "ID - " + (payment?.paymentID)!
        if payment?.status == true{
            lblStatus.text = "  Paid"
            lblStatus.textColor = UIColor.green

        }else{
            
            lblStatus.text = "  Pending"
            lblStatus.textColor = UIColor.red
        }

        lblPrice.text = "  $ " + (payment?.amount)!
            if payment?.paymentType == .CC{
                lblPaymentType.text = "  CC - " + (payment?.previousAccount)!
            }else{
                lblPaymentType.text = "  ACH - " + (payment?.previousAccount)!
            }
        }
    }
    
}
