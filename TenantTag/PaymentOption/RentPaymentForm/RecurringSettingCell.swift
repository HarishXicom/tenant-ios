//
//  RecurringSettingCell.swift
//  TenantTag
//
//  Created by maninder on 30/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class RecurringSettingCell: UITableViewCell {

    @IBOutlet var btnRecuringActive: UIButton!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var btnRecurring: UIButton!
    @IBOutlet var btnOneTime: UIButton!
     var callBackType:((Bool)->Void)? = nil
    var callBackActiveRecurring:((Bool)->Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        btnRecuringActive.underLineButton(text: "Recurring Payment Active", color: APP_ThemeColor)
        btnRecuringActive.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnOneTime(_ sender: Any) {
       
        if callBackType != nil
        {
            self.callBackType!(false)
        }
    }
    @IBAction func actionBtnActiveRecurring(_ sender: Any){
        if callBackActiveRecurring != nil
        {
            self.callBackActiveRecurring!(true)
        }
    }
    
    
    @IBAction func actionBtnRecurring(_ sender: Any) {
        if callBackType != nil
        {
            self.callBackType!(true)
        }
    }
    
    
    func setNewValue (type : Bool)
    {
        if type == false{
            btnRecurring.isSelected = false
            btnOneTime.isSelected = true
        }else{
            btnRecurring.isSelected = true
            btnOneTime.isSelected = false
            
        }
        
        
    }
    
}
