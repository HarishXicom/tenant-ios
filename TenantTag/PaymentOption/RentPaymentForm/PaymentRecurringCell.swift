//
//  PaymentRecurringCell.swift
//  TenantTag
//
//  Created by maninder on 21/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class PaymentRecurringCell: UITableViewCell {

    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var btnACH: UIButton!
    @IBOutlet var btnCC: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
