//
//  RentPaymentFormVC.swift
//  TenantTag
//
//  Created by maninder on 21/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit


enum PayType : Int{
    case CC = 1
    case Bank = 2
    
}
enum PaymentSchedule : Int{
    
    case NotFound = 0
    case OneTime = 1
    case Recurring = 2
    
}
enum PickerSelected : Int{
    
    case RecurringType = 0
    case RecurringDay = 1
    case RecurringTimes = 2
    
}

//When Gloabl recurring is set, we not allowed to make new recurring setting on another card
// Even doesn't allow to open recurring editing cell for another card (based on APi response)
//Recurring cell can be opened with same card having already recurring enabled ( can not edited)

var arrayRecurringDay : [String] = ["Date","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
 var arrayRecurringTimes : [String] = ["Repeat","1","2","3","4","5","6","7","8","9","10","11","12"]



class RentPaymentFormVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var termsCheck = false
    
    var boolAccountRecurring = false // handles recurring on card or account
    var boolGlobalRecurring = false  // handles gloabl recurring throughtout the app
    var boolNewRecurring = false     // handles new set up fpr recurring cells
    var savedCard = false // it tells if app has one  CC saved card
    var savedAccount =  false // it tells if app has one  bank Account saved card
    
    
    var enableRecurring = false
    var alreadyRecurring = false
    var noRecurring = false
    var objFinalPayment = Payment()
    
    let strACh = "ACH Authorization Form: I authorize Coastal Investment Group, LLC, dba TenantTag, to debit the bank account indicated in this form for the noted amount on the date of execution.  I understand that because this an electronic ACH transaction, these funds may be withdrawn from my account as soon as the above noted execution date. If recurring transaction, this payment authorization is valid and will remain effective unless I cancel this authorization by emailing Coastal Investment Group, LLC at support@tenantag.com at least 3 business days in advance."
    var previousPayment = Payment()
    var cardDetails = CreditCard()
    var payType : PayType = .CC
    var paySchedule : PaymentSchedule = .OneTime
    var recurringType = "Monthly"
    var recurringDay = 0
    var recurringTimes = 0
    var pickerSelected : PickerSelected = .RecurringType


    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var btnCommonQuestions: UIButton!
    @IBOutlet var tblRentForm: UITableView!
    
    
    let containerView = UIView()
    var pickerViewRecurring : UIPickerView?
    
    var amountEntered = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(RentPaymentFormVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Rent payment" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        
        if objFinalPayment.recurringEnabled == true{
            boolAccountRecurring = true
        recurringDay = objFinalPayment.recurringData.Day
        recurringTimes = objFinalPayment.recurringData.Times
        }
        
        tblRentForm.registerNibsForCells(arryNib: ["AmountCellTableViewCell","RentPaymentCommonCell","LastPaymentCell","PaymentRecurringCell" ,"RecurringFormCell","RecurringSettingCell" ,"RecurringFormCell","SinglePaymentCell" ,"CCSelectedCell","ACHSelectedCell" , "ACHAuthorizationCell"] )
        
        
        
        tblRentForm.dataSource = self
        tblRentForm.delegate = self
        tblRentForm.allowsSelection = true
        btnSubmit.cornerRadius(5)
          btnCommonQuestions.underLineButton(text: "Payment History", color: APP_ThemeColor)
       
        self.pickerViewViewInialization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(RentPaymentFormVC.actionReceivedNotificationCC(notification:)), name: NSNotification.Name(rawValue: "EditCC"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RentPaymentFormVC.actionReceivedNotificationSynapsePay(notification:)), name: NSNotification.Name(rawValue: "EditNode"), object: nil)
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
          self.getPreviousTransaction()
        
    }
    
    
    func actionReceivedNotificationSynapsePay(notification : NSNotification)
    {
        if let objPayment = notification.object as? Payment
        {
            if objPayment.recurringEnabled == true{
                
                self.boolAccountRecurring = true
                self.objFinalPayment.recurringEnabled = false
                recurringDay = objPayment.recurringData.Day
                recurringTimes = objPayment.recurringData.Times
            }else{
                self.boolAccountRecurring = false
              //  recurringDay = recurringDay
               // recurringTimes = 0
            }
            self.objFinalPayment.selectedNode = objPayment.selectedNode
        }
        self.tblRentForm.reloadData()

    }
    
    func actionReceivedNotificationCC(notification : NSNotification)
    {
        if let objPayment = notification.object as? Payment
        {
            if objPayment.recurringEnabled == true{
                 self.boolAccountRecurring = true
                recurringDay = objPayment.recurringData.Day
                recurringTimes = objPayment.recurringData.Times
            }else{
                 self.boolAccountRecurring = false
              //  recurringDay = 0
               // recurringTimes = 0
            }

            self.objFinalPayment.card = objPayment.card
        }
        self.tblRentForm.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func actionBtnBackPressed(){
            self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func actionBtnCommonQuestions(_ sender: Any) {
//        openInAppBrowser(controller: self)
        self.openHistoryVC()
        
    }
    
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if boolAccountRecurring == true && boolGlobalRecurring == true{
                        return 5
                    }else if  boolAccountRecurring == false && boolGlobalRecurring == true{
            
                        return 4
            
                    }else if boolAccountRecurring == false && boolGlobalRecurring == false{
                        // no global recurring
            
                        if boolNewRecurring == true{
                             return 5
                        }else{
                           return 4
                        }
                       
                    }
                    return 5

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.row == 0 {

            
            let cell: LastPaymentCell = tableView.dequeueReusableCell(withIdentifier: "LastPaymentCell") as! LastPaymentCell
            cell.setPaymentDetails(payment: previousPayment)
            return cell
        }else if indexPath.row == 1{
      
            let cell: AmountCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AmountCellTableViewCell") as! AmountCellTableViewCell
            cell.txtFieldAmount.delegate = self
            cell.txtFieldAmount.placeholder = "0"
            cell.txtFieldAmount.text = amountEntered
            return cell
            
        }else if indexPath.row == 2 {
            let cell: RecurringSettingCell = tableView.dequeueReusableCell(withIdentifier: "RecurringSettingCell") as! RecurringSettingCell
            cell.btnRecuringActive.isHidden = true
            cell.btnRecurring.isHidden = false
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
               cell.setNewValue(type: boolNewRecurring)
            }else  if  boolAccountRecurring == false && boolGlobalRecurring == true {
                cell.setNewValue(type: false) // hight first button
                
            }else {
                cell.setNewValue(type: false)  // shows first hoghlight
                cell.btnRecurring.isHidden = true
                cell.btnRecuringActive.isHidden = false // indicates that same card has recurring enabled.
            }
            cell.callBackActiveRecurring =  {(test) in
                //same card has recurring enabled pop to back
                _ = self.navigationController?.popViewController(animated: true)
            }
            
            cell.callBackType = {(test) in
                if  test ==  true
                {
                    if self.boolGlobalRecurring == true
                    {
                        HSShowAlertView(APP_Name as NSString, message: "You have already configured an account for making recurring payments.")
                        return
                    }
                      self.boolNewRecurring = true
                    cell.btnRecurring.isSelected = true
                    cell.btnOneTime.isSelected = false
                }else{
                    self.boolNewRecurring = false
                    cell.btnRecurring.isSelected = false
                    cell.btnOneTime.isSelected = true
                }
                
                self.tblRentForm.reloadData()
        }
            
            return cell
        }else if indexPath.row == 3
        {
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
                if self.boolNewRecurring == true{
                    let cell: RecurringFormCell = tableView.dequeueReusableCell(withIdentifier: "RecurringFormCell") as! RecurringFormCell
                    cell.isUserInteractionEnabled = true
                    self.assignInfoToCell(cell: cell)
                    cell.callBackType = {(test : PickerSelected) in
                      if  test  ==  .RecurringDay
                              {
                                  self.pickerSelected = .RecurringDay
                            }else{
                                    self.pickerSelected = .RecurringTimes
                               }
                                    self.showPickerWithAnimation()
                          }
                    return cell
                    
                }else{
                    return returnAccountUsedCell(tableView: tableView, indexPath: indexPath as NSIndexPath)
                }
            }else if boolAccountRecurring == false && boolGlobalRecurring == true{
                return returnAccountUsedCell(tableView: tableView, indexPath: indexPath as NSIndexPath)
            }else
            {
                let cell: RecurringFormCell = tableView.dequeueReusableCell(withIdentifier: "RecurringFormCell") as! RecurringFormCell
                cell.isUserInteractionEnabled = false
                self.assignInfoToCell(cell: cell)
                cell.callBackType = {(test : PickerSelected) in
                    if  test  ==  .RecurringDay
                    {
                        self.pickerSelected = .RecurringDay
                    }else{
                        self.pickerSelected = .RecurringTimes
                    }
                    self.showPickerWithAnimation()
                }
                return cell
            }
        
        }else if indexPath.row == 4 {
            
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
                if self.boolNewRecurring == true{
                    return returnAccountUsedCell(tableView: tableView, indexPath: indexPath as NSIndexPath)
                }else{
                    
                    let cell: RentPaymentCommonCell = tableView.dequeueReusableCell(withIdentifier: "RentPaymentCommonCell") as! RentPaymentCommonCell
                    cell.lblTitle.text = "PAYMENT HISTORY"
                    cell.isHidden = true
                    
                    return cell
                }
            }else if boolAccountRecurring == false && boolGlobalRecurring == true{
                let cell: RentPaymentCommonCell = tableView.dequeueReusableCell(withIdentifier: "RentPaymentCommonCell") as! RentPaymentCommonCell
                cell.lblTitle.text = "PAYMENT HISTORY"
                cell.isHidden = true

                return cell
            }else
            {
                return returnAccountUsedCell(tableView: tableView, indexPath: indexPath as NSIndexPath)
            }
        }
        else if indexPath.row == 5{
            let cell: RentPaymentCommonCell = tableView.dequeueReusableCell(withIdentifier: "RentPaymentCommonCell") as! RentPaymentCommonCell
                cell.lblTitle.text = "PAYMENT HISTORY"
            cell.isHidden = true

            return cell
            
        }else{
            
            
            let cell: ACHAuthorizationCell = tableView.dequeueReusableCell(withIdentifier: "ACHAuthorizationCell") as! ACHAuthorizationCell
            cell.isHidden = true

            cell.callBackACHTerms = { (check : Bool) in
            }
            return cell
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.row == 1{
            
            
            return 160
        }
        
        if indexPath.row == 3{
            
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
                if self.boolNewRecurring == true{
                    return 150
                }else{
                    return calculateHeight()

                    //return 140
                }
            }else if boolAccountRecurring == false && boolGlobalRecurring == true{
                return calculateHeight()

                //return 140
            }else
            {
              return 150
            
            }
        }else if indexPath.row == 4{
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
                if self.boolNewRecurring == true{
                     return calculateHeight() // FOR ACH terms
                    
                    //  return 140
                }else{
                  return 70
                }
            }else if boolAccountRecurring == false && boolGlobalRecurring == true{
                 return 70
            }else
            {
                return calculateHeight()
                // return 140
            }
        }else {
            //history
            return 70
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5
        {
            self.openHistoryVC()
            
        }else if indexPath.row == 4 {
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
                if self.boolNewRecurring == false{
                   self.openHistoryVC()
                }
            }else if boolAccountRecurring == false && boolGlobalRecurring == true{
            self.openHistoryVC()
            }
        }
    }
    
    
    
    func calculateHeight() -> CGFloat{
        
        var height : CGFloat = 128
        return height + strACh.getSize(withConstrainedWidth: ScreenWidth - 85, font: UIFont(name: APP_FontRegularNew, size: 14)!)
        
    }
    func openHistoryVC()
    {
        let landlordVC: PaymentHistoryVC = PaymentHistoryVC.init(nibName: "PaymentHistoryVC", bundle: nil)
               self.navigationController?.isNavigationBarHidden = false
          self.navigationController?.pushViewController(landlordVC, animated: true)
        
    }

    
    
    
    
    
    
    func assignInfoToCell(cell : RecurringFormCell )
    {
        var strDay = arrayRecurringDay[recurringDay]
        
        if strDay == "Date"
        {
            cell.btnDay.setTitle(strDay, for: .normal)
            
        }else{
            
            if strDay == "1"{
            strDay =  strDay + "st"
            
        }else if strDay == "2"
        {
            strDay =   strDay + "nd"
        }else if strDay == "3"
        {
            strDay =   strDay + "rd"
        }else{
            strDay =   strDay + "th"
        }
            
            let font = UIFont(name: APP_FontRegularNew, size: 16)!
            let fontSuper:UIFont? = UIFont(name: APP_FontRegularNew , size:13)
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: strDay , attributes: [NSFontAttributeName:font ,NSForegroundColorAttributeName : UIColor.darkGray])
            attString.setAttributes([NSFontAttributeName:fontSuper!,NSBaselineOffsetAttributeName:6], range: NSRange(location: (strDay.characters.count - 2) , length:2))
            
            
            
            
            cell.btnDay.setAttributedTitle(attString, for: .normal)
            cell.btnDay.setTitleColor(UIColor.darkGray, for: .normal)
        
            
        }
        
        if recurringTimes == 0{
            cell.btnTimes.setTitle(arrayRecurringTimes[recurringTimes], for: .normal)
            
        }else{
            cell.btnTimes.setTitle(arrayRecurringTimes[recurringTimes] + " " + "Times", for: .normal)
        }
    }
    
    
    func returnAccountUsedCell(tableView: UITableView , indexPath : NSIndexPath) -> UITableViewCell
    {
        
        if payType == .Bank{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ACHSelectedCell") as! ACHSelectedCell
            cell.paymentSelected = objFinalPayment
            cell.assignDataToCell()
            cell.callBackACHEdit = {(test : Bool ) in
                self.openEditingList()
            }
            
            cell.callBackACHTerms = {(test : Bool ) in
               
                self.termsCheck = !self.termsCheck
                
                self.tblRentForm.reloadData()
                
            }
            
            
            
            if self.termsCheck  == true{
                cell.btnACHTerms.isSelected = true
            }else{
                cell.btnACHTerms.isSelected = false
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CCSelectedCell") as! CCSelectedCell
            cell.paymentSelected = objFinalPayment
            cell.updateCell()
            cell.callBackEdit = {(test : Bool ) in
                
                self.openEditingList()
                
            }
            return cell
        }
        
    }
    
    
    
    func openEditingList()
    {
        if payType == .Bank{
            
            
            if self.savedAccount == false{
                HSShowAlertView(APP_Name as NSString, message: "No saved account found.")
                return
            }
            
            let landlordVC: BankAccountsVC = BankAccountsVC.init(nibName: "BankAccountsVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            landlordVC.changePrevious = true
            self.navigationController?.pushViewController(landlordVC, animated: true)


        }else{
            
            if self.savedCard == false{
                HSShowAlertView(APP_Name as NSString, message: "No saved cards found.")
                return
            }
            
            let landlordVC: CCListingsVC = CCListingsVC.init(nibName: "CCListingsVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            landlordVC.changePrevious = true
            self.navigationController?.pushViewController(landlordVC, animated: true)
            
        }
        
    }
    
    //MARK:- PickerView Delegates
    //MARK:-
    
      func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerSelected == .RecurringDay {
            return arrayRecurringDay[row];
        }else {
            return   arrayRecurringTimes[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerSelected == .RecurringDay {
            return arrayRecurringDay.count;
        }else {
            return arrayRecurringTimes.count;
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           if pickerSelected == .RecurringDay {
            recurringDay = row
        }else {
            recurringTimes = row
        }
        tblRentForm.reloadData()
    }
    
    func pickerViewViewInialization()
    {
        containerView.frame = CGRect(x: 0, y: ScreenHeight, width: ScreenWidth, height: 240)
        containerView.backgroundColor = UIColor.white
        self.view.addSubview(containerView)
        self.pickerViewRecurring = UIPickerView()
        pickerViewRecurring?.frame = CGRect(x: 0, y: 10, width: ScreenWidth, height: 200)
        self.pickerViewRecurring?.delegate = self
        self.pickerViewRecurring?.dataSource = self
        self.pickerViewRecurring?.backgroundColor = UIColor.white
        containerView.addSubview(pickerViewRecurring!)

        
        let toolBarForTextView = UIToolbar()
        toolBarForTextView.tintColor = UIColor.black
        toolBarForTextView.barStyle = .default;
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.hidePickerWithAnimation))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.hidePickerWithAnimation))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBarForTextView.setItems([ cancelButton ,spaceButton,doneButton], animated: true)
        toolBarForTextView.isUserInteractionEnabled = true
        toolBarForTextView.sizeToFit()
        containerView.addSubview(toolBarForTextView)
    }

    
    func showPickerWithAnimation()
    {
        self.view.endEditing(true)
        
        if pickerSelected == .RecurringDay {
            pickerViewRecurring?.reloadAllComponents()
            pickerViewRecurring?.selectRow(recurringDay, inComponent: 0, animated: false)

        }else{
            pickerViewRecurring?.reloadAllComponents()
            pickerViewRecurring?.selectRow(recurringTimes, inComponent: 0, animated: false)
        }
        
        UIView.animate(withDuration: 0.4) {
            self.containerView.frame = CGRect(x: 0, y: ScreenHeight-240, width: ScreenWidth, height: 240)
        }
    }

    
    
    func hidePickerWithAnimation()
    {
        UIView.animate(withDuration: 0.4) {
            self.containerView.frame = CGRect(x: 0, y: ScreenHeight, width: ScreenWidth, height: 240)
        }

    }
    
    
    
    //MARK:- TextField delegate Methods
    //MARK:-
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(range.location==0)
        {
            if string == "0"
            {
                return false
            }else if string == "."{
                return false
            }
        }
        if(string == "." ){
            let countdots = textField.text!.components(separatedBy: ".").count - 1
            if countdots > 0 && string == "."
            {
                return false
            }
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       amountEntered = textField.text!
    }
    
    
    
    @IBAction func actionBtnSubmitPressed(_ sender: Any) {
        if amountEntered == "" || amountEntered.isEMPTY()
        {
            HSShowAlertView("TenantTag", message: "Please enter rent amount!", controller: self)
            return
        }else{
            let confirmVC: ConfirmBankPaymentVC = ConfirmBankPaymentVC.init(nibName: "ConfirmBankPaymentVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            
            if payType == .CC
            {
                confirmVC.payType = .CC
            }else{
                confirmVC.payType = .Bank
            }

            if self.termsCheck == false
            {     HSShowAlertView("TenantTag", message: "Please read ACH terms & conditions.", controller: self)
                return
                
            }
            
            
            
            
            if boolAccountRecurring == false && boolGlobalRecurring == false
            {
                if self.boolNewRecurring == true{
                    if recurringDay == 0 || recurringTimes == 0{
                        HSShowAlertView("TenantTag", message: "Please change recurring details.", controller: self)
                        return
                    }else{
                        objFinalPayment.recurringEnabled = true
                        objFinalPayment.recurringData.Day = NSInteger(arrayRecurringDay[recurringDay])!
                        var strTimes = arrayRecurringTimes[recurringTimes].replacingOccurrences(of: "", with: "")
                        strTimes =  strTimes.removeParticularCharacterString(character: " ")
                        objFinalPayment.recurringData.Times = NSInteger(strTimes)!
                    }
                    
                }
            }
            
            confirmVC.finalPayment = objFinalPayment
            confirmVC.amountPaid = amountEntered
            
            self.navigationController?.pushViewController(confirmVC, animated: true)
       }
    }
    
    
    func getPreviousTransaction()
    {
        Payment.getDuePayment { (isSuccess, responseData, error) in
            if isSuccess {
                if let dictResponse = responseData as? Dictionary<String,AnyObject>
                {
                    if let lastPayment = dictResponse["PreviousPayment"] as? Payment
                    {
                        self.previousPayment = lastPayment
                    }
                    
                    if let savedCard = dictResponse["AlreadySavedCard"] as? Bool
                    {
                        self.savedCard = savedCard
                    }
                    
                    if let savedAccount = dictResponse["AlreadySavedAccount"] as? Bool
                    {
                        self.savedAccount = savedAccount
                    }
                    self.alreadyRecurring = dictResponse["RecurringStatus"] as! Bool
                    self.boolGlobalRecurring = dictResponse["RecurringStatus"] as! Bool
                    self.tblRentForm.reloadData()
                }
            }else{
                
                
            }
        }
    }

}
