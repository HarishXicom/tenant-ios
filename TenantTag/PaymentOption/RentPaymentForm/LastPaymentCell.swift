//
//  LastPaymentCell.swift
//  TenantTag
//  Created by maninder on 21/12/16.
//  Copyright © 2016 Fueled. All rights reserved.

import UIKit

class LastPaymentCell: UITableViewCell {
    
    var payment = Payment()
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var lblPaymentDate: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var viewInner: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setPaymentDetails( payment : Payment)
    {
        lblAmount.text = "$ " + payment.amount
        lblPaymentDate.text = payment.paymentDate
    }
}
