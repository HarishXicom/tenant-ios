//
//  RentPaymentCommonCell.swift
//  TenantTag
//
//  Created by maninder on 20/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class RentPaymentCommonCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
