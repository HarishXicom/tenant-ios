//
//  ACHAuthorizationCell.swift
//  TenantTag
//
//  Created by maninder on 10/16/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class ACHAuthorizationCell: UITableViewCell {

    
    var callBackACHTerms:((Bool)->Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnAuthorizationPressed(_ sender: UIButton) {
        
        if callBackACHTerms != nil
        {
            callBackACHTerms!(true)
        }
        
    }
}
