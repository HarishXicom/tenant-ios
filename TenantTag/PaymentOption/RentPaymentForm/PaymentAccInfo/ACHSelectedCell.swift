//
//  ACHSelectedCell.swift
//  TenantTag
//
//  Created by maninder on 07/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class ACHSelectedCell: UITableViewCell {
    var callBackACHEdit:((Bool)->Void)? = nil
    
    var callBackACHTerms:((Bool)->Void)? = nil


    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    var paymentSelected = Payment()
    
    @IBOutlet var txtThird: UITextField!
    @IBOutlet var txtSecond: UITextField!
    @IBOutlet var txtFirst: UITextField!
  //  @IBOutlet var txtACHNumber: UITextField!
    @IBOutlet var btnShowType: UIButton!
    @IBOutlet var btnEdit: UIButton!
    
    @IBOutlet var btnACHTerms: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnEdit.cornerRadius(4)
        viewInner.cornerRadius(4)
        viewOuter.cornerRadius(5)
        txtFirst.cornerRadius(5)
        txtFirst.addBorderWithColorAndLineWidth(color: APP_ThemeColor, borderWidth: 0.5)
        txtSecond.cornerRadius(5)
        txtSecond.addBorderWithColorAndLineWidth(color: APP_ThemeColor, borderWidth: 0.5)
        txtThird.cornerRadius(5)
        txtThird.addBorderWithColorAndLineWidth(color: APP_ThemeColor, borderWidth: 0.5)

    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func assignDataToCell()
    {
        txtFirst.text =  "  " + paymentSelected.selectedNode.bankFullName
        txtSecond.text =  "  XXXX-" + paymentSelected.selectedNode.accountNo
        txtThird.text =  "  " + paymentSelected.selectedNode.routingNumber
    }
    
    
    @IBAction func actionBtnEditPressed(_ sender: Any) {
        if callBackACHEdit != nil{
            callBackACHEdit!(true)
        }
    }
    @IBAction func actionBtnAuthrization(_ sender: Any) {
        
        if callBackACHTerms != nil{
            callBackACHTerms!(true)
        }

        
    }
    
//    if callBackACHTerms != nil
//    {
//    callBackACHTerms!(true)
//    }
}
