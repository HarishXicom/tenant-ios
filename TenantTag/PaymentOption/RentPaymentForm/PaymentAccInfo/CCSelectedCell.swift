//
//  CCSelectedCell.swift
//  TenantTag
//
//  Created by maninder on 07/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class CCSelectedCell: UITableViewCell {
    var paymentSelected = Payment()
    var callBackEdit:((Bool)->Void)? = nil
    @IBOutlet var txtCCNumber: UITextField!
    @IBOutlet var btnShowType: UIButton!
    @IBOutlet var btnEdit: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnEdit.cornerRadius(4)
        txtCCNumber.cornerRadius(5)
        txtCCNumber.addBorderWithColorAndLineWidth(color: APP_ThemeColor, borderWidth: 0.5)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateCell()
    {
        
        txtCCNumber.text = "  XXXX-" + paymentSelected.card.last4Digits
    }
    
    @IBAction func actionBtnEdit(_ sender: Any) {
        if callBackEdit != nil{
            callBackEdit!(true)
        }
    }
    
}
