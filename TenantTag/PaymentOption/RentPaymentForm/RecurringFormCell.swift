//
//  RecurringFormCell.swift
//  TenantTag
//
//  Created by maninder on 21/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class RecurringFormCell: UITableViewCell {

    
    
    var callBackType:((PickerSelected)->Void)? = nil

    @IBOutlet var btnTimes: UIButton!
    @IBOutlet var btnDay: UIButton!
    @IBOutlet var btnMonthly: UIButton!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func actionBtnDay(_ sender: Any) {
        
        
        if callBackType != nil{
         self.callBackType!(.RecurringDay)
        }
        
    }
    
    
    @IBAction func actionBtnRecurringTime(_ sender: Any) {
        if callBackType != nil{
            self.callBackType!(.RecurringTimes)
        }

        
    }
    
    
    
}
