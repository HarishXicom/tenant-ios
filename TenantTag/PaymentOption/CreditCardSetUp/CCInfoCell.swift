//
//  CCInfoCell.swift
//  TenantTag
//
//  Created by maninder on 18/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class CCInfoCell: UITableViewCell {

    @IBOutlet var lblInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setText()
    {
        self.lblInfo.text = "TPay is not available."
        self.lblInfo.textColor = UIColor.red
        self.lblInfo.textAlignment = .center
    }
    
}
