//
//  CCSetUpCell.swift
//  TenantTag
//
//  Created by maninder on 16/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class CCSetUpCell: UITableViewCell {
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    
    @IBOutlet var btnMonth: UIButton!
    @IBOutlet var btnYear: UIButton!
    
    @IBOutlet var txtSecurityCode: UITextField!
    var callBackMonthYear:((String , String)->Void)? = nil
   // @IBOutlet var txtZipCode: UITextField!
   // @IBOutlet var txtStreet: UITextField!
  //  @IBOutlet var txtCity: UITextField!
   // @IBOutlet var txtAddressSecond: UITextField!
  //  @IBOutlet var txtAddressFirst: UITextField!
  //  @IBOutlet var txtSecurityCode: UITextField!
    @IBOutlet var txtCreditCardNumber: UITextField!
    @IBOutlet var txtUserName: UITextField!
   // var callBack :(
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(4)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnYear(_ sender: Any) {
        if callBackMonthYear != nil{
            callBackMonthYear!("January","2019")
        }
    }
    
    @IBAction func actionBtnMonth(_ sender: Any) {
        if callBackMonthYear != nil{
            callBackMonthYear!("January","2019")
        }
    }
    
//    @IBAction func actionBtnYearPressed(_ sender: Any) {
//       
//    }
//    
//    @IBAction func actionBtnMonthPressed(_ sender: Any) {
//        if callBackMonthYear != nil{
//            callBackMonthYear!("January","2019")
//        }
//    }
    
    
    
}
