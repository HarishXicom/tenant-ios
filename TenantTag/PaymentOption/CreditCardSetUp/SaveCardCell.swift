//
//  SaveCardCell.swift
//  TenantTag
//
//  Created by maninder on 19/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class SaveCardCell: UITableViewCell {
    @IBOutlet var btnCheckBox: UIButton!
    var callBackSaveCard:((Bool)->Void)? = nil

    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBtncheckBoxPressed(_ sender: Any) {
        if callBackSaveCard != nil{
            self.callBackSaveCard!(true)
        }
    }
    
    func customAction(cond: Bool)
    {
        self.btnCheckBox.isSelected = !cond
    }
}
