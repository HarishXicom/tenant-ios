//
//  CCSetUpVC.swift
//  TenantTag
//
//  Created by maninder on 16/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit


class CCSetUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
  
    @IBOutlet var btnAddCard: UIButton!
    let arrayYears : [String] = ["2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028" ,"2029","2030","2031","2032","2033","2034","2035","2036"]
    let arrayMonth : [String] = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    var payment = Payment()

    let datePicker = UIPickerView()
    var fullBGView = UIView()
    var  datePickerContainer = UIView()
    @IBOutlet var tblCCSetUp: UITableView!
    
    var monthSelectedIndex : NSInteger = 0
    var yearSelectedIndex : NSInteger = 0
    var creditCard = CreditCard()
    var boolCheck = false
    override func viewDidLoad() {
        super.viewDidLoad()
        creditCard.saveCard = true
//
        btnAddCard.cornerRadius(5)
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(CCSetUpVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("CC Set Up" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        
        creditCard.state = "USA"
        tblCCSetUp.registerNibsForCells(arryNib: ["CCInfoCell","CCSetUpCell","SaveCardCell"])
        tblCCSetUp.dataSource = self
        tblCCSetUp.delegate = self
        tblCCSetUp.estimatedRowHeight = 60
        tblCCSetUp.rowHeight = UITableViewAutomaticDimension;
        tblCCSetUp.allowsSelection = true
        btnAddCard.setTitle("SUBMIT", for: .normal)

        self.setUpPickerUI()
        
    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CCSetUpVC.textFieldDidToTextField(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }

    
    
    
    func setUpPickerUI()
    {
        
        
        fullBGView.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
        fullBGView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        fullBGView.isHidden = true
        fullBGView.alpha = 0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CCSetUpVC.hideDatePicker))
        tapGesture.numberOfTapsRequired = 1
        fullBGView.addGestureRecognizer(tapGesture)
        
        appDelegate().window?.addSubview(fullBGView)
        
        
        
        datePickerContainer.frame = CGRect(x: 0, y: ScreenHeight, width: ScreenWidth, height: 200)
        datePickerContainer.backgroundColor = UIColor.white
        appDelegate().window?.addSubview(datePickerContainer)
        
        
        let toolBarForTextView = UIToolbar()
        toolBarForTextView.tintColor = UIColor.black
        toolBarForTextView.barStyle = .default;
        let doneButton = UIBarButtonItem(title: "Done   ", style: .done, target: self, action: #selector(self.doneBtnPressed))
        let cancelButton = UIBarButtonItem(title: "   Cancel", style: .plain, target: self, action: #selector(self.hideDatePicker))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBarForTextView.setItems([ cancelButton ,spaceButton,doneButton], animated: true)
        toolBarForTextView.isUserInteractionEnabled = true
        toolBarForTextView.sizeToFit()
        datePickerContainer.addSubview(toolBarForTextView)
        
        
        
        datePicker.frame = CGRect(x: 0, y: 40, width: ScreenWidth, height: 160)
        datePicker.delegate = self
        datePicker.dataSource = self
        datePicker.backgroundColor =  UIColor.white
        datePickerContainer.addSubview(self.datePicker)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func actionBtnBackPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnAddCreditCard(_ sender: Any) {
        self.postTokenWithCardDetails()
    }
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell: CCInfoCell = tableView.dequeueReusableCell(withIdentifier: "CCInfoCell") as! CCInfoCell
            return cell

        }
        
      else  if indexPath.row == 1 {
            let cell: CCSetUpCell = tableView.dequeueReusableCell(withIdentifier: "CCSetUpCell") as! CCSetUpCell
            
            cell.txtUserName.delegate = self
            cell.txtCreditCardNumber.delegate = self
            cell.txtSecurityCode.delegate = self
            cell.txtCreditCardNumber.addTarget(self, action: #selector(self.setCreditCardFormat(txtF:)), for: .editingChanged)
            
            if creditCard.month != -1 {
                cell.btnMonth.setTitleColor(UIColor.black, for: .normal)
                cell.btnMonth.setTitle(arrayMonth[creditCard.month], for: .normal)
            }
         
            cell.txtUserName.text = creditCard.nameOnCard
            cell.txtCreditCardNumber.text = creditCard.cardNumber
            cell.txtSecurityCode.text = creditCard.cvv

            
            if creditCard.year  != -1
            {
                cell.btnYear.setTitleColor(UIColor.black, for: .normal)

                cell.btnYear.setTitle(arrayYears[creditCard.year], for: .normal)
            }

            cell.callBackMonthYear = {(month : String , year : String ) in
                if self.boolCheck == false{
                    self.showDatePicker()
                }
            }
            
            return cell
        }else{
            let cell: SaveCardCell = tableView.dequeueReusableCell(withIdentifier: "SaveCardCell") as! SaveCardCell
            cell.callBackSaveCard = {(test : Bool) in
                self.creditCard.saveCard = !self.creditCard.saveCard
                self.tblCCSetUp.reloadData()
            }
            if self.creditCard.saveCard == true{
                cell.btnCheckBox.isSelected = false

            }else{
                cell.btnCheckBox.isSelected = true
            }
            
            return cell
        }
    }

    //MARK:- Text Field Delegates
    //MARK:-
    func textFieldDidToTextField(_ notification : NSNotification)
    {
        let textField = notification.object as! UITextField
        if textField.tag == 1 {
            creditCard.nameOnCard = textField.text!
            
        }else  if textField.tag == 2{
            creditCard.cardNumber = textField.text!
            
        }else if textField.tag  == 3 {
            creditCard.cvv = textField.text!
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            
      if textField.tag == 2
        {
            if string == "."
            {
                return false
            }

            let newLength =  19 - (textField.text!.characters.count + string.characters.count - range.length)
            if newLength >= 0 && newLength <= 19{
                return true
            }
            return false

        }else if textField.tag == 3 {
                if string == "."
                {
                    return false
                }

            let newLength =  4 - (textField.text!.characters.count + string.characters.count - range.length)
            if newLength >= 0 && newLength <= 4{
                return true
            }
            return false

        }else if textField.tag == 8 {
                if string == "."
                {
                    return false
                }
            let newLength =  6 - (textField.text!.characters.count + string.characters.count - range.length)
            if newLength >= 0 && newLength <= 6{
                return true
            }
            return false
        }
        return true
        
    }
  
    

    //MARK:- Picker View Delegates
    //MARK:-
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return arrayMonth.count;
        }else{
            return arrayYears.count;
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return arrayMonth[row]
        }else{
            return  arrayYears[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
              creditCard.month = row
                 }else{
                   creditCard.year = row
            }
            tblCCSetUp.reloadData()
    }
    

    
    func hideDatePicker(){
        
            boolCheck = false
        UIView.animate(withDuration: 0.3, animations: {
            self.fullBGView.alpha = 0
            self.datePickerContainer.frame = CGRect(x: 0, y: ScreenHeight, width: ScreenWidth, height: 200)
        }) { (test) in
            self.fullBGView.isHidden = true
        }
        
    }
    
    func showDatePicker()
    {
        self.view.endEditing(true)
        self.fullBGView.isHidden = false
        boolCheck = true
        UIView.animate(withDuration: 0.3, animations: {
            self.fullBGView.alpha = 1
            self.datePickerContainer.frame = CGRect(x: 0, y: ScreenHeight-200, width: ScreenWidth, height: 200)
        }) { (test) in
        }
    }
    
    
    func doneBtnPressed(){
        self.hideDatePicker()
    }

    
    func postTokenWithCardDetails()
    {
        self.view.endEditing(true)
        
        if  creditCard.nameOnCard.isEMPTY()
        {
           HSShowAlertView("TenantTag", message: "Please enter name!", controller: self)
            return
        }else if creditCard.cardNumber.isEMPTY()
        {
            HSShowAlertView("TenantTag", message: "Please enter card number!", controller: self)
            return
            
        }else if (creditCard.cardNumber as NSString).isValidCreditCardNumber()  == false
        {
            HSShowAlertView("TenantTag", message: "Please enter valid card number!", controller: self)
            return
            
        } else if  creditCard.cvv.isEMPTY()
        {
            HSShowAlertView("TenantTag", message: "Please enter cvv number!", controller: self)
            return
            
        }else if creditCard.cvv.characters.count <= 2
        {
            
            HSShowAlertView("TenantTag", message: "Please enter valid cvv number!", controller: self)
            return
        }else if creditCard.month == -1 {
            HSShowAlertView("TenantTag", message: "Please enter expiry month!", controller: self)
            return
            
        }else if creditCard.year == -1 {
            HSShowAlertView("TenantTag", message: "Please enter expiry year!", controller: self)
            return
        }
           let yearUInt =   NSInteger(arrayYears[creditCard.year])
        let cardParams = STPCardParams()
        cardParams.number = creditCard.cardNumber.removeParticularCharacterString(character: "-")
        cardParams.expMonth = UInt(creditCard.month + 1 )
        cardParams.expYear = UInt(yearUInt!)
        cardParams.cvc = creditCard.cvv
        HSProgress(.show)
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            if let error = error {
                 HSProgress(.hide)
                HSShowAlertView("TenantTag", message: error.localizedDescription as NSString, controller: self)
                return
                // show the error to the user
            } else if let token = token {
               // self.creditCard.nameOnCard =
                self.creditCard.token = token.tokenId
                let stripeCard = token.card
                self.creditCard.cardID = (stripeCard?.cardId)!
                self.creditCard.last4Digits = (stripeCard?.last4())!
                 self.creditCard.strMonth = Int((stripeCard?.expMonth)!)
                self.creditCard.strYear = Int((stripeCard?.expYear)!)
                self.creditCard.createdAt = "\(token.created!)"
             


                if stripeCard?.brand == STPCardBrand.amex {
                    self.creditCard.cardType = "Amex"
                }else if stripeCard?.brand == STPCardBrand.masterCard {
                    self.creditCard.cardType = "MasterCard"

                }else if stripeCard?.brand == STPCardBrand.visa {
                    self.creditCard.cardType = "Visa"
                    
                }else if stripeCard?.brand == STPCardBrand.JCB {
                    self.creditCard.cardType = "JCB"

                }else if stripeCard?.brand == STPCardBrand.dinersClub {
                    self.creditCard.cardType = "DinersClub"

                    
                }else if stripeCard?.brand == STPCardBrand.discover {
                    self.creditCard.cardType = "Discover"
                }else {
                    self.creditCard.cardType = "Unknown"
                }
                
                
                if stripeCard?.funding == STPCardFundingType.debit
                {
                 self.creditCard.funding = "Debit"
                }else if stripeCard?.funding == STPCardFundingType.credit
                {
                    self.creditCard.funding = "Credit"

                }else if stripeCard?.funding == STPCardFundingType.prepaid
                {
                    self.creditCard.funding = "Prepaid"

                }else if stripeCard?.funding == STPCardFundingType.other
                {
                    self.creditCard.funding = "Other"
                 }
               // print(self.creditCard.saveCard)
                
                self.payment.card = self.creditCard
                if self.creditCard.saveCard == true{
                    self.creditCard.createPayment({ (isSuccess, data, error) in
                        HSProgress(.hide)
                        if isSuccess {
                            let rentObj = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
                            self.navigationController?.isNavigationBarHidden = false
                            rentObj.payType = .CC
                            rentObj.objFinalPayment =  self.payment
                           // rentObj.cardDetails =
                            self.navigationController?.pushViewController(rentObj, animated: true)
                        }else{
                            
                        }
                    })
                }else{
                     HSProgress(.hide)
                    let rentObj = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    rentObj.payType = .CC
                    rentObj.objFinalPayment =  self.payment
                    rentObj.noRecurring = true
                    //rentObj.cardDetails = self.creditCard
                    self.navigationController?.pushViewController(rentObj, animated: true)
                }
            }
        }
        
    }
    
    
    func setCreditCardFormat(txtF : UITextField)
    {
        
     txtF.text =  creditCardTextfieldFormatter(txtField: txtF)
        creditCard.cardNumber = txtF.text!
        
    }

}
