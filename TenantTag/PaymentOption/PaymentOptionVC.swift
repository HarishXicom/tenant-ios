//
//  PaymentOptionVC.swift
//  TenantTag
//
//  Created by maninder on 15/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

enum PaymentType : Int {
    case None = 0 ,
    DebitCard  ,
    BankAccount
}

class PaymentOptionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let options : [String]  = ["Debit, credit, or prepaid card","Bank account (ACH transfer)"]
     let optionDetails : [String]  = ["Visa, Mastercard or Discover cards only.Payments take to 1-2 business days.\n3% fee per transaction (paid by tenant).","Payments take to 2 business days.\nFree! No transaction fees."]
    var paymentSelected  : PaymentType = .BankAccount

    @IBOutlet var tblOptions: UITableView!
    @IBOutlet var btnSetup: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        tblOptions.registerNibsForCells(arryNib: ["TitleTableCell","CommonQuestionsBtnCell","CCInfoCell"])
        tblOptions.dataSource = self
        tblOptions.delegate = self
        tblOptions.estimatedRowHeight = 100
        tblOptions.rowHeight = UITableViewAutomaticDimension;
        tblOptions.allowsSelection = true
       // tblOptions.backgroundColor = UIColor.groupTableViewBackground
        self.btnSetup.cornerRadius(4)
       
        // Do any additional setup after loading the view.
    }
    
    
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        LoginManager.getTenantPropertyInformation { (isSuccess, data, error) in
            if isSuccess{
                if (data as? Dictionary<String ,AnyObject>) != nil
                {
                 self.tblOptions.reloadData()
                }
            }else{
        
        
            }
        }

       
     }
    
    //MARK:- TableView Delegates
    //MARK:-
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if LoginManager.getMe.tPayStatus == .NotAllowed{
            return 4
        }else{
            return 3
        }
        


    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: TitleTableCell = tableView.dequeueReusableCell(withIdentifier: "TitleTableCell") as! TitleTableCell
            cell.callBackType = {(type : PaymentType) in
                if LoginManager.getMe.landLordSubscriptionStatus == false{
                    HSShowAlertView(APP_Name as NSString, message: "Your landlord subscription required.")
                    return
                }
                
                self.paymentSelected = type

                    cell.btnBankAccount.isSelected = false

                    if LoginManager.getMe.ownerSynpsePayEnabled == false
                    {
                        HSShowAlertView(APP_Name as NSString, message: "Your landlord has not configured ACH yet.")
                        return
                    }
                    let landlordVC: BankAccountsVC = BankAccountsVC.init(nibName: "BankAccountsVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    self.navigationController?.pushViewController(landlordVC, animated: true)
            
            }
            
            if LoginManager.getMe.tPayStatus == .NotAllowed{
                cell.isUserInteractionEnabled = false
            }else{
                cell.isUserInteractionEnabled = true
            }
            return cell
        }else if indexPath.row == 1{
            let cell: CommonQuestionsBtnCell = tableView.dequeueReusableCell(withIdentifier: "CommonQuestionsBtnCell") as! CommonQuestionsBtnCell
            cell.callBackCommonQuestion = {(test : Bool) in
               
                let historyVC: PaymentHistoryVC = PaymentHistoryVC.init(nibName: "PaymentHistoryVC", bundle: nil)
                historyVC.checkPresent = true
                let navigation = UINavigationController(rootViewController: historyVC)
                self.navigationController?.present(navigation, animated: true, completion: nil)
            }
            cell.btnCommonQuestions.underLineButton(text: "Payment History", color: APP_ThemeColor)
            return cell
        }else if indexPath.row == 2 {
            let cell: CommonQuestionsBtnCell = tableView.dequeueReusableCell(withIdentifier: "CommonQuestionsBtnCell") as! CommonQuestionsBtnCell
            cell.callBackCommonQuestion = {(test : Bool) in
                openInAppBrowser(controller: self)
            }
            return cell
            
            
        }else{
        
            let cell: CCInfoCell = tableView.dequeueReusableCell(withIdentifier: "CCInfoCell") as! CCInfoCell
            cell.setText()
            return cell
            
            
        }
    }
    
    @IBAction func actionBtnSetupPressed(_ sender: Any) {
        
        if self.paymentSelected == .None{
            
        }else if self.paymentSelected == .DebitCard{
            
            let landlordVC: CCListingsVC = CCListingsVC.init(nibName: "CCListingsVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(landlordVC, animated: true)
            
        }else{
            
            
            
            
        }
      }
   
   }
