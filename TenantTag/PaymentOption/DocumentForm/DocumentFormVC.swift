//
//  DocumentFormVC.swift
//  TenantTag
//
//  Created by maninder on 4/24/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit
import Photos
import SafariServices
import Photos

enum ImageType : String {
    
    case PhotoID = "PhotoID"
      case Selfie = "Selfie"
       case EIN = "EIN"
       case Legal = "Legal"
}







extension String {
        func getDateFromString()-> Date
    {
        let dateFormate:DateFormatter = DateFormatter()
        dateFormate.dateFormat = APP_DOBDateFormateBackEnd
        let dateString : Date = dateFormate.date(from: self)!
        return dateString
        
    }
    
    func getSize(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func getDateFromStringNew()-> Date
    {
        let dateFormate:DateFormatter = DateFormatter()
        dateFormate.dateFormat = APP_DOBDateFormateNew
        let dateString : Date = dateFormate.date(from: self)!
        return dateString
        
    }
}

class DocumentFormVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,LTLinkTextViewDelegate
{
    
    var strDocID : String? = ""
    
    @IBOutlet weak var imgViewLicense: UIImageView!
    @IBOutlet weak var imgViewLeaseCopy: UIImageView!
    
    @IBOutlet var btnSelfie: UIButton!
    
    @IBOutlet var btnEIN: UIButton!
    
    @IBOutlet var btnLegal: UIButton!
  
    @IBOutlet var btnPhotoID: UIButton!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var txtProfession: UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtDOB: UITextField!
    @IBOutlet var txtAddress1: UITextField!
    @IBOutlet var txtStreet: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtZipCode: UITextField!
    @IBOutlet var txtSSN: UITextField!
    @IBOutlet var btnCommonQuestions: UIButton!
    var datePicker : UIDatePicker!
    var selectedDate : Date!
    var genderPicker : UIPickerView!
    var pickerProfession : UIPickerView!

    var selectedButton : Bool = true
    
    var selectedImage : ImageType = .PhotoID
    
    var imageLeaseCopy : UIImage!
    var imageLicense : UIImage!
    var imageEIN : UIImage!
    var imageLegelDocument : UIImage!
    
    @IBOutlet weak var viewTermsAndConditions: LTLinkTextView!
    @IBOutlet weak var viewAuthorization: LTLinkTextView!
    @IBOutlet var btnFirst: UIButton!
    @IBOutlet var btnSecond: UIButton!
    
    var tapGesture : UITapGestureRecognizer!

    
    var arrayGender : [String] = ["Male" ,"Female"]
    var arrayProfession : [String] = ["Not Known" ,"Airport","Arts & Entertainment" ,"Automotive" ,"Bank & Financial Services", "Bar" ,"Book Store" ,"Business Services" ,"Religious Organization","Club" ,"Community/Government" ,"Concert Venue" ,"Doctor","Event Planning/Event Services" ,"Food/Grocery" , "Health/Medical/Pharmacy" , "Home Improvement" , "Hospital/Clinic" , "Hotel" , "Landmark", "Lawyer" , "Library" , "Licensed Financial Representative" , "Local Business" , "Middle School", "Movie Theater" , "Museum/Art Gallery" , "Outdoor Gear/Sporting Goods" , "Pet Services" , "Professional Services" ,"Public Places" ,"Real Estate" ,"Restaurant/Cafe","School" , "Shopping/Retail" , "Spas/Beauty/Personal Care" , "Sports Venue" , "Sports/Recreation/Activities" ]
 
    
    


    @IBOutlet weak var viewTerms2: LTLinkTextView!
    @IBOutlet weak var lblTerms: UILabel!
    
    @IBOutlet weak var lblACHForm: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.layoutIfNeeded()

        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.navigationController?.isNavigationBarHidden = false
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(DocumentFormVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("New User Setup" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
         btnCommonQuestions.underLineButton(text: "Common Questions", color: APP_ThemeColor)
        
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(4)
        
        self.datePicker = UIDatePicker()
        self.datePicker.addTarget(self, action: #selector(DocumentFormVC.datePickerValueChanged(_:)), for: .valueChanged)
        self.datePicker.datePickerMode = .date
        
        self.selectedDate = ((Date() as NSDate).subtractingYears(18) as NSDate).atStartOfDay()
          self.datePicker.setDate(self.selectedDate, animated: false)
        txtDOB.inputView = self.datePicker;
        
        
        genderPicker = UIPickerView()
        genderPicker.delegate = self
        genderPicker.dataSource = self
        
        
        pickerProfession = UIPickerView()
        pickerProfession.delegate = self
        genderPicker.dataSource = self
        
        txtGender.inputView = genderPicker
        txtState.inputView = pickerProfession
        txtState.delegate = self
       // txtGender.text = "Male"
        
        txtName.text = LoginManager.getMe.fullName
        txtEmail.text = LoginManager.getMe.emailAddress
        txtPhoneNumber.text = LoginManager.getMe.phoneNumber
       
        txtPhoneNumber.delegate = self
        
        
        viewTermsAndConditions.delegate = self
        viewTerms2.delegate = self
        viewAuthorization.delegate = self
        
        
        
    
     //   viewTermsAndConditions.setStringAttributes(txtTerms, withButtonsStringsAttributes: btnTerms)
//         viewTerms2.setStringAttributes(txtTermsSecond, withButtonsStringsAttributes: btnTerms2)
       
        
        
        
        if leasedDocImage != nil
        {
            UIImageWriteToSavedPhotosAlbum(leasedDocImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            imgViewLeaseCopy.image = leasedDocImage
            imageLeaseCopy = leasedDocImage
            
        }
         self.getPreviousKYCInfo()

        // Do any additional setup after loading the view.
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
         } else {
           
            print("Image saved")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    
        
        
         let txtTerms = [LTTextStringParameterKey : "By using our services, you agree to Coastal Investment Group LLC's (dba TenantTag) Terms of Service and Privacy Policy", NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 15)! ] as [String : Any]
          let txtTermsSecond = [LTTextStringParameterKey : "and to SynapseFi's Terms of Service and Privacy Policy.", NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 15)! ] as [String : Any]
        
     
        let btnTerms = [[LTTextStringParameterKey : " Terms of Service", NSForegroundColorAttributeName : APP_ThemeColor,NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 14)! ],[LTTextStringParameterKey : " Privacy Policy", NSForegroundColorAttributeName : APP_ThemeColor,NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 14)! ]]
        let btnTerms2 = [[LTTextStringParameterKey : "Terms of Service", NSForegroundColorAttributeName : APP_ThemeColor,NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 14)! ],[LTTextStringParameterKey : "Privacy Policy", NSForegroundColorAttributeName : APP_ThemeColor,NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 14)! ]]

        viewTermsAndConditions.setStringAttributes(txtTerms, withButtonsStringsAttributes: btnTerms)
        viewTerms2.setStringAttributes(txtTermsSecond, withButtonsStringsAttributes: btnTerms2)
        
        
        
          let txtAutorization = [LTTextStringParameterKey : "ACH Disclosure:I understand that because this is an electronic ACH transaction, these funds may be withdrawn from my account as soon as the above noted execution date. I will not dispute Coastal Investment Group, LLC's debiting my checking/savings account as long as the transaction corresponds to the terms indicated in this form.", NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 15)! ] as [String : Any]

        

         let btnAutorization = [[LTTextStringParameterKey : "ACH Disclosure", NSForegroundColorAttributeName : APP_ThemeColor,NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 14)! ],[LTTextStringParameterKey : "Privacy Policy", NSForegroundColorAttributeName : APP_ThemeColor,NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 14)! ]]
        
        viewAuthorization.setStringAttributes(txtAutorization, withButtonsStringsAttributes: btnAutorization)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func actionBtnFirstTerms(_ sender: Any) {
        
        self.btnFirst.isSelected =  !self.btnFirst.isSelected
    }
    
    @IBAction func actionBtnSecondSelect(_ sender: Any) {
        self.btnSecond.isSelected =  !self.btnSecond.isSelected
    }
    
    func actionBtnBackPressed()
    {
      //  self.dismiss(animated: true, completion: nil)
        
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:- API Function
    //MARK:-
    
    @IBAction func actionBtnSubmit(_ sender: Any) {
        
        if txtName.text?.isEMPTY()  == true {
        HSShowAlertView(APP_Name as NSString, message: "Please enter name.")
            return
        }else
        
        
        if txtPhoneNumber.text?.isEMPTY()  == true {
            HSShowAlertView(APP_Name as NSString, message: "Please enter phone number.")
            return
        }else
        
        if (txtPhoneNumber.text?.characters.count)!  != 14 {
            HSShowAlertView(APP_Name as NSString, message: "Please enter valid phone number.")
              return
        }else
        
        if txtEmail.text?.isEMPTY() == true{
            HSShowAlertView(APP_Name as NSString, message: "Please enter email.")
            return
        }else
            if txtEmail.text?.isValidEmail() == false{
                HSShowAlertView(APP_Name as NSString, message: "Please enter valid email address.")
                  return
              }
            
            
//            else  if txtProfession.text?.isEMPTY() == true{
//                HSShowAlertView(APP_Name as NSString, message: "Please enter your profession.")
//                return
//        }
            else  if txtAddress1.text?.isEMPTY() == true{
                HSShowAlertView(APP_Name as NSString, message: "Please enter your address.")
                return
        }
                
//            else if  txtStreet.text?.isEMPTY() == true{
//                HSShowAlertView(APP_Name as NSString, message: "Please enter street.")
//                return
//                
//            }
            
            else if  txtCity.text?.isEMPTY() == true{
                HSShowAlertView(APP_Name as NSString, message: "Please enter city name.")
                return
                
            }
            else if  txtState.text?.isEMPTY() == true{
                HSShowAlertView(APP_Name as NSString, message: "Please enter state name.")
                return
            }
          
        else if txtZipCode.text?.isEMPTY() == true
        {
            HSShowAlertView(APP_Name as NSString, message: "Please enter your zip code.")
            return
         }else if txtSSN.text?.isEMPTY() == true
            {
                HSShowAlertView(APP_Name as NSString, message: "Please enter your SSN.")
                return
        }
            

        
        self.view.endEditing(true)
        if imageLeaseCopy == nil{
            HSShowAlertView(APP_Name as NSString, message: "Please add lease document image first.")
            return
        }

        
        if btnFirst.isSelected == false || btnSecond.isSelected == false{
            HSShowAlertView(APP_Name as NSString, message: "Please agree with terms and conditions.")
         return
        }
        
        
        
        let base64LeaseCopy =  "data:image/jpg;base64," + imageLeaseCopy.getBase64()
        
        var base64License = String()
        if imageLicense != nil{
            base64License =  "data:image/jpg;base64," + imageLicense.getBase64()
        }

        var dictParamter = Dictionary<String , Any>()
        
        dictParamter["Name"] = txtName.text!
        dictParamter["Email"] = txtEmail.text!
        dictParamter["PhoneNumber"] = txtPhoneNumber.text!
        dictParamter["DOB"] = selectedDate
        dictParamter["Profession"] = "Not Known"
        dictParamter["Street"] = txtAddress1.text!
        dictParamter["Gender"] = "NOT_KNOWN"
        dictParamter["Address"] = txtAddress1.text!
        dictParamter["City"] = txtCity.text!
        dictParamter["State"] = txtState.text!
        dictParamter["ZipCode"] = txtZipCode.text!
        dictParamter["SSN"] = txtSSN.text!
        dictParamter["License"] = base64License
        dictParamter["LeaseCopy"] = base64LeaseCopy
        
        
        if strDocID != ""
        {
            dictParamter["KYC_ID"] = strDocID
            LoginManager.share().updateDocuments(dict: dictParamter) { (isSuccess, response, error) in
                if isSuccess
                {
                   if let dictSuccess = response as? Dictionary<String,Any>
                    {
                       LoginManager.share().saveKYCInfoToServer(dict: dictSuccess, callBack: { (Success, responseData, error) in
                           if isSuccess
                            {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                    }
                }
            }
 
        }else{
            
        
        LoginManager.share().addDocuments(dict: dictParamter) { (isSuccess, response, error) in
            if isSuccess
            {
                if let dictSuccess = response as? Dictionary<String,Any>
               {
                   LoginManager.share().saveKYCInfoToServer(dict: dictSuccess, callBack: { (Success, responseData, error) in
                    
                        if isSuccess
                       {
                           self.navigationController?.popToRootViewController(animated: true)
                        }
                    })
                }
            }
        }
        }
    }

    @IBAction func actionBtnCommonQuestions(_ sender: Any) {
         openInAppBrowser(controller: self)
        
    }
    
    
    
    
    
    func datePickerValueChanged(_ datePicker:UIDatePicker) {
        var date:Date = self.datePicker.date
        date = (date as NSDate).atStartOfDay()
        
        if ((date as NSDate).isEarlierThanDate((Date() as NSDate).subtractingYears(18)) ) {
           self.selectedDate = date
            self.settingDate()
        }else{
             print("date")
            
        }

    }
    
    func settingDate()
    {
        
        let dateFormate:DateFormatter = DateFormatter()
        dateFormate.dateFormat = APP_DOBDateFormateBackEnd
        let dateString:NSString = dateFormate.string(from: self.selectedDate) as NSString
        txtDOB.text = dateString as String
    }
    
    //MARK:- UIPicker View Delegates
    //MARK:-
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        if pickerView == genderPicker{
            return arrayGender.count

        }else{
            
            return arrayState.count
        }
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderPicker{
        return arrayGender[row]
            
        }else{
            return arrayState[row]
 
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPicker{
            
            self.txtGender.text = arrayGender[row]
        }else{
           self.txtProfession.text = arrayState[row]
            
        }
        
    }
    
    //MARK:- Text Field Delegates
    //MARK:-
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let strLatest = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == txtPhoneNumber
        {
            return checkUSPhoneFormat(string: string, str: strLatest, textField: textField)
        }
        return true
        
    }
    

    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtGender
        {
            let selectedIndex = genderPicker.selectedRow(inComponent: 0)
            self.txtGender.text = arrayGender[selectedIndex]
            
        }else if textField == txtProfession{
            
            let selectedIndex = pickerProfession.selectedRow(inComponent: 0)
            self.txtState.text = arrayState[selectedIndex]
        }
        
    }
    
    //MARK:- Select Images
    //MARK:-
    
    
    
    @IBAction func actionBtnSelectLicense(_ sender: Any) {
        selectedImage = .PhotoID
        self.openActionSheet()
        
    }
    
  
    
    @IBAction func actionBtnSelectSelfie(_ sender: Any) {
        selectedImage = .Selfie
        self.openActionSheet()
        
    }
    
    @IBAction func actionBtnEIN(_ sender: Any) {
        selectedImage = .EIN
        self.openActionSheet()
        
        
    }
    
    
    @IBAction func actionBtnLegalDoc(_ sender: Any) {
        selectedImage = .Legal
        self.openActionSheet()
    }
   
    
    
    
    
    func openActionSheet()
    {
        
        MSShowActionSheet(arrayBtn: ["Camera","Gallery"], controller: self) { (cancelBool, index) in
            if cancelBool == false{
                if index == 0 {
                    self.openCameraWithPermissions()
                }else
                {
                    self.openGalleryWithPermissions()
                }
            }
        }

        
    }
    
    func openCameraWithPermissions()
    {
        
        let status : AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == AVAuthorizationStatus.authorized
        {
            self.openCamera()
        }else if  status == AVAuthorizationStatus.denied
        {
            //  showAlert(AppName, message: "Camera permissions are disabled.", controller: self)
        }else if  status == AVAuthorizationStatus.restricted
        {
            // showAlert(AppName, message: "Camera permissions are disabled.", controller: self)
        }else if  status == AVAuthorizationStatus.notDetermined{
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (Bool) in
                if Bool {
                    DispatchQueue.main.async {
                        self.openCamera()
                    }
                }            })
        }
        
        
    }
    
    
    func openCamera()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: {
        })
        
    }
    
    
    func openGalleryWithPermissions()
    {
        PHPhotoLibrary.requestAuthorization { (status) -> Void in
            switch status{
            case .authorized:
                DispatchQueue.main.async {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
                    {
                        let imagePicker = UIImagePickerController()
                        imagePicker.sourceType = .photoLibrary
                        imagePicker.allowsEditing = true
                        imagePicker.delegate = self
                        self.present(imagePicker, animated: true, completion: {
                        })
                    }
                }
                break
            case .denied:
                print("Denied")
                break
            default:
                print("Default")
                break
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            if self.selectedImage == .PhotoID{
               
                imageLeaseCopy = self.reduceSize(image: pickedImage)
                
              //  btnPhotoID.setImage(pickedImage, for: .normal)
                
                imgViewLeaseCopy.image = imageLeaseCopy
            }else if self.selectedImage == .Selfie{
                imageLicense = self.reduceSize(image: pickedImage)
                
                imgViewLicense.image = imageLicense
                
              //  btnSelfie.setImage(imageSelfie, for: .normal)
                
            }else if self.selectedImage == .EIN
            {
                imageEIN = self.reduceSize(image: pickedImage)
               // btnEIN.setImage(imageEIN, for: .normal)
            }else if self.selectedImage == .Legal
            {
                imageLegelDocument = self.reduceSize(image: pickedImage)
               // btnLegal.setImage(imageLegelDocument, for: .normal)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func reduceSize(image : UIImage) -> UIImage
    {
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        return UIImage(data: imageData!)!
    }

    
    //MARK:- Local Server API
    //MARK:-
    
    
    
    func getPreviousKYCInfo()
    {
       LoginManager.share().getKYCSavedOnServer { (isSuccess, response, error) in
        if isSuccess{
            
            if let dictResponse = response as? Dictionary<String, Any>
            {
               print(dictResponse)
                

                    self.txtName.text = dictResponse["first_name"] as? String
                    self.txtEmail.text = dictResponse["email"] as? String
                    self.txtPhoneNumber.text = dictResponse["phone"] as? String
                    self.txtDOB.text = dictResponse["dob"] as? String
                    self.selectedDate = (self.txtDOB.text)?.getDateFromString()
                
                self.settingDate()

                    self.txtAddress1.text =  dictResponse["address_1"] as? String
                
                    self.txtCity.text = dictResponse["city"] as? String
                    self.txtState.text = dictResponse["state"] as? String
                
                
                if let index = arrayState.index(of: self.txtState.text!)
                {
                    self.pickerProfession.selectRow( index, inComponent: 0, animated: false)
                    
                }
                
                
                    self.txtZipCode.text = dictResponse["zip"] as? String
                    self.txtSSN.text = dictResponse["social_security"] as? String
                    self.strDocID = dictResponse["kyc_id"] as? String
 

            }
          }
        }
    }
    
    
    
       
    //MARK:- LTLinkTextViewDelegate
    //MARK:-
    
    func linkTextView(_ termsTextView: LTLinkTextView!, didSelectButtonWith buttonIndex: UInt, title: String!) {
        
        if title == " Terms of Service"
        {
            let safariVC = SFSafariViewController(url: NSURL(string: URL_Terms)! as URL)
            // safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)

        }else if title == "Privacy Policy"
        {
            let safariVC = SFSafariViewController(url: NSURL(string: URL_Privacy)! as URL)
            self.present(safariVC, animated: true, completion: nil)
        }else if title == "Terms of Service"
        {
            let safariVC = SFSafariViewController(url: NSURL(string: URL_SynapseFiTOS)! as URL)
            self.present(safariVC, animated: true, completion: nil)
        }else if title == "Terms of Service"
        {
            let safariVC = SFSafariViewController(url: NSURL(string: URL_SynapseFiPP)! as URL)
            self.present(safariVC, animated: true, completion: nil)
        }
    }
        //openWebsiteInAppBrowser(controller: self)
        //  https://www.tenanttag.com
        
    
    
    
}
