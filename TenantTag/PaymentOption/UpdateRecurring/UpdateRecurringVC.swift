//
//  UpdateRecurringVC.swift
//  TenantTag
//
//  Created by maninder on 03/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class UpdateRecurringVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
 
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnUpdate: UIButton!
    var paymentObj = Payment()
    var payType : PayType = .CC

    var recurringType = "Monthly"
    var recurringDay = 0
    var recurringTimes = 0
    
    var delegate : MSUpdateController? = nil
    var pickerSelected : PickerSelected = .RecurringType
    let containerView = UIView()
    var pickerViewRecurring : UIPickerView?

    @IBOutlet var tblRecurringUpdate: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // print(paymentObj.recurringData.Day)
       // print(paymentObj.recurringData.Times)

        recurringDay = paymentObj.recurringData.Day
        recurringTimes = paymentObj.recurringData.Times
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(UpdateRecurringVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Rent payment" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        tblRecurringUpdate.registerNibsForCells(arryNib: ["RecurringFormCell"])
        tblRecurringUpdate.dataSource = self
        tblRecurringUpdate.delegate = self
        
        
        btnUpdate.cornerRadius(6)
        btnCancel.cornerRadius(6)
        btnUpdate.addBorderWithColorAndLineWidth(color: APP_ThemeColor, borderWidth: 1)
         btnCancel.addBorderWithColorAndLineWidth(color: APP_ThemeColor, borderWidth: 1)

        
        self.pickerViewViewInialization()
       // showPickerWithAnimation
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionBtnBackPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
            if indexPath.row == 0 {
                let cell: RecurringFormCell = tableView.dequeueReusableCell(withIdentifier: "RecurringFormCell") as! RecurringFormCell
                var strDay = arrayRecurringDay[recurringDay]
                
                
                if strDay == "Date"
                {
                    
                }else if strDay == "1"{
                    strDay =  strDay + "st"
                    
                }else if strDay == "2"
                {
                    strDay =   strDay + "nd"
                }else if strDay == "3"
                {
                    strDay =   strDay + "rd"
                }else{
                    strDay =   strDay + "th"
                }
                
                cell.btnDay.setTitle(strDay, for: .normal)
                
                
                
                if recurringTimes == 0{
                    cell.btnTimes.setTitle(arrayRecurringTimes[recurringTimes], for: .normal)
                }else{
                    cell.btnTimes.setTitle(arrayRecurringTimes[recurringTimes] + " " + "Times", for: .normal)
                }

                cell.callBackType = {(test : PickerSelected) in
                    if  test  ==  .RecurringDay
                    {
                        self.pickerSelected = .RecurringDay
                    }else{
                        self.pickerSelected = .RecurringTimes
                    }
                    self.showPickerWithAnimation()
                    
                }
                return cell
            }else {
                let cell: LastPaymentCell = tableView.dequeueReusableCell(withIdentifier: "LastPaymentCell") as! LastPaymentCell
                //cell.payment = previousPayment
             //   cell.setPaymentDetails(payment: previousPayment)
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 140
    }
    
    
    //MARK:- PickerView Delegates
    //MARK:-
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerSelected == .RecurringDay {
            return arrayRecurringDay[row];
        }else {
            return   arrayRecurringTimes[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerSelected == .RecurringDay {
            return arrayRecurringDay.count;
        }else {
            return arrayRecurringTimes.count;
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerSelected == .RecurringDay {
            recurringDay = row
        }else {
            recurringTimes = row
        }
        tblRecurringUpdate.reloadData()
    }
    
    func pickerViewViewInialization()
    {
        containerView.frame = CGRect(x: 0, y: ScreenHeight, width: ScreenWidth, height: 240)
        containerView.backgroundColor = UIColor.white
        self.view.addSubview(containerView)
        self.pickerViewRecurring = UIPickerView()
        pickerViewRecurring?.frame = CGRect(x: 0, y: 10, width: ScreenWidth, height: 200)
        self.pickerViewRecurring?.delegate = self
        self.pickerViewRecurring?.dataSource = self
        self.pickerViewRecurring?.backgroundColor = UIColor.white
        containerView.addSubview(pickerViewRecurring!)
        
        let toolBarForTextView = UIToolbar()
        toolBarForTextView.tintColor = UIColor.black
        toolBarForTextView.barStyle = .default;
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.hidePickerWithAnimation))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.hidePickerWithAnimation))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBarForTextView.setItems([ cancelButton ,spaceButton,doneButton], animated: true)
        toolBarForTextView.isUserInteractionEnabled = true
        toolBarForTextView.sizeToFit()
        containerView.addSubview(toolBarForTextView)
    }

    func showPickerWithAnimation()
    {
        self.view.endEditing(true)
        if pickerSelected == .RecurringDay {
            pickerViewRecurring?.reloadAllComponents()
            pickerViewRecurring?.selectRow(recurringDay, inComponent: 0, animated: false)
        }else{
            pickerViewRecurring?.reloadAllComponents()
            pickerViewRecurring?.selectRow(recurringTimes, inComponent: 0, animated: false)
        }
        UIView.animate(withDuration: 0.4) {
            self.containerView.frame = CGRect(x: 0, y: ScreenHeight-240, width: ScreenWidth, height: 240)
        }
    }
    
    
    func hidePickerWithAnimation()
    {
        UIView.animate(withDuration: 0.4) {
            self.containerView.frame = CGRect(x: 0, y: ScreenHeight, width: ScreenWidth, height: 240)
        }
        
    }


    @IBAction func actionBtnUpdateRecurring(_ sender: Any) {

        
        
        if recurringDay == 0 || recurringTimes == 0{
            HSShowAlertView("TenantTag", message: "Please change recurring details.", controller: self)
            return
        }
        
        paymentObj.recurringData.Day = NSInteger(arrayRecurringDay[recurringDay])!
        var strTimes = arrayRecurringTimes[recurringTimes].replacingOccurrences(of: " ", with: "")
        strTimes =  strTimes.removeParticularCharacterString(character: " ")
        paymentObj.recurringData.Times = NSInteger(strTimes)!
        
        
        print(paymentObj.recurringData.Times)
        print(paymentObj.recurringData.Day)

        
        showAlertWithMutipleButtons(title: "TenantTag", message: "Recurring details will be updated?", cancel: "No", destructive: nil, actions: ["Yes"], controller: self, callBack: { (cancel, destruction, index) in
            if index == 0{
                self.updateRecurringInfo()
               // self.deleteParticularCard(index: indexPath!.row)
            }
        })

        
        }
    
    @IBAction func actionBtnDelete(_ sender: Any) {
        
        
        showAlertWithMutipleButtons(title: "TenantTag", message: "Recurring will be removed for this account/card?", cancel: "No", destructive: nil, actions: ["Yes"], controller: self, callBack: { (cancel, destruction, index) in
            if index == 0{
                self.paymentObj.recurringEnabled = false
                self.updateRecurringInfo()
               // self.deleteParticularCard(index: indexPath!.row)
            }
        })
    }
    
    
    func updateRecurringInfo(){
        
        if  payType == .CC
        {
            paymentObj.updatingRecurringInfoForCards(paymentType: "Stripe",  { (isSuccess, response, error) in
                if isSuccess {
                    if self.delegate != nil{
                        self.delegate?.updateRecords!()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
        }else{
            paymentObj.updatingRecurringInfoForAccounts(paymentType:  "Synapse"  , { (isSuccess, response, error) in
                if isSuccess {
                    if self.delegate != nil{
                        self.delegate?.updateRecords!()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
