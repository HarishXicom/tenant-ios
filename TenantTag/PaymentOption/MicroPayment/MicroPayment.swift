//
//  MicroPayment.swift
//  TenantTag
//
//  Created by maninder on 5/10/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class MicroPayment: UIViewController,UITextFieldDelegate{
    
    
    var paymentNew = Payment()
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtSecondAmount: UITextField!
    @IBOutlet var txtFirstAmount: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(MicroPayment.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Confirm Micro payments" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        btnSubmit.cornerRadius(4)
        txtFirstAmount.delegate = self
        txtSecondAmount.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func actionBtnBackPressed()
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func actionBtnSubmitPressed(_ sender: Any) {
        
        self.view.endEditing(true)
        if (txtFirstAmount.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter username!", controller: self)
            return
        }else if (txtSecondAmount.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter account no!", controller: self)
            return
        }else{
            
            let firstValue = Double(txtFirstAmount.text!)
            let secondValue = Double(txtSecondAmount.text!)

            
         
            LoginManager.share().confirmMicroPayment(amountFirst: firstValue! , amountSecond: secondValue! , node : paymentNew.selectedNode, callBack: { (isSuccess, response, error) in
                if isSuccess{
                    
                    self.updateMicroDepositStatus()
                    let landlordVC = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    landlordVC.objFinalPayment = self.paymentNew // Synapse_Pay_userID required
                    landlordVC.payType = .Bank
                    self.navigationController?.pushViewController(landlordVC, animated: true)
                    
                }
            })
       }

        
        //When a new account is added, 2 payments are mad into your account to confirm activation. You will be prompted to confirm these amounts before you begin paying your rent.
    }
    
    
    
    @IBAction func actionBtnNotice(_ sender: Any) {
        
        
        HSShowAlertView( APP_Name as NSString, message: "When a new account is added, 2 payments are mad into your account to confirm activation. You will be prompted to confirm these amounts before you begin paying your rent.", controller: self)
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Text Field delegates
    //MARK:-
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
     func updateMicroDepositStatus()
     {
        
        LoginManager.share().updateNode(node: paymentNew.selectedNode ) { (isSuccess, response, error) in
            
            
            
        }
        
        
        
    }
    

}
