//
//  TitleTableCell.swift
//  TenantTag
//
//  Created by maninder on 15/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class TitleTableCell: UITableViewCell {
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    var callBackType:((PaymentType)->Void)? = nil

    @IBOutlet var btnBankAccount: UIButton!
    @IBOutlet var btnDebitCredit: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(5)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnBankAccount(_ sender: Any) {
        btnBankAccount.isSelected = true
        if callBackType != nil{
            self.callBackType!(.BankAccount)
        }
        
    }
    
    @IBAction func actionBtnDebitCreditPressed(_ sender: Any) {
        
        btnBankAccount.isSelected = false
        btnDebitCredit.isSelected = true
        
        if callBackType != nil{
            self.callBackType!(.DebitCard)
        }

    }
    
}
