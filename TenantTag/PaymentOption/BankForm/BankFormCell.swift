//
//  BankFormCell.swift
//  TenantTag
//
//  Created by maninder on 16/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class BankFormCell: UITableViewCell {
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(5)
        

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
