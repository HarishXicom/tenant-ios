//
//  BankListingVC.swift
//  TenantTag
//
//  Created by maninder on 30/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class BankListingVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    
    var arrayBankListing = [Bank]()
    @IBOutlet var tblBankListing: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(BankListingVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        
        
        self.navigationItem.leftBarButtonItems = [barButton];
        
        self.navTitle("Select Bank" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)

        HSProgress(.show)
       // LoginManager.share().getSynapsePayRefershToken { (isSuccess, result, error) in
            Bank.getBankList { (success, results, error) in
                HSProgress(.hide)
                if success == true{
                    self.tblBankListing.isHidden = false

                    if let res = results as? [Bank]
                    {
                        self.arrayBankListing.append(contentsOf: res)
                        self.tblBankListing.reloadData()
                    }
                }
          //  }
            
        }

      
        tblBankListing.registerNibsForCells(arryNib: ["BankNameCell","AddNewBankCell"])
        tblBankListing.dataSource = self
        tblBankListing.delegate = self
        tblBankListing.estimatedRowHeight = 100
        tblBankListing.rowHeight = UITableViewAutomaticDimension;
        tblBankListing.allowsSelection = true
        tblBankListing.backgroundColor = UIColor.groupTableViewBackground
        tblBankListing.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionBtnBackPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TableView Delegates
    //MARK:-
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section  == 0 ? 1 : arrayBankListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if indexPath.section == 0 {
           let cell: AddNewBankCell = tableView.dequeueReusableCell(withIdentifier: "AddNewBankCell") as! AddNewBankCell
           // cell.setDummyAddBank()
            return cell

        }else{
            let cell: BankNameCell = tableView.dequeueReusableCell(withIdentifier: "BankNameCell") as! BankNameCell
            cell.bankObj = arrayBankListing[indexPath.row ]
            cell.setBankDetails()
            return cell
        }
        
   }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            
            return 75
        }
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let addBankVC: AddAnotherBankVC = AddAnotherBankVC.init(nibName: "AddAnotherBankVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(addBankVC, animated: true)
            
        }else{
            let landlordVC: BankLoginVC = BankLoginVC.init(nibName: "BankLoginVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            landlordVC.bankSelected = arrayBankListing[indexPath.row ]
            self.navigationController?.pushViewController(landlordVC, animated: true)
        }
//        LoginManager.share().getBankNodes(userName: "", password: "", userID: UserDefaults.standard.object(forKey: synapsePayUserID) as! String, callBack: (Bool, AnyObject?, NSError?) -> ())
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
