//
//  AccountInfoCell.swift
//  TenantTag
//
//  Created by maninder on 03/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class AccountInfoCell: UITableViewCell {

    
    @IBOutlet var btnSelected: UIImageView!
    var bankNode : BankNode?
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var lblFourth: UILabel!
    @IBOutlet var lblThird: UILabel!
    @IBOutlet var lblSecond: UILabel!
    @IBOutlet var lblFirst: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(5)
        // Initialization code
        viewInner.cornerRadius(4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func settingValue()
    {
        if bankNode != nil {
            lblFirst.text = bankNode?.bankFullName
            lblSecond.text = bankNode?.accountClass
            lblThird.text = bankNode?.accountNo
            lblFourth.text = "$  " + (bankNode?.amount)!
        }
        
    }
    
}
