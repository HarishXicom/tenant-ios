//
//  AccountsListingVC.swift
//  TenantTag
//
//  Created by maninder on 03/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class AccountsListingVC : UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var paymentObj = Payment()
    var checkMicroPay = false
    
    
    var selectedIndex = -1
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var tblRentForm: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // RentPaymentFormVC
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(AccountsListingVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItems = [barButton];
        
        
        self.navTitle("Select Account" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        tblRentForm.registerNibsForCells(arryNib: ["AccountInfoCell"])
        tblRentForm.dataSource = self
        tblRentForm.delegate = self
        tblRentForm.allowsSelection = true
        btnSubmit.cornerRadius(5)
    }
    
    
    func actionBtnBackPressed()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- TableView delegates
    //MARK:-
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentObj.bankNodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AccountInfoCell = tableView.dequeueReusableCell(withIdentifier: "AccountInfoCell") as! AccountInfoCell
        cell.bankNode = paymentObj.bankNodes[indexPath.row]
        cell.settingValue()
        
        if selectedIndex == indexPath.row{
            cell.btnSelected.image = UIImage(named: "BoxSelected")
        }else{
            cell.btnSelected.image = UIImage(named: "BoxDeselect")
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 165
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndex != indexPath.row
        {
            selectedIndex = indexPath.row
        }
        
        self.tblRentForm.reloadData()
        
    }
    
    
    @IBAction func actionBtnSubmit(_ sender: Any) {
        
        if selectedIndex == -1
        {
            HSShowAlertView("TenantTag", message: "Please select account first!", controller: self)
            return
        }else{
            self.navigationController?.popToRootViewController(animated: true)
            
            
//            if checkMicroPay == true{
//                
//                let selectedNode = paymentObj.bankNodes[selectedIndex]
//                if selectedNode.microPaymentStatus == .MicroDone{
//                    // verfied micro payments
//                    //
//                    let landlordVC = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
//                    self.navigationController?.isNavigationBarHidden = false
//                    landlordVC.objFinalPayment = self.paymentObj  // Synapse_Pay_userID required
//                    landlordVC.payType = .Bank
//                    landlordVC.objFinalPayment.selectedNode =  paymentObj.bankNodes[selectedIndex]
//                    self.navigationController?.pushViewController(landlordVC, animated: true)
//                    //
//                }else{
//                     // verfication is required
//                    let rentObj = MicroPayment.init(nibName: "MicroPayment", bundle: nil)
//                    rentObj.paymentNew = paymentObj // Synapse_Pay_userID required
//                    rentObj.paymentNew.selectedNode =  selectedNode
//                    self.navigationController?.pushViewController(rentObj, animated: true)
//                    
//                }
//                
//                
//                
//            }else{
//                // No need for Mico payment verfied, we are coming through bank logins'
//                
//                let landlordVC = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
//                self.navigationController?.isNavigationBarHidden = false
//                landlordVC.objFinalPayment = paymentObj  // Synapse_Pay_userID required
//                landlordVC.payType = .Bank
//                landlordVC.objFinalPayment.selectedNode =  paymentObj.bankNodes[selectedIndex]
//                self.navigationController?.pushViewController(landlordVC, animated: true)
//            }
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
