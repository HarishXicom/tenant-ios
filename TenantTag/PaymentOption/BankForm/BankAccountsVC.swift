//
//  BankAccountsVC.swift
//  TenantTag
//
//  Created by maninder on 02/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit


@objc protocol MSUpdateController {
    @objc optional func updateRecords( )
    @objc optional func replaceRecors(obj : AnyObject )

}

class BankAccountsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MSUpdateController {
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var imgViewDummy: UIImageView!
     var changePrevious = false
    @IBOutlet var btnCommonQuestions: UIButton!
    @IBOutlet var tblListingAccounts: UITableView!
    @IBOutlet var btnLinkBankAccount: UIButton!
      var selectedIndex = -1
    
    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    
    var arrayBanks = [Payment]()
    var arrayOtherBanks = [Payment]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(BankAccountsVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        btnNext.addTarget(self, action: #selector(BankAccountsVC.nextBtnPressed), for: .touchUpInside)
        self.navigationItem.leftBarButtonItems = [barButton];
        
        self.navTitle("Bank Accounts" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        tblListingAccounts.registerNibsForCells(arryNib: ["AccountPaymentCell" , "OtherNodeCell"])
        
               tblListingAccounts.register(UINib(nibName: "BankAccountType", bundle:nil), forHeaderFooterViewReuseIdentifier: "BankAccountType")
        tblListingAccounts.dataSource = self
        tblListingAccounts.delegate = self
        tblListingAccounts.allowsSelection = true
        
        btnLinkBankAccount.isUserInteractionEnabled = true
        btnLinkBankAccount.cornerRadius(5)
        btnNext.cornerRadius(5)

        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGesture:)))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        tblListingAccounts.addGestureRecognizer(longPressGesture)
        btnCommonQuestions.underLineButton(text: "Common Questions", color: APP_ThemeColor)
        
        
        if changePrevious == false{
        HSProgress(.show)
        LoginManager.share().getSynapsePayRefershToken { (isSuccess, result, error) in
            HSProgress(.hide)
           
            DispatchQueue.main.async(execute: { () -> Void in
                if let status = defaults.value(forKey: synapsePayDocStatus) as? String{
                    if status == DocStatus.UnderReview.rawValue
                    {
                        //Please wait for review proccess to end
                        HSShowAlertView(APP_Name as NSString, message: "Please wait for KYC reviewing process to finish.", controller: self)
                        self.navigationController?.popViewController(animated: true)
                        
                    }else if status == DocStatus.Missed.rawValue || status == DocStatus.Invalid.rawValue || status == DocStatus.ReSubmissionNeeded.rawValue
                    {
                        self.showOptionAlert()
                        //GO to add another doc screen
                    }
                }
                
            })
           }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getSavedBankNode()
        if changePrevious == true
        {
            self.btnLinkBankAccount.isHidden = true
            heightConstraint.constant = 0
            
        }else{
            self.btnLinkBankAccount.isHidden = false
            heightConstraint.constant = 45
        }

    }
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        selectedIndex = -1
        tblListingAccounts.reloadData()
        let point = longPressGesture.location(in: tblListingAccounts)
        let indexPath = tblListingAccounts.indexPathForRow(at: point)
        
        
        if indexPath == nil {
            print("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began)
        {
            print("Long press on row, at \(indexPath!.row)")
            
            
            if indexPath?.section == 1 {
                
                showAlertWithMutipleButtons(title: "TenantTag", message: "Are you sure you want to delete account?", cancel: "No", destructive: nil, actions: ["Yes"], controller: self, callBack: { (cancel, destruction, index) in
                    if index == 0{
                        self.deleteParticularAccount(index: indexPath!.row)
                    }
                })
                
            }else{
                
                showAlertWithMutipleButtons(title: "TenantTag", message: "Are you sure you want to delete account?", cancel: "No", destructive: nil, actions: ["Yes"], controller: self, callBack: { (cancel, destruction, index) in
                    if index == 0{
                        self.deleteParticularOtherAccount(index: indexPath!.row)
                    }
                })
            }
        }
        
    }
    
    
    

    
    
    
    
    
    
    
    
    
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
        
        let paymentInfo: CardDetailsVC = CardDetailsVC.init(nibName: "CardDetailsVC", bundle: nil)
        // self.navigationController?.isNavigationBarHidden = false
        paymentInfo.popUpType = .Bank
        paymentInfo.paymentInfo = arrayBanks[indexPath.row]
        paymentInfo.view.alpha = 0
        paymentInfo.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        paymentInfo.modalPresentationStyle = .overCurrentContext
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.present(paymentInfo, animated: false, completion: nil)
        })
        }else{
            
            
            self.checkStatusForOtherAccount(payment: arrayOtherBanks[indexPath.row])
                  }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return arrayOtherBanks.count

        }else{
            
            return arrayBanks.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell: OtherNodeCell = tableView.dequeueReusableCell(withIdentifier: "OtherNodeCell") as! OtherNodeCell
            cell.paymentObj = arrayOtherBanks[indexPath.row]
            cell.bankNode = arrayOtherBanks[indexPath.row].selectedNode
            cell.settingValue()
            cell.callBackMDStatus = { (myPayment : Payment) in
                
                self.checkStatusForOtherAccount(payment: myPayment)
            }
            
            return cell
            
        }else{
            
            
            
        
        let cell: AccountPaymentCell = tableView.dequeueReusableCell(withIdentifier: "AccountPaymentCell") as! AccountPaymentCell
        
        cell.payment = arrayBanks[indexPath.row]
        cell.assignData(cellIndex: indexPath.row, selectedIndex: selectedIndex)
        cell.callBack = {(test : Payment) in
            self.openRecurringEditingVC(payment: test)
        }
        
        cell.callBackSelection = {(test : Payment) in
            let indexOfObject = self.arrayBanks.index(of: test)
            if self.selectedIndex == indexOfObject{
                self.selectedIndex = -1
            }else{
                self.selectedIndex = indexOfObject!
            }
            
            self.tblListingAccounts.reloadData()
           // self.openRecurringEditingVC(payment: test)
        }
            
            
        return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
             return 190
        }else{
            
            
            return 82
        }
       
    }
    
        func tableView(_ tableView: UITableView,
                       viewForHeaderInSection section: Int) -> UIView?
        {
            let headerView: BankAccountType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BankAccountType") as! BankAccountType
            
            headerView.backgroundColor = APP_ThemeColor
            if section == 0 {
                headerView.lblTitle.text  = "Other Accounts"
                
            }else{
                headerView.lblTitle.text  = "Saved Accounts"

            }

            
          return headerView
        }
    
         func tableView(_ tableView: UITableView,
                                heightForHeaderInSection section: Int) -> CGFloat {
            
            
            if section == 0 {
                if arrayOtherBanks.count == 0 {
                    
                    return 0
                }
                
            }else{
                if arrayBanks.count == 0 {
                    return 0
                }
            }
            return 40
        }
    
    
    
    func nextBtnPressed()
    {
        
        
        if let status = defaults.value(forKey: synapsePayDocStatus) as? String{
            if status == DocStatus.UnderReview.rawValue
            {
                //Please wait for review proccess to end
                HSShowAlertView(APP_Name as NSString, message: "Please wait for KYC process to finish.", controller: self)
                return
            }
        }

        
        
        if selectedIndex != -1
        {
            if changePrevious == true{
                let payment = arrayBanks[selectedIndex]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EditNode"), object: payment, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
                return
            }
          
            let rentObj = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            rentObj.payType = .Bank
            rentObj.objFinalPayment = arrayBanks[selectedIndex]
            if arrayBanks[selectedIndex].recurringEnabled == true{
                rentObj.noRecurring = true
            }else{
                rentObj.noRecurring = false
            }
            self.navigationController?.pushViewController(rentObj, animated: true)
        }
    }

    
    
    
    func openRecurringEditingVC(payment : Payment)
    {
        let landlordVC: UpdateRecurringVC = UpdateRecurringVC.init(nibName: "UpdateRecurringVC", bundle: nil)
        self.navigationController?.isNavigationBarHidden = false
        landlordVC.payType = .Bank
        landlordVC.delegate = self
        self.navigationController?.pushViewController(landlordVC, animated: true)
        
    }
  
    
    @IBAction func actionBtnLinkAccount(_ sender: Any) {
        
        
        if let status = defaults.value(forKey: synapsePayDocStatus) as? String{
       
            if status == DocStatus.UnderReview.rawValue
            {
                //Please wait for review proccess to end
                HSShowAlertView(APP_Name as NSString, message: "Please wait for KYC process to finish.", controller: self)
                return
            }
        }
        
        let landlordVC: BankListingVC = BankListingVC.init(nibName: "BankListingVC", bundle: nil)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(landlordVC, animated: true)
    }

    @IBAction func actionCommonQuestions(_ sender: Any) {
        
        
    }
    func actionBtnBackPressed()
    {

            self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API Functions
    //MARK:-

    
//    func getSavedBankNode()
//    {
//        HSProgress(.show)
//        Payment.getSavedBankNodes { (isSuccess, responseData, error) in
//            HSProgress(.hide)
//            if isSuccess{
//                if let arrayCard = responseData as? [Payment]
//                {
//                   self.selectedIndex = -1
//                    self.arrayBanks.removeAll()
//                    self.arrayBanks.append(contentsOf: arrayCard)
//                   // self.btnNext.isHidden = true
//                    self.btnLinkBankAccount.isHidden = false
//
//                    if self.arrayBanks.count > 0 {
//                         self.selectedIndex = 0
//                        self.imgViewDummy.isHidden = true
//                        self.btnNext.isHidden = false
//
//                    }else if self.arrayBanks.count == 2{
//                              self.selectedIndex = 0
//                         self.imgViewDummy.isHidden = true
//                        self.btnNext.isHidden = false
//                        self.btnLinkBankAccount.isHidden = true
//                    }
//                    
//                    else {
//                        self.selectedIndex = -1
//                        self.imgViewDummy.isHidden = false
//                        self.btnNext.isHidden = true
//                    }
//                    self.tblListingAccounts.reloadData()
//                    
//                }
//            }else{
//                
//                
//            }
//        }
//    }
    
    
    
    
    
    
    
    func getSavedBankNode()
    {
        HSProgress(.show)
        Payment.getSavedBankNodes { (isSuccess, responseData, error) in
            HSProgress(.hide)
            if isSuccess{
                
                
                
               if let dictInfo = responseData as? [String : Any]
              {
                
                
                let arraySaved = dictInfo["Saved"] as! [Payment]
                let arrayOthers = dictInfo["Other"] as! [Payment]
                self.selectedIndex = -1

                 self.arrayBanks.removeAll()
                 self.arrayOtherBanks.removeAll()
                self.arrayBanks.append(contentsOf: arraySaved)
               self.arrayOtherBanks.append(contentsOf: arrayOthers)

                
                self.updateLocalUI()
                
              self.tblListingAccounts.reloadData()
                
                }
            }
        }
    }

    
    func deleteParticularAccount(index : NSInteger)
    {
        let paymentWithCard = arrayBanks[index]
        HSProgress(.show)
        paymentWithCard.deleteSavedBankAccount { (isSuccess, responseData, error) in
            HSProgress(.hide)
            if isSuccess{
                let indexArry = self.arrayBanks.index(of: paymentWithCard)
                self.arrayBanks.remove(at: indexArry!)
                self.btnLinkBankAccount.isHidden = false

                
                
                self.updateLocalUI()
//                if self.arrayBanks.count > 0 {
//                    self.imgViewDummy.isHidden = false
//                    self.selectedIndex = 0
//                    self.btnNext.isHidden = false
//                }else if  self.arrayOtherBanks.count == 0
//                {
//                    self.btnNext.isHidden = true
//                   self.selectedIndex = -1
//                    self.imgViewDummy.isHidden = true
//                }else{
//                    self.imgViewDummy.isHidden = false
//                    self.selectedIndex = 0
//                    self.btnNext.isHidden = false
//                }
                  self.tblListingAccounts.reloadData()
            }
        }
    }
    
    func deleteParticularOtherAccount(index : NSInteger)
    {
        let paymentOther = arrayOtherBanks[index]
        HSProgress(.show)
        
        LoginManager.share().deleteNode(node: paymentOther.selectedNode) { (isSuccess, response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                let indexArry = self.arrayOtherBanks.index(of: paymentOther)
                self.arrayOtherBanks.remove(at: indexArry!)
                self.tblListingAccounts.reloadData()
                self.updateLocalUI()
                
            }

        
        
//        paymentWithCard.deleteSavedBankAccount { (isSuccess, responseData, error) in
//            HSProgress(.hide)
//            if isSuccess{
//                let indexArry = self.arrayBanks.index(of: paymentWithCard)
//                self.arrayBanks.remove(at: indexArry!)
//                self.btnLinkBankAccount.isHidden = false
//                
//                if self.arrayBanks.count > 0 {
//                    self.imgViewDummy.isHidden = false
//                    self.selectedIndex = 0
//                    self.btnNext.isHidden = false
//                }else {
//                    self.btnNext.isHidden = true
//                    self.selectedIndex = -1
//                    self.imgViewDummy.isHidden = true
//                }
//                self.tblListingAccounts.reloadData()
//            }
        }
    }

    
    
    
    func updateLocalUI(){
        
        if self.arrayBanks.count > 0  {
            self.selectedIndex = 0
            self.imgViewDummy.isHidden = true
            self.btnNext.isHidden = false
            
        }else if self.arrayOtherBanks.count == 0  {
            self.selectedIndex = -1
            self.imgViewDummy.isHidden = false
            self.btnNext.isHidden = true
            
        }else if self.arrayOtherBanks.count > 0 {
            self.selectedIndex = -1
            self.imgViewDummy.isHidden = true
            self.btnNext.isHidden = true
        }
    }
    
    
    func checkStatusForOtherAccount(payment : Payment)
    {
        let node =  payment.selectedNode
        if node.microPaymentStatus ==  .MicroDone{
            // verfied micro payments
            let landlordVC = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            landlordVC.objFinalPayment = payment // Synapse_Pay_userID required
            landlordVC.payType = .Bank
            landlordVC.objFinalPayment.selectedNode =  node
            self.navigationController?.pushViewController(landlordVC, animated: true)
            //
        }else if node.microPaymentStatus ==  .MicroNotDone
        {
            // verfication is required
            let rentObj = MicroPayment.init(nibName: "MicroPayment", bundle: nil)
            rentObj.paymentNew = payment // Synapse_Pay_userID required
            rentObj.paymentNew.selectedNode =  node
            self.navigationController?.pushViewController(rentObj, animated: true)
            
        }else if node.microPaymentStatus ==  .MicroLocked
        {
            HSShowAlertView("TenantTag", message: "Your account is locked.", controller: self)
        }
    }

  
    //MARK:- Custom Delegates
    //MARK:-
    
    func updateRecords() {
     
        selectedIndex = -1
        self.getSavedBankNode()
    }
    
    
    func showOptionAlert(){
        
        
        
        
        let alert:UIAlertController =  UIAlertController.init(title: APP_Name , message: "Add required information to link bank account.", preferredStyle: UIAlertControllerStyle.alert)
        let actionCancel:UIAlertAction  = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
            
            
            
            self.navigationController?.popViewController(animated: true)
        })
        
        let action:UIAlertAction  = UIAlertAction.init(title: "Add Information", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
            let landlordVC: DocumentFormVC   = DocumentFormVC.init(nibName: "DocumentFormVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(landlordVC, animated: true)
            
        })
        // Fallback on earlier versions
        alert.addAction(actionCancel)
        alert.addAction(action)
        self.present(alert, animated: true, completion: { () -> Void in
            
        })
        

    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
