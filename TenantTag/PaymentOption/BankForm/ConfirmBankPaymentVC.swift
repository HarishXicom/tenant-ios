 //
//  ConfirmBankPaymentVC.swift
//  TenantTag
//
//  Created by maninder on 04/01/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit


class ConfirmBankPaymentVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var lblConfirmation: UILabel!

    @IBOutlet var tblPaymentDetails: UITableView!
    @IBOutlet var btnPay: UIButton!
    let btn: UIButton = UIButton(type: .custom)
    var payType : PayType = .CC
    var finalPayment = Payment()
    @IBOutlet var lblConfirmPayment: UILabel!
    @IBOutlet var viewPopUpOuter: UIView!
    @IBOutlet var viewPopUpInner: UIView!
    @IBOutlet var lblPopUpAmount: UILabel!
  //  var selectedBankNode = BankNode()
    @IBOutlet var txtTransactionID: UILabel!
    
  //  var cardSelected = CreditCard()

    @IBOutlet var btnSupport: UIButton!
    var amountPaid = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(ConfirmBankPaymentVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Confirm Payment" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        btnPay.cornerRadius(5)
        viewPopUpInner.cornerRadius(5)
        lblConfirmPayment.text = "$ " + amountPaid
        lblPopUpAmount.text = "$ " + amountPaid + " Paid"
        
        viewPopUpInner.isHidden = true
        viewPopUpOuter.isHidden = true
        
        
        
        tblPaymentDetails.registerNibsForCells(arryNib: ["AmountDisplayCell","RecurringDetailsCell"])
        tblPaymentDetails.dataSource = self
        tblPaymentDetails.delegate = self
        tblPaymentDetails.allowsSelection = true
        
        btnSupport.underLineButton(text: "Support", color: UIColor.black)
        

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if finalPayment.recurringEnabled == true{
            return 1
        }else{
            return 0
        }
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            let cell: AmountDisplayCell = tableView.dequeueReusableCell(withIdentifier: "AmountDisplayCell") as! AmountDisplayCell
            return cell
        }else{
            let cell: RecurringDetailsCell = tableView.dequeueReusableCell(withIdentifier: "RecurringDetailsCell") as! RecurringDetailsCell
            cell.setRecurringData(recurringDet: finalPayment.recurringData)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1
        {
            return 115
        }else
        {
            return 110
        }
    }

    
    func actionBtnBackPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnPayPressed(_ sender: Any) {
        
        if payType == .CC
        {
            HSProgress(.show)
            finalPayment.card.makePaymentWithCard(amount: amountPaid, payment: finalPayment, callBack: { (isSuccess, response, error) in
                HSProgress(.hide)
                if isSuccess{
                    
                    if let strTransactionID = response as? String{
                        self.finalPayment.paymentID = strTransactionID
                        self.btn.isEnabled = false
                        self.showViewWithAnimation()
                    }
                }else{
                    self.btn.isEnabled = true
                }
            })
        }else{
            
            
        LoginManager.share().makePaymentWithNode(paymentInfo: finalPayment, amount: amountPaid , currency: "USD") { (isSuccess, response, error) in
                    if isSuccess{
                        
                        self.btn.isEnabled = false
                  let payment = response as! Payment
                        self.finalPayment.amount = self.amountPaid
                        self.finalPayment.paymentID = payment.paymentID
                        LoginManager.share().saveACHPaymentOnServer(paymentObj: self.finalPayment, status: "PAID")
                        self.showViewWithAnimation()
                    }else{
                        self.btn.isEnabled = true
               }
           }

        }
    }
    
    func showViewWithAnimation()
    {
        let finalDate =  NSDate().returnDay() + " " + NSDate().returnMonthName()
        lblConfirmation.text = "Confirmation received on " + finalDate
        txtTransactionID.text = "Trans id : " + self.finalPayment.paymentID
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            self.viewPopUpOuter.isHidden = false
            self.viewPopUpInner.isHidden = false
        }, completion: nil)
    }
    
    @IBAction func actionBtnCancelPayment(_ sender: Any) {
        
        self.viewPopUpInner.isHidden = true
        self.viewPopUpOuter.isHidden = true
        if payType == .CC
        {
            finalPayment.card.cancelPaymentWithCard(payment: finalPayment, callBack: { (isSuccess, response, error) in
                if isSuccess{
                    self.navigationController?.popToRootViewController(animated: true)
                    HSShowAlertView(APP_Name as NSString, message: "The payment has been cancelled.")
                }else{
                    self.btn.isEnabled = true
                    print("Payment failed")
                }
            })
        }else{
            
            LoginManager.share().cancelSynpasePayPayment(payment: self.finalPayment, { (isSuccess, response, error) in
                if isSuccess{
                    LoginManager.share().saveACHPaymentOnServer(paymentObj: self.finalPayment, status: "CANCELLED")
                    self.navigationController?.popToRootViewController(animated: true)
                    HSShowAlertView(APP_Name as NSString, message: "The payment has been cancelled.")


                }else{
                    self.btn.isEnabled = true
                    print("Payment failed")
                    
                }
            })
        }
    }
    
        
    @IBAction func actionBtnSuccessfulPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    
    
    @IBAction func actionBtnSupport(_ sender: Any) {
        let supportVC: SupportVC = SupportVC.init(nibName: "SupportVC", bundle: nil)
        let navControlelr: UINavigationController = UINavigationController(rootViewController: supportVC)
        self.present(navControlelr, animated: true) { () -> Void in
        }
    }
    
    
    
}
