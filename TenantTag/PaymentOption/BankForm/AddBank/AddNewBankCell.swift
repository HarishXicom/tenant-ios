//
//  AddNewBankCell.swift
//  TenantTag
//
//  Created by maninder on 21/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class AddNewBankCell: UITableViewCell {

    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var lblBankName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
