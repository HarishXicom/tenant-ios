//
//  OtherNodeCell.swift
//  TenantTag
//
//  Created by maninder on 10/5/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class OtherNodeCell: UITableViewCell {

    var callBackMDStatus : ((Payment) -> Void)? = nil
    
    @IBOutlet var btnStatusMD: UIButton!
    
    var paymentObj : Payment!
    var bankNode : BankNode?
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!
    @IBOutlet var lblFourth: UILabel!
    @IBOutlet var lblThird: UILabel!
    @IBOutlet var lblSecond: UILabel!
    @IBOutlet var lblFirst: UILabel!
    
    @IBOutlet var lblPermissions: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(5)
        // Initialization code
        viewInner.cornerRadius(4)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func settingValue()
    {
        if bankNode != nil {
            
            lblFirst.text = bankNode?.bankFullName
          //  lblSecond.text = bankNode?.accountClass
            lblThird.text = bankNode?.accountNo
            lblFourth.text = "$ " + (bankNode?.amount)!
            let status = bankNode?.microPaymentStatus.rawValue
            lblPermissions.text = status
            
            
            if bankNode?.microPaymentStatus == .MicroDone
            {
                btnStatusMD.setTitle("Micro Payment Confirmed", for: .normal)
                btnStatusMD.isUserInteractionEnabled = false
            }else if bankNode?.microPaymentStatus == .MicroNotDone{
                   btnStatusMD.setTitle("Confirm Micro Payment", for: .normal)
                btnStatusMD.isUserInteractionEnabled = true

            }else {
                btnStatusMD.setTitle("Account Locked", for: .normal)
                btnStatusMD.isUserInteractionEnabled = false
            }
            
        }
    }
    
    @IBAction func actionBtnMDPressed(_ sender: UIButton) {
        
        
        if callBackMDStatus != nil
        {
            self.callBackMDStatus!(paymentObj!)
        }
        
    }

    
}
