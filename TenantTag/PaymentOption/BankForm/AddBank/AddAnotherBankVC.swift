//
//  AddAnotherBankVC.swift
//  TenantTag
//
//  Created by maninder on 02/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class AddAnotherBankVC: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var newPayment = Payment()
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtAccountNo: UITextField!
    @IBOutlet var txtRoutingNo: UITextField!
    @IBOutlet var viewInner: UIView!
    
    @IBOutlet var tblListingSaved: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(AddAnotherBankVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
              self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Add Bank" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        btnSubmit.cornerRadius(4)
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(4)
        tblListingSaved.registerNibsForCells(arryNib: ["SavedNodeCell"])
        tblListingSaved.delegate = self
       
        tblListingSaved.dataSource = self
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGesture:)))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        tblListingSaved.addGestureRecognizer(longPressGesture)
        
       // self.getSavedMicroNodes()
       // txtAccountNo.text = "123456789"
       // txtRoutingNo.text = "064000020"


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func actionBtnBackPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnSubmit(_ sender: Any) {
        
        self.view.endEditing(true)
        if (txtName.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter username!", controller: self)
            return
        }else if (txtAccountNo.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter account no!", controller: self)
            return
        }else if (txtRoutingNo.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter routing no!", controller: self)
            return
        }else{
            
            LoginManager.share().addNewBank(nickName: txtName.text!, accNo: txtAccountNo.text!, routingNo: txtRoutingNo.text!, callBack: { (isSuccess, response, error) in
                if isSuccess{
                    if let payment = response as? Payment{
                        self.saveNodesForMicroNodes(bankNode: payment.bankNodes[0])
                        self.moveToAccounts(payment: payment)
                    }
                }
             })
        }
    }
    //MARK:- TableView Delegates
    //MARK:-
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newPayment.bankNodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SavedNodeCell = tableView.dequeueReusableCell(withIdentifier: "SavedNodeCell") as! SavedNodeCell
        cell.bankNode = newPayment.bankNodes[indexPath.row]
        cell.settingValue()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedNode = newPayment.bankNodes[indexPath.row]
        
        if selectedNode.microPaymentStatus ==  .MicroDone{
            // verfied micro payments
            let landlordVC = RentPaymentFormVC.init(nibName: "RentPaymentFormVC", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            landlordVC.objFinalPayment = newPayment  // Synapse_Pay_userID required
            landlordVC.payType = .Bank
            landlordVC.objFinalPayment.selectedNode =  selectedNode
            self.navigationController?.pushViewController(landlordVC, animated: true)
            //
        }else if selectedNode.microPaymentStatus ==  .MicroNotDone
        {
            // verfication is required
            let rentObj = MicroPayment.init(nibName: "MicroPayment", bundle: nil)
            rentObj.paymentNew = newPayment // Synapse_Pay_userID required
            rentObj.paymentNew.selectedNode =  selectedNode
            self.navigationController?.pushViewController(rentObj, animated: true)
            
        }else if selectedNode.microPaymentStatus ==  .MicroLocked
        {
            HSShowAlertView("TenantTag", message: "Your Account is locked.", controller: self)
            
        }
//            case MicroLocked = "LOCKED"
//        case MicroDone = "CREDIT-AND-DEBIT"
//        case MicroNotDone = "CREDIT"
        
    }
    
    func getSavedMicroNodes()
    {
        LoginManager.share().getMicroDepositNodes { (isSuccess, response, error) in
            if isSuccess
            {
                
             if let nodes = response as? [BankNode]
              {
                 self.newPayment.bankNodes = nodes
                self.tblListingSaved.reloadData()
             }
            }
        }
    }
    
    func saveNodesForMicroNodes(bankNode : BankNode)
    {
        LoginManager.share().saveMDNode(node: bankNode) { (isSuccess, response, error) in
        }
        
    }

    
    func moveToAccounts(payment : Payment)
    {
        let landlordVC: AccountsListingVC = AccountsListingVC.init(nibName: "AccountsListingVC", bundle: nil)
        self.navigationController?.isNavigationBarHidden = false
        landlordVC.paymentObj = payment
        landlordVC.checkMicroPay = true
        self.navigationController?.pushViewController(landlordVC, animated: true)

    }
    
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        let point = longPressGesture.location(in: tblListingSaved)
        let indexPath = tblListingSaved.indexPathForRow(at: point)
        
        if indexPath == nil {
            print("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began) {
            print("Long press on row, at \(indexPath!.row)")
            
            showAlertWithMutipleButtons(title: "TenantTag", message: "Are you sure you want to delete account?", cancel: "No", destructive: nil, actions: ["Yes"], controller: self, callBack: { (cancel, destruction, index) in
                if index == 0{
                    self.deleteParticularAccount(index: indexPath!.row)
                }
            })
        }
    }

    
    func deleteParticularAccount(index : NSInteger)
    {
        let paymentWithCard = newPayment.bankNodes[index]
        
        LoginManager.share().deleteNode(node: newPayment.bankNodes[index]) { (isSuccess, response, error) in
            if isSuccess == true
            {
                let indexArry = self.newPayment.bankNodes.index(of: paymentWithCard)
                 self.newPayment.bankNodes.remove(at: indexArry!)
                self.tblListingSaved.reloadData()
                
            }
     }
        
        /*
        paymentWithCard.deleteSavedBankAccount { (isSuccess, responseData, error) in
            HSProgress(.hide)
            if isSuccess{
                let indexArry = self.arrayBanks.index(of: paymentWithCard)
                self.arrayBanks.remove(at: indexArry!)
                if self.arrayBanks.count > 0 {
                    self.imgViewDummy.isHidden = false
                    self.selectedIndex = 0
                    self.btnNext.isHidden = false
                }else {
                    self.btnNext.isHidden = true
                    self.selectedIndex = -1
                    self.imgViewDummy.isHidden = true
                }
                self.tblListingAccounts.reloadData()
            }else{
                
                
                
            }
            
        }
        */
        
    }


    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
