//
//  BankLoginVC.swift
//  TenantTag
//
//  Created by maninder on 30/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class BankLoginVC: UIViewController {
    
    var bankSelected = Bank()
    
   // var payment = Payment()
    
    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var txtFieldSecond: UITextField!
    @IBOutlet var txtFieldFirst: UITextField!
    @IBOutlet var imgViewBankLogo: UIImageView!
    @IBOutlet var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(BankFormVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        
        
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle(bankSelected.bankName as NSString , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        imgViewBankLogo.sd_setImage(with: bankSelected.bankLogo as URL, placeholderImage: nil)
        
        
        btnSubmit.cornerRadius(4)
         viewOuter.cornerRadius(5)
         viewInner.cornerRadius(4)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func actionBtnBackPressed(){
      self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func actionBtnSubmit(_ sender: Any) {
        
        if (txtFieldFirst.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter username!", controller: self)
            return
        }else if (txtFieldSecond.text)?.isEmpty == true
        {
            HSShowAlertView("TenantTag", message: "Please enter password!", controller: self)
            return
        }else{
            
            self.view.endEditing(true)
            LoginManager.share().getBankNodes(userName: txtFieldFirst.text!, password: txtFieldSecond.text!, bankName: bankSelected.bankName , bankLogo : bankSelected.bankImageStr) { (isSuccess, response, error) in
            if isSuccess{
                
                let paymentInfo =  LoginManager.getPaymentDetail

                if paymentInfo.mfaEnabled == true{
                    
                    let landlordVC: MFAVerificationVC = MFAVerificationVC.init(nibName: "MFAVerificationVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    landlordVC.bankSelected = self.bankSelected
                    self.navigationController?.pushViewController(landlordVC, animated: true)
                    
                }else{
                    let landlordVC: AccountsListingVC = AccountsListingVC.init(nibName: "AccountsListingVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    landlordVC.paymentObj = paymentInfo
                    self.navigationController?.pushViewController(landlordVC, animated: true)
                }

                
                
                
              

                
            }else{
                
                
                
            }
           // RentPaymentFormVC
            
              }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
