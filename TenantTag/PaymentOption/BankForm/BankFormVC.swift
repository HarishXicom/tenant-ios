//
//  BankFormVC.swift
//  TenantTag
//
//  Created by maninder on 16/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class BankFormVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblBankForm: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(BankFormVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
     
       
        self.navigationItem.leftBarButtonItems = [barButton];
        
        self.navTitle("Bank Account Set Up" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)

        tblBankForm.registerNibsForCells(arryNib: ["BankFormCell"])
        tblBankForm.dataSource = self
        tblBankForm.delegate = self
        tblBankForm.estimatedRowHeight = 70
        tblBankForm.rowHeight = UITableViewAutomaticDimension;
        tblBankForm.allowsSelection = true
       // tblBankForm.backgroundColor = UIColor.groupTableViewBackground

      //  self.btnSetup.cornerRadius(4)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionBtnBackPressed()
    {
  self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TableView Delegates
    //MARK:-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: BankFormCell = tableView.dequeueReusableCell(withIdentifier: "BankFormCell") as! BankFormCell
            return cell
        }else{
            let cell: CommonQuestionsBtnCell = tableView.dequeueReusableCell(withIdentifier: "CommonQuestionsBtnCell") as! CommonQuestionsBtnCell
            cell.callBackCommonQuestion = {(test : Bool) in
                print(test)
            }
            return cell
        }
    }
    
   }
