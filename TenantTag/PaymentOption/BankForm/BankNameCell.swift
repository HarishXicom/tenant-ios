//
//  BankNameCell.swift
//  TenantTag
//
//  Created by maninder on 30/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class BankNameCell: UITableViewCell {
    var bankObj : Bank?
    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var lblBankName: UILabel!
    @IBOutlet var imgViewBankLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.cornerRadius(4)
        viewInner.cornerRadius(3)
        imgViewBankLogo.cornerRadius(5)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    
    func setBankDetails(){
        if bankObj != nil{
            
            let urlStr = URL(string: bankObj!.bankImageStr)
            self.imgViewBankLogo.sd_setImage(with: bankObj!.bankLogo as URL , placeholderImage: UIImage(named: "PH")  )
            self.lblBankName.text = bankObj?.bankName
        }
    }
    
    func setDummyAddBank(){
        self.lblBankName.text = "Add Bank"
        self.lblBankName.font =  UIFont(name: APP_FontMediumNew, size: 15)!
        //AddBank
        self.imgViewBankLogo.image = UIImage(named: "AddBank")
    }
}
