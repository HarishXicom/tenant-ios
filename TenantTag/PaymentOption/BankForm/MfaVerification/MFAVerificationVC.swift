//
//  MFAVerificationVC.swift
//  TenantTag
//
//  Created by maninder on 4/24/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class MFAVerificationVC: UIViewController {
    @IBOutlet var txtAnswer: UITextField!
   
     var bankSelected = Bank()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.navigationController?.isNavigationBarHidden = false
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(MFAVerificationVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Security Check" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionBtnBackPressed()
    {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func actionBtnDone(_ sender: Any)
    {
        
        if txtAnswer.text?.isEMPTY() == true{
            
            HSShowAlertView("TenantTag", message: "Please enter valid answer.")
            return
        }
        
        
        LoginManager.share().postAnswer(answerStr: txtAnswer.text! , bankLogo: bankSelected.bankImageStr) { (isSuccess, response, error) in
            if isSuccess{
                
                let paymentInfo =  LoginManager.getPaymentDetail
                
                if paymentInfo.mfaEnabled == true{
                    self.txtAnswer.text = ""
                    return
                    
                }else{
                    let landlordVC: AccountsListingVC = AccountsListingVC.init(nibName: "AccountsListingVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    landlordVC.paymentObj = LoginManager.getPaymentDetail
                    self.navigationController?.pushViewController(landlordVC, animated: true)
                }
            }else{
                
                
                
            }
 
            
            
            
        }
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
