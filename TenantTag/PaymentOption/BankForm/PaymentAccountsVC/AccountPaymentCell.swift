//
//  AccountPaymentCell.swift
//  TenantTag
//
//  Created by maninder on 03/02/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class AccountPaymentCell: UITableViewCell {
    
    @IBOutlet var btnRecurringStatus: UIButton!
    var callBack :((Payment)->Void)? = nil
    var callBackSelection :((Payment)->Void)? = nil
     var payment = Payment()
    @IBOutlet var viewInner: UIView!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var btnSelection: UIButton!
    
     //  @IBOutlet var btnSelection: UIImageView!
    @IBOutlet var btnRecurringInfo: UIButton!
    @IBOutlet var lblAccountNo: UILabel!
    @IBOutlet var lblBankName: UILabel!
    @IBOutlet var imgViewBank: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(4)
        
        btnSelection.isSelected = false

        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnDeSelect(_ sender: Any) {
        if callBackSelection != nil{
            self.callBackSelection!(payment)
        }
    }
    
    
    @IBAction func actionBtnRecurring(_ sender: Any) {
        if callBack != nil{
            self.callBack!(payment)
        }
    }
    func assignData(cellIndex : Int , selectedIndex : Int)
    {
        lblAccountNo.text = "xxxx-xxxx-xxxx-" + payment.selectedNode.accountNo
        lblBankName.text = payment.selectedNode.bankFullName
         self.imgViewBank.sd_setImage(with:  payment.selectedNode.bankImageURL as URL , placeholderImage: UIImage(named: "PH")  )
        if payment.recurringEnabled == true{
            btnRecurringStatus.isHidden = false
        }else{
            btnRecurringStatus.isHidden = true
        }
        if cellIndex == selectedIndex
        {
            btnSelection.isSelected = true
        }else{
            btnSelection.isSelected = false
        }
    }

}
