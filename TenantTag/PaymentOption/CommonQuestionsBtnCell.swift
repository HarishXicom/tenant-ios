//
//  CommonQuestionsBtnCell.swift
//  TenantTag
//
//  Created by maninder on 16/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

class CommonQuestionsBtnCell: UITableViewCell {
    var callBackCommonQuestion:((Bool)->Void)? = nil

    @IBOutlet var btnCommonQuestions: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnCommonQuestions.underLineButton(text: "Common Questions", color: APP_ThemeColor)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBtnCommonQuestionsPressed(_ sender: Any) {
        
        
        if callBackCommonQuestion != nil{
            self.callBackCommonQuestion!(true)
        }
        
    }
    
    func setNewText()
    {
        btnCommonQuestions.setTitle("Tpay is not available.", for: .normal)
        btnCommonQuestions.setTitleColor(UIColor.red, for: .normal)
    }
}
