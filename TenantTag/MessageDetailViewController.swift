//
//  MessageDetailViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 27/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class MessageDetailViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    weak var message: Message? = nil
    var messageDeleteFromDetailCallBack:((Bool, Message) -> Void)? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        // Do any additional setup after loading the view.
        let nib: UINib = UINib(nibName: "MessageCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MessageCell")
        
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(MessageDetailViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        
        self.navTitle(self.message!.text! as NSString, color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)

    }
    
    func backButtonClicked() {
        self.navigationController!.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // Delete Message....
    func deleteMessage(_ cell: MessageCell) {
        
        HSShowActionSheet("Delete?", message: "", cancel: "Cancel", destructive: "Delete", actions: nil) { (cancel, destruction, index) -> () in
            
            if cancel == true {
                
            }else if destruction == true {
                
                self.message!.deleteMessage({ (isSuccess, data, error) -> () in
                    if isSuccess == true {
                        if self.messageDeleteFromDetailCallBack != nil {
                            self.messageDeleteFromDetailCallBack!(true, self.message!)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                
            }
            
        }
        
        
    }
    
    // MARK: - TAble
    // MARK: -

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            let cell: MessageCell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
            let message = self.message
            cell.messageLabel.numberOfLines = 0;
            cell.updateCell(message)
            cell.messageDeleteCallBack = {(cell: MessageCell) -> () in
                self.deleteMessage(cell)
            }
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let message = self.message
        return MessageCell.heightForFeed(message!, size: tableView.frame.size, fullDetail: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.marginWillShow(false)
        tableView.marginWillShow(false)
        cell.selectionStyle = .none
        
    }


}
