//
//  HTTPErrorView.swift
//  Who's In
//
//  Created by Hardeep Singh on 31/12/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit

class HTTPErrorView: UIView {

    lazy var label: UILabel = UILabel();
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.label.frame = self.bounds
        self.label.textColor = UIColor.white
        self.label.lineBreakMode = .byCharWrapping
        self.label.textAlignment = .center
        self.label.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(self.label)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.label.text = "Oops! You seem to be offline"
        self.label.frame = CGRect(x: 0, y: 20, width: self.frame.width, height: 20)
    }
    
    
    class func show() {
        
    }
    
    
}
