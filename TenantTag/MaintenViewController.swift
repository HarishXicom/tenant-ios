//
//  MaintenViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class MaintenViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet var messageBG: UIView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var textFieldBGView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var textViewBGView: UIView!
    @IBOutlet weak var textView: SLKTextView!
    fileprivate var pickerView: UIPickerView! =  UIPickerView()
    var maintenance: [MaintenanceCat]? = nil
    weak var selectedMaintenanceCat: MaintenanceCat? = nil
    var maintenanceStatus: Bool? = nil
    var messageWillShow: Bool {
        
        if maintenanceStatus == nil {
            self.emptyView()
            return false
            
        }else {
            
            if self.maintenanceStatus! == false {
                self.showMessageLabel()
            }else {
                self.hideMessageLabel()
            }
            return self.maintenanceStatus!
            
        }
        
    }
    
    func emptyView() {
        self.messageLabel.isHidden = true;
        self.textField.isHidden = true;
        self.textView.isHidden = true;
        self.submitButton.isHidden = true;
    }
    
    func showMessageLabel() {
        
        self.messageLabel.isHidden = false;
        self.messageBG.isHidden = false;
        self.textField.isHidden = true;
        self.textView.isHidden = true;
        self.submitButton.isHidden = true;
   
    }
    
    func hideMessageLabel()
    {
        self.messageLabel.isHidden = true;
        self.messageBG.isHidden = true;
        
        self.textField.isHidden = false;
        self.textView.isHidden = false;
        self.submitButton.isHidden = false;
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        LoginManager.getTenantPropertyInformation { (isSuccess, data, error) in
            
            
          }
        


//        if self.textField.inputView == nil {
//            
//            self.pickerView = UIPickerView()
//            self.pickerView.delegate = self;
//            self.pickerView.dataSource = self;
//            self.textField.inputView =  self.pickerView;
//           // self.textField.addDoneOnKeyboardWithTarget(self, action: "doneButtonClicked:")
//            
//        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.messageWillShow;


        //self.view.backgroundColor = UIColor.whiteColor()
        // Do any additional setup after loading the view.
        self.underTopBar(false)
        self.textView.placeholder = "Brief Description of Problem"
        self.textViewBGView.cornerRadius(0.3)
        self.textView.backgroundColor = UIColor.clear
        self.textField.delegate = self;
        
        self.textFieldBGView.cornerRadius(0.3)
        self.textField.backgroundColor = UIColor.clear

        self.submitButton.cornerRadius(0.3)
        self.submitButton.backgroundColor = APP_ThemeColor

        self.pickerView = UIPickerView()
        self.pickerView.delegate = self;
        self.pickerView.dataSource = self;
        
        
        if self.maintenanceStatus == nil {
          //  HSProgress(.show)
        }
        MaintenanceCat.getMaintenanceAllCategories { (isSuccess: Bool?, tmaintenance: Bool, data: AnyObject?, error: NSError?) -> () in
            
           // HSProgress(.hide)
            
            if isSuccess == true {
                
                let list: [MaintenanceCat]? = data as? [MaintenanceCat]
                if list  != nil {
                    self.maintenance = list;
                    self.maintenanceStatus = tmaintenance;
                }
                
                self.maintenanceStatus = tmaintenance;
                self.messageWillShow;
                return;
                
                
            }else {
                
                self.maintenanceStatus = tmaintenance;
                self.messageWillShow;
            }
            
        }
        
       

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dropDownButtonClicked(_ sender: AnyObject) {
        if self.textField.isFirstResponder == false {
            self.textField.becomeFirstResponder()
        }
    }

    @IBAction func submitButtonClicked(_ sender: AnyObject) {
        
        //APISubmitMaintenance
         //HTTPRequest.request().post
        if self.selectedMaintenanceCat == nil {
              HSShowAlertView("TenantTag", message: "Select maintenance type")
            return;
        }
        
        let message: String = self.textView.text;
        
        if message.isEmpty == true {
            HSShowAlertView("TenantTag", message: "Description")
            return;
        }
        
        
        
        if LoginManager.getMe.landLordSubscriptionStatus == true{
            
        
        let parms = ["categoryId": self.selectedMaintenanceCat!.ID,
                     "message":message]
        
        HSProgress(.show)
        HTTPRequest.request().postRequest(APISubmitMaintenance as NSString, param: parms as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
           
            HSProgress(.hide)
            if isSuccess == true {
                //print(data)
                self.textField.text = ""
                self.textView.text = ""
                self.selectedMaintenanceCat = nil
                HSShowAlertView("TenantTag", message: "Maintenance request submit successfully")
            }
            
        }
            
        }else{
            HSShowAlertView(APP_Name as NSString, message: "Your landlord subscription required")
        }
        
    }
    
    
    func doneButtonClicked(_ sender: AnyObject) {
        self.textField.resignFirstResponder()
        
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
//        print(self.textField.inputView)
       self.textField.inputView = self.pickerView;
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - Navigation
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.maintenance != nil {
            return self.maintenance!.count
        }
        return 0;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedMaintenanceCat = self.maintenance![row]
        self.textField.text = self.selectedMaintenanceCat!.name;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let cat = self.maintenance![row]
        return cat.name
    }
    
    

}
