//
//  HomeViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

enum HomeSecType : Int {
    
    case address = 0,
    pickUP,
    moreInfo
    
}

class HomeViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var documentsButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var property: Property? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Property.getProperty { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if isSuccess {
                self.property = data as? Property
                self.tableView.reloadData()
                if leasedDocURL != ""
                {
                    saveLeasedDoc()
                }
            }
            
        }
        
        
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        // Do any additional setup after loading the view.
        
        var nib: UINib = UINib(nibName: "HomeAddressCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "HomeAddressCell")
        
        nib = UINib(nibName: "HomePickUpCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "HomePickUpCell")
       
        nib = UINib(nibName: "HomeListCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "HomeListCell")

        nib = UINib(nibName: "AddressHeaderView", bundle: nil)
        self.tableView.register( nib, forHeaderFooterViewReuseIdentifier: "AddressHeaderView")

        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.property == nil {
            return 0;
        }
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType: HomeSecType = HomeSecType(rawValue: section)!
        
        switch sectionType {
            
        case .address:
            
            if ((self.property!.communityAddress == nil) && (self.property!.communityPhoneNo == nil)) {
                return 0;
            }
                return 1;
            
        case .pickUP:
            
            return self.property!.amenities.count;
        case .moreInfo:
            return self.property!.services.count;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType: HomeSecType = HomeSecType(rawValue: indexPath.section)!
        
        switch sectionType {
            
        case .address:
            
            let cell: HomeAddressCell = tableView.dequeueReusableCell(withIdentifier: "HomeAddressCell") as! HomeAddressCell
            cell.updateCell(self.property!)
           return cell
            
        case .pickUP:
            
            let cell: HomePickUpCell = tableView.dequeueReusableCell(withIdentifier: "HomePickUpCell") as! HomePickUpCell
            let amienty = self.property!.amenities[indexPath.row]
            cell.updateCell(amienty)
            return cell
            
        case .moreInfo:
            
            let cell: HomeListCell = tableView.dequeueReusableCell(withIdentifier: "HomeListCell") as! HomeListCell
            let service = self.property!.services[indexPath.row]
            cell.updateCell(service)
            return cell
            
        }

        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.selectionStyle = .none

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sectionType: HomeSecType = HomeSecType(rawValue: indexPath.section)!
        
        switch sectionType {
            
        case .address:
            
            if self.property!.communityPhoneNo == nil {
                return HomeAddressCell.heightForFeed(self.property!, size: tableView.frame.size, phnExist: false)
            }else {
                return HomeAddressCell.heightForFeed(self.property!, size: tableView.frame.size, phnExist: true)
            }
            
        case .pickUP:
            
           return 34.0
            
        case .moreInfo:
            
            let service = self.property!.services[indexPath.row]
            if service.phone!.isEmpty {
                return 40.0
            }
              return 60.0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let sectionType: HomeSecType = HomeSecType(rawValue: section)!
        
        switch sectionType {
            
        case .address:
            
            return AddressHeaderView.heightForFeed(self.property!, size: tableView.frame.size)
        
        case .pickUP:
            
            return 1.0
            
        case .moreInfo:
            
            return 8.0
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        return 0.001

        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let sectionType: HomeSecType = HomeSecType(rawValue: section)!
        
        switch sectionType {
            
        case .address:
            
            let addressHeaderView: AddressHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "AddressHeaderView") as! AddressHeaderView
            addressHeaderView.updateHeader(self.property!)
            addressHeaderView.navController = self.navigationController;
            return addressHeaderView
            
        case .pickUP:
        
            break;
            
        case .moreInfo:
            break;

        }
        
        
        var pickUP: UITableViewHeaderFooterView? = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PickUP");
       
        if pickUP == nil {
            
            pickUP = UITableViewHeaderFooterView(reuseIdentifier: "PickUP")
            pickUP!.backgroundColor = UIColor.clear
            pickUP!.contentView.backgroundColor = UIColor.clear
            
            let view: UIView = UIView()
            view.backgroundColor = UIColor.clear
            pickUP!.backgroundView = view;

        }
        
        return pickUP

    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      
        var footerView: UITableViewHeaderFooterView? = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterView")
        
        if footerView == nil {
            
            footerView = UITableViewHeaderFooterView(reuseIdentifier: "FooterView")
            footerView!.backgroundColor = UIColor.clear
            footerView!.contentView.backgroundColor = UIColor.clear
            
            let view: UIView = UIView()
            view.backgroundColor = UIColor.clear
            footerView!.backgroundView = view;
            
        }
        
        return footerView

    }
    
    
    @IBAction func photoButtonClicked(_ sender: AnyObject) {
        //PhotosViewController
        
        let photosViewController: PhotosViewController = PhotosViewController(nibName: "PhotosViewController", bundle: nil)
        photosViewController.property = self.property
        photosViewController.landLordPhotos = self.property!.landlordPhoto
        photosViewController.tenantPhotos = self.property!.tenantPhoto
        self.navigationController?.pushViewController(photosViewController, animated: true)

    }

    @IBAction func documentsButtonClicked(_ sender: AnyObject) {
        let docViewController: DocViewController = DocViewController()
        docViewController.property = self.property
        self.navigationController?.pushViewController(docViewController, animated: true)
    }
}
