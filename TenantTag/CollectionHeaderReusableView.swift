//
//  CollectionHeaderReusableView.swift
//  TenantTag
//
//  Created by Hardeep Singh on 27/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class CollectionHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet weak var label: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
