//
//  AddressHeaderView.swift
//  TenantTag
//
//  Created by Hardeep Singh on 05/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class AddressHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var label: UILabel!
    weak var navController: UINavigationController? = nil
    weak var property: Property? = nil
    
    class func heightForFeed(_ property: Property, size: CGSize) ->(CGFloat) {
        
        let text: String = property.propertyAddress!
        
        let topSpace: CGFloat = 10.0;
        let leadingSpace: CGFloat = 8.0;
        let bottomSpace: CGFloat = 10.0;
        let trailingSpace: CGFloat = 8.0;
        let vPadding: CGFloat = 0.0;
        let hPadding: CGFloat = 0.0;
        
        let textWidth = (size.width - (leadingSpace + trailingSpace  + (hPadding*3)))
        
        let font: UIFont =  UIFont(name: APP_FontName, size: 16)!
        let maxSize: CGSize = CGSize( width: CGFloat(textWidth), height: 9999.0)
        let boundingBox = text.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size.height
        
        let maxHeight = ((boundingBox + topSpace + bottomSpace + (vPadding*3)))
        return maxHeight
        
    }
    

    func updateHeader(_ property: Property){
        self.property = property;
        
        let text: String = property.propertyAddress!
        self.label.text = text
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            
            let point:CGPoint = touch.location(in: self)
            if  self.label.frame.contains(point)  {
                self.label.textColor = UIColor.blue
            }
        }

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.label.textColor = UIColor.black
       
        if let touch = touches.first {
           let point:CGPoint = touch.location(in: self)
           
            if  self.label.frame.contains(point)  {
                if self.property != nil {
                    let mapViewController: MapViewController = MapViewController(nibName: "MapViewController", bundle: nil)
                    mapViewController.property = self.property;
                    if self.navController != nil {
                        self.navController?.pushViewController( mapViewController, animated: true)
                    }
                }
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.label.font =  UIFont(name: APP_FontName, size: 16)
    }

}
