//
//  HTTPRequest.swift
//  Who's In
//
//  Created by Hardeep Singh on 22/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit


class FileData: NSObject {
    
    var fileName: String!
    var name: String!
    var mimeType: String!
    var data: Data!
    var file: AnyObject!

    
  
    
    override init() {
        
        self.fileName = "";
        self.name = "";
        self.mimeType = "";

    }
    
    convenience init(fileName:String!, name:String!, mimeType:String!, data:Data!) {
        self.init()
        
        self.fileName = fileName;
        self.name = name;
        self.mimeType = mimeType;
        self.data = data;

    }

}




class HTTPRequest: AFHTTPRequestOperationManager {

 //   class func setUpNetworkReachability(_ manager: AFHTTPRequestOperationManager) {
        
        
        class func setUpNetworkReachability() {
            
            let reachabilityManager: AFNetworkReachabilityManager  = AFNetworkReachabilityManager.shared()
       reachabilityManager.setReachabilityStatusChange { (status: AFNetworkReachabilityStatus) -> Void in
            
            switch status {
                
            case AFNetworkReachabilityStatus.notReachable :
                appDelegate().reachabilityStatusChanged()

                break
                
            case AFNetworkReachabilityStatus.reachableViaWiFi :
                
                break
                
            case AFNetworkReachabilityStatus.reachableViaWWAN :
                
                break
                
            default:
                
                break
                
            }
        
            appDelegate().reachabilityStatusChanged()
            
        }
        
        
        reachabilityManager.startMonitoring()

    }
    
    // Returns the new instance.
    class func request() -> HTTPRequest {
        
        let URL:Foundation.URL =  Foundation.URL.init(string: BASE_URL)!
        let request:HTTPRequest = HTTPRequest.init(baseURL: URL)
        request.responseSerializer = AFJSONResponseSerializer()
        request.requestSerializer = AFJSONRequestSerializer()
        request.requestSerializer.timeoutInterval = 30.0
        return request
        
    }
    
    func updateHeader( _ method:NSString){
        
        let authstring:NSString =  method.appending(APP_LisenceKey as String) as NSString
        let timestamp:NSString = Date.timestamp();
        let clientHash:NSString = authstring.appending(timestamp as String) as NSString
        let clientHashMD5String:NSString = clientHash.md5 as NSString;
        
        self.requestSerializer.setValue(timestamp as String
            , forHTTPHeaderField:"Timestamp")
        self.requestSerializer.setValue(clientHashMD5String as String, forHTTPHeaderField:"Authstring")
        
        let idenitfier:String =  appDelegate().deviceUniqueIdentifier(); //
        self.requestSerializer.setValue(idenitfier, forHTTPHeaderField:"Devicetoken")
        self.requestSerializer.setValue(DeviceType, forHTTPHeaderField:"Devicetype")
        
        let sessionID:String? = LoginManager.getMe.sessionID
        if sessionID != nil
        {
            
            if sessionID!.isEmpty() == false
            {
             self.requestSerializer.setValue(sessionID! as String, forHTTPHeaderField:"Sessionid")
            }
        }
    }
    
    
    func setSynapsePayHeader(_ AuthKey : String)
    {
        
        if AuthKey == ""
        {
            let strHeader =  "|" +  LoginManager.getMe.userFingerPrint
            self.requestSerializer.setValue(strHeader , forHTTPHeaderField:"X-SP-USER")
        }else{
            let strHeader = AuthKey +  "|" +  LoginManager.getMe.userFingerPrint
            print(strHeader)
         self.requestSerializer.setValue(strHeader  , forHTTPHeaderField:"X-SP-USER")
        }
        self.requestSerializer.setValue("application/json"  , forHTTPHeaderField:"Content-Type")
        
       // let gateWay = ClientID + "|" + SecretID // Live
          let gateWay = ClientIDDemo + "|" + SecretIDDemo // Demo

        
         self.requestSerializer.setValue( gateWay , forHTTPHeaderField:"X-SP-GATEWAY") //Client live Credetials
        // self.requestSerializer.setValue("id-c83d6205-785a-4776-b4de-8584aebc045d|secret-076f2412-69cf-46b1-a304-ad27e62f38f3"  , forHTTPHeaderField:"X-SP-GATEWAY") // Demo
         self.requestSerializer.setValue( LoginManager.getMe.userIPAddress  , forHTTPHeaderField:"X-SP-USER-IP")
        self.requestSerializer.setValue("EN"  , forHTTPHeaderField:"X-SP-LANG")
        
    }
    
    //AFNetworkRequest.
    
    func checkUserMoveOutOrNot(_ dict: NSDictionary) -> Bool {
        let move_Status: Int? = dict.value(forKey: "move_status") as? Int
        if move_Status != nil {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kUserCheckOut), object: nil)
            return true;
        }
        return false;
        
    }
    
    
    func postRequest( _ method:NSString, param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }
        self.updateHeader(method)
        
        self.post(method as String, parameters: param, success: { (operation, response) -> Void in
           
             let dict: NSDictionary = response as! NSDictionary
            print(dict)
             let status: Bool = dict.value(forKey: "status") as! Bool
           
            if status == true {
                
                if self.checkUserMoveOutOrNot(dict) == true {
                    HSProgress(.hide)
                    let data = dict.value(forKey: "data")
                    callBack(false,data as AnyObject?,nil)
                    return;
                    
                }else {
                    
                    let data = dict.value(forKey: "data")
                    callBack(true,data as AnyObject?,nil)

                }
                
                
            }else {
                
                let message = dict.value(forKey: "message") as? String
                if  message != nil {
                   HSShowAlertView("TenantTag", message:message! as NSString, controller: nil)
                }
                callBack(false, dict, nil)
            }
           

           }) { (operation, error) -> Void in
           
            
           // let content = String(data: (operation?.responseData)!, encoding: .utf8)
           // print("content received : \(content)")

            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
            HSProgress(.hide)
            print(error)
            callBack(false,nil,error as NSError?)
        }
        
    }
    
    func getRequestReturnFullObj( _ method:NSString, param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }

        self.updateHeader(method)
        
        //HSProgress(.Show)
        self.get(method as String, parameters:param, success: { (operation, response) -> Void in
            
            //  HSProgress(.Hide)
            
            let dict: NSDictionary = response as! NSDictionary
            let status: Bool = dict.value(forKey: "status") as! Bool
            
            
            if status {
                
                if self.checkUserMoveOutOrNot(dict) == true {
                    HSProgress(.hide)
                    callBack(false,dict,nil)
                    return;
                }

                
                callBack(true,dict,nil)
                
            }else{
                
                let message = dict.value(forKey: "message") as? String
                if message != nil {
                    HSShowAlertView("TenantTag", message:message! as NSString, controller: nil)
                }
                callBack(false,dict,nil)
            }
            
            
            }) { (operation,error) -> Void in
                
                HSProgress(.hide)
                
                HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
                
                print(error)
                callBack(false,nil,error as NSError?)
        }
        
    }

    
    func getRequest( _ method:NSString, param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {

        if checkInternetAccess() == false{
            
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }

        self.updateHeader(method)
        self.get(method as String, parameters:param, success: { (operation, response) -> Void in
            let dict:NSDictionary = response as! NSDictionary
            let status:Bool = dict.value(forKey: "status") as! Bool
            if status {
                let data = dict.value(forKey: "data")
                if self.checkUserMoveOutOrNot(dict) == true {
                    HSProgress(.hide)
                    callBack(false, data as AnyObject?,nil)
                    return;
                }
                callBack(true,data as AnyObject?,nil)
                
            }else {
                
                let message = dict.value(forKey: "message") as? String
                if message != nil {
                    HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                }
                callBack( false, dict, nil)
            }
            
            print(response)
            

        }) { (operation,error) -> Void in
            
              HSProgress(.hide)
            
            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)

                print(error)
                callBack(false,nil,error as NSError?)
        }
        
    }
    
    
    func postRequestWithMulitpartData(_ files:[FileData]?, method:NSString, param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {

        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }

        self.updateHeader(method)
        HSProgress(.show)
        self.post(method as String, parameters: param, constructingBodyWith: { (formData:AFMultipartFormData) -> Void in
            
            for fileData in files! {
                
            let fileName:String = fileData.fileName as String
            let name:String = fileData.name as String
            let data:Data = fileData.data as Data
            let mimeType:String = fileData.mimeType as String
          
              formData.appendPart( withFileData: data, name:name, fileName:fileName, mimeType: mimeType)
          
         
            }
            
            }, success: { (operation, data) -> Void in
               
                print(data)
                HSProgress(.hide)
                callBack(true, data as AnyObject?,nil)
                
            }) { (operation, error) -> Void in
               
                print(error)
                HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
                HSProgress(.hide)
                callBack(false,nil,error as NSError?)
                
        }
        
    }
    func getRequestSynapsePay( _ method:NSString, param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }


      //  self.setSynapsePayHeader(userID as String)
   
        self.get(method as String, parameters:param, success: { (operation, response) -> Void in
            let dict:NSDictionary = response as! NSDictionary
            let status:Bool = dict.value(forKey: "status") as! Bool
            
            if status {
                
                let data = dict.value(forKey: "data")
                if self.checkUserMoveOutOrNot(dict) == true {
                    HSProgress(.hide)
                    callBack(false, data as AnyObject?,nil)
                    return;
                }
                callBack(true,data as AnyObject?,nil)
                
            }else {
                
                let message = dict.value(forKey: "message") as? String
                if message != nil {
                    HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                }
                callBack( false, dict, nil)
            }
            
            print(response)
            
            
        }) { (operation,error) -> Void in
            
            HSProgress(.hide)
            
            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
            
            print(error)
            callBack(false,nil,error as NSError?)
        }
        
    }
    func getRequestSynapsePayWithoutHeader( _ method:NSString, param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }
        
        self.setSynapsePayHeader("")

        self.get(method as String, parameters:param, success: { (operation, response) -> Void in
            if let dict = response as? NSDictionary
            {
           
                callBack(true,dict as AnyObject?,nil)
            }else{
                HSProgress(.hide)
            }
            
        }) { (operation,error) -> Void in
            
            HSProgress(.hide)
            
            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
            
            print(error)
            callBack(false,nil,error as NSError?)
        }
     }
    func postRequestToSynpsePay( _ method:NSString, userID : NSString , param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }

        self.setSynapsePayHeader(userID as String)
        self.post(method as String, parameters: param, success: { (operation, response) -> Void in
            if let dict = response as? NSDictionary
            {
                print(dict)
                if dict.object(forKey: "error") as? NSDictionary != nil
                {
                  //  callBack(false,dict as AnyObject?,nil)
                    let strError = (dict.object(forKey: "error") as! NSDictionary).object(forKey: "en") as! String
                    HSShowAlertView("TenantTag", message: strError as NSString, controller: nil)
                    HSProgress(.hide)
                   // callBack(false,strError as AnyObject?,nil)
                    return
                    
                }else{
                    callBack(true,dict as AnyObject?,nil)
                }
                
                
            }else{
                 HSProgress(.hide)
            }
            
        }) { (operation, error) -> Void in
     
            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
            HSProgress(.hide)
            callBack(false,nil,error as NSError?)
        }
        
    }
    
    
    func deleteRequestToSynpsePay( _ method:NSString, userID : NSString , param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }
        
        self.setSynapsePayHeader(userID as String)
        self.delete(method as String, parameters: param, success: { (operation, response) -> Void in
            if let dict = response as? NSDictionary
            {
                print(dict)
                if dict.object(forKey: "error") as? NSDictionary != nil
                {
                   // callBack(false,dict as AnyObject?,nil)
                    let strError = (dict.object(forKey: "error") as! NSDictionary).object(forKey: "en") as! String
                    HSShowAlertView("TenantTag", message: strError as NSString, controller: nil)
                    HSProgress(.hide)
                    // callBack(false,strError as AnyObject?,nil)
                    return
                    
                }else{
                    callBack(true,dict as AnyObject?,nil)
                }
                
                
            }else{
                HSProgress(.hide)
            }
            
        }) { (operation, error) -> Void in
            
            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
            HSProgress(.hide)
            print(error)
            callBack(false,nil,error as NSError?)
        }
        
    }
    
    
    func patchRequestToSynpsePay( _ method:NSString, userID : NSString , param:NSDictionary?, callBack:@escaping (Bool,AnyObject?,NSError?)->()) {
        if checkInternetAccess() == false{
            HSProgress(.hide)
            HSShowAlertView(APP_Name as NSString, message: "Please check your internet connection.")
            return
        }
        
        self.setSynapsePayHeader(userID as String)
        self.patch(method as String, parameters: param, success: { (operation, response) -> Void in
            HSProgress(.hide)
            if let dict = response as? NSDictionary
            {
                print(dict)
                if dict.object(forKey: "error") as? NSDictionary != nil
                {
                    // callBack(false,dict as AnyObject?,nil)
                    HSProgress(.hide)
                    let strError = (dict.object(forKey: "error") as! NSDictionary).object(forKey: "en") as! String
                    HSShowAlertView("TenantTag", message: strError as NSString, controller: nil)
                    HSProgress(.hide)
                    return
                    
                }else{
                    callBack(true,dict as AnyObject?,nil)
                }
                
                
            }else{
                
            }
            
        }) { (operation, error) -> Void in
            
            HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
            HSProgress(.hide)
            callBack(false,nil,error as NSError?)
        }
        
    }

    
 
    
  class  func forwardPostRequestToServer(actionName : String , paraString:String? , callBack:@escaping (Bool, AnyObject? , NSError? )->(Void))
    {
        // createURLwithNoAuthAndNoParamters
        // print(strURL!)
        let urlFinal = NSURL(string: actionName)
        let request = NSMutableURLRequest(url: urlFinal as! URL)
        request.httpMethod = "POST"
        let authstring:NSString =  APISaveStripeCard.appending(APP_LisenceKey as String) as NSString
        let timestamp:NSString = Date.timestamp();
        let clientHash:NSString = authstring.appending(timestamp as String) as NSString
        let clientHashMD5String:NSString = clientHash.md5 as NSString;
        
        request.setValue(timestamp as String
            , forHTTPHeaderField:"Timestamp")
        request.setValue(clientHashMD5String as String, forHTTPHeaderField:"Authstring")
        
        let idenitfier:String =  appDelegate().deviceUniqueIdentifier(); //
        request.setValue(idenitfier, forHTTPHeaderField:"Devicetoken")
        request.setValue(DeviceType, forHTTPHeaderField:"Devicetype")
        
        let sessionID:String? = LoginManager.getMe.sessionID
        if sessionID != nil {
            if sessionID!.isEmpty() == false {
                request.setValue(sessionID! as String, forHTTPHeaderField:"Sessionid")
            }
        }
        

        
       // request.httpBody = nil
        request.httpBody = paraString?.data(using: String.Encoding.utf8)

        
        request.timeoutInterval = 40
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if let httpResponse = response as? HTTPURLResponse {
                // print(httpResponse.statusCode)
                
                if httpResponse.statusCode == 201 {
                    return;
                }
                
                if httpResponse.statusCode == 204 {
                    return;
                }
            }
            if error != nil {
                print("error=\(error)")
                HSShowAlertView("TenantTag", message: "Request failed", controller: nil)
                HSProgress(.hide)
                callBack(false,nil,error as NSError?)
              
            }
            do
            {
               
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                let dict:NSDictionary = json!.mutableCopy() as! NSDictionary
                print(dict)
                let status = dict.value(forKey: "status") as! Bool
                
                if status == true
                {
                    callBack(true,dict,nil)
                }else{
                    callBack(false,dict,nil)
                }
            }
            catch
            {
                
                print("error serializing JSON: \(error)")
            }
        }
        task.resume()
    }
    
}
