//
//  TextFieldCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 05/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {

    @IBOutlet var searchButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var textField: UITextField!
    var cancelButtonCallBack: ((Bool) -> Void)? = nil
    var searchButtonCallBack: ((Bool) -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.textField.placeholder = "Search Messages"
        self.textField.addLeftImage( UIImage(named: "search")!, minimumSize: CGSize(width: 30, height: 30))
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func cancelButtonClicked(_ sender: AnyObject) {
        if self.cancelButtonCallBack != nil {
            self.cancelButtonCallBack!(true)
        }
    }
    
    @IBAction func searchButtonClicked(_ sender: AnyObject) {
        if self.searchButtonCallBack != nil {
            self.searchButtonCallBack!(true)
        }
    }
}
