//
//  LandlordAcceptViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 17/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit
import SafariServices

class LandlordAcceptViewController: UIViewController {

    @IBOutlet var btnTermsAndConditions: UIButton!
    @IBOutlet var btnTermsCheckBox: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var label: UILabel!
    var verfiedSuccess: (( _ success: Bool) -> Void)? = nil

    @IBOutlet var denyButton: UIButton!
    @IBAction func denuButtonClicked(_ sender: AnyObject) {
        self.backButtonClicked()
    }
    
    @IBAction func submitButtonClicked(_ sender: AnyObject) {
        
        if btnTermsCheckBox.isSelected == false{
            
            return
        }
        
        
        let params : Dictionary = ["accept":true]
        HSProgress(.show)
        
        HTTPRequest.request().postRequest( APIAcceptLandlord as NSString , param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            
            if isSuccess == true {
                LoginManager.getMe.verified = true;
               // LoginManager.getMe.updateUser(
                print(data)
                LoginManager.share().saveUserProfile()
                if self.verfiedSuccess != nil {
                    self.verfiedSuccess!(true)
                }
            }
        }
        
    }

    
    @IBAction func actionBtnCheckBoxClicked(_ sender: Any) {
         btnTermsCheckBox.isSelected = !btnTermsCheckBox.isSelected
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        // Do any additional setup after loading the view.
        self.label.font = UIFont(name: APP_FontName, size: 17)
        self.submitButton.cornerRadius(8.0)
        self.denyButton.cornerRadius(8.0)

        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(LandlordAcceptViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle("Landlord's Number", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)
        
        let phnNumber: String = LoginManager.getMe.landlordNumber!
        self.label.text = "Please confirm that \(phnNumber) is your landlord's number."
        
        btnTermsAndConditions.underLineButton(text: "Terms & Conditions", color: APP_ThemeColor)
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBtnOpenTermsAndConditions(_ sender: Any) {
       
        let safariVC = SFSafariViewController(url: NSURL(string: URL_Terms)! as URL)
        self.present(safariVC, animated: true, completion: nil)
        
        
    }
    func backButtonClicked() {
        
        LoginManager.share().logout { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            self.navigationController!.popViewController(animated: true)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
