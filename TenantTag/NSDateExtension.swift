//
//  NSDateExtension.swift
//  Who's In
//
//  Created by Hardeep Singh on 22/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import Foundation

extension Date {

    public static func timestamp() -> NSString {
        return "\(Date().timeIntervalSince1970 * 1000)" as NSString
    }
    public func returnStrFromDate() -> String
    {
        
        let dateFormate:DateFormatter = DateFormatter()
        dateFormate.dateFormat = APP_DOBDateFormateBackEnd
        let strDateBirth : NSString = dateFormate.string(from: self)  as NSString
        
        return strDateBirth as String
    }

}
