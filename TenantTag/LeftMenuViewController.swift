//
//  LeftMenuViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 05/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit
import SafariServices

enum MenuCellType: Int {
    
    case landlord = 0,
    terms,
    privacy,
    profile,
    support,
    logout
    
}

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var topBGView: UIView!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label1: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
   
    @IBOutlet var rightSpaceLbl2: NSLayoutConstraint!
    @IBOutlet var rightSpaceLbl1: NSLayoutConstraint!
   // weak var revealViewController: SWRevealViewController? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.label1.text = LoginManager.getMe.fullName
        self.label2.text = LoginManager.getMe.emailAddress!

    }
    
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let nib: UINib = UINib(nibName: "MenuCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MenuCell")
        // Do any additional setup after loading the view.
        self.tableView.isScrollEnabled = false
        self.tableView.separatorStyle = .none
        self.rightSpaceLbl2.constant = 80
        self.rightSpaceLbl1.constant = 80

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        cell.label?.textColor = UIColor.white

        let menuType: MenuCellType = MenuCellType(rawValue: indexPath.row)!
        switch menuType {
        case .landlord:
            
            cell.label?.text = "Landlord";
            cell.imgView.image = UIImage(named: "LandLord")
            break

        case .terms:
            
            cell.label?.text = "Terms";
            cell.imgView.image = UIImage(named: "Terms")
            break

        case .privacy:
            
            cell.label?.text = "Privacy";
            cell.imgView.image = UIImage(named: "Privacy")
            break

    
        case .profile:
            
            cell.label?.text = "Profile";
            cell.imgView.image = UIImage(named: "profile")
                break
            
        case .support:
            
            cell.label?.text = "Support";
            cell.imgView.image = UIImage(named: "SupportIcon")
            break

            
        case .logout:
            
             cell.label?.text = "Logout";
             cell.imgView.image = UIImage(named: "logout")
            break
        }

        return cell

        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.marginWillShow(false)
        tableView.marginWillShow(false)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
               //let landlordVC: PaymentOptionVC = PaymentOptionVC.init(nibName: "PaymentOptionVC", bundle: nil)
            let landlordVC: LandlordViewController = LandlordViewController.init(nibName: "LandlordViewController", bundle: nil)
            self.navigationController?.isNavigationBarHidden = false
            landlordVC.boolNav = true
            self.navigationController?.pushViewController(landlordVC, animated: true)
        }else if indexPath.row == 1{
            
            let safariVC = SFSafariViewController(url: NSURL(string: URL_Terms)! as URL)
           // safariVC.delegate = self

            self.present(safariVC, animated: true, completion: nil)
        }else if indexPath.row == 2{
            
            let safariVC = SFSafariViewController(url: NSURL(string: URL_Privacy)! as URL)
            self.present(safariVC, animated: true, completion: nil)

        }else if indexPath.row == 3 {
            let forgotPasswordViewController: ProfileViewController = ProfileViewController.init(nibName: "ProfileViewController", bundle: nil)
            let navControlelr: UINavigationController = UINavigationController(rootViewController: forgotPasswordViewController)
            self.present(navControlelr, animated: true) { () -> Void in
            }
        }else if indexPath.row == 5 {
            HSShowActionSheet("Logout", message: nil, cancel: "Cancel", destructive: "Logout", actions: nil, callBack: { (cancel, destruction, index) -> () in
                if destruction == true {
                    LoginManager.share().logout({ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                        if isSuccess == true {
                            self.navigationController!.popToRootViewController(animated: true)
                        }
                    })
                }
            })
        }else if indexPath.row == 4{
            
            let supportVC: SupportVC = SupportVC.init(nibName: "SupportVC", bundle: nil)
            let navControlelr: UINavigationController = UINavigationController(rootViewController: supportVC)
            self.present(navControlelr, animated: true) { () -> Void in
            }
        }
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

}
