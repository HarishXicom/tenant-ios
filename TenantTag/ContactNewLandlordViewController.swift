//
//  ContactNewLandlordViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 25/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class ContactNewLandlordViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var landlordEmailFieldBG: UIView!
    @IBOutlet var landlordEmailField: UITextField!
    @IBOutlet var landlordNameBGView: UIView!
    @IBOutlet var landlordNameField: UITextField!
    @IBOutlet var textViewBGView: UIView!
    @IBOutlet var textView: SLKTextView!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var tableView: UITableView!
    
    @IBAction func sendButtonClicked(_ sender: AnyObject) {
       
        self.landlordEmailField.resignFirstResponder()
        self.landlordNameField.resignFirstResponder()
        self.textView.resignFirstResponder()

        let emailAddress: String? = self.landlordEmailField.text
        if emailAddress == nil {
            HSShowAlertView("TenantTag", message: "Please enter email Address!",controller: self)
            return;
        }
        
        if emailAddress!.isEmpty() {
            HSShowAlertView("TenantTag", message: "Please enter email Address!",controller: self)
            return
         }
        
        if !emailAddress!.isValidEmail() {
            HSShowAlertView("TenantTag", message: "Invalid email address!",controller: self)
            return
        }

        let name: String? = self.landlordNameField.text!
        if name == nil {
            HSShowAlertView("TenantTag", message: "Landlord name is required.",controller: self)
            return;
        }
        if name!.isEmpty() {
            HSShowAlertView("TenantTag", message: "Landlord name is required.",controller: self)
        }
        
        let message: String? = self.textView.text
        if message == nil {
            HSShowAlertView("TenantTag", message: "Message is required",controller: self)
            return;
        }
        
        if message!.isEmpty() {
            HSShowAlertView("TenantTag", message: "Message is required",controller: self)
        }
        
        let parma: NSDictionary = ["email":emailAddress!,"name":name!,"message":message!]
        
        HSProgress(.show)
        HTTPRequest.request().postRequest(APIContactNewLandlord as NSString, param: parma) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                 
           HSProgress(.hide)
            if isSuccess == true {
                self.navigationController?.popViewController(animated: true)
                HSShowAlertView("TenantTag", message: "Email sent to landlord successfully")
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.landlordEmailFieldBG.backgroundColor = UIColor.white
        self.landlordEmailFieldBG.cornerRadius(3.0)
        self.landlordEmailField.keyboardType = .emailAddress;
        
        self.landlordNameBGView.backgroundColor = UIColor.white
        self.landlordNameBGView.cornerRadius(3.0)
        
        self.textViewBGView.backgroundColor = UIColor.white
        self.textViewBGView.cornerRadius(3.0)
        self.sendButton.cornerRadius(3.0)
        
        self.textView.placeholder = "Write message..."
        self.textView.text = "";

        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
//        // Do any additional setup after loading the view.
//        let nib: UINib = UINib(nibName: "DocCell", bundle: nil)
//        self.tableView.registerNib(nib, forCellReuseIdentifier: "DocCell")
//        
//        self.tableView.backgroundColor = UIColor.groupTableViewBackgroundColor()
//        self.tableView.separatorStyle = .None
//        self.tableView.delegate = self;
//        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: "popOneViewController", for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        
        self.navTitle("CONTACT NEW LANDLORD", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: DocCell = tableView.dequeueReusableCell(withIdentifier: "DocCell") as! DocCell
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
