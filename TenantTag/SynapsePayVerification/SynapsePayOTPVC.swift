//
//  SynapsePayOTPVC.swift
//  TenantTag
//
//  Created by maninder on 7/14/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit


extension String{
    
    
    func removeEndingSpaces() -> String
    {
        let strTrimmed = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return strTrimmed
    }
}

class SynapsePayOTPVC: UIViewController {
    

    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtOTP: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        btnSubmit.cornerRadius(8)
        txtOTP.cornerRadius(8)
        self.txtOTP.layer.borderColor = APP_ThemeColor.cgColor
        self.txtOTP.layer.borderWidth = 1.0

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBtnPressed(_ sender: Any) {
        
        
        if txtOTP.text?.isEMPTY() == true{
            HSShowAlertView(APP_Name as NSString, message: "Please enter otp first.", controller: self)
            return
        }
        let strOTP = txtOTP.text!.removeEndingSpaces()
        LoginManager.share().synapsePayOTP(  otpCode: strOTP) { (isSuccess, response, strError) in
            if isSuccess{
                self.updateLocalServerInfo()
            }
        }
        
        
        
    }

    
    
    func updateLocalServerInfo(){
        
          LoginManager.share().updateSynapseUserID(synapsePayUserID: LoginManager.getMe.synapsePayUserID, { (isSuccess, response, error) in
                  if isSuccess{
                LoginManager.share().saveUserProfile()
                                
                    self.openMenuViewController(true)
                                
                     }else{
                            }
                 })
    }
    
    
    
    func openMenuViewController(_ animation:Bool) {
        
        if  LoginManager.getMe.verified == true {
            
            if  LoginManager.getMe.moveStatus == .out {
                
                let checkOutMainViewController: CheckOutMainViewController = CheckOutMainViewController()
                let mainNavControlelr: UINavigationController = UINavigationController(rootViewController: checkOutMainViewController)
                mainNavControlelr.navigationBar.barTintColor = APP_ThemeColor
                mainNavControlelr.navigationBar.tintColor = UIColor.white
                
                let leftMenuViewController: LeftMenuViewController = LeftMenuViewController.init(nibName: "LeftMenuViewController", bundle: nil)
                let revealViewController = SWRevealViewController(rearViewController: leftMenuViewController, frontViewController: mainNavControlelr)
                //leftMenuViewController.revealViewController = revealViewController
                revealViewController?.rearViewRevealWidth = self.view.frame.width - 80
                //revealViewController?.delegate = self
                
                self.navigationController!.isNavigationBarHidden = true
                self.navigationController!.pushViewController( revealViewController!, animated: animation)
            }else {
                
                let mainContainerViewController: MainContainerViewController = MainContainerViewController.init(nibName: "MainContainerViewController", bundle: nil)
                let mainNavControlelr: UINavigationController = UINavigationController(rootViewController: mainContainerViewController)
                mainNavControlelr.navigationBar.barTintColor = APP_ThemeColor
                mainNavControlelr.navigationBar.tintColor = UIColor.white
                
                let leftMenuViewController: LeftMenuViewController = LeftMenuViewController.init(nibName: "LeftMenuViewController", bundle: nil)
                let revealViewController = SWRevealViewController(rearViewController: leftMenuViewController, frontViewController: mainNavControlelr)
                //leftMenuViewController.revealViewController = revealViewController
                revealViewController?.rearViewRevealWidth = self.view.frame.width - 80
                //  revealViewController?.delegate = self
                //  revealViewController?.frontViewPosition = FrontViewPosition.left
                self.navigationController!.isNavigationBarHidden = true
                self.navigationController!.pushViewController( revealViewController!, animated: animation)
                
            }
            
        }else {
            
            let landlordAcceptViewController = LandlordAcceptViewController.init(nibName: "LandlordAcceptViewController", bundle: nil)
            self.navigationController!.pushViewController(landlordAcceptViewController, animated: true)
            landlordAcceptViewController.verfiedSuccess = {(success: Bool) -> () in
                LoginManager.getMe.verified = true
                LoginManager.share().saveUserProfile()
                self.openMenuViewController(true)
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
