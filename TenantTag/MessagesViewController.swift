//
//  MessagesViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var txtFieldFilter: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var searchTextField: UITextField!

    @IBOutlet weak var tableView: HSTableView!
    var messageList: [Message] = [Message]()
    var searchList: [Message] = [Message]()
    var searching: Bool = false
    var pageManager: HSPageManager? = nil
    var searchPageManager: HSPageManager? = nil
    var refreshController: UIRefreshControl? = nil;
    var datePicker: UIDatePicker = UIDatePicker()
    var selectedDate :Date? = nil
    var selectedDateString :String? = nil
    
    let viewDatePicker : UIView = UIView()
    let viewLine : UIView = UIView()

    let btnSearch = UIButton()
    let btnCancel = UIButton()



    var list: [Message] {
        
        get {
            
            if self.searching {
                return self.searchList
            }else {
                return self.messageList
            }
            
        }
        
        set (messages) {
            if self.searching {
                 self.searchList = messages
            }else {
                 self.messageList = messages
            }
            
        }
    }


    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        createDatePickerView()
    }
    
    func refreshControllerValueChanged(_ sender: AnyObject) {
        
        if self.searching {
            
            self.refreshController!.beginRefreshing()
            self.list.removeAll()
            
            if self.searchPageManager != nil {
                self.searchPageManager!.refreshPaginator()
            }

            self.tableView.reloadData()
            
        }else {
            
            self.refreshController!.beginRefreshing()
            self.list.removeAll()
            self.pageManager!.refreshPaginator()
            self.tableView.reloadData()
            
        }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.groupTableViewBackground

       // if self.list.count <= 0 {
            
          //  self.refreshController!.beginRefreshing()
            self.list.removeAll()
            self.pageManager!.refreshPaginator()
            self.tableView.reloadData()

      //  }
    }
    
    func startSearching() {
        
        
        var searchText: String = "";
        var searchDate: String = ""
        
        if self.selectedDateString != nil {
            searchDate = self.selectedDateString!
        }
        
        if self.searchTextField.text != nil {
            searchText = self.searchTextField.text!;
        }
        
        if (searchText.isEmpty && searchDate.isEmpty) {

            HSShowAlertView("TenantTag", message: "Please enter text for search.")
            return
        }

        
        self.searchTextField.resignFirstResponder()
        
        UIView.animate(withDuration: 0.4, animations: {
            self.viewDatePicker.frame =    CGRect(x: 0, y: self.tableView.frame.origin.y + self.tableView.frame.size.height, width: self.view.frame.width, height: 250)
            
        }, completion: { (test) in
            
        })

        
        self.searchPageManager = HSPageManager(pageNumber: 0, pageSize: 50)

        self.searchPageManager?.setPagination({ (pageManager, page, pageSize) -> Void in
            
            let nextOfSet: Int  = (page*pageSize)
            
            if  self.refreshController!.isRefreshing != true {
                HSProgress(.show)
            }
            
            let searchText: String = self.searchTextField!.text!;
            var searchDate: String = ""
            if self.selectedDateString != nil {
                 searchDate = self.selectedDateString!
            }
            
            
            self.list.removeAll()
            self.tableView.reloadData()
            Message.searchMessagesFor(searchText, searchDate: searchDate, pageManager: pageManager, pageNumber: nextOfSet, pageSize: pageSize, callBack: { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                HSProgress(.hide)
                self.refreshController!.endRefreshing()
                
                if isSuccess == true {
                    if let list: [Message]? = data as? [Message] {
                        self.list.append(contentsOf: list!)
                    }
                    self.tableView.reloadData()
                }
                
                
            })
            
            }, success: { (pageManager, data) -> Void in
                
                
            }, failed: { () -> Void in
                
        })

    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        
        self.refreshController = UIRefreshControl()
        self.refreshController!.tintColor = APP_ThemeColor
        self.refreshController?.addTarget(self, action: #selector(MessagesViewController.refreshControllerValueChanged(_:)), for: .valueChanged)
       // self.tableView.addSubview(self.refreshController!);

        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false

        // Do any additional setup after loading the view.
        var nib: UINib = UINib(nibName: "MessageCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MessageCell")
        
        nib = UINib(nibName: "TextFieldCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "TextFieldCell")
        
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        
        self.pageManager = HSPageManager(pageNumber: 0, pageSize: 20)
        self.pageManager?.setPagination({ (pageManager, page, pageSize) -> Void in
            
            let nextOfSet: Int  = ((page*pageSize) + 1)
            
            if  self.refreshController!.isRefreshing != true {
                HSProgress(.show)
            }
            
            Message.getMessages(pageManager, pageNumber: nextOfSet, pageSize: pageSize, callBack: { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                HSProgress(.hide)
                self.refreshController!.endRefreshing()
                
                if isSuccess == true {
                    if let list: [Message]? = data as? [Message] {
                        self.list.append(contentsOf: list!)
                    }
                    self.tableView.reloadData()
                }
                
            })

            }, success: { (pageManager, data) -> Void in
               
                
            }, failed: { () -> Void in
                
         })
        
        
     
    
        self.searchTextField.delegate = self;
        // self.txtFieldFilter.delegate = self;
        self.searchTextField!.addRightLeftOnKeyboardWithTarget(self, leftButtonTitle: "Cancel", rightButtonTitle: "Search", rightButtonAction: #selector(MessagesViewController.keyboardSearchButtonClicked), leftButtonAction: #selector(MessagesViewController.keyboardCancelButtonClicked))

        self.searchTextField.placeholder = "Search Messages"
        self.searchTextField.addLeftImage( UIImage(named: "search")!, minimumSize: CGSize(width: 30, height: 30))

        
        
        
    }

    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.viewDatePicker.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func createDatePickerView()
    {
        
        viewDatePicker.removeFromSuperview()
        
        viewDatePicker.frame = CGRect(x: 0, y: tableView.frame.origin.y + tableView.frame.size.height, width: self.view.frame.width, height: 250)
        
          viewLine.frame = CGRect(x: 0, y: 0, width: viewDatePicker.frame.width, height: 1)
        viewLine.backgroundColor = UIColor.black
        viewDatePicker.backgroundColor = UIColor.white
        
     
        self.datePicker.frame = CGRect(x: 0, y: 50, width: viewDatePicker.frame.size.width, height: 200)
        self.datePicker.addTarget(self, action: #selector(MessagesViewController.datePickerValueChanged(_:)), for: .valueChanged)
        self.datePicker.datePickerMode = .date
        
        
        btnSearch.frame = CGRect(x: self.view.frame.width - 100, y: 0, width: 90, height: 50)
        btnSearch.setTitle("Search", for: .normal)
        btnSearch.setTitleColor(UIColor.black, for: .normal)
        btnSearch.addTarget(self, action:  #selector(MessagesViewController.keyboardSearchButtonClicked), for:  .touchUpInside)
        viewDatePicker.addSubview(btnSearch)
        btnCancel.frame = CGRect(x:  10 , y: 0, width: 90, height: 50)
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(UIColor.black, for: .normal)
        btnCancel.addTarget(self, action:  #selector(MessagesViewController.cancelButtonClicked), for:  .touchUpInside)
        viewDatePicker.addSubview(btnCancel)
        
        
        viewDatePicker.addSubview(self.datePicker)
        viewDatePicker.addSubview(self.viewLine)

        self.view.addSubview(viewDatePicker)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - actions
    
    func deleteMessage(_ cell: MessageCell) {
        
        let indexPath: IndexPath = self.tableView.indexPath(for: cell)!
        HSShowActionSheet("Delete?", message: "", cancel: "Cancel", destructive: "Delete", actions: nil) { (cancel, destruction, index) -> () in
            
            if cancel == true {
                
            }else if destruction == true {
                
                let message: Message = self.list[indexPath.row]
                
                message.deleteMessage({ (isSuccess, data, error) -> () in
                    if isSuccess == true {
                        self.list.remove(at: indexPath.row)
                        self.tableView.reloadData()
                    }
                })
                
                
            }
            
        }
        
        
    }
    
    // MARK: - ScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contentY: Int = Int(scrollView.contentOffset.y)
        let scrollHeight: Int = Int(scrollView.contentSize.height - scrollView.bounds.size.height)
        
                if (contentY >= scrollHeight){
                    
                    if self.searching {
                        if self.pageManager!.isReachedLastPage == false {
                            self.pageManager?.fetchNextpage()
                        }
                        
                    }else {
                        if self.searchPageManager == nil {
                            return;
                        }
                        if self.searchPageManager!.isReachedLastPage == false {
                            self.searchPageManager!.fetchNextpage()
                        }
                    }
                }
    }

    // MARK: - UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.searching = true
        self.list.removeAll()

        self.tableView.reloadData()
        
        UIView.animate(withDuration: 0.4, animations: {
            self.viewDatePicker.frame =    CGRect(x: 0, y: self.tableView.frame.origin.y + self.tableView.frame.size.height , width: self.view.frame.width, height: 250)
            
        }, completion: { (test) in
            
        })

        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true;
    }

    // MARK: - SearchingFunctinality
    
    func datePickerValueChanged(_ datePicker:UIDatePicker) {
        
        var date:Date = self.datePicker.date
            date = (date as NSDate).atStartOfDay()

        
        if ((date as NSDate).isLaterThanDate(Date())) {
        
            self.datePicker.setDate( Date(), animated: true)
            self.selectedDate =  self.datePicker.date
        
        }else {
            
          self.selectedDate = date
          let dateFormate:DateFormatter = DateFormatter()
          dateFormate.dateFormat = Message.searchDateFormate()
          let dateString: String = dateFormate.string(from: self.selectedDate!)
          self.selectedDateString = dateString;
        
        }
        
    }
    
    func keyboardCancelButtonClicked() {
       // self.searchTextField!.resignFirstResponder()
        
        
        self.cancelButtonClicked()
    }
    
    func keyboardSearchButtonClicked() {
        self.startSearching()
    }
    
    @IBAction func searchButtonClicked() {
        
        self.selectedDate = self.datePicker.date
        let dateFormate:DateFormatter = DateFormatter()
        dateFormate.dateFormat = Message.searchDateFormate()
        let dateString: String = dateFormate.string(from: self.selectedDate!)
        self.selectedDateString = dateString;

        self.searching = true
        self.searchTextField!.resignFirstResponder()
        
        UIView.animate(withDuration: 0.4, animations: {
        self.viewDatePicker.frame =    CGRect(x: 0, y: self.tableView.frame.origin.y + self.tableView.frame.size.height - 250, width: self.view.frame.width, height: 250)
            
        }, completion: { (test) in
            
        })
        
      //  self.txtFieldFilter!.inputView = self.datePicker
       
        self.tableView.reloadData()
      //   self.txtFieldFilter!.becomeFirstResponder()
    }
    
     @IBAction func cancelButtonClicked() {
        
        self.searching = false
        self.searchTextField!.resignFirstResponder()
        self.selectedDate = nil
        self.selectedDateString = nil
        self.searchTextField!.inputView = nil
        self.searchPageManager = nil
        self.searchList.removeAll()
        self.searchTextField!.text = ""
        self.tableView.reloadData()
        
        
        
        UIView.animate(withDuration: 0.4, animations: {
            self.viewDatePicker.frame =    CGRect(x: 0, y: self.tableView.frame.origin.y + self.tableView.frame.size.height, width: self.view.frame.width, height: 250)
            
        }, completion: { (test) in
            
        })
        self.list.removeAll()
        self.pageManager!.refreshPaginator()
        self.tableView.reloadData()


    }

    // MARK: - TAble

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 0
        }else {
            return self.list.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
            
            if self.searchTextField == nil {
                
                self.searchTextField = cell.textField;
//                self.searchTextField!.addRightLeftOnKeyboardWithTarget(self, leftButtonTitle: "Cancel", rightButtonTitle: "Search", rightButtonAction: "keyboardSearchButtonClicked", leftButtonAction: "keyboardCancelButtonClicked")
                self.searchTextField!.delegate = self;

            }
            
            cell.cancelButtonCallBack = {(cancel: Bool) -> Void in
                self.cancelButtonClicked()
            }
            
            cell.searchButtonCallBack = {(cancel: Bool) -> Void in
                self.searchButtonClicked()
                
            }
            
            return cell
            
        }else {
            
            let cell: MessageCell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
            let message = self.list[indexPath.row]
            cell.messageLabel.numberOfLines = 3;
            cell.updateCell(message)
            cell.messageDeleteCallBack = {(cell: MessageCell) -> () in
                self.deleteMessage(cell)
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 45.0
        }else {
            let message = self.list[indexPath.row]
            return MessageCell.heightForFeed(message, size: tableView.frame.size, fullDetail: false)
        }
        
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.marginWillShow(false)
        tableView.marginWillShow(false)
        cell.selectionStyle = .none
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let messageDetailViewController: MessageDetailViewController = MessageDetailViewController(nibName: "MessageDetailViewController", bundle: nil)
        messageDetailViewController.message = self.list[indexPath.row]
        self.navigationController?.pushViewController(messageDetailViewController, animated: true)
        
        messageDetailViewController.messageDeleteFromDetailCallBack = { (messageDelete: Bool, message: Message) -> Void in
            
            if messageDelete == true {
                let index: Int = self.list.index(of: message)!
                self.list.remove(at: index)
                self.tableView.reloadData()
            }
            
        }
        
    }

}
