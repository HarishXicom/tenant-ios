//
//  MapViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 20/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate
{
    @IBOutlet var mapView: MKMapView!
    weak var property: Property? = nil
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.showLocationOnMap()
    }
    
    func backButtonClicked() {
        self.navigationController!.popViewController(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.mapView.delegate = self;
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(MapViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle(self.property!.propertyAddress! as NSString, color: UIColor.white, font: UIFont(name: APP_FontName, size: 13)!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showLocationOnMap() {
        
        let userLocation: CLLocation? = self.property!.coordinates!.location()
        if userLocation == nil {
            HSShowAlertView("TenantTag", message: "User location not available")
            return;
        }
        
        let location = CLLocationCoordinate2DMake(userLocation!.coordinate.latitude, userLocation!.coordinate.longitude)
       
        
        let region: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(location, 800, 800)
        self.mapView.setRegion(region, animated: true)
        self.mapView.centerCoordinate = userLocation!.coordinate
        self.addPin()
        
    }
    
    func addPin() {
        
//        let region: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2D, <#T##latitudinalMeters: CLLocationDistance##CLLocationDistance#>, <#T##longitudinalMeters: CLLocationDistance##CLLocationDistance#>)
//               MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//               [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
       
        let annotations: [MKAnnotation] = self.mapView.annotations;
        self.mapView.removeAnnotations(annotations)
        
        let userLocation: CLLocation? = self.property!.coordinates!.location()!
        if userLocation == nil {
            return;
        }
        
        let point: MKPointAnnotation = MKPointAnnotation()
        point.coordinate = userLocation!.coordinate
        point.title = "Address";
        point.subtitle = self.property!.propertyAddress;
        self.mapView .addAnnotation(point)
        
        let location = CLLocationCoordinate2DMake(userLocation!.coordinate.latitude, userLocation!.coordinate.longitude)
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
