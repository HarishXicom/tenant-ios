//
//  InviteLandLordVC.swift
//  TenantTag
//
//  Created by maninder on 5/11/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit

class InviteLandLordVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,LTLinkTextViewDelegate {
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var viewInner: UIView!

    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var txtFieldName: UITextField!
    @IBOutlet var txtViewInfo: UITextView!
    
    @IBOutlet var viewWithLabel: LTLinkTextView!
    @IBOutlet var btnSend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        self.view.backgroundColor = UIColor.white

        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(InviteLandLordVC.actionBtnBackPressed), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("Invite your lanlord" , color: UIColor.white, font: UIFont(name: APP_FontRegularNew, size: 15)!)
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        btnSend.cornerRadius(4)
        
        viewOuter.cornerRadius(5)
        viewInner.cornerRadius(4)
        
       
        txtFieldName.delegate = self
        txtFieldEmail.delegate = self
        
        
        
        viewWithLabel.delegate = self
        
        let paragraphStyle : NSMutableParagraphStyle  = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = 5
        
        let txtAttributes = [LTTextStringParameterKey : "Hi, your tenant would like to pay their rent using TenantTag.", NSFontAttributeName : UIFont(name: APP_FontRegularNew, size: 15)! ] as [String : Any]
        
        let btnAttributes = [[LTTextStringParameterKey : "TenantTag", NSForegroundColorAttributeName : APP_ThemeColor ]]
        viewWithLabel.setStringAttributes(txtAttributes, withButtonsStringsAttributes: btnAttributes)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func actionBtnBackPressed()
    {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func actionBtnSendPressed(_ sender: Any) {
        if txtFieldName.text?.isEMPTY() == true{
            HSShowAlertView(APP_Name as NSString, message: "Please enter your name.")
        }else if  txtFieldEmail.text?.isEMPTY() == true
        {
            HSShowAlertView(APP_Name as NSString, message: "Please enter your landlord email.")
            
        }else if txtFieldEmail.text?.isValidEmail() == false{
            
            HSShowAlertView(APP_Name as NSString, message: "Please enter valid email address.")
            
        }else{
            
            
      LoginManager.share().sendInvitationToLandlord(name: txtFieldName.text!, email: txtFieldEmail.text!, info: txtViewInfo.text!, callBack: { (isSuccess, response, error) in
        if isSuccess{
            self.dismiss(animated: true, completion: nil)
            self.dismiss(animated: true, completion: {
                HSShowAlertView(APP_Name as NSString, message: "Invitation has been sent to your landlord.")
            })
        }
          })
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- LTLinkTextViewDelegate
    //MARK:-
    
    func linkTextView(_ termsTextView: LTLinkTextView!, didSelectButtonWith buttonIndex: UInt, title: String!) {
        
    openWebsiteInAppBrowser(controller: self)
      //  https://www.tenanttag.com
        
    }
    

}
