//
//  PhotoPreviewViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 01/03/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class PhotoPreviewViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var deleteButton: UIButton!
    weak var photo: HSPhoto? = nil;
    var deleteImgPreviewViewCallBack:((HSPhoto)->Void)? = nil

    @IBAction func deleteButtonClicked(_ sender: AnyObject) {
    
      HSShowActionSheet(nil, message: "Delete image?", cancel: "Cancel", destructive: "Delete", actions: nil) { (cancel, destruction, index) -> () in
            if destruction == true {
                self.dismiss(animated: true, completion: { () -> Void in
                    if self.deleteImgPreviewViewCallBack != nil {
                        self.deleteImgPreviewViewCallBack!(self.photo!)
                    }
                })
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(PhotoPreviewViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle("Preview", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)
        self.navigationController!.navigationBar.barTintColor = APP_ThemeColor
        self.navigationController!.navigationBar.isTranslucent = false;
        
        self.imageView.isUserInteractionEnabled = true;
        self.imageView.contentMode = .scaleAspectFit
        self.scrollView.minimumZoomScale = 1.0;
        self.scrollView.maximumZoomScale = 5.0;
        self.scrollView.delegate = self;
       // self.scrollView.layer.cornerRadius = CornerRadius;
        
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PhotoPreviewViewController.tapTwice(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 2
        self.imageView.addGestureRecognizer(tapGestureRecognizer)

        // Do any additional setup after loading the view.
        if self.photo  != nil {
            
            let URL: Foundation.URL = photo!.URL as URL;
            let image: UIImage? = photo!.image;
            
            if  image != nil {
                self.imageView.setImageWith(URL, placeholderImage: image)
            }else {
                self.imageView.setImageWith(URL, placeholderImage: nil)
            }
      
        }
        
    }
    
    func backButtonClicked() {
        
        self.navigationController!.dismiss(animated: true) { () -> Void in
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - ScrollView

    func tapTwice( _ gesture: UIGestureRecognizer)
    {
    
    if (self.scrollView.zoomScale == 1.0) {
        let zoomScale: CGFloat = 2.0;
        var zoomRect: CGRect = CGRect.zero;
        let center: CGPoint = gesture.location(in: self.scrollView)
        
        zoomRect.size.height = self.scrollView.frame.size.height / zoomScale;
        zoomRect.size.width  = self.scrollView.frame.size.width  / zoomScale;
    
        // choose an origin so as to get the right center.
        zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
        //on a double tap, call zoomToRect in UIScrollView
        self.scrollView.zoom(to: zoomRect, animated: true)
    
    }else{
    
        let zoomScale: CGFloat = 1.0;
        var zoomRect: CGRect = CGRect.zero;
        let center: CGPoint = gesture.location(in: self.scrollView)
        
        zoomRect.size.height = self.scrollView.frame.size.height / zoomScale;
        zoomRect.size.width  = self.scrollView.frame.size.width  / zoomScale;
    
        // choose an origin so as to get the right center.
        zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
        //on a double tap, call zoomToRect in UIScrollView
        self.scrollView.zoom(to: zoomRect, animated: true)
    
    }
    
    }

    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView;

    }
    

  
    
}
