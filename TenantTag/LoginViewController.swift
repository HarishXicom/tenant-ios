//
//  LoginViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 31/12/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit



class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var btnInvite: UIButton!
    @IBOutlet var scroollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var emailBGView: UIView!
    @IBOutlet  var emailAddressTextField: UITextField!
    @IBOutlet  var passwordTextField: UITextField!
    @IBOutlet weak var passBGView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    
    
    func userCheckOut(_ obj: AnyObject?) {
        
        LoginManager.share().logout { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if isSuccess == true {
                self.navigationController!.popToRootViewController(animated: true)
                HSShowAlertView("TenantTag", message: "You have moved-out.Please login again to continue.", controller: nil)
            }
        }
        
    }
    
    //MARK:- View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        if self.passwordTextField.isFirstResponder {
            self.passwordTextField.resignFirstResponder()
        }
        
        if self.emailAddressTextField.isFirstResponder {
            self.emailAddressTextField.resignFirstResponder()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if self.passwordTextField.isFirstResponder {
            self.passwordTextField.resignFirstResponder()
        }
        
        if self.emailAddressTextField.isFirstResponder {
            self.emailAddressTextField.resignFirstResponder()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      self.navigationController!.isNavigationBarHidden = true

        let sessionID: String = LoginManager.getMe.sessionID
        
        
        if (sessionID.length > 0) {
            self.openMenuViewController(false)
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
      //  IQKeyboardManager.sharedManager().enable = false

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;
        self.navigationController!.isNavigationBarHidden = true


        self.emailAddressTextField.delegate = self;
        self.passwordTextField.delegate = self;

        self.emailAddressTextField.cornerRadius(15.0)
        self.emailAddressTextField.layer.borderColor = APP_ThemeColor.cgColor
        self.emailAddressTextField.layer.borderWidth = 1.0
        self.emailAddressTextField.keyboardType = .emailAddress
        self.emailAddressTextField.returnKeyType = .next

        self.passwordTextField.cornerRadius(15.0)
        self.passwordTextField.layer.borderColor = APP_ThemeColor.cgColor
        self.passwordTextField.layer.borderWidth = 1.0
        self.passwordTextField.returnKeyType = .go

        
        self.passwordTextField.isSecureTextEntry = true

        self.loginButton.cornerRadius(15.0)
        self.loginButton.backgroundColor = APP_ThemeColor
        self.loginButton.setTitleColor(UIColor.white, for: UIControlState())

        self.forgotButton.setTitle("Forgot your password?", for: UIControlState())
        self.forgotButton.setTitleColor(UIColor.lightGray, for: UIControlState())
        
        
        self.btnInvite.underLineButton(text: "Tell your landlord about TenantTag", color: APP_ThemeColor)
        
        
     

        
//        self.emailAddressTextField.text = "anirudh@xicom.biz"
//        self.passwordTextField.text = "111111"
        

            NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.userCheckOut(_:)), name: NSNotification.Name(rawValue: kUserCheckOut), object: nil)
    

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //self.loginButton.cor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    //MARK:- TextFieldDelegate

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailAddressTextField {
          self.passwordTextField.becomeFirstResponder()
        }else {
            self.passwordTextField.resignFirstResponder()
            self.loginButtonClicked(self.loginButton)
        }
        return true
    }
    
    //MARK:- Actions

    @IBAction func forgotButtonClicked(_ sender: AnyObject) {
       
        let forgotPasswordViewController: ForgotPasswordViewController = ForgotPasswordViewController.init(nibName: "ForgotPasswordViewController", bundle: nil)
        let navControlelr: UINavigationController = UINavigationController(rootViewController: forgotPasswordViewController)
        self.present(navControlelr, animated: true) { () -> Void in
            
        }

    }
    
    
    @IBAction func actionTellYourlandLord(_ sender: Any) {
        let InviteVC: InviteLandLordVC = InviteLandLordVC.init(nibName: "InviteLandLordVC", bundle: nil)
        let navControlelr: UINavigationController = UINavigationController(rootViewController: InviteVC)

        self.present(navControlelr , animated: true) { () -> Void in
        }
        
    }
    

    
    func openMenuViewController(_ animation:Bool) {
        
        if  LoginManager.getMe.verified == true {
            
            if  LoginManager.getMe.moveStatus == .out {
                
                let checkOutMainViewController: CheckOutMainViewController = CheckOutMainViewController()
                let mainNavControlelr: UINavigationController = UINavigationController(rootViewController: checkOutMainViewController)
                mainNavControlelr.navigationBar.barTintColor = APP_ThemeColor
                mainNavControlelr.navigationBar.tintColor = UIColor.white
                
                let leftMenuViewController: LeftMenuViewController = LeftMenuViewController.init(nibName: "LeftMenuViewController", bundle: nil)
                let revealViewController = SWRevealViewController(rearViewController: leftMenuViewController, frontViewController: mainNavControlelr)
                //leftMenuViewController.revealViewController = revealViewController
                revealViewController?.rearViewRevealWidth = self.view.frame.width - 80
                //revealViewController?.delegate = self

                self.navigationController!.isNavigationBarHidden = true
                self.navigationController!.pushViewController( revealViewController!, animated: animation)
            }else {
                
                let mainContainerViewController: MainContainerViewController = MainContainerViewController.init(nibName: "MainContainerViewController", bundle: nil)
                let mainNavControlelr: UINavigationController = UINavigationController(rootViewController: mainContainerViewController)
                mainNavControlelr.navigationBar.barTintColor = APP_ThemeColor
                mainNavControlelr.navigationBar.tintColor = UIColor.white
                
                let leftMenuViewController: LeftMenuViewController = LeftMenuViewController.init(nibName: "LeftMenuViewController", bundle: nil)
                let revealViewController = SWRevealViewController(rearViewController: leftMenuViewController, frontViewController: mainNavControlelr)
                //leftMenuViewController.revealViewController = revealViewController
                revealViewController?.rearViewRevealWidth = self.view.frame.width - 80
              //  revealViewController?.delegate = self
              //  revealViewController?.frontViewPosition = FrontViewPosition.left
                self.navigationController!.isNavigationBarHidden = true
                self.navigationController!.pushViewController( revealViewController!, animated: animation)
                
            }
            
        }else {
            
            let landlordAcceptViewController = LandlordAcceptViewController.init(nibName: "LandlordAcceptViewController", bundle: nil)
            self.navigationController!.pushViewController(landlordAcceptViewController, animated: true)
            landlordAcceptViewController.verfiedSuccess = {(success: Bool) -> () in
                LoginManager.getMe.verified = true
                LoginManager.share().saveUserProfile()
              self.openMenuViewController(true)
            }
        }
        

    }
    
    
 
    
    


    @IBAction func loginButtonClicked(_ sender: AnyObject) {
        
     
        let emailAddress: NSString = self.emailAddressTextField.text! as NSString
        if emailAddress.isEmpty() {
            HSShowAlertView("TenantTag", message: "Please enter username or email.",controller: self)
            return
        }
        

        
        let pass:NSString = self.passwordTextField.text! as NSString
        if pass.isEmpty() {
            HSShowAlertView("TenantTag", message: "Please enter password!",controller: self)
            return
        }
        
        self.view.endEditing(true)
        let me: Me = LoginManager.getMe
        me.emailAddress = emailAddress as String
        me.password = pass as String

        
        
        LoginManager.share().login { (success:Bool, data:AnyObject?, error:NSError?) -> () in
            
            if success {
                    self.emailAddressTextField.text! = ""
                    self.passwordTextField.text = ""
                    self.openMenuViewController(true)
            }
        }

    }
}
