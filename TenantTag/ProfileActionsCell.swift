//
//  ProfileActionsCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 29/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class ProfileActionsCell: UITableViewCell {

    @IBOutlet var saveButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var optionBGView: UIView!
    @IBOutlet var editBGView: UIView!
    
    var actionCallBack: ((_ edit: Bool, _ cancel: Bool, _ save: Bool) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func editingMode(_ editionOn: Bool) {
        
        if editionOn == true {
            
            self.editButton.isSelected = true
            self.saveButton.isSelected = false
            self.editBGView.alpha = 0.0;
            self.optionBGView.alpha = 1.0;

        }else {
            
            self.saveButton.isSelected = true
            self.editButton.isSelected = false
            
            self.editBGView.alpha = 1.0;
            self.optionBGView.alpha = 0.0;
        }
        
    }

    
    @IBAction func editButtonClicked(_ sender: AnyObject) {
        if self.editButton.isSelected == true {
            return;
        }
        
        self.editButton.isSelected = true
        self.saveButton.isSelected = false
        
        self.editBGView.alpha = 0.0;
        self.optionBGView.alpha = 1.0;
        self.actionCallBack!(true,false,false);
        
        
    }
    @IBAction func cancelButtonClicked(_ sender: AnyObject) {
        
        if self.saveButton.isSelected == true {
            return;
        }
        
        self.saveButton.isSelected = true
        self.editButton.isSelected = false
        
        self.editBGView.alpha = 1.0;
        self.optionBGView.alpha = 0.0;
        self.actionCallBack!(false,true,false);

    }
    
    @IBAction func saveButtonClicked(_ sender: AnyObject) {
        
        if self.saveButton.isSelected == true {
            return;
        }
        
//        self.saveButton.selected = true
//        self.editButton.selected = false
//        
//        self.editBGView.alpha = 0.0;
//        self.optionBGView.alpha = 0.0;
        
        self.actionCallBack!(false,false,true);
        
    }
    
    
}
