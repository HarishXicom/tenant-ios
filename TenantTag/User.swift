//
//  User.swift
//  TenantTag
//
//  Created by Hardeep Singh on 06/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

enum MoveStatus: Int {
    case `in` = 1,
    out
}

class User: NSObject {

    var ID: String!
    var firstName: String? = nil
    var lastName: String? = nil
    var emailAddress: String? = nil
    var phoneNumber: String? = nil
    var landlordNumber: String? = nil;
    var language: String? = nil
    var brithdayStr: String? = nil
    var brithdayDate: Date? = nil
    var companyName: String? = nil
    var verified: Bool = false
    var moveStatus: MoveStatus = .in
    

    
    var fullName: String {
        
        get {
            let fName = firstName != nil ? firstName : ""
            let lName = lastName != nil ? lastName : ""
            return "\(fName!) \(lName!)"
        }
    }

    //MARK: -

    override init() {
        super.init()
        
        self.ID = ""
        self.firstName = ""
        self.lastName = ""
        self.emailAddress = ""
        self.phoneNumber = ""
        self.language = ""
        self.brithdayStr = ""
        self.brithdayDate = Date()
        self.companyName = ""
        self.landlordNumber = ""
    }
    
    
    func updateUser(_ obj: AnyObject) {
        
        let dict: Dictionary? = obj as? Dictionary<String,AnyObject>
        if dict == nil {
            return
        }
        
        //
        let ID: String = dict!["mem_id"] as! String
        self.ID = ID
        
        let email = dict!["email"] as! String
        self.emailAddress = email
        
        let first_name = dict!["first_name"] as? String
        self.firstName = first_name
        
        let last_name = dict!["last_name"] as? String
        self.lastName = last_name
        
        let company_name = dict!["company_name"] as? String
        self.companyName = company_name
        
        let dob = dict!["dob"] as? String
        if dob != nil {
            
            let date = Date(timeIntervalSince1970:Double(dob!)!)
            self.brithdayDate = date
            let dateFormate: DateFormatter = DateFormatter()
             dateFormate.dateFormat = APP_DOBDateFormate
            let dateStr: String? = dateFormate.string(from: date)
            if dateStr != nil {
                self.brithdayStr = dateStr
            }
        }
        
        
        let mobile_no = dict!["mobile_no"] as? String
        self.phoneNumber = mobile_no
        
        let landlord_number = dict!["landlord_number"] as? String
        self.landlordNumber = landlord_number
        
        let app_verified: String? = dict!["app_verified"] as? String
        if app_verified != nil {
            if app_verified == "1" {
                self.verified = true
            }else {
                self.verified = false
            }
        }else {
            self.verified = true
        }
        
        let move_status: String? = dict!["move_status"] as? String
        if move_status != nil {
            self.moveStatus = MoveStatus(rawValue: Int(move_status!)!)!
            //self.moveStatus = MoveStatus.Out
        }

    }
    
    //MARK: - Display
    func displayName() -> String {
      //  self.fullName.capitalized
        return  self.fullName.capitalizedFirstChr
    }
    
   


    
}
