//
//  HomeListCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class HomeListCell: UITableViewCell {

    @IBOutlet weak var containerBGView: UIView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var phnImgView: UIImageView!
    @IBOutlet var phnImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    var service: Service? = nil;
    
    func updateCell( _ service: Service?) {
        self.service = service
        
        if self.service != nil {
            self.label1.text = self.service!.serviceName;
            self.label2.text = self.service!.serviceProvider;

            if self.service!.phone != nil {
                if self.service!.phone!.isEmpty == false {
                    self.label3.isHidden = false
                    self.phnImgView.isHidden = false
                    self.phnImgHeight.constant = 20.0
                    self.label3.text = self.service!.phone;

                }else {
                    self.label3.isHidden = true
                    self.phnImgView.isHidden = true
                    self.phnImgHeight.constant = 0.0
                    self.label3.text = ""
                }
            }else {
                self.label3.isHidden = true
                self.phnImgView.isHidden = true
                self.phnImgHeight.constant = 0.0
                self.label3.text = ""
            }
            
            self.label1.isHidden = false
            self.label2.isHidden = false
            
        }else {
            
            self.label1.text = ""
            self.label2.text = ""
            self.label3.text = ""
            
            self.label1.isHidden = true
            self.label2.isHidden = true
            self.label3.isHidden = true
            self.phnImgView.isHidden = true
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.containerBGView.backgroundColor = UIColor.white
        self.label1.font = UIFont(name: APP_FontName, size: 13)
        self.label2.font = UIFont(name: APP_FontName, size: 15)
        self.label3.font = UIFont(name: APP_FontName, size: 15)
        self.label3.textColor = APP_ThemeColor;

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            
            let point:CGPoint = touch.location(in: self)
            if  self.label3.frame.contains(point)  {
                self.label3.textColor = UIColor.blue
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.label3.textColor = APP_ThemeColor
        
        if let touch = touches.first {
            let point:CGPoint = touch.location(in: self)
            if  self.label3.frame.contains(point)  {
                if self.service!.phone != nil {
                    
                    let phnNumber = self.service!.phone!.phoneNumberWithoutFormate()
                    let URLStr = "telprompt://\(phnNumber)"
                    let URL: Foundation.URL? = Foundation.URL(string: URLStr)
                    if URL == nil {
                        UIAlertView(title: nil, message: "Phone number is not valid.", delegate: nil, cancelButtonTitle: "Ok").show()
                        return;
                    }
                    
                    if UIApplication.shared.canOpenURL(URL!) {
                        UIApplication.shared.openURL(URL!)
                    }else {
                        UIAlertView(title: nil, message: "Your device doesn't support this feature.", delegate: nil, cancelButtonTitle: "Ok").show()
                    }
                    
                }
            }
        }
        
        
    }

    
}
