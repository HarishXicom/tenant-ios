//
//  HSViewExtension.swift
//  Who's In
//
//  Created by Hardeep Singh on 21/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UILabel

extension UILabel {
    
}

extension UIImageView {
    
    public func round( _ borderColor: UIColor?, borderWidth: Float? ) {
       
        let view: UIImageView = self
        view.clipsToBounds = true
        view.layer.cornerRadius = (view.frame.width/2.0)
       
        if borderWidth == nil {
            view.layer.borderWidth = 0.7
        }else{
            view.layer.borderWidth = CGFloat(borderWidth!)
        }
        
        if borderColor == nil {
            view.layer.borderColor = UIColor.lightGray.cgColor
        }else{
            view.layer.borderColor = borderColor!.cgColor
        }

    }
    
}

//MARK: - UIVIEW

extension UIView {

    public func cornerRadius(_ value: CGFloat){
        let view:UIView = self
        view.layer.cornerRadius = value;
    }
    
}

//MARK: - UITableView
extension UITableView {
    
    public func marginWillShow(_ show:Bool) {
        
        if self.responds(to: #selector(getter: UIView.layoutMargins)) {
            
            if #available(iOS 8.0, *) {

            self.layoutMargins = UIEdgeInsets.zero;
                
            }
        }
        
        if self.responds(to: #selector(getter: UITableViewCell.separatorInset))  {
            self.separatorInset = UIEdgeInsets.zero;
        }
        
    }

    public func indexPathForSubView(_ subview:UIView) -> IndexPath {
        let tableView:UITableView = self
        let location:CGPoint =  (subview.superview?.convert(subview.center, to: tableView))!
        let indexPath:IndexPath = tableView.indexPathForRow(at: location)!
        return indexPath
    }

}

//MARK: - UITableViewCell
extension UITableViewCell {
    
    public func marginWillShow(_ show:Bool) {
        
        if show == false {
            
            if self.responds(to: #selector(getter: UIView.layoutMargins)) {
                if #available(iOS 8.0, *) {
                    self.layoutMargins = UIEdgeInsets.zero
                } else {
                    // Fallback on earlier versions
                }
            }
            
            if self.responds(to: #selector(getter: UITableViewCell.separatorInset)) {
                self.separatorInset = UIEdgeInsets.zero;
            }

            
        }
        
    }

}

//MARK: - UITextField

extension UITextField {
    
    public func placeHolderTextColor(_ color: UIColor){
        let textField: UITextField = self
        if textField.isKind(of: UITextField.self) {
            textField.setValue(color, forKeyPath: "_placeholderLabel.textColor")
        }
    }
    
    public func addLeftImage(_ image: UIImage){
        let textField: UITextField = self
        if textField.isKind(of: UITextField.self) {
            let imageView:UIImageView = UIImageView(image: image)
            imageView.sizeToFit()
             textField.leftView = imageView
            textField.leftViewMode = UITextFieldViewMode.always
        }
    }
    
    public func addLeftImage(_ image: UIImage, minimumSize:CGSize){
        let textField: UITextField = self
        if textField.isKind(of: UITextField.self) {
            let imageView: UIImageView = UIImageView(image: image)
            imageView.contentMode = UIViewContentMode.center
            imageView.clipsToBounds = true
            imageView.frame = CGRect(x: 0, y: 0, width: minimumSize.width, height: minimumSize.height)
            textField.leftView = imageView
            textField.leftViewMode = UITextFieldViewMode.always
        }
    }

    
    public func addRightImage(_ image: UIImage){
        let textField: UITextField = self
        if textField.isKind(of: UITextField.self) {
            let imageView: UIImageView = UIImageView(image: image)
            imageView.sizeToFit()
            imageView.contentMode = UIViewContentMode.center
            imageView.clipsToBounds = true
            textField.rightView = imageView
            textField.rightViewMode = UITextFieldViewMode.always
        }
    }
    
    
    public func addRightImage(_ image: UIImage, minimumSize:CGSize){
        let textField: UITextField = self
        if textField.isKind(of: UITextField.self) {
            let imageView: UIImageView = UIImageView(image: image)
            imageView.contentMode = UIViewContentMode.center
            imageView.clipsToBounds = true
            imageView.frame = CGRect(x: 0, y: 0, width: minimumSize.width, height: minimumSize.height)
            textField.rightView = imageView
            textField.rightViewMode = UITextFieldViewMode.always
        }
    }
    
    
}
