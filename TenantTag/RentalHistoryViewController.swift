//
//  RentalHistoryViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 25/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class RentalHistoryViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    var landlord: User? = nil;
    var property: Property? = nil
    var leaseDetail: LeaseDetail? = nil

    @IBOutlet var indicatorView: UIActivityIndicatorView!
    var titles: [String] = ["Address", "Landlord", "Period", "Rent Paid"]
    var images: [String] = ["address", "contact2", "clander2", "amount"]

    func showIndicatorView() {
        
            self.indicatorView.isHidden = false;
            self.indicatorView.hidesWhenStopped = true
           //self.indicatorView!.activityIndicatorViewStyle = .WhiteLarge;
            self.indicatorView!.tintColor = APP_ThemeColor
            self.indicatorView!.startAnimating()

    }
    
    func getRentalHistory() {
        
        //RentalHistory
        
        self.showIndicatorView()
        HTTPRequest.request().getRequest(APIRentalHistory as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            self.indicatorView!.stopAnimating()
            self.indicatorView.isHidden = true;
            
            if (isSuccess == false || error != nil) {
                return;
            }
            
            let dict: Dictionary? = data as? Dictionary<String,AnyObject>
            if dict != nil
            {
                
                let landlord: Dictionary? = dict!["landlord"] as? Dictionary<String,AnyObject>
                
                if landlord != nil {
                    let user: User = User()
                    user.updateUser(landlord! as AnyObject)
                    self.landlord = user;
                }
                
                let propertyDetails: Dictionary? = dict!["propertyDetails"] as? Dictionary<String,AnyObject>
                if propertyDetails != nil {
                    let property: Property = Property(obj: dict! as AnyObject) // Pass main object not propertyDetails
                    self.property = property;
                }
               
                
                let leaseDetails: Dictionary? = dict!["leaseDetails"] as? Dictionary<String,AnyObject>
                if leaseDetails != nil {
                    let leaseDetail: LeaseDetail = LeaseDetail(leaseObj: leaseDetails!)
                    self.leaseDetail = leaseDetail;
                }
                
//                let documentsAttached: [AnyObject]? = dict!["documentsAttached"] as? [AnyObject]

                self.tableView.reloadData()
                
            }
            
        }
        
    }

    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        // Do any additional setup after loading the view.
        let nib: UINib = UINib(nibName: "RentalHistoryCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "RentalHistoryCell")
        
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.separatorStyle = .singleLine
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: "popOneViewController", for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        
        self.navTitle("RENTAL HISTORY", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)

        self.getRentalHistory()
        
        let fview: UIView = UIView()
        fview.frame = CGRect(x: 0, y: 0, width: 20, height: 3.0)
        fview.backgroundColor = UIColor.clear
        self.tableView.tableFooterView = fview;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: - DeailString 
    
    func getDetailString(_ indexPath: IndexPath) -> String {
        
        var detailString = ""
        switch indexPath.section {
        case 0:
            detailString = "\(self.property!.propertyAddress!)"
            break;
            
        case 1:
            detailString = "\(self.landlord!.fullName)"
            break;
            
        case 2:
            detailString = "\(self.leaseDetail!.period!)"
            break;
            
        case 3:
            detailString = "\(self.leaseDetail!.amount!)"
            break;
            
        default:
            detailString = ""
            break;
            
        }
        
        return detailString
    }
    
    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.titles.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            
            if self.property != nil {
                return 1;
            }else {
                return 0;
            }
            
        case 1:
            
            if self.landlord != nil {
                return 1;
            }else {
                return 0;
            }
            
        case 2:
            
            if self.leaseDetail != nil {
                return 1;
            }else {
                return 0;
            }
            
        case 3:
            
            if self.leaseDetail != nil {
                return 1;
            }else {
                return 0;
            }
            
        default:
            
            return 0;

            
        }
        

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: RentalHistoryCell = tableView.dequeueReusableCell(withIdentifier: "RentalHistoryCell") as! RentalHistoryCell
        cell.label1.text = self.titles[indexPath.section];
        cell.imgView.image = UIImage(named:self.images[indexPath.section])
        
        let detailString = self.getDetailString(indexPath)
        cell.label2.text = detailString;
        return cell;
        
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
         let celll: RentalHistoryCell = cell as!  RentalHistoryCell
        if indexPath.section == 3 {
            celll.label2.textColor = APP_ThemeColor
        }else {
            celll.label2.textColor = UIColor.black
        }

    }
    
    class func heightForFeed( _ property: Property, size: CGSize, phnExist: Bool) ->(CGFloat) {
        
        let text: String = property.propertyAddress!;
        
        let topSpace: CGFloat = 8.0;
        let leadingSpace: CGFloat = 15.0;
        let trailingSpace: CGFloat = 8.0;
        let vPadding: CGFloat = 8.0;
        let hPadding: CGFloat = 8.0;
        let locationImageHeight: CGFloat = 25.0;
        let textWidth = (size.width - (leadingSpace + trailingSpace + locationImageHeight + (hPadding*3)))
        
        let font: UIFont =  UIFont(name: APP_FontName, size: 17)!
        let maxSize: CGSize = CGSize( width: CGFloat(textWidth), height: 9999.0)
        let boundingBox = text.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size.height
       
        let maxHeight = (locationImageHeight + (boundingBox + topSpace  + (vPadding*3)))
        return maxHeight
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            let height: CGFloat = RentalHistoryViewController.heightForFeed(self.property!, size: tableView.frame.size, phnExist: false)
            
            if height < 90.0 {
                return 90.0
            }else {
                return height
            }
            
        }

        return 90.0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if indexPath.section == 2 {
//            
//            if self.landlord!.phoneNumber != nil {
//                
//                let phnNumber = self.landlord!.phoneNumber!.phoneNumberWithoutFormate()
//                let URLStr = "telprompt://\(phnNumber)"
//                let URL: NSURL? = NSURL(string: URLStr)
//                if URL == nil {
//                    UIAlertView(title: nil, message: "Phone number is not valid.", delegate: nil, cancelButtonTitle: "Ok").show()
//                    return;
//                }
//                if UIApplication.sharedApplication().canOpenURL(URL!) {
//                    UIApplication.sharedApplication().openURL(URL!)
//                }else {
//                    UIAlertView(title: nil, message: "Your device doesn't support this feature.", delegate: nil, cancelButtonTitle: "Ok").show()
//                }
//            }
//
//        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
