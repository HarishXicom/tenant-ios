//
//  CheckOutMainViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 24/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class CheckOutMainViewController: UIViewController {
    @IBOutlet var lblBGView: UIView!
    @IBOutlet var label: UIView!

    @IBOutlet var contactButton: UIButton!
    @IBOutlet var documentButton: UIButton!
    @IBOutlet var retalHistroy: UIButton!
    var property: Property? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        self.navigationController?.navigationBar.isTranslucent = false;
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "menu"), for: UIControlState())
        btn.addTarget(self, action: #selector(UIViewController.presentLeftMenuViewController as (UIViewController) -> () -> Void), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        
        let labelWelcom: UILabel = UILabel()
        labelWelcom.text = "Welcome Home"
        labelWelcom.textAlignment = .right
        labelWelcom.font = UIFont(name: APP_FontName, size: 10)
        labelWelcom.textColor = UIColor.white
        labelWelcom.sizeToFit()
        // let WelcomBarButton: UIBarButtonItem =  UIBarButtonItem(customView: labelWelcom)
        
        let firstName = LoginManager.getMe.firstName;
        let nameLabel: UILabel = UILabel()
        nameLabel.text = " \(firstName!)"
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont(name: APP_FontName, size: 14)
        nameLabel.textColor = UIColor.white
        nameLabel.sizeToFit()
        
        let width: CGFloat = labelWelcom.frame.width + nameLabel.frame.width
        let height: CGFloat = nameLabel.frame.height
        let righView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        var rect: CGRect = labelWelcom.frame
        rect.origin.y = nameLabel.frame.height - labelWelcom.frame.height
        labelWelcom.frame = rect;
        righView.addSubview(labelWelcom)
        
        rect = nameLabel.frame
        rect.origin.x = labelWelcom.frame.width
        nameLabel.frame = rect;
        righView.addSubview(nameLabel)
        
        let nameBarButton: UIBarButtonItem =  UIBarButtonItem(customView: righView)
        self.navigationItem.rightBarButtonItem = nameBarButton

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func updateButton() {
        
        if self.property != nil {
            self.documentButton.isEnabled = true;
            self.documentButton.alpha = 1.0;
        }else {
            self.documentButton.isEnabled = false;
            self.documentButton.alpha = 0.7;
        }
        
    }
    
    func openDocView() {
        
        let docViewController: DocViewController = DocViewController()
        docViewController.property = self.property
        self.navigationController?.pushViewController(docViewController, animated: true)
    }
    
    func getProperty() {
        
        HSProgress(.show)
        Property.getProperty { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            
            if isSuccess {
                
                self.property = data as? Property
                self.updateButton()
                self.openDocView();
                //self.documentsButtonClicked(self.documentButton);
                
            }else {
                
                self.property = nil
                self.updateButton()
                
            }
            
        }

    }
    
    
    @IBAction func rentalHistroyButtonClicked(_ sender: AnyObject) {
        let rentalHistoryViewController: RentalHistoryViewController = RentalHistoryViewController()
        self.navigationController?.pushViewController(rentalHistoryViewController, animated: true)
    }
    
    @IBAction func documentsButtonClicked(_ sender: AnyObject) {
        
        self.getProperty()

//
//        if self.property == nil {
//             self.getProperty()
//        }else {
//          
//        }
        
        
    }
    
    
    @IBAction func contactButtonClicked(_ sender: AnyObject) {
        let contactNewLandlordViewController: ContactNewLandlordViewController = ContactNewLandlordViewController()
        self.navigationController?.pushViewController(contactNewLandlordViewController, animated: true)
        
    }

}
