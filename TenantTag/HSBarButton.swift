//
//  HSBarButton.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//


import UIKit
protocol HSBarButtonDelegate {
    
    func barButtonClicked(_ barButton: HSBarButton)
    
}
class HSBarButton: UIControl {

    var title: String?
    var image: UIImage?
    
    var imageSelected: UIImage?

    var label: UILabel! = UILabel()
    
    var imgView: UIImageView! = UIImageView()

    var delegate: HSBarButtonDelegate? = nil
    var selectedColor: UIColor = UIColor.black
    var normalColor: UIColor = UIColor.lightGray
  
    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set (selected){
            super.isSelected = selected
            self.updateButton()
        }
    }
    
    func updateButton() {
        
        if self.isSelected {
            //self.imag
            self.label.textColor = self.selectedColor
            self.imgView.image = imageSelected
        }else{
            self.label.textColor = self.normalColor
            self.imgView.image = image

        }
        
    }
    
    //var selected: Bool! = false;
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addTarget(self, action: #selector(HSBarButton.barButtonClicked(_:)), for: .touchUpInside)
    }
    
    func barButtonClicked(_ sender: HSBarButton) {
        
        if self.delegate != nil {
            print("position \(self.frame)")
            self.delegate!.barButtonClicked(self)
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title: String?, image: UIImage?) {
        self.init(frame: CGRect.zero);
        
        self.backgroundColor = UIColor.white
        self.title = title
        self.image = image
        
        if self.title != nil {
            self.label.textAlignment = .center
            self.label.font = UIFont(name: "arial", size: 13)
            self.addSubview(self.label)
            self.label.text = self.title
        }
        
        self.updateButton()
        
    }
    
    convenience init(image: UIImage? , imageSelected : UIImage?) {
        self.init(frame: CGRect.zero);
        
        self.backgroundColor = UIColor.white
        self.image = image
        self.imageSelected = imageSelected
        
        
//        if self.title != nil {
//            self.label.textAlignment = .center
//            self.label.font = UIFont(name: "arial", size: 13)
//            self.addSubview(self.label)
//            
//            self.label.text = "Home"
//            print(self.label.frame)
//           
//            
//        }
        self.imgView.image = image
       self.imgView.center = self.center
        self.addSubview(imgView)
        self.imgView.contentMode = .center
       // print(self.label.frame)

        self.updateButton()
        
    }

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.label.frame = self.bounds.insetBy(dx: 5, dy: 5)
      //  print(self.center)
        self.imgView.frame = self.bounds.insetBy(dx: 5, dy: 5)
    }
    
    

}
