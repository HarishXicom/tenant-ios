//
//  DocCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 24/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class DocCell: UITableViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.bgView.backgroundColor = UIColor.white
        self.bgView.cornerRadius(4.0)
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
