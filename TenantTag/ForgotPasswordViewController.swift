//
//  ForgotPasswordViewController.swift
//  Who's In
//
//  Created by Hardeep Singh on 30/11/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var emailTextFieldBGView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
//    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
//        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//        
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.emailTextField.resignFirstResponder()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        self.view.backgroundColor = UIColor.white
        
        self.emailTextFieldBGView.backgroundColor = UIColor.clear;
        self.emailTextFieldBGView.cornerRadius(15.0)
        self.emailTextFieldBGView.layer.borderColor = APP_ThemeColor.cgColor
        self.emailTextFieldBGView.layer.borderWidth = 1.0

        self.emailTextField.textColor = UIColor.black;
        self.emailTextField.autocapitalizationType = .none
        self.emailTextField.autocorrectionType = .no
        self.emailTextField.placeholder = "Username or email"

        self.submitButton.cornerRadius(15.0)
        self.submitButton.backgroundColor = APP_ThemeColor
        self.submitButton.setTitleColor(UIColor.white, for: UIControlState())
        
        let barButton:UIBarButtonItem = UIBarButtonItem.init(image:UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ForgotPasswordViewController.leftBarButtonClicked))
        self.navigationItem.setLeftBarButton(barButton, animated: true)
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        self.navTitle("FORGOT PASSWORD", color: UIColor.white, font: UIFont(name: APP_FontName, size: 17)!)

    }
    
    func leftBarButtonClicked() {
        self.dismiss(animated: true) { () -> Void in
            
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
   
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func submitButtonClicked(_ sender: AnyObject) {
        
        let emailAddres:String? = self.emailTextField.text;
      
        if emailAddres != nil {
            
            
            if emailAddres?.isEMPTY() == true{
                HSShowAlertView("TenantTag", message: "Please enter username or email.",controller: self)
                return;
                
            }

        LoginManager.share().forgotPassword(emailAddres) { (isSuccess:Bool, data:AnyObject?, error:NSError?) -> () in
            if isSuccess {
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.dismiss(animated: true, completion: { () -> Void in
                        HSShowAlertView("TenantTag", message: "instructions sent to email.")
                    })
                })
               }
            }
        }
    }
    
    @IBAction func backButtonClicked(_ sender: AnyObject) {
       
        self.dismiss(animated: true) { () -> Void in
            
        }
    }
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
