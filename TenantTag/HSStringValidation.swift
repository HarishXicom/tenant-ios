//
//  HSStringValidation.swift
//  Who's In
//
//  Created by Hardeep Singh on 21/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var length : Int {
        return NSString(string: self).length
    }
}


extension NSString {
    
    var capitalizedFirstChr : String {
        
       // var startIndex:Int = 0
        var nameOfString:String? = self as? String
        
        if (nameOfString != nil) && !(nameOfString!.isEmpty) {
            nameOfString!.replaceSubrange(nameOfString!.startIndex...nameOfString!.startIndex, with: String(nameOfString![nameOfString!.startIndex]).capitalized)
            return nameOfString!
        }else {
            return ""

        }
        
    }
    
    
    
    func encodeSpecialCharacter() -> String {
        let string:String = self.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        return string

    }
    
    public func sizeOfString(_ font:UIFont, constrainedToWidth width: Double) -> CGSize {
        
        let string:String = self as String
        if string.isEmpty {
            return CGSize.zero
        }
        
        let rect: CGRect = string.boundingRect(with: CGSize(width: width, height: DBL_MAX),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil)
        return rect.size
        
    }
    
    public func isValidPhoneNumver() -> Bool {
        return true
    }
    
    public func isValidEmail() ->Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
        
    }
    
    func phoneNumberWithoutFormate() -> String {
        
        let origNumber  = self;
        let stringArray = origNumber.components(
            separatedBy: CharacterSet.decimalDigits.inverted)
        let newString = stringArray.joined(separator: "")
        return newString;
    }
    
//    public func isEmpty() -> Bool {
//        
//        var string:NSString  = self;
//        if string.length == 0 {
//            return true
//        }
//        
//        let charSet:NSCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
//        string = string.stringByTrimmingCharactersInSet(charSet)
//        
//        if string.length == 0 {
//            return true
//        }
//        else {
//            return false
//        }
//        
//    }
    
    //    func MD5() -> String {
    //        return (self as NSString).dataUsingEncoding(NSUTF8StringEncoding)!.MD5().hexString()
    //    }
    //
    //    func SHA1() -> String {
    //        return (self as NSString).dataUsingEncoding(NSUTF8StringEncoding)!.SHA1().hexString()
    //    }
    
    var md5: String!
    {
        
        let str = self.cString(using: String.Encoding.utf8.rawValue)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8.rawValue))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.deallocate(capacity: digestLen)
        return String(format: hash as String)
        
    }
    
    
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8.rawValue)!
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA1_DIGEST_LENGTH))
        CC_SHA1((data as NSData).bytes, CC_LONG(data.count), &digest)
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined(separator: "")
    }
    
    

    
}
