//
//  HSGlobalFunctions.swift
//  Who's In
//
//  Created by Hardeep Singh on 23/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//



import Foundation
import SafariServices
import Photos

let APP_Name = "TenantTag"
typealias responseCallBack = (Bool, AnyObject?, NSError?) -> ()
typealias actionSheetCallBack = (_ cancel: Bool, _ destruction: Bool, _ index: Int) -> ()

public enum ProgressShow: Int {
    case show = 0,
    hide
}
public func checkInternetAccess() -> Bool
{
    let reach:Reachability = Reachability.forInternetConnection()
    let netStatus:NetworkStatus = reach.currentReachabilityStatus()
    
    if (netStatus == .NotReachable)
    {
        return false
    }
    return true
}


func openInAppBrowser(controller : UIViewController)
{
    let safariVC = SFSafariViewController(url: NSURL(string: URL_CommonQuestions)! as URL)
    controller.present(safariVC, animated: true, completion: nil)
}


func openWebsiteInAppBrowser(controller : UIViewController)
{
    let safariVC = SFSafariViewController(url: NSURL(string: URL_Website)! as URL)
    controller.present(safariVC, animated: true, completion: nil)
}


public func getIPAddresses() -> [String] {
    var addresses = [String]()
    
    // Get list of all interfaces on the local machine:
    var ifaddr : UnsafeMutablePointer<ifaddrs>?
    guard getifaddrs(&ifaddr) == 0 else { return [] }
    guard let firstAddr = ifaddr else { return [] }
    
    // For each interface ...
    for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
        let flags = Int32(ptr.pointee.ifa_flags)
        var addr = ptr.pointee.ifa_addr.pointee
        
        // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
        if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
            if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                
                // Convert interface address to a human readable string:
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                    let address = String(cString: hostname)
                    addresses.append(address)
                }
            }
        }
    }
    
    freeifaddrs(ifaddr)
    return addresses
}


//ProgressView

public func NextOffSet( _ page:Int, pageSize: Int) -> Int  {
    
    if page == 0 {
        return page;
    }
    return (page*10)+1
    
}


public func HSProgress(_ status:ProgressShow) {
    
    if status == .show {
         HSShowProgress()
    }else{
         HSHideProgress()
    }
}

public let FingerPrints = ""
public let FingerIP = ""

 public func getDeviceID() -> String
{
    
    let strDeviceID =  (UIDevice.current.identifierForVendor?.uuidString)! + "-AAPL"
    return strDeviceID
}


public func makeJSONFromAnyObject(tempObject : AnyObject) -> String{
    do
    {
        let json = try JSONSerialization.data(withJSONObject: tempObject, options: []) as NSData
        let   jsonStr = NSString(data: json as Data, encoding: String.Encoding.utf8.rawValue) as! String
        return jsonStr
    }
    catch{
        
    }
    
    let emptyString = String()
    return emptyString
    
}


//- (NSString *)deviceID{
//    return [NSString stringWithFormat:@"%@-AAPL",[UIDevice currentDevice].identifierForVendor.UUIDString];
//}

private func HSShowProgress() {
    MBProgressHUD.showAdded(to: appDelegate().window, animated: true)
}

private func HSHideProgress() {
    MBProgressHUD.hide(for: appDelegate().window, animated: true)
}


//AlertView
func HSShowAlertView(_ title:NSString, message:NSString, controller:UIViewController?) {
    var controller = controller
    
    //make and use a UIAlertController
    
    if objc_getClass("UIAlertController") != nil {
        
        if #available(iOS 8.0, *) {

        if controller == nil {
            controller = appDelegate().topViewController();
        }
        
        let alert:UIAlertController =  UIAlertController.init(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        
            let action:UIAlertAction  = UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
                
            })
            // Fallback on earlier versions
        alert.addAction(action)
        controller?.present(alert, animated: true, completion: { () -> Void in
            
        })
            
            
        }else{
            
        }
        
    }
    else {
        
        //make and use a UIAlertView
        let alert = UIAlertView()
        alert.title = title as String
        alert.message = message as String
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
    
}

public func HSShowAlertView(_ title:NSString,message:NSString) {
    HSShowAlertView(title, message: message, controller: appDelegate().topViewController())
}


public func HSShowActionSheet( _ title: String?, message: String?,  cancel: String?, destructive:String?, actions:[String]?, callBack: @escaping (_ cancel: Bool, _ destruction: Bool, _ index: Int) -> ()) {
    
    if objc_getClass("UIAlertController") != nil {
        
        if #available(iOS 8.0, *) {

        let alert:UIAlertController =  UIAlertController.init(title: title, message: message , preferredStyle: UIAlertControllerStyle.actionSheet)
        
            if actions != nil {
                
                for action in actions! {
                    
                    let action1:UIAlertAction  = UIAlertAction.init(title: action, style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
                        
                        let title: String =  action.title!
                        let index:Int = actions!.index(of: title)!
                        callBack(false, false, index)
                        
                    })
                    alert.addAction(action1)
                }

        }
            
        if cancel != nil {
            let action2:UIAlertAction  = UIAlertAction.init(title: cancel, style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) -> Void in
                
                callBack(true,false, 0)
                
            })
            alert.addAction(action2)
        }
        
        if cancel != nil {
            
            let action:UIAlertAction  = UIAlertAction.init(title: destructive, style: UIAlertActionStyle.destructive, handler: { (action:UIAlertAction) -> Void in
                callBack(false,true,0)
            })
            alert.addAction(action)
            
        }

       
        appDelegate().topViewController().present(alert, animated: true, completion: { () -> Void in
            
        })
          
            
        }else {
            
            
        }
        
    }
    else {
        
        UIActionSheet.show(in: appDelegate().window!, withTitle: title, cancelButtonTitle: cancel, destructiveButtonTitle: destructive, otherButtonTitles: actions, tap: { (actionSheet: UIActionSheet!, index: Int) -> Void in
            
            if (index == actionSheet.destructiveButtonIndex)
            {
                // Do something...
                callBack(false, true, index)
            }else if (index == actionSheet.cancelButtonIndex) {
                callBack(true, false, index)
            }else {
                callBack(false, false, index)
            }
            
        })
    }
    

}

public func showAlertWithMutipleButtons( title: String?, message: String?, cancel:String?, destructive: String?, actions: [String]?, controller : UIViewController, callBack:@escaping (_ : Bool? ,_ : Bool? ,_ : Int? ) -> (Void)) {
    
    let alert:UIAlertController =  UIAlertController.init(title: "TenantTag", message: message , preferredStyle: UIAlertControllerStyle.alert)
    if cancel != nil {
        
        let action2:UIAlertAction  = UIAlertAction.init(title: cancel, style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) -> Void in
            callBack(true,false, 10)
        })
        alert.addAction(action2)
    }
    
    if actions != nil {
        
        for action in actions! {
            
            let action1:UIAlertAction  = UIAlertAction.init(title: action, style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
                
                let title: String =  action.title!
                let index: Int = actions!.index(of: title)!
                alert.dismiss(animated: true, completion: nil)
                
                callBack(false, false, index)
            })
            alert.addAction(action1)
        }
        
    }
    controller.present(alert, animated: true, completion: { () -> Void in
    })
    
    
}


//MARK:- Extensions
extension UIImage{
   
  func getBase64() -> String
   {
    
    let imageData : Data  = UIImageJPEGRepresentation(self, 0.5)!
    let strBase64 = imageData.base64EncodedString()
    return strBase64
    
    }


}


extension UITableView{
    public func registerNibsForCells(arryNib : NSArray)
    {
        for i in 0  ..< arryNib.count
        {
            let nibName = arryNib.object(at: i) as! String
            print(nibName)
            let nibCell = UINib.init(nibName:nibName, bundle: nil);
            self.register(nibCell, forCellReuseIdentifier:nibName)
        }
    }
}
extension UIButton{
    public func underLineButton( text: String , color : UIColor) {
        let btnRef:UIButton = self
        let titleString : NSMutableAttributedString = NSMutableAttributedString(string: text, attributes: [NSUnderlineStyleAttributeName : 1 ,NSUnderlineColorAttributeName : color ,NSForegroundColorAttributeName : color ])
        btnRef.setAttributedTitle(titleString, for: .normal)
  }
}


extension UIView{
    public func addBorderWithColorAndLineWidth(color: UIColor , borderWidth : Float){
        let view:UIView = self
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = CGFloat(borderWidth)
    }
 }





extension NSDate {
    
  public  func returnMonthName()-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        return dateFormatter.string(from: self as Date)
    }
    
    public  func returnDay()-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        return dateFormatter.string(from: self as Date)
    }
}

extension String
{
    public func removeParticularCharacterString(character: String) -> String
    {
        
        return self.replacingOccurrences(of: character, with: "", options: .literal, range: nil)
        
        
    }

    
    
    public func isValidEmail() ->Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
        
    }

    
    public func changeDateFormat() -> String
    {
        var strLocal = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: strLocal)!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        strLocal = dateFormatter.string(from: date)
        print(strLocal)
        return strLocal

    }
    
    public func changeDateString() -> String
    {
        var strLocal = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strLocal)!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        strLocal = dateFormatter.string(from: date)
        print(strLocal)
        return strLocal
        
        
    }


public func isEMPTY() -> Bool
{
    let testingString = self
    if testingString.characters.count == 0 || checkIsEmpty()
    {
        return true
    }
    return false
}

func checkIsEmpty()-> Bool{
    
  //  let charset = NSCharacterSet.whitespaces
    let trimmedString = self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    if trimmedString.characters.count == 0 {
        return true
    }else{
        return false
    }
    }
}

public func creditCardTextfieldFormatter(txtField : UITextField) -> String
{
        var strInput = txtField.text
    if strInput == ""
    {
        return strInput!
    }
        let index = strInput?.index((strInput?.endIndex)!, offsetBy: -1)
        let subStringTo = (strInput?.substring(to: index!))!
        let length = strInput?.length
        if length == 5 || length == 10 || length == 15{
            let strLast: String = (strInput?.substring(from: index!))!
            if strLast == " "{
                strInput = String(format: "%@", subStringTo)
            }
            else{
                strInput = String(format: "%@ %@", subStringTo, strLast)
            }
            return strInput!
        }
        if (strInput?.length)! > 19{
            strInput = String(format: "%@", subStringTo)
            return strInput!
        }
    return strInput!
}


public func MSShowActionSheet(arrayBtn : [String] , controller : UIViewController ,callBack: @escaping (_ cancel: Bool, _ index: Int) -> ()) {
    
    let actionSheet:UIAlertController =  UIAlertController.init(title: nil, message: nil , preferredStyle: UIAlertControllerStyle.actionSheet)
    
        for action in arrayBtn {
            let action1:UIAlertAction  = UIAlertAction.init(title: action, style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
                let title: String =  action.title!
                let index:Int = arrayBtn.index(of: title)!
                callBack(false, index)
            })
            actionSheet.addAction(action1)
        }
    
        let action2:UIAlertAction  = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) -> Void in
            callBack(true, 0)
        })
        actionSheet.addAction(action2)
    controller.present(actionSheet, animated: true, completion: nil)
}




public func saveLeasedDoc(){
    
    if leasedDocImage == nil
    {
    
    let url = URL(string: leasedDocURL)
    
    if leasedDocURL.hasSuffix("png") || leasedDocURL.hasSuffix("jpg") ||  leasedDocURL.hasSuffix("jepg")
    {
        var image : UIImage!
        DispatchQueue.global().async {
                 do {
                        try image = UIImage(data: Data(contentsOf: url!))!
                    } catch {
                        print("Failed")
                    }
                    DispatchQueue.main.async(execute: {
                        if image != nil {
                            
                          
                            leasedDocImage = image
                            print("Image Saved")
                            
                        }
                    })

        }
        
    }else if leasedDocURL.hasSuffix("pdf")
    {
        
    }else if leasedDocURL.hasSuffix("doc") || leasedDocURL.hasSuffix("docx")
    {
       
        
    }
    }else{
        
        
        
    }
}



func checkUSPhoneFormat(string: String?, str: String? ,textField : UITextField) -> Bool{
    
    if string == ""{ //BackSpace
        return true
    }else if str!.characters.count < 3{
        
        if str!.characters.count == 1{
            textField.text = "("
        }
    }else if str!.characters.count == 5{
        
        textField.text = textField.text! + ") "
        
    }else if str!.characters.count == 10{
        
        textField.text = textField.text! + "-"
        
    }else if str!.characters.count > 14{
        return false
    }
    return true
}



    


