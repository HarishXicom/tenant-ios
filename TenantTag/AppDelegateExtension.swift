//
//  AppDelegateExtension.swift
//  Who's In
//
//  Created by Hardeep Singh on 26/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import Foundation

extension AppDelegate {
    
   class var appDelegate: AppDelegate {
        
        get {
            let delegate = UIApplication.shared.delegate
              //  .shared.delegate as! AppDelegate
            return delegate as! AppDelegate
        }
    }
    
    class var app: UIApplication {
        get {
           
            return UIApplication.shared
        }
    }


    func deviceUniqueIdentifier() -> String {

        let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
        var strApplicationUUID = SSKeychain.password(forService: appName, account: "incoding")
        if (strApplicationUUID == nil)
        {
           strApplicationUUID = UIDevice.current.identifierForVendor?.uuidString;
            SSKeychain.setPassword(strApplicationUUID, forService: appName, account: "incoding")
        }
        
        return strApplicationUUID!;

    }
    
    //MARK - 
    func unregisterAppPushNotifications(){
        
        if #available(iOS 8.0, *) {

          AppDelegate.app.registerForRemoteNotifications();
            
        }
    }
    
    func registerAppPushNotificaiton() {
        
        if AppDelegate.app.responds(to: "registerUserNotificationSettings") {
           
            if #available(iOS 8.0, *) {

                
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                AppDelegate.app.registerUserNotificationSettings(settings)
            } else {
                // Fallback on earlier versions
            }
            
        }else {
            
            let types: UIRemoteNotificationType = [.alert, .badge, .sound]
            AppDelegate.app.registerForRemoteNotifications(matching: types)
            
            
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    
    //MARK -
    fileprivate func topViewControllerWithRootViewController(_ rootViewController:UIViewController) -> UIViewController {
        
        if (rootViewController is UITabBarController) {
            
            let tabBarController:UITabBarController = rootViewController as! UITabBarController
            return  self.topViewControllerWithRootViewController(tabBarController.selectedViewController!)
            
        }else if (rootViewController is UINavigationController) {
            let tabBarController:UINavigationController = rootViewController as! UINavigationController
            return  self.topViewControllerWithRootViewController(tabBarController.visibleViewController!)
            
        }else if ((rootViewController.presentedViewController) != nil) {
            
            let presentedViewController: UIViewController = rootViewController
            return  self.topViewControllerWithRootViewController(presentedViewController)
        }else{
            return rootViewController;
        }
    }
    
    func topViewController() -> UIViewController {
        return self.topViewControllerWithRootViewController((UIApplication.shared.keyWindow?.rootViewController)!)
    }

}

func app() ->UIApplication {
   return UIApplication.shared
}

func appDelegate() -> AppDelegate {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return delegate
}

