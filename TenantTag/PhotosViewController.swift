//
//  PhotosViewController.swift
//  Who's In
//
//  Created by Hardeep Singh on 22/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class PhotosViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate {

    @IBOutlet var topTitleLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    var property: Property!
    var landLordPhotos: [HSPhoto] = [HSPhoto]()
    var tenantPhotos: [HSPhoto] = [HSPhoto]()

    var user: User? = nil

    var pageManager: HSPageManager? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.topTitleLabel.text = self.property.propertyAddress;
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor
        self.view.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
        
        self.navTitle("Photos", color: UIColor.white, font: UIFont(name: APP_FontName, size: 17)!)
        var barButton:UIBarButtonItem = UIBarButtonItem.init(image:UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PhotosViewController.leftBarButtonClicked))
        self.navigationItem.setLeftBarButton(barButton, animated: true)
        
         barButton = UIBarButtonItem.init(image: UIImage(named: "add"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PhotosViewController.addButtonClikced))
        self.navigationItem.setRightBarButton(barButton, animated: true)

        //CollectionHeaderReusableView
        let headerNib = UINib.init(nibName:"CollectionHeaderReusableView", bundle: nil);
        self.collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeaderReusableView")
        self.collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "CollectionHeaderReusableView")


        // Do any additional setup after loading the view.
        let nib = UINib.init(nibName:"PhotoCollectionCell", bundle: nil);
        self.collectionView.register(nib, forCellWithReuseIdentifier:"PhotoCollectionCell")
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = UIColor.clear;
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.showsVerticalScrollIndicator = false
        
        /*
        self.pageManager = HSPageManager(pageNumber: 0, pageSize: 100)
        self.pageManager?.setPagination({ (pageManager: HSPageManager!, page: Int, pageSize: Int) -> Void in
           
            let nextOfSet: Int  = (page*pageSize)
            let param = ["offset":nextOfSet,
                "limit":pageSize]
            
            HTTPRequest.request().getRequest(APIBusinessUserPhotos, param: param, callBack: { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                if error != nil {
                    pageManager.failed()
                    return
                }
                
                if isSuccess == true {
                    
                    let dict:NSDictionary? = data as? NSDictionary
                    let photos:[AnyObject]? = dict!.valueForKey("photos") as? [AnyObject]
                    let total_photos: Int = dict!["total_photos"] as! Int
                    self.upatePhotos(photos)
                    pageManager.updateRecords([AnyObject](), total: total_photos);
                    return
                    
                }
                
                pageManager.failed()
                return
            })
            
            }, success: { (pageManager: HSPageManager!, data: AnyObject!) -> Void in
                
            }, failed: { () -> Void in
                
        })
        
        */

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addButtonClikced() {
        
        self.uploadPhotosButtonClicked()
    }
    
    func leftBarButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    
    
    //MARK: ActionSheet
    
     func uploadPhotosButtonClicked() {
        
        if objc_getClass("UIAlertController") != nil {
            
            if #available(iOS 8.0, *) {
                
                let alert:UIAlertController =  UIAlertController.init(title: nil, message: nil , preferredStyle: UIAlertControllerStyle.actionSheet)
                
                
                let action1:UIAlertAction  = UIAlertAction.init(title: "Camera", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
                    
                    self.openPickerController(.camera)
                    
                })
                alert.addAction(action1)
                
                let action2:UIAlertAction  = UIAlertAction.init(title: "Library", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) -> Void in
                    
                    self.openPickerController(.photoLibrary)
                    
                })
                alert.addAction(action2)
                
                let action:UIAlertAction  = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) -> Void in
                    
                })
                alert.addAction(action)
                
                self.present(alert, animated: true, completion: { () -> Void in
                    
                })
                
            }else{
                
            }
            
        }
        else {
            
            //make and use a UIAlertView
            let action = UIActionSheet.init(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Library")
            action.show(in: appDelegate().window!)
        }
        
        
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 0 {
        }else if buttonIndex == 1 {
            self.openPickerController(.camera)
        }else if buttonIndex == 2 {
            self.openPickerController(.photoLibrary)
        }
        
    }
    
    
    //MARK: ImagePickerDelegate
    func deletePhoto(_ photo: HSPhoto) {
        //APIDeletePhoto
        
        let index: Int = self.tenantPhotos.index(of: photo)!
        let params: Dictionary = ["id": photo.ID! as AnyObject] as Dictionary<String,AnyObject>
        
        HSProgress(.show)
        HTTPRequest.request().postRequest(APIDeletePhoto as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
             HSProgress(.hide)
            if isSuccess == true {
                self.tenantPhotos.remove(at: index)
                self.collectionView.reloadData()

            }
        }
        
    }
    
    func uploadImage(_ photo: HSPhoto) {
        //APIUploadPropertyPhotos
        
        let image: UIImage = photo.image!
        let imgData:Data = UIImageJPEGRepresentation(image, 0.1)!
        
        if self.tenantPhotos.count > 0 {
            self.tenantPhotos.insert(photo, at: 0)
        }else {
            self.tenantPhotos.append(photo)
        }
        self.collectionView.reloadData()
        
        let fileData:FileData = FileData(fileName: "image.jpg", name: "userfile", mimeType: "image/jpeg", data: imgData);

        HTTPRequest.request().postRequestWithMulitpartData( [fileData], method: APIUploadPropertyPhotos as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if isSuccess == true {
                print(data)
                
                let images: [AnyObject]? = data!["data"]! as? [AnyObject]
                if images != nil {
                    if images!.count > 0 {
                        
                        print(images)
                        let dict: Dictionary? = images![0] as?  Dictionary<String,AnyObject>
                        
                        if dict != nil {
                            
                            let photoTemp: HSPhoto = self.photoModel(dict! as AnyObject)
                            photoTemp.image = photo.image
                            self.tenantPhotos[0] = photoTemp
                            self.collectionView.reloadData()
                            
                        }
                        
                    }

                }
                
            }else {
                
                self.tenantPhotos.remove(at: 0)
                self.collectionView.reloadData()

            }
        }
        
    }
    
    
    func photoModel(_ obj: AnyObject) -> HSPhoto {
        
        let dict: Dictionary = obj as! Dictionary<String,AnyObject>
        let  id = dict["id"] as! String
        let  thumb = dict["thumb"] as! String
        let  url = dict["url"] as! String
        let photo: HSPhoto = HSPhoto(thumbURLStr: thumb, URLStr: url)
        photo.ID = id;
        return photo;
        
    }

    
    func openPickerController(_ sourceType: UIImagePickerControllerSourceType ){
        
//        self.hideFromImagePicker = true
        
        if sourceType == .camera {
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                let imagePickerController:UIImagePickerController = UIImagePickerController()
                imagePickerController.sourceType = .camera
                imagePickerController.delegate = self
                imagePickerController.allowsEditing = true
                [self .present(imagePickerController, animated: true, completion: { () -> Void in
                    
                })]
                
            }else{
                
                let imagePickerController:UIImagePickerController = UIImagePickerController()
                imagePickerController.sourceType = .photoLibrary
                imagePickerController.delegate = self
                imagePickerController.allowsEditing = true
                [self .present(imagePickerController, animated: true, completion: { () -> Void in
                    
                })]
                
            }
            
        }else{
            
            let imagePickerController:UIImagePickerController = UIImagePickerController()
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            imagePickerController.allowsEditing = true
            [self .present(imagePickerController, animated: true, completion: { () -> Void in
                
            })]
            
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if (picker.allowsEditing) {
            
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                let photo: HSPhoto = HSPhoto()
                photo.image = pickedImage
                photo.thumbImage = pickedImage
               self.uploadImage(photo)
            }
            
        }else {
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let photo: HSPhoto = HSPhoto()
                photo.image = pickedImage
                photo.thumbImage = pickedImage
                self.uploadImage(photo)

            }
            
        }
        
        // let getSize:CGRect = (self.selectedImage?.uploadRect())!
        dismiss(animated: true) { () -> Void in
            
            
        };
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true) { () -> Void in
       
        }
        
    }

    
    
//    func upatePhotos( photos_ :[AnyObject]?) {
//
//        if (self.photos.count > 0) {
//            self.photos.removeAll()
//        }
//        
//        for photo in photos_! {
//            let urlStr: String?  =   photo["dealPhoto"] as? String
//            let photoId: String?  =   photo["id"] as? String
//            if urlStr != nil {
//                let photo: HSPhoto = HSPhoto(urlStr: urlStr!)
//                photo.ID = photoId != nil ? photoId : "";
//                self.photos.append(photo)
//            }
//        }
//        self.collectionView.reloadData()
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: -
    //MARK: - ScrollView
    //MARK: -
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contentY: Int = Int(scrollView.contentOffset.y)
        let scrollHeight: Int = Int(scrollView.contentSize.height - scrollView.bounds.size.height)
        
        if (contentY >= scrollHeight){
            if  self.pageManager!.isReachedLastPage == false {
                    self.pageManager!.fetchNextpage()
            }
        }
    }
    
    //MARK: -
    //MARK: - CollectionDelegate
    //MARK: -
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return self.landLordPhotos.count;
        }else {
            return self.tenantPhotos.count;
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: PhotoCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionCell", for: indexPath) as! PhotoCollectionCell
        
        let photo: HSPhoto!
        if indexPath.section == 0 {
            photo = self.landLordPhotos[indexPath.row]
            cell.marked = false;

        }else {
            photo = self.tenantPhotos[indexPath.row]
            cell.marked = true;

        }
        
        cell.imgView.contentMode = .scaleAspectFit
        cell.udpateCell(photo)
        cell.deleteImgCallBack = {(photo: HSPhoto) -> () in
             self.deletePhoto(photo)
        }
        
        return cell;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var photo: HSPhoto? = nil
        if indexPath.section == 0 {
            photo = self.landLordPhotos[indexPath.row]
            
        }else {
            photo = self.tenantPhotos[indexPath.row]
        }

        let photoPreviewViewController: PhotoPreviewViewController = PhotoPreviewViewController.init(nibName: "PhotoPreviewViewController", bundle: nil)
        photoPreviewViewController.photo = photo;
        let navController: UINavigationController  = UINavigationController(rootViewController: photoPreviewViewController)
        self.present(navController, animated: true, completion: { () -> Void in
            
        })
        
        photoPreviewViewController.deleteImgPreviewViewCallBack = {(photo: HSPhoto) -> () in
            self.deletePhoto(photo)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let numberOfCellInRow: CGFloat =  (self.view.bounds.width / 80)
        let cellWidth: CGFloat =  (collectionView.bounds.width - CGFloat(numberOfCellInRow + 1)) / CGFloat(numberOfCellInRow);
        return CGSize(width: cellWidth, height: cellWidth);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0,1,0,1);  // top, left, bottom, right
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40);
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        if (kind == UICollectionElementKindSectionHeader) {
            let header: CollectionHeaderReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeaderReusableView", for: indexPath) as! CollectionHeaderReusableView
            
            let label: UILabel? = header.viewWithTag(100) as? UILabel;
            if label != nil {
                if indexPath.section == 0 {
                    label!.text = "Landlord Photos"
                }else {
                    label!.text = "Tenant Photos"
                }
                label!.textColor = UIColor.black
            }else{
                label!.text = ""

            }
            
            label!.backgroundColor = UIColor.clear
            return header;
            
            
        }else {
            
            let footer: CollectionHeaderReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeaderReusableView", for: indexPath) as! CollectionHeaderReusableView
          
            let label: UILabel? = footer.viewWithTag(100) as? UILabel;
            if label != nil {
                label!.text = ""
            }
            
            label!.backgroundColor = UIColor.clear
            return footer;

        }

        
    }

    

}
