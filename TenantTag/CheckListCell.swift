//
//  CheckListCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 27/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class CheckListCell: UITableViewCell {

    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func updateCell(_ checkList: CheckList) {
        self.label.text  = checkList.content;
    }
    
}
