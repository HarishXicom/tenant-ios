//
//  CheckListTopCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 27/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class CheckListTopCell: UITableViewCell {

    @IBOutlet var bgView1: UIView!
    @IBOutlet var titleLabel1: UILabel!
    @IBOutlet var detailLabel1: UILabel!
    @IBOutlet var imgView1: UIView!
    
    
    @IBOutlet var bgView2: UIView!
    @IBOutlet var imgView2: UIImageView!
    @IBOutlet var label2: UILabel!
    @IBOutlet var detailLabel2: UILabel!
    
    @IBOutlet var centerLine: UIView!
    var emailYourFriendsCallBack:((Void)->Void)? = nil
    var forwarYourmailCallBack:((Void)->Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.detailLabel1.text = kForwardYourMail;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        var maskLayer1: CAShapeLayer? = self.bgView1.layer.mask as? CAShapeLayer
        if maskLayer1 == nil {
            maskLayer1 = CAShapeLayer()
        }
        maskLayer1!.path = UIBezierPath(roundedRect: self.bgView1.bounds, byRoundingCorners:[UIRectCorner.topLeft , UIRectCorner.topRight], cornerRadii: CGSize(width: 3.0, height: 3.0)).cgPath
        self.bgView1.layer.mask = maskLayer1!;
        
        var maskLayer2: CAShapeLayer? = self.bgView2.layer.mask as? CAShapeLayer
        if maskLayer2 == nil {
            maskLayer2 = CAShapeLayer()
        }
        maskLayer2!.path = UIBezierPath(roundedRect: self.bgView2.bounds, byRoundingCorners:[UIRectCorner.bottomLeft , UIRectCorner.bottomRight], cornerRadii: CGSize(width: 3.0, height: 3.0)).cgPath
        self.bgView2.layer.mask = maskLayer2!;
    }
    
    @IBAction func button1Clicked(_ sender: AnyObject) {
        
        if self.forwarYourmailCallBack != nil {
            self.forwarYourmailCallBack!()
        }
        
    }
    
    @IBAction func button2Clicked(_ sender: AnyObject) {
        
        if self.emailYourFriendsCallBack != nil {
            self.emailYourFriendsCallBack!()
        }
        
    }
    
    
}
