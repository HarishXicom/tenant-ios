//
//  RentalHistoryCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 25/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class RentalHistoryCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label1: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.label2.numberOfLines = 0;
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
