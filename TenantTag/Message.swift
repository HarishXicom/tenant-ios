//
//  Message.swift
//  TenantTag
//
//  Created by Hardeep Singh on 05/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    var text: String? = nil
    var dateStr: String? = nil
    var ID: String
    

    override init() {
        self.ID = ""
        self.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore maqna aliqua."
        self.dateStr = "11-Nov-2015, 3:55PM"
    }
    
    convenience init( message: AnyObject) {
        
        self.init()
        
        
        let dict: Dictionary? = message as? Dictionary<String, AnyObject>
        
        if dict == nil {
            return
        }
        
        let ID: String = dict!["id"] as! String
        self.ID = ID;
        
        let text: String? = dict!["message"] as? String
        if text != nil {
            self.text = text!
        }else{
            self.text = ""
        }
        
        let dateStr: String? = dict!["date_created"] as? String
        if dateStr != nil {
            self.dateStr = dateStr!
        }else{
            self.dateStr = ""
        }

    }
    
    func deleteMessage( _ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        //APIDeleteMessage
        let params = [
            "messageId":self.ID]
        
        HSProgress(.show)
        HTTPRequest.request().postRequest(APIDeleteMessage as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            
            if isSuccess == true {
                 callBack(true, nil, nil)
            }else  {
                 callBack(false, nil, nil)
            }
        }
    }
    
    class func searchMessagesFor(_ searchText: String, searchDate: String, pageManager: HSPageManager?, pageNumber: Int, pageSize: Int, callBack:@escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        
        //APP_SearchMessages
        let params = [
            "limit":pageSize,
            "offset":pageNumber,
        "searchTerm":searchText,
        "searchDate":searchDate] as [String : Any]
        
        print(pageSize,pageNumber)
        
        HTTPRequest.request().getRequest(APP_SearchMessages as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if error != nil {
                
                callBack(false, nil, nil)
                if pageManager != nil {
                    pageManager!.failed()
                }
                return;
            }
            
            if isSuccess == true {
                
                let dict: Dictionary? = data as? Dictionary<String, AnyObject>
                if dict != nil {
                    
                    let list: [AnyObject]? = dict!["messages"] as? [AnyObject]
                    let total: Int? = dict!["total"] as? Int
                    
                    var messagesList: [Message] = [Message]()
                    for obj in list! {
                        let message = Message(message: obj)
                        messagesList.append(message)
                    }
                    
                    if pageManager != nil {
                        pageManager!.updateRecords([AnyObject](), total: total!);
                    }
                    callBack(true, messagesList as AnyObject?, nil)
                    return;
                }
                
                
                callBack(true, data, nil)
                if pageManager != nil {
                    pageManager!.failed()
                }
                
            }else {
                callBack(false, data, nil)
                if pageManager != nil {
                    pageManager!.failed()
                }
            }
            
            
        }

        
    }
    
    class func getMessages(_ pageManager: HSPageManager?, pageNumber: Int, pageSize: Int, callBack:@escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        let params = [
            "limit":pageSize,
            "offset":pageNumber]
        
        print(pageSize,pageNumber)
        
        HTTPRequest.request().getRequest(APIGetMessageList as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if error != nil {
                
                callBack(false, nil, nil)
                if pageManager != nil {
                    pageManager!.failed()
                }
                return;
            }
            
            if isSuccess == true {
                
                let dict: Dictionary? = data as? Dictionary<String, AnyObject>
                if dict != nil {
                    
                    let list: [AnyObject]? = dict!["messages"] as? [AnyObject]
                    let total: Int? = dict!["total"] as? Int
                    
                    var messagesList: [Message] = [Message]()
                    for obj in list! {
                       let message = Message(message: obj)
                        messagesList.append(message)
                    }
                    
                    if pageManager != nil {
                        pageManager!.updateRecords([AnyObject](), total: total!);
                    }
                    callBack(true, messagesList as AnyObject?, nil)
                    return;
                }
                
                
                callBack(true, data, nil)
                if pageManager != nil {
                    pageManager!.failed()
                }
                
            }else {
                callBack(false, data, nil)
                if pageManager != nil {
                    pageManager!.failed()
                }
            }

        }

    }
    
    class func searchDateFormate() -> String {
          return "dd-MM-yyyy"
    }
    
}
