//
//  MainContainerViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 31/12/15.
//  Copyright © 2015 XICOM. All rights reserved.
//


import UIKit

class MainContainerViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollContainerView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    var topTabBar: HSTabBar!
    fileprivate var selectedIndex: Int = 0
    var currentViewController: UIViewController? = nil

    lazy fileprivate var viewControllers: [UIViewController] = [UIViewController]()
    var pageViewController: UIPageViewController!
    weak var nextViewController: UIViewController? = nil
    weak var preViewController: UIViewController? = nil
    var maintenance: [MaintenanceCat]? = nil

    //MARK: -  CheckListView
    //
    //
    @IBOutlet var checkListView: UIView!
    @IBOutlet var checkListCroseButton: UIButton!
    @IBOutlet var checkListMessageButton: UIButton!
    @IBOutlet var checkListLabel: UILabel!
    @IBOutlet var checkListHeight: NSLayoutConstraint!
   
    @IBAction func checkListCroseButtonClicked(_ sender: AnyObject) {
        
    }
    @IBAction func checkListMessageButtonClicked(_ sender: AnyObject) {
        
    }
    
    @IBAction func tapOnCheckList(_ sender: AnyObject) {
        
        let checkListViewController: CheckListViewController = CheckListViewController(nibName: "CheckListViewController", bundle: nil)
        self.navigationController?.pushViewController(checkListViewController, animated: true)
        
    }
    
    func showCheckList() {
    
        self.checkListView.isHidden = false;
        self.checkListHeight.constant = 30.0;
    }
    
    func hideCheckList() {
        
        self.checkListView.isHidden = true;
        self.checkListHeight.constant = 0.0;
        
    }
    
    
    //MARK: - 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController!.isNavigationBarHidden = false
        
      
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
       // self.presentLeftMenuViewController()
        
        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        self.navigationController?.navigationBar.isTranslucent = false;
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "menu"), for: UIControlState())
        btn.addTarget(self, action: #selector(UIViewController.presentLeftMenuViewController as (UIViewController) -> () -> Void), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        
        let labelWelcom: UILabel = UILabel()
        labelWelcom.text = "Welcome Home"
        labelWelcom.textAlignment = .right
        labelWelcom.font = UIFont(name: APP_FontName, size: 10)
        labelWelcom.textColor = UIColor.white
        labelWelcom.sizeToFit()
       // let WelcomBarButton: UIBarButtonItem =  UIBarButtonItem(customView: labelWelcom)

        let firstName = LoginManager.getMe.firstName;
        let nameLabel: UILabel = UILabel()
        nameLabel.text = " \(firstName!)"
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont(name: APP_FontName, size: 14)
        nameLabel.textColor = UIColor.white
        nameLabel.sizeToFit()
        
        let width: CGFloat = labelWelcom.frame.width + nameLabel.frame.width
        let height: CGFloat = nameLabel.frame.height
        let righView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        var rect: CGRect = labelWelcom.frame
        rect.origin.y = nameLabel.frame.height - labelWelcom.frame.height
        labelWelcom.frame = rect;
        righView.addSubview(labelWelcom)
        
        rect = nameLabel.frame
        rect.origin.x = labelWelcom.frame.width
        nameLabel.frame = rect;
        righView.addSubview(nameLabel)
        
        let nameBarButton: UIBarButtonItem =  UIBarButtonItem(customView: righView)
        self.navigationItem.rightBarButtonItem = nameBarButton

        //self.scrollContainerView.hidden = true

        var arra: [UIViewController] = [UIViewController]()

        //HomeViewController....
        let homeViewController: HomeViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
        arra.append(homeViewController)
        
        //MaintenViewController....
        let maintenViewController: PaymentOptionVC = PaymentOptionVC(nibName: "PaymentOptionVC", bundle: nil)
        arra.append(maintenViewController)

        //LandlordViewController....
        let landlordViewController: MaintenViewController = MaintenViewController(nibName: "MaintenViewController", bundle: nil)
        landlordViewController.view.backgroundColor = UIColor.clear
       // landlordViewController.isNavigationBarHidden = true
        arra.append(landlordViewController)

        //MessagesViewController....
        let messagesViewController: MessagesViewController = MessagesViewController(nibName: "MessagesViewController", bundle: nil)
        messagesViewController.view.backgroundColor = UIColor.clear
        arra.append(messagesViewController)
        
        self.setViewController = arra;
      //  self.setUpContainerView();
        
        homeViewController.view.frame = self.view.bounds
        self.containerView.addSubview(homeViewController.view)
        self.addChildViewController(homeViewController);
        homeViewController.willMove(toParentViewController: self)
        self.currentViewController = homeViewController
        
        let button1: HSBarButton =  HSBarButton(image:  UIImage(named : "HomeDeselectedIcon"), imageSelected:  UIImage(named : "HomeSelectedIcon"))
        button1.isSelected = true;

        let button2: HSBarButton = HSBarButton(image:  UIImage(named : "PaymentDeselectedIcon"), imageSelected:  UIImage(named : "PaymentSelectedIcon"))

        let button3: HSBarButton = HSBarButton(image:  UIImage(named : "SettingDeselectedIcon"), imageSelected:  UIImage(named : "SettingSelectedIcon"))

        let button4: HSBarButton = HSBarButton(image:  UIImage(named : "MessageDeselectedIcon"), imageSelected:  UIImage(named : "MessageSelectedIcon"))

        let topTabBar: HSTabBar = HSTabBar(buttons: [button1,button2,button3,button4])
        self.topView.addSubview(topTabBar);
        self.topTabBar = topTabBar;
        
        self.topTabBar.setTabBarCallBack { (tabBar: HSTabBar, barButton: HSBarButton, index: Int) -> Void in
            self.setSelectedIndex = index;
        }
        
        
        if self.revealViewController() != nil {
            self.revealViewController().panGestureRecognizer().isEnabled = false;
            self.revealViewController().tapGestureRecognizer().isEnabled = true;
        }

        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.topTabBar.frame = self.topView.bounds
        self.currentViewController!.view.frame = self.containerView.bounds


    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.topTabBar.frame = self.topView.bounds
        self.currentViewController!.view.frame = self.containerView.bounds
    }
    
    func leftBarButtonClicked() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - PageController...

    
    @IBAction func pageControlValueChanged(_ sender: AnyObject) {
        
        
    }
    
    
    // MARK: - ScrollViewDelegate

    
    func updateScrollViewFrame() {
        
     //   let maxWidth: CGFloat = CGRectGetWidth(self.view.frame)
      //  let maxHeight: CGFloat = CGRectGetHeight(self.view.frame)
        //let maxContentWidth: CGFloat  = (CGFloat(self.viewControllers.count) * maxWidth)
       // let maxContentHeight: CGFloat  = (maxHeight)
       // self.scrollContainerView.contentSize = CGSizeMake(maxContentWidth, maxContentHeight)
        
    }
    
    
    func setUpCurrentView() {
        
        if self.viewControllers.count <= 0 {
            return;
        }
        
        let viewController = self.viewControllers[self.selectedIndex]
        let oldViewController = self.currentViewController
        
        self.currentViewController = viewController
        
        oldViewController!.view.removeFromSuperview()
        oldViewController!.removeFromParentViewController()
        oldViewController!.didMove(toParentViewController: self)
        
        self.currentViewController!.view.frame = self.containerView.bounds
        self.containerView.addSubview(self.currentViewController!.view)
        self.addChildViewController(self.currentViewController!);
        self.currentViewController!.willMove(toParentViewController: self)

    }
    
    func setUpContainerView() {
        
        
        
        
    }
    
    
    var setViewController: [UIViewController] {
       
        set (controllers) {
            self.viewControllers = controllers
        }
        get {
            return self.viewControllers;
        }
        
    }
    
    var setSelectedIndex: Int {
        
        set (index) {
            self.selectedIndex = index;
            self.setUpCurrentView()
        }
        
        get {
            return self.selectedIndex;
        }
        
    }

    // MARK: - ScrollViewDelegate 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
      
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
