//
//  Property.swift
//  TenantTag
//
//  Created by Hardeep Singh on 20/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit
import MapKit

struct Coordinates {
    
    var lat: String
    var lng: String
    
    init(obj: AnyObject) {
        
        let dict: Dictionary? = obj as? Dictionary<String, AnyObject>
        let lat: String = dict!["lat"] as! String
        self.lat = lat
        if lat.isEmpty {
            self.lat = "0"
        }
        
        let lng: String = dict!["lng"] as! String
        self.lng = lng
        if lat.isEmpty {
            self.lng = "0"
        }

    }
    
    func location() -> CLLocation? {
        
        let doubleLat: Double? = Double(self.lat)
        let doubleLng: Double? = Double(self.lng)

        if ( doubleLat != nil &&  doubleLng != nil ) {
          
            let location: CLLocation = CLLocation(latitude: doubleLat!, longitude: doubleLng!)
            return location
            
        }else {
            return nil;
            
        }
       
    }
    
}

struct Amenity {
    
    var day: String
    var name: String
    
    init(obj: AnyObject) {
        
        let dict: Dictionary? = obj as? Dictionary<String, AnyObject>
        let day: String = dict!["day"] as! String
        self.day = day
        let name: String = dict!["name"] as! String
        self.name = name
        
    }

}

struct Service {
    
    var filter: String? = nil
    var phone: String? = nil
    var serviceName: String? = nil
    var serviceProvider: String? = nil
    
    init(obj: AnyObject) {
        
        let dict: Dictionary? = obj as? Dictionary<String, AnyObject>
        
        let filter: String? = dict!["filter"] as? String
        self.filter = filter
        
        let phoneIS: String? = dict!["phone"] as? String
        self.phone = phoneIS != nil ? phoneIS : ""
        
        let serviceNameIS: String? = dict!["serviceName"] as? String
        self.serviceName = serviceNameIS != nil ? serviceNameIS : ""
      
        let serviceProviderIS: String? = dict!["serviceProvider"] as? String
        self.serviceProvider = serviceProviderIS != nil ? serviceProviderIS : ""
        
    }


}

class Property: NSObject {

    var propertyAddress: String? = nil
    var communityAddress: String? = nil
    var communityPhoneNo: String? = nil
    var coordinates: Coordinates? = nil
    
    var amenities: [Amenity] = [Amenity]()
    var services: [Service] = [Service]()
    var landlordPhoto: [HSPhoto]! = [HSPhoto]()
    var tenantPhoto: [HSPhoto]! = [HSPhoto]()

    var leaseDoc: [HSDoc]! = [HSDoc]()
    var communityDoc: [HSDoc]! = [HSDoc]()

    override init() {
        super.init()
    }
    
    convenience init(obj: AnyObject) {
        
        self.init()
        
        let dict: Dictionary? = obj as? Dictionary<String, AnyObject>
        
        if dict != nil {
            
            //Address detail...
            let propertyDetails: Dictionary? = dict!["propertyDetails"] as? Dictionary<String, AnyObject>
            if propertyDetails != nil {
                
                let propertyAddress: String? = propertyDetails!["propertyAddress"] as? String
                self.propertyAddress = propertyAddress != nil ? propertyAddress: ""
                LoginManager.share().propertyAddress = self.propertyAddress;
                
                let communityAddress: String? = propertyDetails!["communityAddress"] as? String
                self.communityAddress = communityAddress != nil ? communityAddress: ""
                
                let communityPhoneNo: String? = propertyDetails!["communityPhoneNo"] as? String
                self.communityPhoneNo = communityPhoneNo != nil ? communityPhoneNo: ""
                
                let coordinates: Dictionary? = propertyDetails!["coordinates"]  as? Dictionary<String, AnyObject>
                if coordinates != nil  {
                    let coordinate = Coordinates(obj: coordinates! as AnyObject)
                    self.coordinates = coordinate
                }else {
                    self.coordinates = nil
                }

            }
            
            
            //AmenitiesList...
            let amenitiesList: [AnyObject]? = dict!["amenities"] as? [AnyObject]
            if amenitiesList != nil {
                
                if self.amenities.count > 0 {
                    self.amenities.removeAll()
                }
                
                for obj in amenitiesList! {
                    let amenity: Amenity = Amenity(obj: obj)
                    self.amenities.append(amenity)
                }
                
            }
            
            //Servicess...
            let serviceList: [AnyObject]? = dict!["services"] as? [AnyObject]
            if serviceList != nil {
                
                if self.services.count > 0 {
                    self.services.removeAll()
                }
                
                for obj in serviceList! {
                    let service: Service = Service(obj: obj)
                    self.services.append(service)
                }
                
            }
            
            let imagessAttached: Dictionary? = dict!["imagessAttached"]  as? Dictionary<String, AnyObject>
            if imagessAttached != nil {
                let landlordPhotos: [AnyObject]? = imagessAttached!["landlord"] as? [AnyObject]
                let tenantPhotos: [AnyObject]? = imagessAttached!["tenant"]  as? [AnyObject]
                
                if landlordPhotos != nil {
                    
                    for obj in landlordPhotos! {
                        let photoM: HSPhoto = self.photoModel(obj)
                        self.landlordPhoto!.append(photoM)
                    }
                    
                }
                
                if tenantPhotos != nil {
                    
                    for obj in tenantPhotos! {
                        let photoM: HSPhoto = self.photoModel(obj)
                        self.tenantPhoto!.append(photoM)

                    }
                }
                
            }
            
            //Docs
            let documentsAttached: Dictionary? = dict!["documentsAttached"]  as? Dictionary<String, AnyObject>
            if documentsAttached != nil {
                
                let community: [AnyObject]? = documentsAttached!["community"] as? [AnyObject]
                let lease: [AnyObject]? = documentsAttached!["lease"]  as? [AnyObject]
                
                if community != nil {
                    for obj in community! {
                        let doc: HSDoc = HSDoc(docObj: obj as! Dictionary<String, AnyObject>)
                        self.communityDoc!.append(doc)
                    }
                }
                
                if lease != nil {
                    for obj in lease! {
                        let doc: HSDoc = HSDoc(docObj: obj as! Dictionary<String, AnyObject>)
                        self.leaseDoc!.append(doc)
                        
                    }
                }
                
            }
           
        }
        
    }
    
    func photoModel(_ obj: AnyObject) -> HSPhoto {
    
        let dict: Dictionary = obj as! Dictionary<String,AnyObject>
        let  id = dict["id"] as! String
        let  thumb = dict["thumb"] as! String
        let  url = dict["url"] as! String
        let photo: HSPhoto = HSPhoto(thumbURLStr: thumb, URLStr: url)
        photo.ID = id;

        return photo;

    }
    
    class func getProperty( _ callBack:@escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        
        //APIGetProperty
        HTTPRequest.request().getRequest(APIGetProperty as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            
            if isSuccess == true {
                let property: Property = Property(obj: data!)
                callBack(true, property, nil)
            }else {
                callBack(false, data, nil)
            }
            
        }
        
    }
    
    
}
