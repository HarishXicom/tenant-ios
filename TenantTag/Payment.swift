//
//  Payment.swift
//  TenantTag
//
//  Created by maninder on 29/12/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import UIKit

enum PaymentStatus : Int{
    
    case Paid = 1
    case Pending = 0
}


 struct RecurringDetails {
    var Day : Int = 1
    var Times : Int = 1
    var RType : String = "Monthly"
}

class Bank : NSObject
{
    var bankName : String
    var bankLogo : NSURL!
    var bankCode : String
    var bankImageStr : String!
    
    override init()
    {
        self.bankName = ""
        self.bankImageStr = ""
        self.bankLogo = NSURL(string: "")
        self.bankCode = ""
    }

    convenience init(obj: AnyObject) {
        self.init()
        
        self.bankName = obj.object(forKey: "bank_name") as! String
        self.bankLogo = NSURL(string : obj.object(forKey: "logo") as! String)
        
        print(self.bankLogo)
        self.bankImageStr = obj.object(forKey: "logo") as! String
        self.bankCode = obj.object(forKey: "bank_code") as! String
    }
    
    
    
    class func getBankList( _ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        HTTPRequest.request().getRequestSynapsePayWithoutHeader("https://synapsepay.com/api/v3/institutions/show", param: nil, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                var bankArray = [Bank]()

                if let banksData = data as? NSDictionary
                {
                    let arrayBanks = banksData.object(forKey: "banks") as! NSArray
                    for item in arrayBanks
                    {
                        let bankNew = Bank(obj: item as AnyObject)
                        bankArray.append(bankNew)
                    }
                    callBack(true, bankArray as AnyObject?, nil)
                }
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
}

class CreditCard : NSObject
{
                   //  Parameters:- token,card_id,address_city,country,exp_month,exp_year,funding,last_digits,brand,created
    var nameOnCard : String = ""
    var token : String = ""
    var cardNumber : String = ""
    var cvv : String = ""
    
    var recordID : String = ""

    
    var saveCard  = true
    var cardID : String = ""
    var last4Digits : String = ""
    var createdAt : String = ""
    var cardType : String = ""
    var funding : String = ""
    
    
  
    var month  : NSInteger = -1 // holds index of arrays
    var year  : NSInteger = -1 // holds index of arrays
    var address1 : String = ""
    var address2 : String = ""
    var strMonth : NSInteger = 1
    var strYear : NSInteger = 2016
    var city : String = ""
    var state : String = ""
    var country = ""
    var zipCode : String = ""
    override init()
    {
     
    }
    
    
    
    
    //    days = 0;
    //    recurring = 0;
    //    times = 0;
    
    
    
    func getStrValue(dict : AnyObject, keyValue : String) -> String
    {
        if let finalDict = dict as? Dictionary<String,AnyObject>{
            if finalDict[keyValue] as? String != nil{
                
                  return finalDict[keyValue] as! String
            }else if finalDict[keyValue] as? NSInteger != nil
            {
               return  (finalDict[keyValue] as! NSInteger).description
            }
              return ""
        }
            return ""
    }
    
    convenience init(obj: AnyObject) {
        self.init()
        
        let objDict = obj as! Dictionary<String,AnyObject>
        print(objDict)
        
        self.nameOnCard = objDict["name_on_card"] as! String
        self.month = NSInteger(objDict["exp_month"] as! String)!
        self.recordID = objDict["record_id"] as! String
        self.cardID = objDict["card_id"] as! String
       self.year = NSInteger(objDict["exp_year"] as! String)!
        self.funding = objDict["funding"] as! String
        self.last4Digits = objDict["last_digits"] as! String
        
        self.cardType = objDict["brand"] as! String
        
        print(self.month)
        
        print(self.cardID)
        print(self.year)
    }
    
    
    
    func deleteSavedCard(_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        //token,card_id,address_city,country,exp_month,exp_year,funding,last_digits,brand,created
        let parameters : NSDictionary = ["card_id": self.cardID ]
        
       // print(parameters)
        
        HTTPRequest.request().postRequest(APIDeleteSavedCard as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
                let newCard =  data as! Dictionary<String,AnyObject>
                let responseDict = newCard["data"]
               // print(responseDict)
                callBack(true, responseDict as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
    func createPayment(_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
         //token,card_id,address_city,country,exp_month,exp_year,funding,last_digits,brand,created
        let parameters : NSDictionary = ["token": self.token ,"save_card" :  1 , "name_on_card" : self.nameOnCard]
        print(parameters)
        HTTPRequest.request().postRequest(APISaveStripeCard as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                //callBack(false, nil, nil)

                let responseDict =  data as! NSDictionary
               
                if responseDict.allKeys.count == 0{
                    HSProgress(.hide)
                    HSShowAlertView("TenantTag", message: "Card already exists!")
                    return
                }else{
                    self.cardID = responseDict["card_id"] as! String
                    self.recordID = responseDict["record_id"] as! String
                    print("No Value")
                      callBack(true, self as AnyObject?, nil)

                }
          
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
    
    func makePaymentWithCard (amount : String , payment : Payment , callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        var parameter : NSDictionary = NSDictionary()
        if self.saveCard == true{
            
           //  params  = ["record_id": self.card.recordID  , "recurring" : 1 , "payment_type" : "Monthly" ,"days" : self.recurringData.Day , "times" : self.recurringData.Times , "payment_method" : paymentType]
            
            
            if payment.recurringEnabled == true{
                
                let parameters : NSDictionary = ["card_id": self.cardID ,"amount" : amount ,"save_card" : 1 ,"record_id" : self.recordID ,"recurring" : 1 , "payment_type" : "Monthly" , "days" : payment.recurringData.Day , "times" : payment.recurringData.Times ,"payment_method" : "Stripe"]
                parameter = parameters
            }else{
                 let parameters : NSDictionary = ["card_id": self.cardID ,"amount" : amount ,"save_card" : 1 ,"record_id" : self.recordID ,"recurring" : 0 , "payment_type" : "Monthly" , "days" : payment.recurringData.Day , "times" : payment.recurringData.Times ,"payment_method" : "Stripe"]
                parameter = parameters
            }
        }else{
            
         let parameters : NSDictionary = ["card_id": self.cardID ,"amount" : amount ,"save_card" : 0 ,"record_id" : self.recordID ,"recurring" : 0 , "payment_type" : "Monthly" , "days" : payment.recurringData.Day , "times" : payment.recurringData.Times , "payment_method" : "Stripe"];
            
            parameter = parameters
            
        }
     print(parameter)
        HTTPRequest.request().postRequest(APIPaymentWithCard as NSString, param: parameter, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
               if let payment =  data as? Dictionary<String,AnyObject>
               {
                
                let dictInner = payment["transaction_id"] as! String
               // self
                callBack(true, dictInner as AnyObject?, nil)
                
                }
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
    
    func makePaymentWithoutSavedCard (amount : String , callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        let parameters : NSDictionary = ["card_id": self.cardID ," amount" : amount ,"save_card" : 0]
        // print(parameters)
        
        HTTPRequest.request().postRequest(APIPaymentWithCard as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                let newCard =  data as! Dictionary<String,AnyObject>
                let responseDict = newCard["data"]
                
                callBack(true, responseDict as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
     }
    
    
    
    func cancelPaymentWithCard (payment : Payment , callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        let parameter : NSDictionary = ["transaction_id": payment.paymentID]
          HSProgress(.show)
        HTTPRequest.request().postRequest(APICancelCCPayment as NSString, param: parameter, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true
            {
                let newCard =  data as! Dictionary<String,AnyObject>
                //let responseDict = newCard["data"]
                callBack(true, newCard as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
  }


enum StatusMicro : String
{
    
    case MicroLocked = "LOCKED"
    case MicroDone = "CREDIT-AND-DEBIT"
    case MicroNotDone = "CREDIT"


    
}


class BankNode : NSObject
{
    
    var nodeType : String = ""
    var nodeID : String = ""
    var bankFullName : String = ""
    var bankShortName = ""
    var accountClass  : String = ""
    var synapseUserID : String = ""
    var bankNodeImage : String = ""
    var bankImageURL : NSURL =  NSURL()
 //   var accountNumber : String = ""
    var amount = ""
    var currency = ""
    
    var microPaymentStatus : StatusMicro = .MicroNotDone
    var routingNumber = ""
    var accountNo = ""
    var recordID : String = ""

    
    override init()
    {
        
    }
    
    
    convenience init(obj: AnyObject) {
        self.init()
        
        let nodeData = obj as! Dictionary<String, AnyObject>
        self.nodeID = nodeData["_id"] as! String
        var infoData = nodeData["info"] as! Dictionary<String, AnyObject>
      //  self.accountNumber = infoData["account_num"] as! String
        
        if infoData["bank_long_name"] as? String != nil{
          //  if
            self.bankFullName = infoData["bank_long_name"] as! String
            if infoData["bank_long_name"] as! String == "0"{
                self.bankFullName = "Other Bank"
            }
        }else
        {
            self.bankFullName = "Other Name"
        }
        
        if let microPayment = nodeData["allowed"] as? String{
//            if microPayment == "CREDIT" || microPayment == "LOCKED"
//            {
                self.microPaymentStatus = StatusMicro(rawValue: microPayment)!
//            }
//            else{
//                self.microPaymentStatus = true
//
//            }
        }
        self.accountClass = infoData["class"] as! String
        self.routingNumber = infoData["routing_num"] as! String
        self.accountNo = infoData["account_num"] as! String
        
        
        if let nodeType = infoData["type"] as? String{
                self.nodeType = nodeType
        }

       var  balanceData = infoData["balance"] as! Dictionary<String, AnyObject>
        if let amount = balanceData["amount"] as? NSNumber{
            self.amount = amount.description

        }else{
            self.amount = balanceData["amount"] as! String
            
        }
        self.currency = balanceData["currency"] as! String
    }
    
    
    convenience init(localNode: Any) {
        self.init()
        
    let dictNew = localNode as! Dictionary<String,Any>
        self.accountNo = dictNew["account_number"] as! String
        self.amount = dictNew["amount"] as! String
        self.routingNumber = dictNew["routing_number"] as! String
        self.bankFullName = dictNew["bank_name"] as! String
        self.accountNo = dictNew["account_number"] as! String
        self.accountNo = dictNew["account_number"] as! String
        self.accountClass = dictNew["class"] as! String
        self.nodeID = dictNew["oid"] as! String
        
        
        
        let  microPayment  = dictNew["synapse_micro_permission"] as! String
        
        self.microPaymentStatus = StatusMicro(rawValue: microPayment)!

//        if dictNew["synapse_micro_permission"] as! String == "CREDIT"
//        {
//            self.microPaymentStatus = false
//        }else{
//            self.microPaymentStatus = true
//        }

//        amount = "";
//        "bank_code" = Other;
//        class = CHECKING;
//        "created_date" = "2017-05-17 00:30:34";
//        id = 8;
//        "is_deleted" = 0;
//        "modified_date" = "2017-05-17 00:30:34";
//        oid = 591a8b0798021600202ac214;
//        status = 0;
//        "synapse_micro_permission" = "CREDIT-AND-DEBIT";
//        "user_id" = 408;
//
        
        
    }
    
    
}



class Payment: NSObject {
   
    var previousAccount : String = ""
    var recurringEnabled = false
    var recurringData = RecurringDetails()
    var card = CreditCard()
    var amount : String = "0"
    var paymentDate : String = ""
    var status : Bool = false
    
    var userID : String = ""
    var paymentID : String = ""
    var refreshToken : String = ""
     var authKey : String = ""
    var routeNumber : String = ""
    var paymentType : PayType = .CC
    var bankAccountNumber : String = ""
    
    var bankNodes = [BankNode]()
    var mfaEnabled = false
    
    var previousToken = String()
    var selectedNode = BankNode()
    
    override init()
    {
        
        
    }
    
    
    convenience init(objAccounts: AnyObject) {
        self.init()
        
         let payment = objAccounts as! Dictionary<String, AnyObject>
        self.amount = payment["amount"] as! String
        self.selectedNode.nodeID = payment["node_id"] as! String
        self.selectedNode.accountNo = payment["account_number"] as! String
        self.selectedNode.recordID = payment["record_id"] as! String
        self.selectedNode.synapseUserID =  payment["synapse_user_id"] as! String
        var recurringValue : String = ""
       if  (payment["recurring"] as? String ) != nil{
             recurringValue = payment["recurring"] as! String
       }else  if (payment["recurring"] as? NSNumber ) != nil{
        recurringValue = (payment["recurring"] as! NSNumber).stringValue
       }
        if recurringValue == "1"{
            self.recurringEnabled = true
            self.recurringData.Day = Int(payment["days"] as! String)!
            self.recurringData.Times = Int(payment["times"] as! String)!
        }else{
            self.recurringEnabled = false
        }
    
        self.selectedNode.bankFullName =  payment["bank_name"] as! String
        self.selectedNode.bankImageURL = NSURL(string: payment["bank_logo"] as! String)!
        self.selectedNode.routingNumber = payment["routing_number"] as! String
    }
    
    
    
    
    convenience init(objOthers: Any) {
        self.init()
        
        let payment = objOthers as! Dictionary<String, AnyObject>
        
        
//        let param = ["oid": node.nodeID  , "account_number" : node.accountNo , "bank_name" : node.bankFullName ,"bank_code" : "other" , "class" : node.accountClass , "synapse_micro_permission" :  status ,"routing_number" : node.routingNumber ,"amount" : node.amount  ] as [String : Any]
//
        
        
//        "synapse_unverfied_account" =         (
//            {
//                "account_number" = 6789;
//                amount = "0.00";
//                "bank_code" = other;
//                "bank_name" = "BANK OF AMERICA";
//                class = CHECKING;
//                "created_date" = "2017-10-05 02:36:20";
//                id = 53;
//                "is_deleted" = 0;
//                "modified_date" = "2017-10-05 02:36:20";
//                oid = 59d5fd136d7d8a002f791d25;
//                "routing_number" = 20;
//                status = 1;
//                "synapse_micro_permission" = CREDIT;
//                "user_id" = 1278;
//            }
//        );
//        
//
        
        
        self.selectedNode.nodeID = payment["oid"] as! String
         self.selectedNode.accountClass = payment["class"] as! String

        self.selectedNode.accountNo = payment["account_number"] as! String
        self.selectedNode.recordID = payment["id"] as! String
        
        
        
        let status = payment["synapse_micro_permission"] as! String
        self.selectedNode.microPaymentStatus = StatusMicro(rawValue: status)!
        self.selectedNode.amount = payment["amount"] as! String
        self.selectedNode.bankFullName =  payment["bank_name"] as! String
        self.selectedNode.routingNumber = payment["routing_number"] as! String
        
    }

    
    
    convenience init(objCreditCard: AnyObject) {
        self.init()
        
        
        let payment = objCreditCard as! Dictionary<String, AnyObject>
        self.amount = "0"
        
        var recurringValue : String = ""
        if  (payment["recurring"] as? String ) != nil{
            recurringValue = payment["recurring"] as! String
        }else  if (payment["recurring"] as? NSNumber ) != nil{
            recurringValue = (payment["recurring"] as! NSNumber).stringValue
        }
        if recurringValue == "1"{
            self.recurringEnabled = true
            self.recurringData.Day = Int(payment["days"] as! String)!
            self.recurringData.Times = Int(payment["times"] as! String)!
        }else{
            self.recurringEnabled = false
        }
        self.card = CreditCard(obj: payment as AnyObject)
        
        
    }
    

    convenience init(objNew: AnyObject) {
        self.init()
        let paymentObj = objNew as! Dictionary<String,AnyObject>
        
        
        print(paymentObj)
        if paymentObj["due_amount"] as? String != nil{
            self.amount = paymentObj["due_amount"] as! String
        }else {
            self.amount = (paymentObj["due_amount"] as! NSNumber).description
        }
        let strDate = paymentObj["due_date"] as! String
        
        if self.amount == ""
        {
            self.amount = "0"
        }
        
        if strDate == "" || strDate == "0000-00-00"
        {
            self.paymentDate = ""
        }else{
            self.paymentDate = strDate.changeDateString()
        }
    
    }
    

    convenience init(obj: AnyObject) {
        self.init()
        
        let paymentObj = obj as! Dictionary<String,AnyObject>
        self.amount = paymentObj["amount"] as! String
        let strDate = paymentObj["created_date"] as! String
        self.paymentDate = strDate.changeDateFormat()
        
        if (paymentObj["status"] as? String) != nil
        {
            if (NSInteger(paymentObj["status"] as! String) == 1){
                self.status = true
            }else if paymentObj["status"] as! String == "PAID" {
                self.status = true
            }else{
                self.status = false
                
            }
        }else if paymentObj["status"] as! Bool == true {
            self.status = true
        }else{
            
            print("NO Entry")
        }
        
        if (paymentObj["payment_type"] as? String) != nil
        {
            if(paymentObj["payment_type"] as! String == "Stripe"){
                paymentType = .CC
            }else{
                paymentType = .Bank
            }
         }
        
        
        if (paymentObj["account_number"] as? String) != nil
        {
            previousAccount  = paymentObj["account_number"] as! String
        }

        
        if let transactionID = paymentObj["transaction_id"] as? String
        {
            paymentID  = transactionID

        }
        
     }
    
    class func getPreviousTransactions(_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        let parameters : NSDictionary = ["limit": 50]
        HTTPRequest.request().postRequest(APIGetPreviousTransaction as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
              var paymentPrevious = [Payment]()
                let arrayTransaction =  data as! Array<AnyObject>
                
                if arrayTransaction.count > 0 {
                    
                    for item in arrayTransaction
                    {
                        let newPayment = Payment(obj: item)
                        
                        paymentPrevious.append(newPayment)
                    }
                }
                callBack(true, paymentPrevious as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
        
    }
    
    
    class func getDuePayment(_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        
        HSProgress(.show)
        HTTPRequest.request().getRequest(APIRentDueAmount as NSString, param: nil, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
               HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
                
                let dataTransaction = data as! Dictionary<String,AnyObject>
                print(dataTransaction)
                let paymentPrevious = Payment(objNew:dataTransaction as AnyObject)
                
                let statusRecurring = dataTransaction["recurring_info"]?["recurring_status"] as! Bool
                let savedCard = dataTransaction["saved_stripe_card"] as! Bool
                
                print(savedCard)
                let savedAccount = dataTransaction["saved_synapse_card"] as! Bool
                let dictFull:[String:AnyObject] = [
                    "RecurringStatus": statusRecurring as AnyObject,
                    "PreviousPayment": paymentPrevious as AnyObject,
                    "AlreadySavedCard" : savedCard as AnyObject ,
                    "AlreadySavedAccount" : savedAccount as AnyObject
                ]
                callBack(true, dictFull as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
        
    }
    
    class  func getSavedBankNodes (_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        let parameters : NSDictionary = [:]
        
        HTTPRequest.request().postRequest(APIGetSavedAccountList as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
                var dictBoth =  [String : Any]()
                var payments = [Payment]()
                let cards =  data as! Dictionary<String,AnyObject>
                
                let paymentsData = cards["synapse_account"] as! [AnyObject]
                for payment in paymentsData
                {
                    let paymentNew = Payment(objAccounts: payment)
                  payments.append(paymentNew)
                }
                
                dictBoth["Saved"] = payments
                
                var otherAccs = [Payment]()

                if let otherAccounts = cards["synapse_unverfied_account"] as? [Any]
                {
                    for payment in otherAccounts
                    {
                        let paymentNew = Payment(objOthers: payment)
                        otherAccs.append(paymentNew)
                    }
                    
                }
                dictBoth["Other"] = otherAccs
                
                callBack(true, dictBoth as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
    class  func getSavedCreditCards (_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        let parameters : NSDictionary = [:]
        
        HTTPRequest.request().postRequest(APIGetSavedCardsList as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
                var creditCards = [Payment]()
                
                let cards =  data as! Dictionary<String,AnyObject>
                
                let payments = cards["stripe_card"] as! [AnyObject]
                for pCard in payments
                {
                    let cardNew = Payment(objCreditCard: pCard)
                    creditCards.append(cardNew)
                }
                callBack(true, creditCards as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
    func updatingRecurringInfoForCards(paymentType : String , _ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        var params : NSDictionary! = NSDictionary()
        if self.recurringEnabled == true
        {
            params  = ["record_id": self.card.recordID  , "recurring" : 1 , "payment_type" : "Monthly" ,"days" : self.recurringData.Day , "times" : self.recurringData.Times , "payment_method" : paymentType]
        }else{
            ///delete recurring info
             params  = ["record_id": self.card.recordID  , "recurring" : 0 , "payment_type" : "Monthly" ,"days" : self.recurringData.Day , "times" : self.recurringData.Times, "payment_method" : paymentType]
        }
        
        print()
        HSProgress(.show)
        HTTPRequest.request().postRequest(APIUpdateRecurringInfo as NSString, param: params, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                callBack(true, data as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }

    
    
    
    func updatingRecurringInfoForAccounts(paymentType : String , _ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        var params : NSDictionary! = NSDictionary()
        if self.recurringEnabled == true
        {
            params  = ["record_id": self.selectedNode.recordID  , "recurring" : 1 , "payment_type" : "Monthly" ,"days" : self.recurringData.Day , "times" : self.recurringData.Times , "payment_method" : paymentType]
        }else{
            ///delete recurring info
            params  = ["record_id": self.selectedNode.recordID   , "recurring" : 0 , "payment_type" : "Monthly" ,"days" : self.recurringData.Day , "times" : self.recurringData.Times, "payment_method" : paymentType]
        }
        
        
        HSProgress(.show)
        HTTPRequest.request().postRequest(APIUpdateRecurringInfo as NSString, param: params, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                callBack(true, data as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }
    
    
    func deleteSavedBankAccount(_ callBack: @escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        //token,card_id,address_city,country,exp_month,exp_year,funding,last_digits,brand,created
        let parameters : NSDictionary = ["record_id": self.selectedNode.recordID ]
        
        HTTPRequest.request().postRequest(APIDeleteSavedAccount as NSString, param: parameters, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
                let newCard =  data as! Dictionary<String,AnyObject>
                let responseDict = newCard["data"]
                // print(responseDict)
                callBack(true, responseDict as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
    }

}

