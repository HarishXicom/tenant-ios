//
//  LoginManager.swift
//  Who's In
//
//  Created by Hardeep Singh on 21/10/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit




//extension Bool : NSInteger {
public  func intValue(testBool : Bool) -> Int {
    if testBool {
        return 1
    }
    return 0
}
//}



enum DocStatus: String {
    case Missed = "MISSING|INVALID"
    case UnderReview = "SUBMITTED|REVIEWING"
    case Invalid    = "SUBMITTED|INVALID"
    case Approved        = "SUBMITTED|VALID"
    case ReSubmissionNeeded = "RESUBMIT|INVALID"
    case Submitted = "SUBMITTED"

}



typealias closureType = () -> (Bool,AnyObject)

@objc open class LoginManager: NSObject {
    
    fileprivate var me: Me! = Me()
    
    fileprivate var paymentInfo : Payment = Payment()
    
    var propertyAddress: String? = nil
    
    class var getMe: Me {
        return LoginManager.share().me
    }
    
    class var getPaymentDetail: Payment {
        return LoginManager.share().paymentInfo
    }
    
    override init() {
        super.init()
        
        let me = self.getMeArchiver()
        if (me == nil) {
            self.me = Me()
        }else{
            self.me = me
            
        }
    }
    
    /**
     Returns the default singleton instance.
     */
    
    open class func share() -> LoginManager {
        
        struct Static {
            //Singleton instance. Initializing keyboard manger.
            static let kbManager = LoginManager()
        }
        /** @return Returns the default singleton instance. */
        return Static.kbManager
    }
    //MARK: - *******************************************
    //MARK: -
    
    func logout( _ callBack:@escaping (Bool,AnyObject?,NSError?)->())  {
        HSProgress(.show)
        HTTPRequest.request().getRequest(APILogout as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            
            if isSuccess {
                self.removeUserProfile()
                callBack(true,data,nil)
            }else {
                callBack(false,data,nil)
            }
        }
    }
    
    func login( _ callBack:@escaping (Bool,AnyObject?,NSError?)->())  {
        
        //ForgotPasswordViewController
        let emailAddress: String = self.me.emailAddress!
        let password: String = self.me.password!
        
        let params : NSDictionary = ["email":emailAddress,
                                     "password":password,
                                     "deviceType": DeviceType
        ]
        
        HSProgress(.show)
        
        HTTPRequest.request().postRequest(APILogin as NSString, param: params) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            
            if error != nil {
                callBack(false, nil, nil)
                HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                return;
            }
            if isSuccess == true {
                let userParams =  data as! Dictionary<String,AnyObject>
                print(userParams)
                self.me.updateUser(userParams as AnyObject)
                
              
                
                if  self.me.synapsePayUserID == ""
                {
                    self.me.userIPAddress = getIPAddresses()[0]
                    self.me.userFingerPrint = getDeviceID()
                    self.saveUserProfile()
                    self.createUserForPayment(callBack: { (isSucess, data, error) in
                        HSProgress(.hide)
                        if isSuccess
                        {
                            callBack(true, data, nil)
                        }else
                        {
                            callBack(false, nil, nil)
                        }
                    })
                }else{
                    HSProgress(.hide)
                    callBack(true, data, nil)
                    return
                }
                
            }else {
                HSProgress(.hide)
                if data != nil {
                    
                    let userParams =  data as? Dictionary<String,AnyObject>
                    let message: String? =  userParams!["message"] as? String
                    if  message != nil {
                        //   HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                    }else{
                        HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                    }
                    
                }else {
                    HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                }
                
                callBack(false, nil, nil)
                return
            }
        }
        
        callBack(false, nil, nil)
        return
        
    }
    
    func getUpdateUserProfile( _ callBack:@escaping (Bool,AnyObject?,NSError?)->())  {
        
        
        HSProgress(.show)
        
        HTTPRequest.request().getRequest( APIGetUserProfile as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            
            if error != nil {
                callBack(false, nil, nil)
                HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                return;
            }
            
            if isSuccess == true {
                
                // print(data)
                
                //let message =  responseDict["message"]
                let userParams =  data as! Dictionary<String,AnyObject>
                self.me.updateUser(userParams as AnyObject)
                self.saveUserProfile()
                callBack(true, data, nil)
                return
                
            }else {
                
                if data != nil {
                    
                    let userParams =  data as? Dictionary<String,AnyObject>
                    let message: String? =  userParams!["message"] as? String
                    if  message != nil {
                        HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                    }else{
                        HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                    }
                    
                }else {
                    HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                }
                
                callBack(false, nil, nil)
                return
                
            }
            
        }
        
        callBack(false, nil, nil)
        return
        
    }
    
    func forgotPassword(_ emailAddress: String!, callBack:@escaping (Bool, AnyObject?, NSError?)->()) -> Bool {
        
        let params : NSDictionary = ["email":emailAddress,
                                     ]
        
        HSProgress(.show)
        
        HTTPRequest.request().postRequest(APIForgotPassword as NSString, param: params) { (isSuccess:Bool, data:AnyObject?, error:NSError?) -> () in
            HSProgress(.hide)
            
            if error != nil {
                callBack(false, nil, nil)
                HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                return;
            }
            
            if isSuccess {
                
                callBack(true,data,nil)
                let dictIs:Dictionary? = data as? Dictionary<String, AnyObject>
                
                if dictIs != nil {
                    let string:String? = dictIs!["message"] as? String
                    if string != nil {
                        HSShowAlertView("TenantTag", message: string! as NSString)
                    }else {
                        HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                    }
                }
                
            }else {
                
                callBack(false, data, error)
                
                let dictIs:Dictionary? = data as? Dictionary<String, AnyObject>
                if dictIs != nil {
                    let string: String? = dictIs!["message"] as? String
                    if string != nil {
                        HSShowAlertView("TenantTag", message: string! as NSString)
                    }else {
                        HSShowAlertView("TenantTag", message: "unknown error occurred ")
                    }
                }else {
                    HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                }
                
            }
        }
        
        
        return true
        
    }
    
    func updateUserProfile( _ callBack:@escaping (Bool, AnyObject?, NSError?) ->( )) {
        
        //ForgotPasswordViewController
        let first_name: String = self.me.firstName!
        let last_name: String = self.me.lastName!
        
        
        let date: Date = self.me.brithdayDate! as Date;
        let timeInterval: Double = date.timeIntervalSince1970;
        let dob: String = "\(timeInterval)"
        
        let params : NSDictionary = ["first_name": first_name,
                                     "last_name": last_name,
                                     "dob": dob,
                                     "third_party_sharing" : intValue(testBool: self.me.thirdParty)
        ]
        
        print(params)
        HSProgress(.show)
        
        HTTPRequest.request().postRequest( APIUpdateUserInfo as NSString, param: params) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            
            if error != nil {
                callBack(false, nil, nil)
                HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                return;
            }
            
            if isSuccess == true {
                //let message =  responseDict["message"]
                let userParams =  data as! Dictionary<String,AnyObject>
                self.me.updateUser(userParams as AnyObject)
                self.saveUserProfile()
                callBack(true, data, nil)
                return
            }else {
                if data != nil {
                    let userParams =  data as? Dictionary<String,AnyObject>
                    let message: String? =  userParams!["message"] as? String
                    if  message != nil {
                        HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                    }else{
                        HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                    }
                }else {
                    HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                }
                callBack(false, nil, nil)
                return
            }
        }
        
    }
    func updateSynapseUserID( synapsePayUserID : String , _ callBack:@escaping (Bool, AnyObject?, NSError?) ->( )) {
        
        let newFingerPrint = LoginManager.getMe.userFingerPrint
        
        let params : NSDictionary = ["synapse_user_id": synapsePayUserID, "synapse_fingerprint" : newFingerPrint!  ,  "synapse_ip_address" : LoginManager.getMe.userIPAddress ]
         HSProgress(.show)
        HTTPRequest.request().postRequest( APIUpdateUserInfo as NSString, param: params) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                return;
            }
            if isSuccess == true {
                
                self.me.synapsePayUserID = synapsePayUserID
                self.saveUserProfile()
                
                callBack(true, data, nil)
                return
                
            }else {
                
              
                if data != nil {
                    let userParams =  data as? Dictionary<String,AnyObject>
                    let message: String? =  userParams!["message"] as? String
                    if  message != nil {
                        HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                    }else{
                        HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                    }
                    
                }else {
                    HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                }
                
                callBack(false, nil, nil)
                return
                
            }
            
        }
        
    }
    
    
    
    
    
    func sendInvitationToLandlord(name : String , email : String , info : String ,  callBack:@escaping (Bool, AnyObject?, NSError?) ->()) {
        
        let params : NSDictionary = ["name":name,
                                     "email":email,
                                     "text": info]
        HSProgress(.show)
        HTTPRequest.request().postRequest(APISendInvitationToLandLord as NSString, param: params) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                HSShowAlertView("TenantTag", message: "Unknow error occurred", controller: nil)
                return;
            }
            if isSuccess == true {
                //let message =  responseDict["message"]
                callBack(true, "Invitation sent." as AnyObject?, nil)
                
            }else {
                
                if data != nil {
                    
                    let userParams =  data as? Dictionary<String,AnyObject>
                    let message: String? =  userParams!["message"] as? String
                    if  message != nil {
                        //   HSShowAlertView("TenantTag", message: message! as NSString, controller: nil)
                    }else{
                        HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                    }
                    
                }else {
                    HSShowAlertView("TenantTag", message: "Unknown error occurred", controller: nil)
                }
                
                callBack(false, nil, nil)
                return
                
            }
            
        }
        
        callBack(false, nil, nil)
        return
        
        
        
        
        
    }
    
    // MARK: - Archive
    // MARK: -
    
    func saveUserProfile() {
        print(self.me.verified)
        
        let data = NSKeyedArchiver.archivedData(withRootObject: self.me)
        UserDefaults.standard.set(data, forKey: "Me")
        UserDefaults.standard.synchronize()
        
    }
    
    func removeUserProfile(){
        
        UserDefaults.standard.removeObject(forKey: "Me")
        UserDefaults.standard.synchronize()
        self.me = Me()
        
    }
    
    func getMeArchiver() -> Me? {
        
        
        if let data = UserDefaults.standard.object(forKey: "Me") as? Data {
            let me = NSKeyedUnarchiver.unarchiveObject(with: data)
            return me as? Me
        }else{
            return nil
        }
    }
    
    class  func getTenantPropertyInformation(callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        HTTPRequest.request().postRequest(APIGetTenantPropertyInfo as NSString, param: nil, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                
                let newDict = data as? Dictionary<String,AnyObject>
                //  let intNew = newDict?["subscription_status"] as! NSNumber
                if let strSubscriptionStatus = newDict?["subscription_status"] as? NSNumber{
                    if strSubscriptionStatus.intValue == 1{
                        LoginManager.getMe.landLordSubscriptionStatus = true
                    }else{
                        LoginManager.getMe.landLordSubscriptionStatus = false
                    }
                }else if let intStatus = newDict?["subscription_status"] as? String{
                    
                    if intStatus == "1"{
                        LoginManager.getMe.landLordSubscriptionStatus = true
                    }else{
                        LoginManager.getMe.landLordSubscriptionStatus = false
                    }
                    
                }
                if let ownerSynapseFi = newDict?["synapseLinkedAccountStatus"] as? NSNumber{
                    if ownerSynapseFi.intValue == 1{
                        LoginManager.getMe.ownerSynpsePayEnabled = true
                    }else{
                        LoginManager.getMe.ownerSynpsePayEnabled = false
                    }
                }
                
               if let strDoc = newDict?["lease_doc"] as? String
               {
                leasedDocURL = strDoc
                
                }
                
                let propertyDetails =  newDict?["property_detail"] as! Dictionary<String,AnyObject>
                if  let synapseDetails =  newDict?["synapseDetail"] as? Dictionary<String,AnyObject>
                {
                    LoginManager.getMe.landlordAccountNo =  synapseDetails["synapse_deposit_account_id"] as! String
                }
                let strTPay = propertyDetails["tpay"] as! String
                if strTPay == "Yes"
                {
                    LoginManager.getMe.tPayStatus = .Allowed
                }else{
                    LoginManager.getMe.tPayStatus = .NotAllowed
                }
                LoginManager.share().saveUserProfile()
                callBack(true, data as AnyObject?, nil)
                
            }else  {
                callBack(false, nil, nil)
            }
        })
        
    }
    
    
    //MARK:- SynapsePay API
    //MARK:-
    
    
    func createUserForPayment(callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let first_name: String = self.me.firstName!
        let last_name: String = self.me.lastName!
        let params : NSDictionary = ["legal_names": [first_name + " " + last_name] ,
                                     "logins":  [["email": self.me.emailAddress!]],
                                     "phone_numbers":  [self.me.phoneNumber!]
        ]
        print(params)
        HTTPRequest.request().postRequestToSynpsePay(createUserSP as NSString, userID: "", param: params) { (isSuccess, Response, error) in
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    let newUserID = dictResponse.object(forKey: "_id") as! String
                    defaults.set(DocStatus.Missed.rawValue, forKey: synapsePayDocStatus)
                    let token = dictResponse.object(forKey: "refresh_token") as! String
                    defaults.set(token, forKey: synapsePayRefreshToken)
                    self.me.synapsePayUserID = newUserID
                    self.updateSynapseUserID(synapsePayUserID: newUserID, { (isSuccess, response, error) in
                         HSProgress(.hide)
                        if isSuccess{
                            callBack(true, newUserID as AnyObject?, nil)
                        }else{
                          
                            callBack(false, nil, nil)
                        }
                    })
                }
            }else{
                HSProgress(.hide)
                
            }
        }
    }
    
    
    func synapsePayOTP( otpCode : String  , callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        
        let strLink = authenticateUserSP + self.me.synapsePayUserID
        let strToken =    defaults.value(forKey: synapsePayRefreshToken) as! String
        
        
        let params : NSDictionary = ["refresh_token": strToken, "phone_number" : self.me.phoneNumber  , "validation_pin" : otpCode ]
      HSProgress(.show)
        HTTPRequest.request().postRequestToSynpsePay(strLink as NSString, userID: "" as NSString, param: params) { (isSuccess, Response, error) in
            
             HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    callBack(true,dictResponse , nil)
                }
            }else{
                
                HSProgress(.hide)
                
            }
        }
        
        
    }
    
    
    
    func AuthenticateUser( userID : String , refreshToken : String , callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let strLink = authenticateUserSP + userID
        
        let params : NSDictionary = ["refresh_token": refreshToken]
        HTTPRequest.request().postRequestToSynpsePay(strLink as NSString, userID: "" as NSString, param: params) { (isSuccess, Response, error) in
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    callBack(true,dictResponse , nil)
                }
            }else{
                
                HSProgress(.hide)
                
            }
        }
    }
    
    //MARK:- ADD Documents
    //MARK:-
    
    
    func addDocuments(dict : Dictionary<String , Any> ,  callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        
        var dictReturning = dict
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID
        let dateBirth = dict["DOB"] as! Date
        
        dictReturning["DOBStr"] = dateBirth.returnStrFromDate()
        
        let month = (dateBirth as NSDate).month
        let day = (dateBirth as NSDate).day
        let year = (dateBirth as NSDate).year
        
        

        
        let dictVirtual  :  [String : String ] = ["document_type" : "SSN" ,"document_value" : dict["SSN"] as! String ]
        let vitualDoc : [[String : String ]] = [dictVirtual]
        
        var physicalDocArray = [[String : String ]]()
        if dict["License"] as! String != ""
        {
                 physicalDocArray  = [["document_value" : dict["License"] as! String ,"document_type" : "GOVT_ID" ],["document_value" : dict["LeaseCopy"] as! String ,"document_type" : "LEGAL_AGREEMENT" ]]
        }else{
                 physicalDocArray = [["document_value" : dict["LeaseCopy"] as! String ,"document_type" : "LEGAL_AGREEMENT" ]]
        }
        
     
//
//        let physicalDocArray : [[String : String ]] = [["document_value" : dict["LicenseID"] as! String ,"document_type" : "GOVT_ID" ],["document_value" : dict["Selfie"] as! String ,"document_type" : "SELFIE" ],["document_value" : dict["LeaseCopy"] as! String ,"document_type" : "LEGAL_AGREEMENT" ]
//            ,["document_value" : dict["EIN"] as! String ,"document_type" : "W9_DOC" ]]
        
        let dictDocument = ["name" : dict["Name"] as! String  , "email" : dict["Email"] as! String ,"phone_number"  :  dict["PhoneNumber"] as! String  ,"alias" : dict["Name"] as! String ,"entity_scope" :  dict["Profession"] as! String ,"entity_type": dict["Gender"] as! String  ,"address_postal_code" : dict["ZipCode"] as! String , "address_country_code": "US" ,"address_city" : dict["City"] as! String , "address_street" : dict["Address"] as! String  ,"day" : day , "month" : month  , "year" : year    ,"ip" : self.me.userIPAddress ,"virtual_docs" : vitualDoc ,"address_subdivision" : dict["State"] as! String,"physical_docs" :  physicalDocArray] as [String : Any]
        let params : NSDictionary = ["documents": [dictDocument]]
       // print(params)

        HSProgress(.show)
        HTTPRequest.request().patchRequestToSynpsePay(strLink as NSString, userID: defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                  let dictStatus = dictResponse.object(forKey: "doc_status") as? Dictionary<String, Any>
                    self.setDoctStatus(dictStatus: dictStatus)
                    let arrayDocuments = dictResponse.object(forKey: "documents") as? NSArray
                    if arrayDocuments != nil
                    {
                        if (arrayDocuments?.count)! > 0 {
                            let dictDocument = arrayDocuments?.object(at: 0) as! NSDictionary
                            let documentID = dictDocument.object(forKey: "id") as! String
                            dictReturning["ID"] = documentID
                            dictReturning["Permission"] = dictDocument.object(forKey: "permission_scope") as! String
//                            let arrayVirtualDoc = dictDocument.object(forKey: "virtual_docs") as! NSArray
//                            if arrayVirtualDoc.count > 0 {
//                                let dictVirtualFirst = arrayVirtualDoc.object(at: 0) as! NSDictionary
//                                dictReturning["VirtualDocStatus"] = dictVirtualFirst.object(forKey: "status") as! String
//                                dictReturning["VirtualDocID"] = documentID
//                            }
//                            let arrayPhysicalDoc = dictDocument.object(forKey: "physical_docs") as! NSArray
//                            if arrayPhysicalDoc.count > 0 {
//                                let dictPhysicalFirst = arrayVirtualDoc.object(at: 0) as! NSDictionary
//                                dictReturning["PhysicalDocStatus"] = dictPhysicalFirst.object(forKey: "status") as! String
//                                dictReturning["PhysicalDocID"] = documentID
//                            }
                        }
                        callBack(true,dictReturning as AnyObject? , nil)
                    }
                }else{
                    HSProgress(.hide)
                }
            }
            
        }
    }
    
    func updateDocuments(dict : Dictionary<String , Any> ,  callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        var dictReturning = dict
        
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID
        let dateBirth = dict["DOB"] as! Date
        dictReturning["DOBStr"] = dateBirth.returnStrFromDate()
        
        let month = (dateBirth as NSDate).month
        let day = (dateBirth as NSDate).day
        let year = (dateBirth as NSDate).year
        
        
        let dictVirtual  :  [String : String ] = ["document_type" : "SSN" ,"document_value" : dict["SSN"] as! String ]
        let vitualDoc : [[String : String ]] = [dictVirtual]
        
        
        let physicalDocArray : [[String : String ]] = [["document_value" : dict["License"] as! String ,"document_type" : "GOVT_ID" ],["document_value" : dict["LeaseCopy"] as! String ,"document_type" : "LEGAL_AGREEMENT" ]]
        
        let dictDocument = ["id" :  dict["KYC_ID"] as! String , "name" : dict["Name"] as! String  , "email" : dict["Email"] as! String ,"phone_number"  :  dict["PhoneNumber"] as! String  ,"alias" : dict["Name"] as! String ,"entity_scope" :  dict["Profession"] as! String ,"entity_type": dict["Gender"] as! String  ,"address_postal_code" : dict["ZipCode"] as! String  ,"address_city" : dict["City"] as! String , "address_subdivision" : dict["State"] as! String,"address_street" : dict["Address"] as! String  ,"day" : day , "month" : month  , "year" : year    ,"ip" : self.me.userIPAddress ,"virtual_docs" : vitualDoc , "physical_docs" :  physicalDocArray] as [String : Any]
        
       let params : NSDictionary = ["documents": [dictDocument]]
        HSProgress(.show)
        HTTPRequest.request().patchRequestToSynpsePay(strLink as NSString, userID: defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    let dictStatus = dictResponse.object(forKey: "doc_status") as? Dictionary<String, Any>
                    self.setDoctStatus(dictStatus: dictStatus)
                    let arrayDocuments = dictResponse.object(forKey: "documents") as? NSArray
                    
                    if arrayDocuments != nil
                    {
                        if (arrayDocuments?.count)! > 0 {
                            let dictDocument = arrayDocuments?.object(at: 0) as! NSDictionary
                            let documentID = dictDocument.object(forKey: "id") as! String
                            dictReturning["ID"] = documentID
                            dictReturning["Permission"] = dictDocument.object(forKey: "permission_scope") as! String
                            let arrayVirtualDoc = dictDocument.object(forKey: "virtual_docs") as! NSArray
                            if arrayVirtualDoc.count > 0 {
                                let dictVirtualFirst = arrayVirtualDoc.object(at: 0) as! NSDictionary
                                dictReturning["VirtualDocStatus"] = dictVirtualFirst.object(forKey: "status") as! String
                                dictReturning["VirtualDocID"] = documentID
                                
                            }
                            let arrayPhysicalDoc = dictDocument.object(forKey: "physical_docs") as! NSArray
                            if arrayPhysicalDoc.count > 0 {
                                let dictPhysicalFirst = arrayVirtualDoc.object(at: 0) as! NSDictionary
                                dictReturning["PhysicalDocStatus"] = dictPhysicalFirst.object(forKey: "status") as! String
                                dictReturning["PhysicalDocID"] = documentID
                            }
                        }
                        callBack(true,dictReturning as AnyObject? , nil)
                    }
                }else{
                    HSProgress(.hide)
                }
            }
            
        }
    }
    
    
    func getBankNodes(userName : String , password : String , bankName : String , bankLogo : String ,  callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID + "/nodes"
        
        let dict = ["bank_id": userName , "bank_pw" : password, "bank_name" : bankName  ]
        let params : NSDictionary = ["type": "ACH-US",
                                     "info":  dict]
        HSProgress(.show)
        HTTPRequest.request().postRequestToSynpsePay(strLink as NSString, userID:  defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                
                
                if let dictResponse = Response as? NSDictionary
                {
                    var nodes = [BankNode]()
                    let mfaStatus = dictResponse.object(forKey: "http_code") as! String
                    
                    if mfaStatus == "202"
                    {
                        self.paymentInfo.mfaEnabled = true
                        let dictMFA = dictResponse.object(forKey: "mfa") as! NSDictionary
                        self.paymentInfo.previousToken = dictMFA.object(forKey: "access_token") as! String
                    }else{
                        if let nodesArry = dictResponse.object(forKey: "nodes") as? NSArray
                        {
                            for item in nodesArry {
                                let newNode = BankNode(obj: item as AnyObject)
                                newNode.bankNodeImage = bankLogo
                                nodes.append(newNode)
                            }
                        }
                        self.paymentInfo.mfaEnabled = false
                        self.paymentInfo.bankNodes = nodes
                        
                    }
                    
                    callBack(true,dictResponse , nil)
                }
            }else{
                HSProgress(.hide)
            }
        }
    }
    
    
    
    
    
    func postAnswer(  answerStr : String , bankLogo : String ,  callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID + "/nodes"
        
        let params : NSDictionary = ["access_token": self.paymentInfo.previousToken  ,
                                     "mfa_answer":  answerStr]
        HSProgress(.show)
        HTTPRequest.request().postRequestToSynpsePay(strLink as NSString, userID:  defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    var nodes = [BankNode]()
                    let mfaStatus = dictResponse.object(forKey: "http_code") as! String
                    
                    if mfaStatus == "202"
                    {
                        self.paymentInfo.mfaEnabled = true
                        let dictMFA = dictResponse.object(forKey: "mfa") as! NSDictionary
                        self.paymentInfo.previousToken = dictMFA.object(forKey: "access_token") as! String
                    }else{
                        if let nodesArry = dictResponse.object(forKey: "nodes") as? NSArray
                        {
                            for item in nodesArry {
                                let newNode = BankNode(obj: item as AnyObject)
                                newNode.bankNodeImage = bankLogo
                                nodes.append(newNode)
                            }
                        }
                        self.paymentInfo.mfaEnabled = false
                        self.paymentInfo.bankNodes = nodes
                    }
                    
                    callBack(true,dictResponse , nil)
                }
            }else{
                HSProgress(.hide)
            }
        }
    }
    
    
    func makePaymentWithNode(paymentInfo : Payment , amount : String , currency : String ,  callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID + "/nodes/" + paymentInfo.selectedNode.nodeID + "/trans"
        let amountDict = ["amount": amount , "currency" : "USD" ]
        let debitedAccount = ["id": LoginManager.getMe.landlordAccountNo , "type" : "ACH-US" ]
        let feeDouble =  [["fee": -0.10,"note" : "Facilitator Fee", "to" : TTFeeNodeID1]]
        
        let params : NSDictionary = ["amount": amountDict,
                                     "extra":  ["ip" : self.me.userIPAddress],
                                     "to" : debitedAccount,
                                     "fee" : feeDouble
                                     ]
        print(params)
        HSProgress(.show)
        HTTPRequest.request().postRequestToSynpsePay(strLink as NSString, userID:  defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    let extraDict = dictResponse.object(forKey: "extra") as! NSDictionary
                    let payment = Payment()
                    payment.paymentDate = (extraDict.object(forKey: "created_on") as! NSNumber).description
                    payment.paymentID = dictResponse.object(forKey: "_id") as! String
                    callBack(true, payment as AnyObject? , nil)
                }
            }else{
                
                callBack(false, Response as AnyObject? , nil)
                
            }
        }
    }
    
    func saveACHPaymentOnServer(paymentObj : Payment, status : String)
    {
        var params : NSDictionary!
        if paymentObj.recurringEnabled == true
        {
            params  = ["transaction_id": paymentObj.paymentID ,"amount" : paymentObj.amount , "date" :  Date.timestamp() ,"status" : status , "routing_number" : paymentObj.selectedNode.routingNumber , "account_number" : paymentObj.selectedNode.accountNo , "node_id" : paymentObj.selectedNode.nodeID ,"synapse_user_id" :paymentObj.userID , "recurring" : 1 , "payment_type" : "Monthly" ,"days" : paymentObj.recurringData.Day , "times" : paymentObj.recurringData.Times,"bank_logo" : paymentObj.selectedNode.bankNodeImage , "bank_name" : paymentObj.selectedNode.bankFullName]
        }else{
            params  = ["transaction_id": paymentObj.paymentID ,"amount" : paymentObj.amount , "date" : Date.timestamp() ,"status" : status , "routing_number" : paymentObj.selectedNode.routingNumber , "account_number" : paymentObj.selectedNode.accountNo , "node_id" : paymentObj.selectedNode.nodeID ,"synapse_user_id" :paymentObj.userID , "recurring" : 0 , "payment_type" : "Monthly" ,"days" : paymentObj.recurringData.Day , "times" : paymentObj.recurringData.Times ,"bank_logo" : paymentObj.selectedNode.bankNodeImage , "bank_name" : paymentObj.selectedNode.bankFullName]
        }
        print(params)
        HTTPRequest.request().postRequest(APISaveTransactionStatus as NSString, param: params) { (isSuccess, Response, error) in
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    print(dictResponse)
                }
            }else{
                
            }
        }
    }
    
    func getSynapsePayRefershToken(_ callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        
        let strLink = getRefreshTokenAPI + LoginManager.getMe.synapsePayUserID
       // HSProgress(.show)
        HTTPRequest.request().getRequestSynapsePayWithoutHeader(strLink as NSString, param: nil) { (isSuccess, Response, error) in
          //  HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    
                    //  print(dictResponse)
                    let token = dictResponse.object(forKey: "refresh_token") as! String
                    let dictStatus = dictResponse.object(forKey: "doc_status") as? Dictionary<String, Any>
                    self.setDoctStatus(dictStatus: dictStatus)

                    defaults.set(token, forKey: synapsePayRefreshToken)
                    
                    self.AuthenticateUser(userID:  LoginManager.getMe.synapsePayUserID, refreshToken: token , callBack: { (isSuccess, response, error) in
                        if isSuccess == true
                        {
                            HSProgress(.hide)
                            if let dictResponse = response as? NSDictionary
                            {
                                
                                defaults.set(dictResponse.object(forKey: "oauth_key") as! String, forKey: synapsePayAuthKey)
                                callBack(true,dictResponse , nil)
                            }
                        }
                    })
                }
            }else{
                HSProgress(.hide)
            }
        }
    }
    
    
    
    func addNewBank(nickName : String , accNo : String , routingNo: String, callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID + "/nodes"
        let infoDict : NSDictionary = ["nickname" : nickName ,"account_num" : accNo , "routing_num": routingNo , "type" : "PERSONAL" , "class" : "CHECKING"]
        let params : NSDictionary = ["type": "ACH-US",
                                     "info":  infoDict]
        HSProgress(.show)
        HTTPRequest.request().postRequestToSynpsePay(strLink as NSString, userID:  defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    var nodes = [BankNode]()
                    let nodeArray = dictResponse.object(forKey: "nodes") as! NSArray
                    for item in nodeArray {
                        let newNode = BankNode(obj: item as AnyObject)
                        nodes.append(newNode)
                    }
                    self.paymentInfo.bankNodes = nodes
                    callBack(true, self.paymentInfo as AnyObject , nil)
                }
            }else{
                if let errorStr = Response as? String
                {
                    HSShowAlertView("TenantTag", message: errorStr as NSString, controller: nil)
                    return
                }
            }
        }
    }
    
    
    func confirmMicroPayment(amountFirst : Double , amountSecond : Double , node : BankNode  , callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID + "/nodes/" + node.nodeID
        let infoDict : [Double] = [amountFirst , amountSecond ]
        let params : NSDictionary = [ "micro" : infoDict]
        
        HSProgress(.show)
        HTTPRequest.request().patchRequestToSynpsePay(strLink as NSString, userID:  defaults.object(forKey: synapsePayAuthKey) as! NSString , param: params) { (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    callBack(true, dictResponse as AnyObject , nil)
                }
            }else{
                if let errorStr = Response as? String
                {
                    HSShowAlertView("TenantTag", message: errorStr as NSString, controller: nil)
                    return
                }
            }
        }
    }
    
    
    
    func cancelSynpasePayPayment( payment: Payment ,_ callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        //print(nodeInfo.nodeID)
        let strLink = createUserSP + "/" + LoginManager.getMe.synapsePayUserID + "/nodes/" + payment.selectedNode.nodeID + "/trans/" + payment.paymentID
        HSProgress(.show)
        HTTPRequest.request().deleteRequestToSynpsePay(strLink as NSString, userID: defaults.object(forKey: synapsePayAuthKey) as! NSString, param: nil){ (isSuccess, Response, error) in
            HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = Response as? NSDictionary
                {
                    
                    callBack(true, dictResponse as AnyObject? , nil)
                }
            }else{
                if let errorStr = Response as? String
                {
                    HSShowAlertView("TenantTag", message: errorStr as NSString, controller: nil)
                    return
                }
                
            }
        }
        
    }
    
    
    //MARK:- Local Server APIs
    //MARK:-
    
    
    
    
    func getKYCSavedOnServer(callBack:@escaping (Bool,AnyObject?,NSError?)->())
    {
        // HSProgress(.show)
        HTTPRequest.request().postRequest(APIGetSynapseKYCInfo as NSString, param: nil) { (isSuccess, response, error) in
            //  HSProgress(.hide)
            if isSuccess == true
            {
                if let dictResponse = response as? NSDictionary
                {
                    // if dictResponse["data"] != nil
                    //  {
                    callBack(true, dictResponse as? AnyObject , nil)
                    //  }
                }
            }else{
                
                if let errorStr = response as? String
                {
                    HSShowAlertView("TenantTag", message: errorStr as NSString, controller: nil)
                    return
                }
            }
        }
    }
    
    func saveKYCInfoToServer(dict : Dictionary< String , Any> , callBack:@escaping (Bool,AnyObject?,NSError?)->() )
    {
        
        
        //kyc_id,kyc_permission,physical_doc_status,virtual_doc_status,
      //first_name,middle_name,last_name,phone,email,address1,address2,unit,city,state,zip,dob,social_security,driving_document,recent_bank_statment
        
        
        HSProgress(.show)
        let params : NSDictionary = ["first_name":dict["Name"] as! String ,
                                     "phone":dict["PhoneNumber"] as! String,
                                     "email": dict["Email"] as! String,
                                     "dob": dict["DOBStr"] as! String,
                                     "middle_name": dict["Gender"] as! String,
                                     "last_name": dict["Profession"] as! String,
                                     "address1": dict["Address"] as! String,
                                     "city": dict["City"] as! String,
                                     "zip": dict["ZipCode"] as! String,
                                     "address2": dict["Street"] as! String,
                                     "state": dict["State"] as! String,
                                     "social_security": dict["SSN"] as! String,
                                     "driving_document": "",
                                     "kyc_id": dict["ID"] as! String,
                                     "kyc_permission": dict["Permission"] as! String,
                                     "physical_doc_status": "",
                                     "recent_bank_statment": "",
                                     "virtual_doc_status": "",
                                     "unit" : "" ]
        
        print(params)
        HTTPRequest.request().postRequest(APISaveSynapseKYCInfo as NSString, param: params, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            if error != nil {
                
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                let dictResponse = data as? Dictionary<String,AnyObject>
                if dictResponse != nil
                {
                    callBack(true, dictResponse as AnyObject?  , nil)
                }
                
            }else{
                  HSProgress(.hide)
                return
            }
        })
        
        
        
    }
    
    //MARK:- Save Micro-Payment Deposit
    //MARK:-
    
    
    
    
    func getMicroDepositNodes(callBack:@escaping (Bool,AnyObject?,NSError?)->() )
    {
        
        HSProgress(.show)
        
        HTTPRequest.request().postRequest(APIGetSynapsePayMDT as NSString, param: nil, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                let arrayNodes = data as? [Dictionary<String,Any>]
                var nodes = [BankNode]()
                for item in arrayNodes!
                {
                    let nodeNew = BankNode(localNode: item)
                    nodes.append(nodeNew)
                }
                callBack(true, nodes as AnyObject? , nil)
            }else{
                return
            }
        })
        
        
        
    }
    
    
    func saveMDNode(node : BankNode  , callBack:@escaping (Bool,AnyObject?,NSError?)->() )
    {
        //  Parameter: oid,account_number,bank_name,bank_code,class,synapse_micro_permission
        var status = node.microPaymentStatus.rawValue
//        if node.microPaymentStatus  == true{
//            status = "CREDIT-AND-DEBIT"
//        }
        
        let param = ["oid": node.nodeID  , "account_number" : node.accountNo , "bank_name" : node.bankFullName ,"bank_code" : "other" , "class" : node.accountClass , "synapse_micro_permission" :  status ,"routing_number" : node.routingNumber ,"amount" : node.amount  ] as [String : Any]
        
        print(param)
        
        HTTPRequest.request().postRequest(APISaveSynapsePayMDT as NSString, param: param as NSDictionary?, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                let dictResponse = data as? Dictionary<String,AnyObject>
                if dictResponse != nil
                {
                    callBack(true, dictResponse as AnyObject? , nil)
                }
            }else{
                return
            }
        })
    }
    
    func updateNode(node: BankNode ,callBack:@escaping (Bool,AnyObject?,NSError?)->() )
    {
        let status = "CREDIT-AND-DEBIT"
        
        let param = ["id": node.nodeID , "synapse_micro_permission" :  status ] as [String : Any]
        HTTPRequest.request().postRequest(APIUpdateSynapsePayMDT as NSString, param: param as NSDictionary?, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                let dictResponse = data as? Dictionary<String,AnyObject>
                if dictResponse != nil
                {
                    callBack(true, dictResponse as AnyObject? , nil)
                }
            }else{
                return
            }
        })
    }
    func deleteNode(node: BankNode ,callBack:@escaping (Bool,AnyObject?,NSError?)->() )
    {
           HSProgress(.show)
        let param = ["id": node.recordID ] as [String : String]
        print(param)
        
        HTTPRequest.request().postRequest(APIDeleteSynapsePayMDT as NSString, param: param as NSDictionary?, callBack:{ (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            HSProgress(.hide)

            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            if isSuccess == true {
                    callBack(true, data as AnyObject? , nil)
              
            }else{
                return
            }
        })
    }

    
    func setDoctStatus (dictStatus : Dictionary<String, Any>?)
    {
        
        
        if dictStatus != nil
        {
            let statusPhysical = dictStatus?["physical_doc"] as! String
            let statusVirtual = dictStatus?["virtual_doc"] as! String
            
            if statusPhysical == DocStatus.Missed.rawValue || statusVirtual == DocStatus.Missed.rawValue
            {
                defaults.set(DocStatus.Missed.rawValue, forKey: synapsePayDocStatus)
                
            }else if statusPhysical == DocStatus.UnderReview.rawValue || statusVirtual == DocStatus.UnderReview.rawValue
            {
                defaults.set(DocStatus.UnderReview.rawValue, forKey: synapsePayDocStatus)
                
            }else if statusPhysical == DocStatus.Approved.rawValue && statusVirtual == DocStatus.Approved.rawValue
            {
                defaults.set(DocStatus.Approved.rawValue, forKey: synapsePayDocStatus)
                
            }else if  statusPhysical == DocStatus.Invalid.rawValue || statusVirtual == DocStatus.Invalid.rawValue
            {
                defaults.set(DocStatus.Invalid.rawValue, forKey: synapsePayDocStatus)
            }
            else if  statusPhysical == DocStatus.ReSubmissionNeeded.rawValue || statusVirtual == DocStatus.ReSubmissionNeeded.rawValue
            {
                defaults.set(DocStatus.ReSubmissionNeeded.rawValue, forKey: synapsePayDocStatus)
            }
        }
        

    }
    
    
    func setDoctStatusComplete (dictStatus : Dictionary<String, Any>?)
    {
        
        
        if dictStatus != nil
        {
            let statusPhysical = dictStatus?["physical_doc"] as! String
            let statusVirtual = dictStatus?["virtual_doc"] as! String
            
            if statusPhysical == DocStatus.Missed.rawValue || statusVirtual == DocStatus.Missed.rawValue
            {
                defaults.set(DocStatus.Missed.rawValue, forKey: synapsePayDocStatus)
                
            }else if statusPhysical == DocStatus.UnderReview.rawValue || statusVirtual == DocStatus.UnderReview.rawValue
            {
                defaults.set(DocStatus.UnderReview.rawValue, forKey: synapsePayDocStatus)
                
            }else if statusPhysical == DocStatus.Approved.rawValue && statusVirtual == DocStatus.Approved.rawValue
            {
                defaults.set(DocStatus.Approved.rawValue, forKey: synapsePayDocStatus)
                
            }else if  statusPhysical == DocStatus.Invalid.rawValue || statusVirtual == DocStatus.Invalid.rawValue
            {
                defaults.set(DocStatus.Invalid.rawValue, forKey: synapsePayDocStatus)
            }
            else if  statusPhysical == DocStatus.ReSubmissionNeeded.rawValue || statusVirtual == DocStatus.ReSubmissionNeeded.rawValue
            {
                defaults.set(DocStatus.ReSubmissionNeeded.rawValue, forKey: synapsePayDocStatus)
            }
        }
        
        
    }

    
}
