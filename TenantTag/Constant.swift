//
//  Constant.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import Foundation
import UIKit


//let BASE_URL = "http://54.152.53.196/api"  //"http://demo.xicom.us/TenantTag/api"

let BASE_URL = "https://tenanttag.com/api"


//let BASE_URL = "http://demo.xicom.us/tenanttag/api"
let kForwardYourMail = "https://moversguide.usps.com" //"https://www.usps.com/manage/forward.htm"
let APP_LisenceKey = "5d8728f5d2618ab61f02c9023185518c7bb"
let DeviceType = "2"  //  1  for Android and 2 for IOS
let WindowFrame  =  UIScreen.main.bounds
let ScreenHeight  =  WindowFrame.size.height
let ScreenWidth = WindowFrame.size.width
let defaults = UserDefaults.standard


var leasedDocURL = ""
var leasedDocImage : UIImage!


var myIPAddress = ""
var deviceID = ""
var createUserSP = "https://sandbox.synapsepay.com/api/3/users"
var authenticateUserSP = "https://sandbox.synapsepay.com/api/3/oauth/"
var getRefreshTokenAPI = "https://sandbox.synapsepay.com/api/3/users/"
///var createUserSP = "https://synapsepay.com/api/3/users"
//var authenticateUserSP = "https://synapsepay.com/api/3/oauth/"
//var getRefreshTokenAPI = "https://synapsepay.com/api/3/users/"
//{{user_id}}






// API

let APIForgotPassword = "forgotpassword" //_ GET__email
let APILogin = "signin" //_ POST
let APIHome = "userHome" //_ GET
let APILogout = "logout" // _GET
let APIGetProperty = "userHome" //_GET __ ?messageId=2
let APIUploadPropertyPhotos = "uploadPhotos" //POST
let APIDeletePhoto = "deletePhoto"
let APIGetMainTenanceCat = "maintenanceCategories" //_GET __ ?messageId=2
let APISubmitMaintenance = "submitMaintenanceReport"
let APIGetMessageList = "messages" //_GET __
let APIDeleteMessage = "deleteMessage"
let APIGetLandlordDetail = "landlordDetails"
let APISendOTPToLandlord = "landlordOTP" 
let APISubmitOTPLandlord = "verifyTenant"
let APIGetCheckList = "checklist"
let APIUpdateUserInfo = "editUserInfo"
let APIGetUserProfile = "userinfo"
let APIAcceptLandlord = "acceptLandlord"
let APIEditPhone = "editPhone"
let APIVerifyPhoneNumber = "verifyOtp"
let APIRentalHistory = "rentalHistory"
let APIContactNewLandlord = "contactNewLandlord"
let APISaveStripeCard = "setStripeCardToken"
let APIPaymentWithCard = "payByStripe"
let APICancelCCPayment = "refundAmount"

//let APIGetSavedCardsList = "getUserCardsDetail"
let APIGetSavedCardsList = "getStripeCardsDetail"
let APIGetPreviousTransaction = "getPymentHistory"
let APIDeleteSavedCard = "deleteUserStripeCard"
let APIDeleteSavedAccount = "deleteSynapsepayAccount"

let APIRentDueAmount = "rentDueAmount"
let APIEditEmail = "editEmail"
let APIVerifyOTPForEmail = "verifyOtpForEmail"
let APIGetTenantPropertyInfo = "getTenantPropertyInfo"
let APISaveTransactionStatus = "setSynapseTransactionDetail"
let APIUpdateRecurringInfo = "updateRecurringInfo"
let APIGetSavedAccountList = "getSynapsepayAccounts"
let APISendInvitationToLandLord = "sendMailToLandlord"
let APIGetSynapseKYCInfo = "getSynapseKycDetail"
let APISaveSynapseKYCInfo = "setSynapseKycDetail"

let APIGetSynapsePayMDT = "getSynapsepayLinkedAccounts"
let APISaveSynapsePayMDT = "setSynapseLinkedAccount"
let APIUpdateSynapsePayMDT = "updateSynapseLinkedAccountStatus"
let APIDeleteSynapsePayMDT = "deleteSynapseLinkedAccount"

//let TTFeeNodeID1 = "58222aee9506290c88e55b74"
let TTFeeNodeID1 = "5a15a20de552da002bcee8cc"
//5a15a20de552da002bcee8cc
//let TTFeeNodeID2 = "582ab43186c273138c1d982d"








let URL_Terms = "https://tenanttag.com/page/app/ios-terms"
let URL_Privacy = "http://tenanttag.com/page/app/privacy"
let URL_CommonQuestions = "http://tenanttag.com/page/app/common-questions"
let URL_Website = "https://www.tenanttag.com"

var URL_SynapseFi = "https://synapsefi.com/legal"
var URL_SynapseFiTOS = "https://synapsefi.com/tos-evolve"
var URL_SynapseFiPP = "https://synapsefi.com/privacy"

var ClientID = "client_id_0UM8SnNfuXFca7gZh62PQ9lV3LW4YxGdiEKsezqp" //live
var SecretID = "client_secret_KhArjpdwz4eI08xfJBL3ZGmvHNDqVQoX0MTOtYun" //live
   // self.requestSerializer.setValue("id-c83d6205-785a-4776-b4de-8584aebc045d|secret-076f2412-69cf-46b1-a304-ad27e62f38f3"  , forHTTPHeaderField:"X-SP-GATEWAY")
var ClientIDDemo = "id-c83d6205-785a-4776-b4de-8584aebc045d"
var SecretIDDemo = "secret-076f2412-69cf-46b1-a304-ad27e62f38f3"

//let APP_ThemeColor: UIColor =  UIColor(patternImage: UIImage(named: "nav")!)
let APP_FontName = "Questrial-Regular"
let APP_FontRegularNew = "Roboto-Regular"
let APP_FontLightNew = "Roboto-Light"
let APP_FontMediumNew = "Roboto-Medium"
let APP_FontThinNew = "Roboto-Thin"

let APP_FontRegular = UIFont(name: APP_FontName, size: 16)
let APP_ThemeColor =  UIColor(red: 10.0/255.0, green: 81.0/255.0, blue: 141.0/255.0, alpha: 1.0)
let APP_DOBDateFormate = "yyyy-MM-dd"
let APP_DOBDateFormateNew = "MM-dd-yyyy"
let APP_DOBDateFormateBackEnd = "yyyy-MM-dd"


let APP_SearchMessages = "messages" // GET_
let kUserCheckOut = "UserCheckOut"  //  1  for Android and 2 for IOS

let kUpdateRecords = "Update"

let synapsePayUserID = "synapsePayUserID"
let synapsePayRefreshToken = "synapsePayRefreshToken"
let synapsePayVirtualDoc = "synapsePayVirtualDoc"
let synapsePayDocStatus = "synapsePayDocStatus"


let synapsePayAuthKey = "oauth_key"

let visaImage =  UIImage(named: "CardVisa")



var arrayState : [String] = ["AL" ,"AK","AZ" ,"AR" ,"CA", "CO" ,"CT" ,"DE" ,"DC","FL" ,"GA" ,"HI" ,"ID","IL" ,"IN" , "IA" , "KS" , "KY" , "LA" , "ME", "MD" , "MA" , "MI" , "MN" , "MS", "MO" , "MT" , "NE" , "NV" , "NH" ,"NJ" ,"NM" ,"NY","NC" , "ND" , "OH" , "OK" , "OR" , "PA" , "RI" , "SC" , "SD" , "TN" , "TX" , "UT" , "VT", "VA" , "WA" , "WV" , "WI" , "WY"]



//
//var arrayState : [String] = ["Alabama AL" ,"Alaska AK","Arizona AZ" ,"Arkansas AR" ,"California CA", "Colorado CO" ,"Connecticut CT" ,"Delaware DE" ,"District of Columbia DC","Florida FL" ,"Georgia GA" ,"Hawaii HI" ,"Idaho ID","Illinois IL" ,"Indiana IN" , "Iowa IA" , "Kansas KS" , "Kentucky KY" , "Louisiana LA" , "Maine ME", "Maryland MD" , "Massachusetts MA" , "Michigan MI" , "Minnesota MN" , "Mississippi MS", "Missouri MO" , "Montana MT" , "Nebraska NE" , "Nevada NV" , "New Hampshire NH" ,"New Jersey NJ" ,"New Mexico NM" ,"New York NY","North Carolina NC" , "North Dakota ND" , "Ohio OH" , "Oklahoma OK" , "Oregon OR" , "Pennsylvania PA" , "Rhode Island RI" , "South Carolina SC" , "South Dakota SD" , "Tennessee TN" , "Texas TX" , "Utah UT" , "Vermont VT", "Virginia VA" , "Washington WA" , "West Virginia WV" , "Wisconsin WI" , "Wyoming WY"]
