//
//  HSPhoto.swift
//  Who's In
//
//  Created by Hardeep Singh on 04/12/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit

class HSPhoto: NSObject {
    
    var ID: String? = nil

    var serverURLStr : String? = nil
    var serverthumbURLStr : String? = nil
    
    var URL : Foundation.URL!
    var thumbURL : Foundation.URL!

    var image: UIImage? = nil
    var thumbImage: UIImage? = nil
    
    var selected: Bool! = false
   
    var localImage: Bool {
        
        if self.serverURLStr == nil {
            return true
        }
        if self.serverURLStr!.isEmpty {
            return true
        }else {
            return false
        }
        
    }


    
    func selectionToggle() {
        
        if selected == true {
            self.selected = false
        }else {
            self.selected = true
        }
    }

    
    override init() {
    
    }
    
    
    convenience init(image: UIImage) {
        self.init()
        self.image = image;
    }

    convenience init(urlStr: String) {
        
        self.init()
        self.serverURLStr = urlStr.encodeSpecialCharacter();
        self.URL = Foundation.URL(string: self.serverURLStr!)
        
    }
    
    convenience init(thumbURLStr: String, URLStr: String) {
        
        self.init()
        
        self.serverthumbURLStr = thumbURLStr.encodeSpecialCharacter();
        self.thumbURL = Foundation.URL(string: self.serverthumbURLStr!)
        
        self.serverURLStr = URLStr.encodeSpecialCharacter();
        self.URL = Foundation.URL(string: self.serverURLStr!)
        
    }

    
    
    
//    convenience init(eventPhotos: AnyObject) {
//        
//        self.init()
//        
//        self.serverURLStr = urlStr.encodeSpecialCharacter();
//        self.URL = NSURL(string: self.serverURLStr)
//        
//    }
    
    convenience init(eventPhoto: Dictionary<String, AnyObject>) {
        
        self.init()
        
        let ID:String? =  eventPhoto["id"] as? String
        if ID != nil {
            self.ID = ID
        }else{
            self.ID = ""

        }
        
        let eventImage:String? =  eventPhoto["eventImage"] as? String
        if eventImage != nil {
            self.serverURLStr = eventImage!.encodeSpecialCharacter();
            self.URL = Foundation.URL(string: self.serverURLStr!)
        }else {
            self.URL = Foundation.URL(string: "");
        }
        
    }


    func imageData() -> Data? {
        
        if self.image == nil {
            return nil
        }
        
             var imageData: Data? = UIImageJPEGRepresentation(self.image!, 0.1);
             var compression: CGFloat  = 0.9;
             let maxCompression: CGFloat  = 0.1;
             let maxFileSize: Int = 100*1024
             while ( (imageData!.count > maxFileSize) && (compression > maxCompression) ) {
                 compression = compression - 0.1;
                 imageData = UIImageJPEGRepresentation(self.image!, compression);
             }
        
        return imageData

    }
    
}
