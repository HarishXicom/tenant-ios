//
//  ProfileCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 20/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var textField: UITextField!
    
    func editingMode(_ editionOn: Bool) {
     
        if editionOn == true {
            self.textField.borderStyle = .roundedRect
            self.textField.isEnabled = true
            self.textField.textColor = UIColor.black
        }else {
            self.textField.borderStyle = .none
            self.textField.isEnabled = false
            self.textField.textColor = UIColor.lightGray
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
}
