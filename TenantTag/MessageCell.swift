//
//  MessageCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 05/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var dateImgView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    var message: Message? = nil;
    var messageDeleteCallBack:( ( MessageCell)-> Void)? = nil
    
    class func heightForFeed(_ address: Message, size: CGSize, fullDetail: Bool) ->(CGFloat) {
        
        let text: String = address.text!;
        
        let topSpace: CGFloat = 8.0;
        let bottomSpace: CGFloat = 8.0;

        let leadingSpace: CGFloat = 8.0;
        let trailingSpace: CGFloat = 8.0;
        
        let vPadding: CGFloat = 5.0;
        let hPadding: CGFloat = 5.0;
        
        let timeImgHeight: CGFloat = 20.0;
        
        let textWidth = (size.width - (leadingSpace + trailingSpace + (hPadding*2)))
        
        let font: UIFont =  UIFont(name: APP_FontName, size: 16)!
        
        var maxSize: CGSize = CGSize.zero;

        if fullDetail {
             maxSize =  CGSize( width: CGFloat(textWidth), height: 9999.0)
        }else {
             maxSize = CGSize( width: CGFloat(textWidth), height: (font.lineHeight*3))
        }
        
        let boundingBox = text.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size.height
        let maxHeight = ((boundingBox + topSpace + bottomSpace + timeImgHeight + (vPadding*3)))
        return maxHeight
        
    }

    
    func updateCell(_ message: Message?) {
       
        self.message = message
        
        let message = self.message!.text;
        self.messageLabel.text = message;
        
        let date = self.message!.dateStr;
        self.dateLabel.text = date;
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.backgroundColor = UIColor.white
        
        self.messageLabel.font =  UIFont(name: APP_FontName, size: 16)
        self.dateLabel.font = UIFont(name: APP_FontName, size: 13)


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonClicked(_ sender: AnyObject) {
        
        if self.messageDeleteCallBack != nil {
            self.messageDeleteCallBack!(self)
        }
        
    }
}
