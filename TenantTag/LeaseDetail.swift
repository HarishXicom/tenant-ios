//
//  LeaseDetail.swift
//  TenantTag
//
//  Created by Hardeep Singh on 25/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class LeaseDetail: NSObject {
    
    var leaseEnd: String? = nil
    var leaseStart: String? = nil
    fileprivate var rentAmount: String? = nil
    var period: String? {
        get {
            return "\(leaseStart!) to \(leaseEnd!)"
        }
    }
    
    var amount: String? {
        get {
            return "$ \(rentAmount!)"
        }
    }
    
    
    override init() {
        
    }
    
    convenience init(leaseObj: Dictionary<String, AnyObject>) {
        
        self.init()
        
        let leaseEnd:String? =  leaseObj["leaseEnd"] as? String
        if leaseEnd != nil {
            self.leaseEnd = leaseEnd
        }
        
        let leaseStart:String? =  leaseObj["leaseStart"] as? String
        if leaseStart != nil {
            self.leaseStart = leaseStart
        }
        
        let rentAmount:String? =  leaseObj["rentAmount"] as? String
        if rentAmount != nil {
            self.rentAmount = rentAmount
        }
        
    }

}
