//
//  HomeAddressCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class HomeAddressCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addressImgView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var callImgView: UIImageView!
    @IBOutlet weak var phnLabel: UILabel!
    weak var property: Property? = nil
    
    class func heightForFeed(_ property: Property, size: CGSize, phnExist: Bool) ->(CGFloat) {
        
        let text: String = property.communityAddress!;
        
        let topSpace: CGFloat = 0.0;
        let leadingSpace: CGFloat = 8.0;
        let bottomSpace: CGFloat = 0.0;
        let trailingSpace: CGFloat = 8.0;
        let vPadding: CGFloat = 5.0;
        let hPadding: CGFloat = 5.0;
        let locationImageHeight: CGFloat = 20.0;
        var phnImageHeight: CGFloat =  0.0;
        if phnExist == true {
             phnImageHeight = 20.0;
        }
        
        let textWidth = (size.width - (leadingSpace + trailingSpace + locationImageHeight + (hPadding*3)))
        
        let font: UIFont =  UIFont(name: APP_FontName, size: 16)!
        let maxSize: CGSize = CGSize( width: CGFloat(textWidth), height: 9999.0)
        let boundingBox = text.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size.height
        let maxHeight = ((boundingBox + topSpace + bottomSpace + phnImageHeight + (vPadding*3)))
        return maxHeight

        
    }
    
    
    
    func updateCell(_ property: Property?) {
       
        self.property = property;
        if self.property == nil {
            return
        }
        
        if self.property!.communityAddress != nil {
            self.addressLabel.text = self.property!.communityAddress;
            self.addressLabel.isHidden = false
        }else {
            self.addressLabel.text = ""
            self.addressLabel.isHidden = true
        }

        if self.property!.communityPhoneNo != nil {
            self.phnLabel.text = self.property!.communityPhoneNo;
            self.phnLabel.isHidden = false
        }else {
            self.phnLabel.text = ""
            self.phnLabel.isHidden = true
        }
        
    }

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.containerView.backgroundColor = UIColor.white
        // Initialization code
        self.addressLabel.font =  UIFont(name: APP_FontName, size: 16)
        self.phnLabel.font = UIFont(name: APP_FontName, size: 13)
   
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            
            let point:CGPoint = touch.location(in: self)
            if  self.phnLabel.frame.contains(point)  {
                self.phnLabel.textColor = UIColor.black
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.phnLabel.textColor = APP_ThemeColor
        
        if let touch = touches.first {
            let point:CGPoint = touch.location(in: self)
            if  self.phnLabel.frame.contains(point)  {
                if self.property!.communityPhoneNo != nil {
                    
                    let phnNumber = self.property!.communityPhoneNo!.phoneNumberWithoutFormate()
                    let URLStr = "telprompt://\(phnNumber)"
                    let URL: Foundation.URL? = Foundation.URL(string: URLStr)
                    if URL == nil {
                        UIAlertView(title: nil, message: "Phone number is not valid.", delegate: nil, cancelButtonTitle: "Ok").show()
                        return;
                    }
                    
                    if UIApplication.shared.canOpenURL(URL!) {
                        UIApplication.shared.openURL(URL!)
                    }else {
                        UIAlertView(title: nil, message: "Your device doesn't support this feature.", delegate: nil, cancelButtonTitle: "Ok").show()
                    }
                    
                }
            }
        }
        
        
    }

    
}
