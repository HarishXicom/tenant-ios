//
//  ProfileViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 20/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

enum ProfileSectionType: Int {
    
    case firstName = 0,
    lastName,
    emailAddress,
    phoneNumber,
    birthday,
    language,
    ThirdParty,
    editButton,KYCEdit
}



class ProfileViewController: UITableViewController, UITextFieldDelegate {
    
    // var  me: Me  = Me()
    var boolPresent = false
    weak var firstNameCell: ProfileCell? = nil
    weak var lastNameCell: ProfileCell? = nil
    weak var emailAddressCell: ProfileCell? = nil
    weak var phoneNumberCell: ProfileCell? = nil
    weak var birthdayCell: ProfileCell? = nil
    weak var languageCell: ProfileCell? = nil
    weak var saveThirdPartyCell: SaveCardCell? = nil
    
    var previousAction = false

    var images: [String]!
    var editMode: Bool = false
    var datePicker: UIDatePicker = UIDatePicker()
    var selectedDate:Date!

    
    func backButtonClicked() {
        
        if boolPresent == true
        {
            self.navigationController?.isNavigationBarHidden = true
            
           self.navigationController?.popViewController(animated: true)
        }else{
          self.dismiss(animated: true) { () -> Void in
            
                    }
            
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         self.tableView.reloadData()
        
        
        let me: Me = LoginManager.getMe
        if self.phoneNumberCell != nil {
            self.phoneNumberCell!.textField.text = me.phoneNumber!;
        }
        
        
        if self.emailAddressCell != nil {
            self.emailAddressCell!.textField.text = me.emailAddress!;
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.images = ["icon1", "icon1", "mail1", "phone", "calendar", "lang", ""];
        var nib: UINib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "FirstName")
        
        nib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "LastName")
        
        nib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "EmailAddress")

         nib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "PhoneNumber")

        nib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "Birthday")

        nib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "Language")
        
        nib = UINib(nibName: "SaveCardCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SaveCardCell")
        
        nib = UINib(nibName: "ProfileActionsCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProfileActionsCell")
        nib = UINib(nibName: "CommonQuestionsBtnCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CommonQuestionsBtnCell")
        
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;

        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(ProfileViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle("PROFILE", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)

        
        self.datePicker = UIDatePicker()
        self.datePicker.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged(_:)), for: .valueChanged)
        self.datePicker.datePickerMode = .date
        self.selectedDate = Date()
        let date: Date = LoginManager.getMe.brithdayDate! as Date;
        self.selectedDate = date
        self.datePicker.setDate(self.selectedDate, animated: false)
        
        let rect = CGRect(x: 0, y: 0, width: 20, height: 5.0)
        let fView = UIView()
        fView.frame = rect;
        fView.backgroundColor = UIColor.white
        self.tableView.tableFooterView = fView;
        LoginManager.share().getUpdateUserProfile { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            if isSuccess == true {
                self.previousAction = LoginManager.getMe.thirdParty
              // self.me  = LoginManager.getMe
                self.tableView.reloadData()
            }
            
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indexPathForDOB() -> IndexPath {
       let index:IndexPath =  IndexPath(row: ProfileSectionType.birthday.rawValue, section: 0)
       return index
    }

    func datePickerValueChanged(_ datePicker:UIDatePicker) {
        var date:Date = self.datePicker.date
        date = (date as NSDate).atStartOfDay()
        if ((date as NSDate).isEarlierThanDate((Date() as NSDate).subtractingYears(18)) ) {
            self.selectedDate = date
            let dateFormate:DateFormatter = DateFormatter()
            dateFormate.dateFormat = APP_DOBDateFormate
            let dateString:NSString = dateFormate.string(from: self.selectedDate) as NSString
            
            let indexPath = self.indexPathForDOB()
            let me: Me = LoginManager.getMe
            me.brithdayStr = dateString as String
            
            let signUpTableViewCell:ProfileCell = self.tableView.cellForRow(at: indexPath) as! ProfileCell
            signUpTableViewCell.textField.text = me.brithdayStr!  as String
            
        }else{
            
            let date:Date = self.selectedDate as Date
            self.datePicker.setDate(date, animated: true)
            self.selectedDate = date;
            
            let dateFormate:DateFormatter = DateFormatter()
            dateFormate.dateFormat = APP_DOBDateFormate
            let dateString:NSString = dateFormate.string(from: self.selectedDate) as NSString
            
            let indexPath = self.indexPathForDOB()
            let me: Me = LoginManager.getMe
            me.brithdayStr = dateString as String;
            let signUpTableViewCell: ProfileCell = self.tableView.cellForRow(at: indexPath) as! ProfileCell
            signUpTableViewCell.textField.text = me.brithdayStr!  as String
            
        }
        
    }
   
    // MARK: - actions
    
    func cancelButtonCliceked() {
        self.editMode = false;
        LoginManager.getMe.thirdParty = self.previousAction
        self.tableView.reloadData()

    }
    
    func editButtonClicked() {
        self.editMode = true;
        self.tableView.reloadData()

    }
    
    func saveButtonClicked() {
        
        let firstName: String  =  self.firstNameCell!.textField.text!
        if firstName.isEmpty {
            HSShowAlertView("TenantTag", message: "Please enter firstname!",controller: self)
            return;
        }
        LoginManager.getMe.firstName = firstName;
        
        
        let lastName: String  =  self.lastNameCell!.textField.text!
        if lastName.isEmpty {
            HSShowAlertView("TenantTag", message: "Please enter lastname!",controller: self)
            return;
        }
        LoginManager.getMe.lastName = lastName;
        LoginManager.getMe.brithdayDate = self.selectedDate;

        let thirdPartyCell = self.tableView.cellForRow(at: NSIndexPath(row: 6, section: 0) as IndexPath) as! SaveCardCell
        if thirdPartyCell.btnCheckBox.isSelected == false{
            LoginManager.getMe.thirdParty = true
        }else{
            LoginManager.getMe.thirdParty = false
        }
        
        
        
       // saveCardCell
      //  LoginManager.getMe.thirdParty = self.me.thirdParty
        
        LoginManager.share().updateUserProfile { ( isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if isSuccess == true {
                self.editMode = false;
                self.previousAction = LoginManager.getMe.thirdParty

                HSShowAlertView("TenantTag", message: "Profile update successfully",controller: self)
                self.tableView.reloadData()
            }
            
        }
        
       // APIUpdateUserInfo
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType: ProfileSectionType = ProfileSectionType(rawValue: indexPath.row)!
        
        let me: Me = LoginManager.getMe

        switch sectionType {
            
        case .firstName:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FirstName", for: indexPath) as! ProfileCell
               if self.firstNameCell == nil {
                  self.firstNameCell = cell;
                  self.firstNameCell?.textField.placeholder = "First Name"
                  self.firstNameCell?.textField.text = me.firstName;
               }
            
            cell.textField.delegate = self;
            cell.editingMode(self.editMode);
            let imgName = self.images[indexPath.row]
            cell.imgView.image = UIImage(named: imgName)

            return cell
            
        case .lastName:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LastName", for: indexPath) as! ProfileCell
            if self.lastNameCell == nil {
                self.lastNameCell = cell;
                self.lastNameCell!.textField.placeholder = "First Name"
                self.lastNameCell!.textField.text = me.lastName;
            }
            
            cell.textField.delegate = self;
            cell.editingMode(self.editMode);
            let imgName = self.images[indexPath.row]
            cell.imgView.image = UIImage(named: imgName)
            return cell
        case .emailAddress:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmailAddress", for: indexPath) as! ProfileCell
            if self.emailAddressCell == nil {
                self.emailAddressCell = cell;
                self.emailAddressCell!.textField.placeholder = "Email Address"
                self.emailAddressCell!.textField.text = me.emailAddress;
                self.emailAddressCell!.textField!.isUserInteractionEnabled = false;

            }
            
            cell.textField.delegate = self;
            cell.editingMode(self.editMode);
            let imgName = self.images[indexPath.row]
            cell.imgView.image = UIImage(named: imgName)
            return cell
            
        case .phoneNumber:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneNumber", for: indexPath) as! ProfileCell
            if self.phoneNumberCell == nil {
                self.phoneNumberCell = cell;
                self.phoneNumberCell!.textField.placeholder = "Phone Number"
                self.phoneNumberCell!.textField.text = me.phoneNumber;
                self.phoneNumberCell!.textField!.isUserInteractionEnabled = false;
            }
            
            cell.textField.delegate = self;
            cell.editingMode(self.editMode);
            let imgName = self.images[indexPath.row]
            cell.imgView.image = UIImage(named: imgName)
            return cell
            
        case .birthday:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Birthday", for: indexPath) as! ProfileCell
            if self.birthdayCell == nil {
                self.birthdayCell = cell;
                self.birthdayCell!.textField.placeholder = "Birthday"
                self.birthdayCell!.textField.text = me.brithdayStr;
                self.birthdayCell!.textField.inputView = self.datePicker;

            }
            
            cell.textField.delegate = self;
            cell.editingMode(self.editMode);
            let imgName = self.images[indexPath.row]
            cell.imgView.image = UIImage(named: imgName)
            return cell
            
        case .language:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Language", for: indexPath) as! ProfileCell
            if self.languageCell == nil {
                self.languageCell = cell;
                self.languageCell?.textField.placeholder = "Language"
                self.languageCell?.textField.text = me.language;
            }
            
            cell.textField.delegate = self;
            cell.editingMode(self.editMode);
            let imgName = self.images[indexPath.row]
            cell.imgView.image = UIImage(named: imgName)
            return cell
            
        case .ThirdParty:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SaveCardCell", for: indexPath) as! SaveCardCell
                cell.leadingConstraint.constant = -15
                cell.btnCheckBox.titleLabel?.font = UIFont(name: APP_FontName, size: 14)!
                
                if self.saveThirdPartyCell == nil {
                    self.saveThirdPartyCell = cell;
                }
                
                cell.customAction(cond: me.thirdParty)
                cell.callBackSaveCard = { (test : Bool) in
                    print(test)
                   me.thirdParty = !me.thirdParty
                    self.tableView.reloadData()
                }

                if self.editMode == true{
                    cell.btnCheckBox.isUserInteractionEnabled = true
                    cell.btnCheckBox.setTitleColor(UIColor.black, for: .normal)


                }else{
                    
                    cell.btnCheckBox.setTitleColor(UIColor.lightGray, for: .normal)
                    cell.btnCheckBox.isUserInteractionEnabled = false
                }
                cell.btnCheckBox.setTitle("Allow 3rd Party Sharing", for: .normal)
                cell.btnCheckBox.setTitle("Allow 3rd Party Sharing", for: .selected)


                return cell

            
        case .editButton:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileActionsCell", for: indexPath) as! ProfileActionsCell
            cell.actionCallBack = {(edit: Bool, cancel: Bool, save: Bool) -> () in
            
                if (edit == true) {
                    
                    self.editButtonClicked()
                }else if (cancel == true) {
                    
                    self.cancelButtonCliceked()
                }else if (save == true) {
                    
                    self.saveButtonClicked()
                }
                
            }
            
            cell.editingMode(self.editMode);
            return cell
            
            
        case .KYCEdit:
            
            let cell: CommonQuestionsBtnCell = tableView.dequeueReusableCell(withIdentifier: "CommonQuestionsBtnCell") as! CommonQuestionsBtnCell
            cell.btnCommonQuestions.setTitle("Update SynapsePay KYC", for: .normal)
            cell.callBackCommonQuestion = {(test : Bool) in
               self.editKYCClicked()
            }
            cell.btnCommonQuestions.underLineButton(text: "Update SynapsePay KYC", color: APP_ThemeColor)

            return cell
            
            
            
        }
        
     //   if indexPath.section == 6 {
            
     //   }else {
            
         //   let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
           // return cell

      //  }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath)
        //Configure the cell...
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60;
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        cell.marginWillShow(false)
        tableView.marginWillShow(false)
        cell.selectionStyle = .none
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionType: ProfileSectionType = ProfileSectionType(rawValue: indexPath.row)!

        if (self.editMode == true) && (sectionType == .phoneNumber) {
            
            let phoneEditViewController = PhoneEditViewController.init(nibName: "PhoneEditViewController", bundle: nil)
            phoneEditViewController.editType = .PhoneNumber
            self.navigationController!.pushViewController( phoneEditViewController, animated: true)

        }else if (self.editMode == true) && (sectionType == .emailAddress)
        {
            let phoneEditViewController = PhoneEditViewController.init(nibName: "PhoneEditViewController", bundle: nil)
            phoneEditViewController.editType = .EmailEdit
            self.navigationController!.pushViewController( phoneEditViewController, animated: true)
            
        }
        
    }
    
    func editKYCClicked()
    {
        
       HSProgress(.show)
        LoginManager.share().getSynapsePayRefershToken { (isSuccess, result, error) in
            HSProgress(.hide)
                    let landlordVC: DocumentFormVC   = DocumentFormVC.init(nibName: "DocumentFormVC", bundle: nil)
                    self.navigationController?.isNavigationBarHidden = false
                    self.navigationController?.pushViewController(landlordVC, animated: true)
        }
        
    }
   
   

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
