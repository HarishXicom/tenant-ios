//
//  OPTViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 27/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class OPTViewController: UIViewController {

    
    var editOTP : EditType = .PhoneNumber
    @IBOutlet var resendButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var textField: UITextField!
    @IBOutlet var textFieldBGView: UIView!
    @IBOutlet var label: UILabel!
    @IBOutlet var imgView: UIImageView!
   // var updatePhone: Bool
    var newPhoneNumber: String? = nil
    
    var verfiedSuccess: (( _ success: Bool) -> Void)? = nil
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.textField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;

        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;


        // Do any additional setup after loading the view.
        self.label.font = UIFont(name: APP_FontName, size: 17)
        self.textFieldBGView.cornerRadius(15.0)
        self.textFieldBGView.layer.borderColor = APP_ThemeColor.cgColor
        self.textFieldBGView.layer.borderWidth = 1.0
        self.textField.keyboardType = .default
        self.textField.returnKeyType = .send
        self.textField.font = UIFont(name: APP_FontName, size: 15)
        self.textField.borderStyle = .none
        submitButton.cornerRadius(15.0)

        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(OPTViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle("ENTER OTP", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)
        
        if self.newPhoneNumber != nil {
            
            let phnNumber: String = self.newPhoneNumber!
            self.label.text = "Enter the OTP received at \(phnNumber) to confirm your registration"
        }else {
            
            let phnNumber: String = LoginManager.getMe.phoneNumber!
            self.label.text = "Enter the OTP received at \(phnNumber) to confirm your registration"
        }
      

    }
    
    
    func backButtonClicked() {
        
        if self.newPhoneNumber != nil {
            
            self.navigationController!.popToRootViewController(animated: true)
       
        }else {
            
            LoginManager.share().removeUserProfile()
            self.navigationController!.popViewController(animated: true)
        
        }
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitButtonClicked(_ sender: AnyObject) {
       self.view.endEditing(true)
        let OTP: String = self.textField.text!;
        if OTP.isEmpty {
            HSShowAlertView("TenantTag", message: "OTP is required.")
            return;
        }

        if self.newPhoneNumber != nil {
            if editOTP == .PhoneNumber{
            let params : Dictionary = ["phoneNo":self.newPhoneNumber!,
                "otp":OTP]
            HSProgress(.show)
            HTTPRequest.request().postRequest( APIVerifyPhoneNumber as NSString , param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                HSProgress(.hide)
                
                if isSuccess == true {
                    
                    LoginManager.getMe.phoneNumber = self.newPhoneNumber!;
                    LoginManager.share().saveUserProfile()
                    self.navigationController?.popToRootViewController(animated: true)
                    
                }
            }
            }else{
                
                //email,email_otp
                let params : Dictionary = ["email":self.newPhoneNumber!,
                                           "email_otp":OTP]
                HSProgress(.show)
                HTTPRequest.request().postRequest(APIVerifyOTPForEmail as NSString , param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                    
                    HSProgress(.hide)
                    
                    if isSuccess == true {
                        
                        LoginManager.getMe.emailAddress = self.newPhoneNumber!;
                        LoginManager.share().saveUserProfile()
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }

                
                
            }

            
        }else {
            
            let params : Dictionary = ["otp":OTP]
            HSProgress(.show)
            HTTPRequest.request().postRequest(APISubmitOTPLandlord as NSString , param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                HSProgress(.hide)
                
                if isSuccess == true {
                    LoginManager.getMe.verified = true;
                    LoginManager.share().saveUserProfile()
                    if self.verfiedSuccess != nil {
                        self.verfiedSuccess!(true)
                    }
                }
            }

        }
        
        
        
        
    }
    
    @IBAction func resendButtonClicked(_ sender: AnyObject) {
       
        var phnNumber: String = LoginManager.getMe.phoneNumber!

        if self.newPhoneNumber != nil {
            
            phnNumber = self.newPhoneNumber!
            
            
            if editOTP == .PhoneNumber
            {
                
                let params : Dictionary = ["phoneNo":phnNumber]
                
                HSProgress(.show)
                HTTPRequest.request().postRequest( APIEditPhone as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                    
                    HSProgress(.hide)
                    if isSuccess {
                        HSShowAlertView("TenantTag", message: "OTP sent at \(self.newPhoneNumber!)" as NSString)
                    }
                }

            
            }else{
                
                let params : Dictionary = ["email":phnNumber]
                
                HSProgress(.show)
                HTTPRequest.request().postRequest( APIEditEmail as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                    
                    HSProgress(.hide)
                    if isSuccess {
                        HSShowAlertView("TenantTag", message: "OTP sent at \(self.newPhoneNumber!)" as NSString)
                    }
                }

                
            }
            
     }else {
            
            let params : Dictionary = ["phoneNo":phnNumber]
            HSProgress(.show)
            HTTPRequest.request().postRequest( APISendOTPToLandlord as NSString, param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                HSProgress(.hide)
                if isSuccess {
                    HSShowAlertView("TenantTag", message: "OTP sent to landlord")
                }
            }
            
        }
        
    }
    
        
        
}
