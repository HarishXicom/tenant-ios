//
//  PhoneEditViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 17/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

enum EditType : Int{
    
    case PhoneNumber = 0
    case EmailEdit = 1
}

class PhoneEditViewController: UIViewController, UITextFieldDelegate {
    var editType : EditType = .PhoneNumber
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var textField: UITextField!
    @IBOutlet var textFieldBGView: UIView!
    @IBOutlet var label: UILabel!
    @IBOutlet var imgView: UIImageView!
    var verfiedSuccess: (( _ success: Bool) -> Void)? = nil
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.textField.resignFirstResponder()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        // Do any additional setup after loading the view.
        self.label.font = UIFont(name: APP_FontName, size: 17)
        self.textFieldBGView.cornerRadius(15.0)
        self.textFieldBGView.layer.borderColor = APP_ThemeColor.cgColor
        self.textFieldBGView.layer.borderWidth = 1.0
        self.textField.keyboardType = .phonePad
        self.textField.returnKeyType = .send
        self.textField.font = UIFont(name: APP_FontName, size: 15)
        self.textField.borderStyle = .none
        self.textField.delegate = self;
        self.textField.textAlignment = .center
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(PhoneEditViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle("GENERATE OTP", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)
        
       // let phnNumber: String = LoginManager.getMe.phoneNumber!
        
        if editType == .PhoneNumber{
        self.label.text = "Enter the new phone number."
        }else{
            self.label.text = "Enter the new email address."
        }
        submitButton.cornerRadius(15)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func backButtonClicked() {
        self.navigationController!.popViewController(animated: true)
    }

    
    func openOTPViewController(_ phoneNumber: String) {
        
        let otpViewController = OPTViewController.init(nibName: "OPTViewController", bundle: nil)
        otpViewController.newPhoneNumber = phoneNumber;
        otpViewController.editOTP = editType
        self.navigationController!.pushViewController( otpViewController, animated: true)
        
    }
    
    
    @IBAction func submitButtonClicked(_ sender: AnyObject) {
        self.view.endEditing(true)
        let phoneNumber: String = self.textField.text!;

        if editType == .PhoneNumber{
       
        
        if phoneNumber.isEmpty {
            HSShowAlertView("TenantTag", message: "Phone number is required.")
            return;
        }
        
       else if phoneNumber.validatePhoneNumber() == false {
            HSShowAlertView("TenantTag", message: "Phone number is not valid.")
            return;
        }

        let params : Dictionary = ["phoneNo":phoneNumber]
        
        HSProgress(.show)
        HTTPRequest.request().postRequest( APIEditPhone as NSString , param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            HSProgress(.hide)
            
            if isSuccess == true {
                self.openOTPViewController(phoneNumber)
            }
        }
            
        }else{
            if phoneNumber.isEmpty {
                HSShowAlertView("TenantTag", message: "Email is required.")
                return;
            }
          else  if phoneNumber.isValidEmail() == false {
                HSShowAlertView("TenantTag", message: "Email is not valid.")
                return;
            }
            
            let params : Dictionary = ["email":phoneNumber]
            HSProgress(.show)
            HTTPRequest.request().postRequest( APIEditEmail as NSString , param: params as NSDictionary?) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                HSProgress(.hide)
                
                if isSuccess == true {
                    self.openOTPViewController(phoneNumber)
                }
            }



        }

        
    }
    
//    @IBAction func resendButtonClicked(sender: AnyObject) {
//        
//        let phnNumber: String = LoginManager.getMe.phoneNumber!
//        let params : Dictionary = ["phoneNo":phnNumber]
//        HSProgress(.Show)
//        HTTPRequest.request().postRequest( APISendOTPToLandlord, param: params) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
//            
//            HSProgress(.Hide)
//            if isSuccess {
//                HSShowAlertView("TenantTag", message: "OTP sent to landlord")
//            }
//        }
//        
//    }

}
