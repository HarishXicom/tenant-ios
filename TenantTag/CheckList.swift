//
//  CheckList.swift
//  TenantTag
//
//  Created by Hardeep Singh on 29/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class CheckList: NSObject {

    var ID: String
    var content: String
    var status: Bool
    var date_created: String
    var date_modified: String

    override init(){
        
        self.ID = ""
        self.content = ""
        self.status = false
        self.date_created = ""
        self.date_modified = ""

    }
    
    convenience init(obj: AnyObject) {
        self.init()
        
        let dict: Dictionary? = obj as? Dictionary<String,AnyObject>
        if dict == nil {
            return
        }
        
        self.ID = dict!["id"] as! String
        self.content = dict!["content"] as! String
       // self.status = dict!["status"] as! Bool
        self.date_created = dict!["date_created"] as! String
        self.date_modified = dict!["date_modified"] as! String

    }
    
    class func getCheckList( _ callBack: @escaping (Bool, AnyObject?, NSError?) ->())  {
        
        
        HTTPRequest.request().getRequest(APIGetCheckList as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if error != nil {
                
                callBack(false, nil, error)
                return
            }
            
            if isSuccess == true {
                
                let list: [AnyObject]? = data as? [AnyObject]
                if list != nil {
                  
                    var tempList: [CheckList] = [CheckList]()
                    for obj in list! {
                        let checkList: CheckList = CheckList(obj: obj)
                        tempList.append(checkList)
                    }
                    callBack(true, tempList as AnyObject?, nil)
                    return
                }
                
            }else {
                callBack(false, nil, nil)
            }
            
        }
        
    }
    
    

}
