//
//  HSTabBar.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class LineView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.orange
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateLinePosition(_ button: HSBarButton) {
        
        let buttonRect: CGRect = button.frame;
        var lineRect: CGRect = buttonRect
        lineRect.origin.y = buttonRect.maxY
        
        
        if lineRect.equalTo(self.frame) == true {
            
            let tempRect: CGRect = lineRect
            lineRect.origin.y = buttonRect.maxY
            lineRect.origin.x = lineRect.origin.x - 5;
            self.frame = lineRect

            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.frame = tempRect
                }, completion: { (completee: Bool) -> Void in
            })
            
        }else {
            
            let tempRect: CGRect = lineRect

            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.9, options: UIViewAnimationOptions(), animations: { () -> Void in
                   self.frame = tempRect
                }, completion: { (completee: Bool) -> Void in
           
            })
            
        }
        
    }
    
    
}

//
//Tabbar...
//
class HSTabBar: UIView, HSBarButtonDelegate {

    var barButtons: [HSBarButton] = [HSBarButton]()
    var lineView: LineView = LineView()
    
    var barCallBack: ((_ tabBar: HSTabBar, _ barButton: HSBarButton, _ index: Int)-> Void)? = nil

    fileprivate var index: Int = 0
    var selectedIndex : Int {
        get {
            return self.index
        }
        set (index) {
            self.index = index
        }
    }
    
    var selectedButton: HSBarButton? {
        get {
            return self.barButtons[self.selectedIndex]
        }
    }

    
   override var frame: CGRect {
        set (frame) {
            super.frame = frame;
            self.layoutIfNeeded()
        }
        get {
            return super.frame;
        }
    
    }
   
    var hPadding: CGFloat = 0.5;
    var leadingSpace: CGFloat = 0.5
    var tralingSpace: CGFloat = 0.5
    var topSpace: CGFloat = 0.0
    var bottomSpace: CGFloat = 3.0


   // var vPadding: Float = 1.0;

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
    init( buttons: [HSBarButton]) {
        super.init(frame: CGRect.zero)
        self.barButtons = buttons
        self.lineView.frame = CGRect.zero;
        self.addSubview(self.lineView)
        self.lineView.backgroundColor = APP_ThemeColor
        
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        self.setupButtons()
        print("frameUpdate");
        
    }

    
    //MARK:-
    
    func setTabBarCallBack(_ callBack: @escaping ((_ tabBar: HSTabBar, _ barButton: HSBarButton, _ index: Int)-> Void) ) {
        self.barCallBack = callBack
    }
    
    func barButtonClicked(_ barButton: HSBarButton) {
        
        if (self.selectedButton != nil) && (self.selectedButton != barButton) {
            
            self.selectedButton!.isSelected = false;
            self.selectedButton!.label.textColor = UIColor.lightGray
            self.selectedIndex = self.barButtons.index(of: barButton)!
            
            self.lineView.updateLinePosition(barButton)
            self.selectedButton!.isSelected = true;
            self.selectedButton!.label.textColor = APP_ThemeColor

            if self.barCallBack != nil {
             self.barCallBack!(self,
                self.selectedButton!,
                self.selectedIndex)
            }
            
        }else {
            self.lineView.updateLinePosition(barButton)
        }
        
    }
    

    
    func setupButtons() {
        
        if self.barButtons.count == 0 {
            return
        }
        
        if self.barButtons.count == 1 {
            self.hPadding = 0.0;
        }

        
        let totalButton: Int = self.barButtons.count;
        let width = self.frame.width
        let height = self.frame.height

        let buttonWidth: CGFloat = (width - ( leadingSpace + (hPadding * CGFloat((totalButton-1))) + tralingSpace )) / CGFloat(totalButton)
        let buttonHeight: CGFloat = height - (topSpace + bottomSpace)

        var buttonRect = CGRect.zero
        
        for view in self.barButtons {
            
            let index: Int? = self.subviews.index(of: view)
            buttonRect = CGRect(x: buttonRect.maxX + hPadding, y: self.topSpace, width: buttonWidth, height: buttonHeight)
            view.frame = buttonRect
            if index == nil {
                view.delegate = self;
                self.addSubview(view);
            }
            
            if view.isSelected == true {
                let selecetedIndex: Int = self.barButtons.index(of: view)!
                self.lineView.updateLinePosition(view)
                self.selectedIndex = selecetedIndex
                view.label.textColor = APP_ThemeColor
            }else {
                view.label.textColor = UIColor.lightGray
            }

        }
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
