//
//  HSDoc.swift
//  TenantTag
//
//  Created by Hardeep Singh on 25/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class HSDoc: NSObject {
    
    var ID: String? = nil
    var serverURLStr : String? = nil
    
    var URL : Foundation.URL? {
        get {
            if serverURLStr == nil {
                return nil
            }
            return Foundation.URL(string: self.serverURLStr!)
        }
    }
    
    
    override init() {
        
    }
    
    convenience init(docObj: Dictionary<String, AnyObject>) {
        
        self.init()
        
        let ID:String? =  docObj["id"] as? String
        if ID != nil {
            self.ID = ID
        }else{
            self.ID = ""
        }
        
        let docURL: String? =  docObj["url"] as? String
        if docURL != nil {
            self.serverURLStr = docURL!.encodeSpecialCharacter();
        }
        
    }

}
