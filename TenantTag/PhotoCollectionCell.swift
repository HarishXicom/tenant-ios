//
//  PhotoCollectionCell.swift
//  Who's In
//
//  Created by Hardeep Singh on 14/12/15.
//  Copyright © 2015 XICOM. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var overlayView: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
    var photo:HSPhoto? = nil
    var marked:Bool! = false
    var deleteImgCallBack:((HSPhoto)->Void)? = nil

   func udpateCell( _ photo:HSPhoto? ){
    
      if photo == nil {
          return
      }
    
    if photo!.image != nil {
        self.imgView.image = photo!.image!;
    }else {
       /// let img: UIImage? = UIImage(named: "") as?  UIImage
         self.imgView .setImageWith(photo!.URL as URL, placeholderImage:nil)
    }
    
      self.photo = photo
    
      if marked == true {
          self.overlayView.alpha = 0.0;
          self.crossButton.isHidden = false
      }else{
          self.overlayView.alpha = 0.0;
          self.crossButton.isHidden = true
      }
    
    
    }
    
    @IBAction func crossButtonClicked(_ sender: AnyObject) {
        
        HSShowActionSheet(nil, message: "Delete image?", cancel: "Cancel", destructive: "Delete", actions: nil) { (cancel, destruction, index) -> () in
           
            if destruction == true {
                if self.deleteImgCallBack != nil {
                    self.deleteImgCallBack!(self.photo!)
                }
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.overlayView.backgroundColor = UIColor.white

    }

}
