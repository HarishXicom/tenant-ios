//
//  LandlordViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class LandlordViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var landlord: Landlord? = nil
    
    var boolNav = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Landlord.getLandlordDetail { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if isSuccess == true {
                self.landlord = data as? Landlord
                self.tableView.reloadData()
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if boolNav == true{
        
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(LandlordViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
//        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
//        imageView.contentMode = UIViewContentMode.left
//        imageView.sizeToFit()
     //   let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("LANDLORD", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)
       
        }
        
        let nib: UINib = UINib(nibName: "LandlordCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "LandlordCell")
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground

    

        // Do any additional setup after loading the view.
    }
    
    func backButtonClicked()
    {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.popViewController(animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - TAble
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.landlord == nil {
            return 0;
        }else {
            return 1;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell: LandlordCell = tableView.dequeueReusableCell(withIdentifier: "LandlordCell") as! LandlordCell
            cell.updateCell(self.landlord)
            return cell
            
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let message: Message = Message()
        return LandlordCell.cellHeight(message, size: tableView.frame.size)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.marginWillShow(false)
        tableView.marginWillShow(false)
        cell.selectionStyle = .none
    }


}
