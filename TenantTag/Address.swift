//
//  Address.swift
//  TenantTag
//
//  Created by Hardeep Singh on 05/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class Address: NSObject {
    
    var address: String? = nil
    var latitude: String? = nil
    var longitute: String? = nil
    var ID: String = ""
    var phoneNumber: String? =  nil
    
    override init() {
        
        self.address = "Windsor Falls Community, Gate: 345 Po Box 123456 Windsor Falls Management"
        self.phoneNumber = "904-559-5885"
        
    }
    
}
