//
//  DocViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 24/02/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class DocViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var tableView: UITableView!
    weak var property: Property? = nil
    
    var topTabBar: HSTabBar!
    fileprivate var selectedIndex: Int = 0

    var setSelectedIndex: Int {
        
        set (index) {
            self.selectedIndex = index;
            self.setUpCurrentView()
        }
        
        get {
            return self.selectedIndex;
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
       // print(self.topView.bounds)
       // self.topTabBar.frame = self.topView.bounds
       // self.tableView!.frame = self.container.bounds
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        // Do any additional setup after loading the view.
        let nib: UINib = UINib(nibName: "DocCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "DocCell")
        
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: Selector(("popOneViewController")), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        
        self.navTitle("DOCUMENTS", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)

        
        let button1: HSBarButton = HSBarButton(title: "Lease", image: nil)
        button1.isSelected = true;
        let button2: HSBarButton = HSBarButton(title: "Community", image: nil)
        let topTabBar: HSTabBar = HSTabBar(buttons: [button1,button2])
        self.topView.addSubview(topTabBar);
        self.topTabBar = topTabBar;
        self.topTabBar.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: 35)
        
        self.topTabBar.setTabBarCallBack { (tabBar: HSTabBar, barButton: HSBarButton, index: Int) -> Void in
            self.setSelectedIndex = index;
        }
        
        
       

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setUpCurrentView() {
        self.tableView.reloadData()
    }

    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.property == nil {
            return 0;
        }
        
        if self.selectedIndex == 0 {
            return self.property!.leaseDoc.count;
        }else {
            return self.property!.communityDoc.count;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: DocCell = tableView.dequeueReusableCell(withIdentifier: "DocCell") as! DocCell
        cell.lable.text = "DOCUMENT \((indexPath.row+1))"
        cell.selectionStyle = .none;
        return cell;

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        var doc: HSDoc? = nil
        
        if self.selectedIndex == 0 {
            doc = self.property!.leaseDoc[indexPath.row];
        }else {
            doc = self.property!.communityDoc[indexPath.row];
        }
        
        if doc!.URL == nil {
            UIAlertView(title: nil, message: "URL is not exist.", delegate: nil, cancelButtonTitle: "Ok").show()
            return;
        }
        
        if UIApplication.shared.canOpenURL(doc!.URL! as URL) {
            UIApplication.shared.openURL(doc!.URL! as URL)
        }else {
            UIAlertView(title: nil, message: "Your device doesn't support this feature.", delegate: nil, cancelButtonTitle: "Ok").show()
        }

        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
