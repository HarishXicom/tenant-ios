//
//  Landlord.swift
//  TenantTag
//
//  Created by Hardeep Singh on 20/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class Landlord: User {
    
    var address: String
    
    override init() {
        self.address = ""
    }
    
    convenience init(obj: AnyObject) {
        
        self.init()
        let dict: Dictionary? = obj as? Dictionary<String, AnyObject>
        if dict == nil {
            return
        }
        
        let mem_id: String = dict!["mem_id"] as! String
        self.ID = mem_id

        let email: String = dict!["email"] as! String
        self.emailAddress = email

        let first_name: String = dict!["first_name"] as! String
        self.firstName = first_name

        let last_name: String = dict!["last_name"] as! String
        self.lastName = last_name

        let mobile_no: String = dict!["mobile_no"] as! String
        self.phoneNumber = mobile_no

        let company_name: String = dict!["company_name"] as! String
        self.companyName = company_name
        let address: String = dict!["address"] as! String
        self.address = address

    }
    
    func displayAddress() -> String {
        return  self.address.capitalizedFirstChr
    }
    
    class  func getLandlordDetail( _ callBack:@escaping (_ isSuccess: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
        
        
        HTTPRequest.request().getRequest(APIGetLandlordDetail as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if error != nil {
                callBack(false, nil, nil)
                return;
            }
            
            if isSuccess == true {
                
                let dict: Dictionary? = data as? Dictionary<String, AnyObject>
               
                if dict != nil {
                    let landlord: Landlord = Landlord(obj: dict! as AnyObject)
                    callBack(true, landlord, nil)
                    return;
                }
                callBack(true, nil, nil)
            }else {
                callBack(false, data, nil)
            }
        }
    }
    
}
