//
//  NSString+XCString.h
//  VIP LIst
//
//  Created by Hardeep Singh on 13/05/15.
//  Copyright (c) 2015 Hardeep Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (XCString)

/*!
 * @description To validate string is empty or contains only white spaces
 * @param
 * @return BOOL describing if it's empty or contains only white spaces
 */
- (BOOL)isEmpty;

+ (BOOL)isEmpty:(NSString *)string;

- (NSString *)trimmingWhiteSpaceAndNewLine;


/*!
 * @description To validate string for mobile number
 * @param
 * @return BOOL describing if string is valid mobile numbe or not
 */
- (BOOL)validatePhoneNumber;

/*!
 * @description To validate string for email
 * @param
 * @return BOOL describing if string is valid email or not
 */
- (BOOL)validateEmail:(BOOL)stricterFilter;

- (BOOL)validateUserNameAlphanumericAndSymbol:(NSString *)symbols;

- (NSString *)buildRankString;

@end
