//
//  CheckListViewController.swift
//  TenantTag
//
//  Created by Hardeep Singh on 27/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit
import MessageUI

class CheckListViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var tableView: UITableView!
    var checkList: [CheckList] = [CheckList]()
    var indicatorView: UIActivityIndicatorView? = nil
    
    // MARK: - Navigation

    
    func backButtonClicked() {
            self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(CheckListViewController.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = UIViewContentMode.left
        imageView.sizeToFit()
        let barButton2: UIBarButtonItem =  UIBarButtonItem(customView: imageView)
        self.navigationItem.leftBarButtonItems = [barButton, barButton2];
        self.navTitle("NEW TENANT CHECKLIST", color: UIColor.white, font: UIFont(name: APP_FontName, size: 14)!)

        var nib: UINib = UINib(nibName: "CheckListTopCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CheckListTopCell")
        
        nib = UINib(nibName: "CheckListCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CheckListCell")
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        
        let fView: UIView = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 1.0))
        fView.backgroundColor = UIColor.clear
        self.tableView.tableFooterView = fView;
        
        let rect = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 50.0)
        self.indicatorView = UIActivityIndicatorView(frame: rect)
        
        CheckList.getCheckList { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
            
            if isSuccess == true {
                
                let list: [CheckList]? = data as? [CheckList]
                
                if list != nil {
                    
                    self.checkList.removeAll()
                    self.checkList.append(contentsOf: list!)
                    self.tableView.reloadData()
                    self.indicatorView!.stopAnimating()
                    self.indicatorView!.removeFromSuperview()
                    
                }
                
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    func forwardYourEmail() {
        
        let URL: Foundation.URL = Foundation.URL(string: kForwardYourMail)!
        if UIApplication.shared.canOpenURL(URL) == true {
            UIApplication.shared.openURL(URL)
        }
        
    }
    
    func iMovedEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
           // mail.setToRecipients([0"])
            mail.setSubject("I moved")
            
            let address: String = LoginManager.share().propertyAddress!;
            let bodyMessage: String = "I moved! My new address is \(address). Have a great day!"
            mail.setMessageBody("Hi,\n\(bodyMessage)", isHTML: true)
            present(mail, animated: true, completion: nil)
       
        }else {
            
            self.showSendMailErrorAlert()
        }
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1;
        }
        
        if self.checkList.count == 0 {
            
            return 1
        }else {
            
          return self.checkList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.section == 0 {
            
            let cell: CheckListTopCell = tableView.dequeueReusableCell(withIdentifier: "CheckListTopCell") as! CheckListTopCell
            
            cell.forwarYourmailCallBack = { (void)-> () in
                 self.forwardYourEmail()
            }
            
            cell.emailYourFriendsCallBack = { (void)-> () in
                self.iMovedEmail()
            }
            
            return cell
            
        }else {
            
            if self.checkList.count == 0 {
                
                var progressCell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "progressCell")

                if progressCell == nil {
                    
                    progressCell = UITableViewCell(style: .default, reuseIdentifier: "progressCell")
                    progressCell!.selectionStyle = .none
                    progressCell!.contentView.addSubview(self.indicatorView!)
                    progressCell!.backgroundColor = UIColor.clear
                    progressCell!.contentView.backgroundColor = UIColor.clear
                    self.indicatorView!.frame = CGRect( x: 0, y: 0, width: tableView.frame.width, height: 50.0)
                    self.indicatorView!.tintColor = UIColor.blue
                    self.indicatorView!.activityIndicatorViewStyle = .whiteLarge;
                    self.indicatorView!.startAnimating()
                    self.indicatorView!.color = APP_ThemeColor
                
                }
                
                return progressCell!;
                
            }else {
                
                let cell: CheckListCell = tableView.dequeueReusableCell(withIdentifier: "CheckListCell") as! CheckListCell
                let checkList: CheckList =  self.checkList[indexPath.row]
                cell.updateCell(checkList)
                return cell
                
            }
            
           
            
        }
        
        //CheckListTopCell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            cell.backgroundColor = UIColor.groupTableViewBackground
            cell.contentView.backgroundColor = UIColor.groupTableViewBackground
            cell.selectionStyle = .none

        }else {
            
            if self.checkList.count == 0 {
                
                cell.backgroundColor = UIColor.groupTableViewBackground
                cell.contentView.backgroundColor = UIColor.groupTableViewBackground
                cell.selectionStyle = .none
                
            }else {
                
                cell.backgroundColor = UIColor.white
                cell.contentView.backgroundColor = UIColor.white
                cell.selectionStyle = .none
                
            }

            
            

        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 152.0
        }
        
        if (self.checkList.count > 0 ) {
        
            let checkList: CheckList =  self.checkList[indexPath.row]
            
            let maxSize: CGSize = CGSize( width: (tableView.frame.size.width - 51), height: 999)
            let font: UIFont =  UIFont(name: APP_FontName, size: 16)!
            
//            let text: String = "testing text is date_created and date_modified.testing text is date_created and date_modified testing text is date_created and date_modified testing text is date_created and date_modified testing text is date_created and date_modified testing text is date_created and date_modified testing text is date_created and date_modified"
            var boundingBox = checkList.content.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size.height
            boundingBox =   (boundingBox + 33.0)
            return boundingBox;
            
        }
        
       return 0
    
    }
    


}
