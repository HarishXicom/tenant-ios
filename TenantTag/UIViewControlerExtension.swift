//
//  UIViewControlerExtension.swift
//  EZSwipeController
//
//  Created by Hardeep Singh on 31/12/15.
//  Copyright © 2015 Goktug Yilmaz. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    /*
    func addChildViewController(viewController: UIViewController!, containerView: UIView? ){
        
        self.addChildViewController(viewController)
    }
    
    func removeChildViewController(viewController: UIViewController) {
        
    }
    
    */
    
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    public var nameOfClass: String{
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
    
    
    public func navTitle(_ title:NSString,color:UIColor,font:UIFont){
        
        let label: UILabel = UILabel()
        label.backgroundColor = UIColor.clear
        label.text = title as String
        label.font = font
        label.textAlignment = NSTextAlignment.center
        label.isUserInteractionEnabled = true;
        label.textColor = color
        self.navigationItem.titleView = label
        label.sizeToFit()
        
    }
    
    public func underTopBar( _ no: Bool) {
        
        if no == true  {
            self.edgesForExtendedLayout = UIRectEdge()
            self.automaticallyAdjustsScrollViewInsets = false
            self.extendedLayoutIncludesOpaqueBars = false
        }else {
            
        }
    
       
    }
    
    public func hideMessage() {
        
        var label: UILabel? = self.view.viewWithTag(5905) as? UILabel
        if label != nil {
            label!.removeFromSuperview()
            label = nil;
        }
    }
    

    public func showMessageInView( _ message: NSString?, view: UIView?, edgeInsets: UIEdgeInsets?) {
        var message = message, view = view, edgeInsets = edgeInsets
        
        if view == nil {
            view = self.view
        }
        
        if message == nil {
            message = "Not found"
        }
        
        if edgeInsets == nil {
            edgeInsets = UIEdgeInsets()
        }
        
        var label: UILabel? = self.view.viewWithTag(5905) as? UILabel
        if label == nil {
            label = UILabel()
        }
        label!.textAlignment = .center
        label!.backgroundColor = UIColor.groupTableViewBackground
        label!.textColor = UIColor.black
        label!.font = UIFont.systemFont(ofSize: 17)
        label!.isUserInteractionEnabled = true
        label!.tag = 5905;
        label!.numberOfLines = 1;
        label!.text = message! as String
        
        var bounds: CGRect = view!.bounds
        
        bounds.origin.x = edgeInsets!.left
        bounds.origin.y = edgeInsets!.top
        bounds.size.height = (bounds.size.height - ((edgeInsets!.top) + edgeInsets!.bottom))
        bounds.size.width = (bounds.size.width - ((edgeInsets!.left) + edgeInsets!.right))
        label!.frame = bounds
        view!.addSubview(label!)
        
    }
    
    public func showMessageInView( _ message: NSString?, view: UIView? ) {
        self.showMessageInView(message, view: view, edgeInsets: nil)
    }
    
    public func showMessage(_ message: NSString?) {
        self.showMessageInView(message, view: nil)
    }
    
    func popOneViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    


}


