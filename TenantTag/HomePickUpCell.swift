//
//  HomePickUpCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 04/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class HomePickUpCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    var amenity: Amenity? = nil
    
    
    func updateCell(_ amenity: Amenity?) {
        
        self.amenity = amenity;
        
        if self.amenity != nil {
            self.label1.text = self.amenity!.name
            self.label2.text = self.amenity!.day
        }else {
            self.label1.text = ""
            self.label2.text = ""
        }
        
        self.bgView.backgroundColor = UIColor.white
        
    }
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        self.label1.font = UIFont(name: APP_FontName, size: 15)
        self.label2.font = UIFont(name: APP_FontName, size: 15)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
