//
//  UIActionSheet+Blocks.h
//  UIActionSheetBlocks
//
//  Created by Ryan Maxwell on 31/08/13.
//
//  The MIT License (MIT)
//
//  Copyright (c) 2013 Ryan Maxwell
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of
//  this software and associated documentation files (the "Software"), to deal in
//  the Software without restriction, including without limitation the rights to
//  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//  the Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import <UIKit/UIKit.h>

typedef void (^UIActionSheetBlock) ( UIActionSheet * _Nonnull  actionSheet);
typedef void (^UIActionSheetCompletionBlock) (UIActionSheet * _Nonnull actionSheet, NSInteger  buttonIndex);

@interface UIActionSheet (Blocks)

+ (nonnull instancetype)showFromTabBar:( UITabBar * _Nonnull)tabBar
                     withTitle:( NSString * _Nullable)title
             cancelButtonTitle:( NSString * _Nullable)cancelButtonTitle
        destructiveButtonTitle:( NSString * _Nullable)destructiveButtonTitle
             otherButtonTitles:( NSArray * _Nullable)otherButtonTitles
                      tapBlock:( UIActionSheetCompletionBlock _Nonnull)tapBlock;

+ (nonnull instancetype)showFromToolbar:( UIToolbar * _Nonnull)toolbar
                      withTitle:( NSString * _Nullable )title
              cancelButtonTitle:( NSString * _Nullable )cancelButtonTitle
         destructiveButtonTitle:( NSString * _Nullable )destructiveButtonTitle
              otherButtonTitles:( NSArray * _Nullable )otherButtonTitles
                       tapBlock:( UIActionSheetCompletionBlock _Nonnull)tapBlock;

+ (nonnull instancetype)showInView:( UIView * _Nonnull)view
                 withTitle:( NSString * _Nullable)title
         cancelButtonTitle:( NSString * _Nullable)cancelButtonTitle
    destructiveButtonTitle:( NSString * _Nullable)destructiveButtonTitle
         otherButtonTitles:( NSArray * _Nullable)otherButtonTitles
                  tapBlock:( UIActionSheetCompletionBlock _Nonnull)tapBlock;

+ (nonnull instancetype)showFromBarButtonItem:( UIBarButtonItem * _Nonnull)barButtonItem
                             animated:(BOOL)animated
                            withTitle:( NSString * _Nullable)title
                    cancelButtonTitle:( NSString * _Nullable)cancelButtonTitle
               destructiveButtonTitle:( NSString * _Nullable)destructiveButtonTitle
                    otherButtonTitles:( NSArray * _Nullable)otherButtonTitles
                             tapBlock:( UIActionSheetCompletionBlock _Nonnull)tapBlock;

+ (nonnull instancetype)showFromRect:(CGRect)rect
                      inView:( UIView * _Nonnull)view
                    animated:(BOOL)animated
                   withTitle:( NSString * _Nullable)title
           cancelButtonTitle:( NSString * _Nullable)cancelButtonTitle
      destructiveButtonTitle:( NSString * _Nullable)destructiveButtonTitle
           otherButtonTitles:( NSArray * _Nullable)otherButtonTitles
                    tapBlock:( UIActionSheetCompletionBlock _Nonnull)tapBlock;

@property (copy, nonatomic, ) UIActionSheetCompletionBlock _Nullable tapBlock;
@property (copy, nonatomic, ) UIActionSheetCompletionBlock _Nullable willDismissBlock;
@property (copy, nonatomic, ) UIActionSheetCompletionBlock _Nullable didDismissBlock;

@property (copy, nonatomic, ) UIActionSheetBlock _Nullable willPresentBlock;
@property (copy, nonatomic, ) UIActionSheetBlock _Nullable didPresentBlock;
@property (copy, nonatomic, ) UIActionSheetBlock _Nullable cancelBlock;

@end
