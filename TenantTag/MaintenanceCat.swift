//
//  MaintenanceCat.swift
//  TenantTag
//
//  Created by Hardeep Singh on 20/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class MaintenanceCat: NSObject {

    var ID: String
    var name: String
    
    override init() {
        self.ID = ""
        self.name = ""
    }
    
    convenience init(obj: AnyObject) {
        
        self.init()
        
        let dict: Dictionary? = obj as? Dictionary<String, AnyObject>
        if dict != nil {
            let categoryId: String = dict!["categoryId"] as! String
            self.ID = categoryId
            let categoryName: String = dict!["categoryName"] as! String
            self.name = categoryName
        }
        
    }

    
    class func getMaintenanceAllCategories( _ callBack:@escaping (_ isSuccess: Bool, _ tmaintenance: Bool, _ data: AnyObject?, _ error: NSError?) -> ()) {
           
            //APIGetProperty
            HTTPRequest.request().getRequestReturnFullObj( APIGetMainTenanceCat as NSString, param: nil) { (isSuccess: Bool, data: AnyObject?, error: NSError?) -> () in
                
                if error != nil {
                    callBack(false, false, nil, nil)
                    return;
                }
                
                if isSuccess == true {
                    
                    let dataDict: Dictionary? = data as? Dictionary<String,AnyObject>
                    if dataDict == nil {
                        callBack(false, false, nil, nil)
                        return;
                    }
                    
                    let catList: [AnyObject]? = dataDict!["data"] as? [AnyObject]
                    let tmaintenance: Bool? = dataDict!["tmaintenance"] as? Bool
                    
                    if catList != nil {
                        var mcList: [MaintenanceCat] = [MaintenanceCat]()
                        for cat in catList! {
                            let property: MaintenanceCat = MaintenanceCat(obj: cat)
                            mcList.append(property)
                        }
                        callBack(true, tmaintenance!, mcList as AnyObject?, nil)
                        return;
                        
                    }else {
                        callBack(false, false, [MaintenanceCat]() as AnyObject?, nil)
                    }
                    
                }else {
                    callBack(false, false, data, nil)
                }
                
            }
            
    }

}
