//
//  LandlordCell.swift
//  TenantTag
//
//  Created by Hardeep Singh on 06/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

class LandlordCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailAddress: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    @IBOutlet weak var addressImgView: UIImageView!
    @IBOutlet weak var emailImgView: UIImageView!
    @IBOutlet weak var phnImgView: UIImageView!
    weak var landlord: Landlord? = nil
    
    
    class func cellHeight(_ address: Message, size: CGSize) ->(CGFloat) {
        
        let text: String = "New Address Name, 123 St. PO Box 123456, New Address"; //address.text!;
        
        let topSpace: CGFloat = 12.0;
        let bottomSpace: CGFloat = 12.0;
        
        let leadingSpace: CGFloat = 8.0;
        let trailingSpace: CGFloat = 8.0;
        
        let vPadding: CGFloat = 8.0;
        let hPadding: CGFloat = 5.0;
        
        let locationImgWH: CGFloat = 20.0;
        let messageImgWH: CGFloat = 20.0;
        let phnImgWH: CGFloat = 20.0;
        
        let nameLabelHeight: CGFloat = 20.0

        let textWidth = (size.width - (leadingSpace + trailingSpace + locationImgWH + (hPadding*3)))
        
        let font: UIFont =  UIFont(name: APP_FontName, size: 18)!
        let maxSize: CGSize = CGSize( width: CGFloat(textWidth), height: 9999.0)
        let boundingBox = text.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size.height
        
        let maxHeight = ((boundingBox + topSpace + bottomSpace + messageImgWH + phnImgWH + nameLabelHeight + (vPadding*5)+15))
        return maxHeight
        
    }
    
    func updateCell(_ landloard: Landlord?) {
        self.landlord = landloard;
        
        if self.landlord != nil {
            
            self.nameLabel.text = self.landlord!.displayName()
            self.addressLabel.text = self.landlord!.displayAddress()
            self.emailAddress.text =  self.landlord!.emailAddress;
            self.phoneLabel.text = self.landlord!.phoneNumber;
            
        }else {
            
            self.nameLabel.text = ""
            self.addressLabel.text = ""
            self.emailAddress.text =  ""
            self.phoneLabel.text = ""
        }
      
        
    }


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.containerView.backgroundColor = UIColor.white
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        self.cornerRadius(2.0)
        self.containerView.layer.borderWidth = 0.2;
        
        self.nameLabel.font =  UIFont(name: APP_FontName, size: 18)
        self.addressLabel.font = UIFont(name: APP_FontName, size: 18)
        self.emailAddress.font = UIFont(name: APP_FontName, size: 15)
        self.phoneLabel.font = UIFont(name: APP_FontName, size: 15)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            
            let point:CGPoint = touch.location(in: self)
            if  self.phoneLabel.frame.contains(point)  {
                self.phoneLabel.textColor = UIColor.blue
            }else if self.emailAddress.frame.contains(point)  {
                
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.phoneLabel.textColor = APP_ThemeColor
        
        if let touch = touches.first {
            let point:CGPoint = touch.location(in: self)
            
            if  self.phoneLabel.frame.contains(point)  {
                
                if self.landlord!.phoneNumber != nil {

                    let phnNumber = self.landlord!.phoneNumber!.phoneNumberWithoutFormate()
                    let URLStr = "telprompt://\(phnNumber)"
                    let URL: Foundation.URL? = Foundation.URL(string: URLStr)
                    if URL == nil {
                        UIAlertView(title: nil, message: "Phone number is not valid.", delegate: nil, cancelButtonTitle: "Ok").show()
                        return;
                    }
                    if UIApplication.shared.canOpenURL(URL!) {
                        UIApplication.shared.openURL(URL!)
                    }else {
                        UIAlertView(title: nil, message: "Your device doesn't support this feature.", delegate: nil, cancelButtonTitle: "Ok").show()
                    }
                }
                
            }else if self.emailAddress.frame.contains(point)  {
                
            }
            
        }
        
        
    }

    
    
}
