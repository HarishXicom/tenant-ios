 //
//  Me.swift
//  TenantTag
//
//  Created by Hardeep Singh on 06/01/16.
//  Copyright © 2016 XICOM. All rights reserved.
//

import UIKit

enum TPayStatus : Int{
    case Allowed = 1
    case NotAllowed = 0
}

 class Me: NSObject,NSCoding {
    
   
    
    
    var ID: String!
    var firstName: String? = nil
    var lastName: String? = nil
    var emailAddress: String? = nil
    var phoneNumber: String? = nil
    var landlordNumber: String? = nil;
    var language: String? = nil
    var brithdayStr: String? = nil
    var brithdayDate: Date? = nil
    var companyName: String? = nil
    var verified: Bool = false
    var moveStatus: MoveStatus = .in
    var userIPAddress : String!
    
    var ownerSynpsePayEnabled = false

    
    var userFingerPrint : String!
    
    
    var fullName: String {
        
        get {
            let fName = firstName != nil ? firstName : ""
            let lName = lastName != nil ? lastName : ""
            return "\(fName!) \(lName!)"
        }
    }
    
    var synapsePayUserID : String!
    var sessionID: String!
    var password: String? = nil
    var tPayStatus : TPayStatus = .NotAllowed
    var landLordSubscriptionStatus = false

    var landlordAccountNo : String!
    
    var thirdParty  = false
    
    //MARK: - Display

    override init() {
        
        self.ID = ""
        self.firstName = ""
        self.lastName = ""
        self.emailAddress = ""
        self.phoneNumber = ""
        self.language = ""
        self.brithdayStr = ""
        self.brithdayDate = Date()
        self.companyName = ""
        self.landlordNumber = ""

        
        
        self.synapsePayUserID = ""
        self.sessionID = ""
        self.landlordAccountNo = ""
        userIPAddress = ""
          userFingerPrint = ""
    }
    
   // required convenience init?(coder aDecoder: NSCoder) {
        
        
    
    
    
    required init?(coder aDecoder: NSCoder) {

        let firstName: String? = aDecoder.decodeObject(forKey: "firstName") as? String
        if firstName != nil {
            self.firstName = firstName;
        }
        
        let lastName: String? = aDecoder.decodeObject(forKey: "lastName") as? String
        if lastName != nil {
            self.lastName = lastName;
        }
        
        let phoneNumber: String? = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        if phoneNumber != nil {
            self.phoneNumber = phoneNumber;
        }
        
        let landlordNumber: String? = aDecoder.decodeObject(forKey: "landlordNumber") as? String
        if landlordNumber != nil {
            self.landlordNumber = landlordNumber;
        }
        
        let emailAddress: String? = aDecoder.decodeObject(forKey: "emailAddress") as? String
        if emailAddress != nil {
            self.emailAddress = emailAddress;
        }
        
        let birthday: String? = aDecoder.decodeObject(forKey: "brithdayStr") as? String
        if birthday != nil {
            self.brithdayStr = birthday;
            let dateFormate: DateFormatter = DateFormatter()
            dateFormate.dateFormat = APP_DOBDateFormate
            let date: Date? = dateFormate.date(from: self.brithdayStr!)
            self.brithdayDate = date;
            
        }
        
        let language: String? = aDecoder.decodeObject(forKey: "language") as? String
        if language != nil {
            self.language = language;
        }
        
        
        
        let synapsePayUserID: String? = aDecoder.decodeObject(forKey: "synapsePayUserID") as? String
        if synapsePayUserID != nil {
            self.synapsePayUserID = synapsePayUserID;
        }

        
        let sessionID: String? = aDecoder.decodeObject(forKey: "sessionID") as? String
        if sessionID != nil {
            self.sessionID = sessionID;
        }
        
        let password: String? = aDecoder.decodeObject(forKey: "password") as? String
        if password != nil {
            self.password = password;
        }
        
        let ver = aDecoder.decodeBool(forKey: "verified")
            self.verified = ver
        
        let moveStatus = aDecoder.decodeInteger(forKey: "moveStatus")
            self.moveStatus = MoveStatus(rawValue: moveStatus)!
        

        
        let thirdParty: Bool? = aDecoder.decodeBool(forKey: "thirdParty")
        if thirdParty != nil {
          self.thirdParty = thirdParty!
        }
        
        let subScription: Bool? = aDecoder.decodeBool(forKey: "landLordSubscriptionStatus")
        if subScription != nil {
            self.landLordSubscriptionStatus = subScription!
        }
        
        
        let payStatus: Int? = aDecoder.decodeInteger(forKey: "tPayStatus")
            self.tPayStatus = TPayStatus(rawValue: payStatus!)!
        
        let landLordAccount: String? = aDecoder.decodeObject(forKey: "landlordAccountDetails") as? String
        if landLordAccount != nil {
            self.landlordAccountNo = landLordAccount
        }
        
        
        
        
        let ipAddress: String? = aDecoder.decodeObject(forKey: "userIPAddress") as? String
        if ipAddress != nil {
            self.userIPAddress = ipAddress
        }
        
        
        let userFingerPrint: String? = aDecoder.decodeObject(forKey: "userFingerPrint") as? String
        if userFingerPrint != nil {
            self.userFingerPrint = userFingerPrint
        }
        
        
        let ownerEnabled  = aDecoder.decodeBool(forKey: "ownerSynpsePayEnabled")
            self.ownerSynpsePayEnabled = ownerEnabled;
        
        
    }
    
    public func encode(with aCoder: NSCoder) {
        
      aCoder.encode(self.firstName, forKey: "firstName")
        aCoder.encode(self.lastName, forKey: "lastName")
        aCoder.encode(self.phoneNumber, forKey: "phoneNumber")
        aCoder.encode(self.landlordNumber, forKey: "landlordNumber")
        aCoder.encode(self.emailAddress, forKey: "emailAddress")
        aCoder.encode(self.brithdayStr, forKey: "brithdayStr")
        aCoder.encode(self.language, forKey: "language")
        aCoder.encode(self.sessionID, forKey: "sessionID")
        aCoder.encode(self.synapsePayUserID, forKey: "synapsePayUserID")
        aCoder.encode(self.verified, forKey: "verified")
        aCoder.encode(self.moveStatus.rawValue, forKey: "moveStatus")
        aCoder.encode(self.tPayStatus.rawValue, forKey: "tPayStatus")
        aCoder.encode(self.thirdParty, forKey: "thirdParty")
        aCoder.encode(self.landLordSubscriptionStatus, forKey: "landLordSubscriptionStatus")
        aCoder.encode(self.landlordAccountNo, forKey: "landlordAccountDetails")
        aCoder.encode(self.userIPAddress, forKey: "userIPAddress")
        aCoder.encode(self.userFingerPrint, forKey: "userFingerPrint")
        aCoder.encode(self.ownerSynpsePayEnabled, forKey: "ownerSynpsePayEnabled")


        
    }
    
    
    
    
    func updateUser(_ obj: AnyObject) {
       // super.updateUser(obj)
        let dict: Dictionary? = obj as? Dictionary<String,AnyObject>
        if dict == nil {
            return
        }
        
       if let ID = dict!["mem_id"] as? String
         {
            self.ID = ID
        }
       
        if let email = dict!["email"] as? String
        {
        self.emailAddress = email
        }

        
      if   let first_name = dict!["first_name"] as? String
        {
        self.firstName = first_name

        }
        
        if let last_name = dict!["last_name"] as? String
        {
            
            self.lastName = last_name

        }
        
        if let company_name = dict!["company_name"] as? String
        {
            self.companyName = company_name
        }

        
        //let company_name = dict!["company_name"] as? String
        
        let dob = dict!["dob"] as? String
        if dob != nil {
            let date = Date(timeIntervalSince1970:Double(dob!)!)
            self.brithdayDate = date
            let dateFormate: DateFormatter = DateFormatter()
            dateFormate.dateFormat = APP_DOBDateFormate
            let dateStr: String? = dateFormate.string(from: date)
            if dateStr != nil {
                self.brithdayStr = dateStr
            }
        }
        
        
        if let mobile_no = dict!["mobile_no"] as? String
        {
            self.phoneNumber = mobile_no
        }
        
        
        if let landlord_number = dict!["landlord_number"] as? String
        {
            self.landlordNumber = landlord_number
        }
        

        
        let app_verified: String? = dict!["app_verified"] as? String
        if app_verified != nil {
            if app_verified == "1" {
                self.verified = true
            }else {
                self.verified = false
            }
        }else {
            //self.verified = true
        }
        
        let move_status: String? = dict!["move_status"] as? String
        if move_status != nil {
            self.moveStatus = MoveStatus(rawValue: Int(move_status!)!)!
            //self.moveStatus = MoveStatus.Out
        }
        
        
        let seesss: String? =  dict!["sessionId"] as? String
        if seesss != nil {
            self.sessionID = seesss
        }
        let synapse_user_id: String? =  dict!["synapse_user_id"] as? String
        if synapse_user_id != nil {
            self.synapsePayUserID = synapse_user_id
        }
        
     /*   let thirdParty: String? =  dict!["third_party_sharing"] as? String
        if thirdParty != nil {
            if thirdParty == "0"
            {
                //forcefully set true
                
                self.thirdParty = true
            }else{
                self.thirdParty = true
            }
        }
  */
        
   
        let thirdParty: String? =  dict!["third_party_sharing"] as? String
        if thirdParty != nil {
            if thirdParty == "0"
            {
                self.thirdParty = false
            }else{
                self.thirdParty = true
            }
        }

        
        if let fingerPrint = dict!["synapse_fingerprint"] as? String
        {
            self.userFingerPrint = fingerPrint
        }
        
        if let IPAddress = dict!["synapse_ip_address"] as? String
        {
            self.userIPAddress = IPAddress
        }
        
        if let ownerSynapseFi = dict!["synapseLinkedAccountStatus"] as? Bool
        {
            self.ownerSynpsePayEnabled = ownerSynapseFi
        }
        
        

    }
    
    
    
 //MARK: - Display
 func displayName() -> String {
    //  self.fullName.capitalized
    return  self.fullName.capitalizedFirstChr
 }

 
 
 
 
    
}
