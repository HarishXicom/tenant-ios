//
//  SupportVC.swift
//  TenantTag
//
//  Created by maninder on 5/5/17.
//  Copyright © 2017 Fueled. All rights reserved.
//

import UIKit
import MessageUI

class SupportVC: UIViewController,MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.white;
        self.navigationController!.view.backgroundColor = UIColor.white;
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        
        self.navigationController?.navigationBar.barTintColor = APP_ThemeColor;
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let btn: UIButton = UIButton(type: .custom)
        btn.setImage(UIImage(named: "back"), for: UIControlState())
        btn.addTarget(self, action: #selector(SupportVC.backButtonClicked), for: .touchUpInside)
        btn.sizeToFit()
        let barButton: UIBarButtonItem =  UIBarButtonItem(customView: btn)
    
        self.navigationItem.leftBarButtonItems = [barButton];
        self.navTitle("SUPPORT", color: UIColor.white, font: UIFont(name: APP_FontName, size: 15)!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonClicked() {
         self.dismiss(animated: true) { () -> Void in
            }
    }
    
    //MARK:- Action Buttons
    //MARK:-
    
    
    
    @IBAction func actionBtnTTSupport(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            
            let mailController = MFMailComposeViewController()
            
            mailController.setMessageBody("Regarding TenantTag service", isHTML: true)
            mailController.mailComposeDelegate = self
            mailController.setToRecipients(["support@tenanttag.com"])
            self.present(mailController, animated: true, completion: nil)
        }else{
            
            HSShowAlertView(APP_Name as NSString, message: "Email is not configured on device.")
            
        }

        
       // mailController.setSubject("Regarding TenantTag service")
        
       // self.sendMailWithInbuildComposer(strPara: "support@tenanttag.com")
    }

    
    @IBAction func actionBtnSynapsePay(_ sender: Any) {
        
    //   mailController.setSubject("Regarding SynapsePay service")
       // self.sendMailWithInbuildComposer(strPara: "issues@synapsepay.com")
        if MFMailComposeViewController.canSendMail() {
            
            let mailController = MFMailComposeViewController()
            
            mailController.setMessageBody("Regarding SynapsePay service", isHTML: true)
            mailController.mailComposeDelegate = self
            mailController.setToRecipients(["issues@synapsepay.com"])
            self.present(mailController, animated: true, completion: nil)
        }else{
            
            HSShowAlertView(APP_Name as NSString, message: "Email is not configured on device.")
            
        }


    }
    
    func sendMailWithInbuildComposer(strPara : String)
    {
        if MFMailComposeViewController.canSendMail() {
            
            let mailController = MFMailComposeViewController()

            mailController.setMessageBody("", isHTML: true)
            mailController.mailComposeDelegate = self
            mailController.setToRecipients([strPara])
            self.present(mailController, animated: true, completion: nil)
        }else{
            
        HSShowAlertView(APP_Name as NSString, message: "Email is not configured on device.")
            
        }
        
    }
    //MARK:- Mail Composer Delegates
    //MARK:-
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
